<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Choices">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Text</th>
            <th>Responses</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        <xsl:for-each select="Choice">
          <tr>            
            <td>
              <span class="poll-chart-colour" style="border-color: {ChartColour}FF; background-color: {ChartColour}88; background: linear-gradient(to right, {GradientLight}, {ChartColour}, {GradientDark});"></span>
              <xsl:if test="Url != ''"> 
                <img src="{Url}" class="ml-1 mr-1" alt="Picture for this choice" width="64" />
              </xsl:if> 
              <xsl:value-of select="Choice" />
            </td>
            <td><span class=""><xsl:value-of select="SelectionCount" /></span></td>
            <td class="text-right">
              <div class="btn-group">
                <button type="button" title="Edit this choice" class="btn btn-sm btn-primary" data-digifi-action="Edit" data-digifi-id="{PollChoiceId}"><i class="fa fa-pencil"></i></button>
                <button type="button" title="Set the picture for this choice" class="btn btn-sm btn-warning" data-digifi-action="SetImage" data-digifi-id="{PollChoiceId}"><i class="fa fa-image"></i></button>
                <xsl:choose>
                  <xsl:when test="SelectionCount = 0">
                    <button type="button" title="Delete this choice" class="btn btn-sm btn-danger" data-digifi-action="Delete" data-digifi-id="{PollChoiceId}" digifi-delete-prompt="Are you sure you want do delete this choice? This cannot be undone."><i class="fa fa-trash"></i></button>
                  </xsl:when>
                  <xsl:otherwise>
                    <button type="button" title="You can't delete a choice that has answers against it" class="btn btn-sm btn-danger" disabled="disabled"><i class="fa fa-trash"></i></button>
                  </xsl:otherwise>
                </xsl:choose>
              </div>
            </td>
          </tr>
        </xsl:for-each>
        </tbody>
      </table>
    </xsl:template>
</xsl:stylesheet>
