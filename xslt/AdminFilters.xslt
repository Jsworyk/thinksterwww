<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Filters">        
      <table class="table">        
        <tbody>
          <xsl:for-each select="Filter">
            <xsl:variable name="Prev" select="position() - 1" />
            <xsl:variable name="Cat" select="@FilterCategoryId" />
            <xsl:if test="position() = 1 or /Filters/Filter[$Prev]/@FilterCategoryId != @FilterCategoryId">
            <tr style="background-color: #cecece; font-size: 1.05rem;">
              <th colspan="2"><i class="fa-fw fa-2x {@IconClass}"></i> <xsl:value-of select="@Category" /></th>
              <th class="text-right">
                <div class="btn-group btn-group-md" role="group" aria-label="Edit actions for category {@Category}">
                  <button type="button" class="btn btn-primary" aria-label="Edit {@Category}" title="Edit {@Category}" data-digifi-action="Edit" data-digifi-id="{@FilterCategoryId}"><i class="fa fa-pencil fa-fw"></i></button>  
                  <button type="button" class="btn btn-success" aria-label="Add Filter to {@Category}" title="Add Filter to {@Category}" data-digifi-action="AddFilter" data-digifi-id="{@FilterCategoryId}"><i class="fa fa-plus fa-fw"></i></button>
                </div>
              </th>
            </tr>            
            </xsl:if>
            <xsl:if test="@FilterId != ''">
            <tr>
              <td class="pl-5" title="{@FilterId}"><xsl:value-of select="@Filter" /></td>
              <td class="poll-filter-display" data-filter-id="{@FilterId}" data-filter-parameters="{@Parameters}"><xsl:value-of select="@FilterDisplay" /></td>
              <th class="text-right">
                <div class="btn-group btn-group-sm" role="group" aria-label="Edit actions for category {@Category}">
                  <button type="button" class="btn btn-primary" aria-label="Edit {@Category}" title="Edit {@Category}" data-digifi-action="EditFilter" data-digifi-id="{@FilterId}"><i class="fa fa-pencil fa-fw"></i></button>  
                  <button type="button" class="btn btn-danger" aria-label="Delete {@Filter}" title="Delete {@Filter}" data-digifi-action="DeleteFilter" data-digifi-id="{@FilterId}"><i class="fa fa-trash fa-fw"></i></button>
                </div>
              </th>              
            </tr>
            </xsl:if>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
