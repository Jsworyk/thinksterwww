<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Events/Event">                
       <h2><xsl:value-of select="@Name" /></h2>
       <div class="row">  
        <div class="col-sm-3"><strong>When?</strong></div>
        <div class="col-sm-9"><i class="fa fa-clock-o mr-1"></i> <time class="timeago" datetime="{@Starting}"><xsl:value-of select="@Starting" /></time></div>
       </div>
       <div class="row">  
        <div class="col-sm-3"><strong>Registration</strong></div>
        <div class="col-sm-9"><xsl:value-of select="@AttendanceRestriction" /><br /><small><xsl:value-of select="@ARDescription" /></small></div>
       </div>
       <div class="row">  
        <div class="col-sm-3"><strong>Description</strong></div>
        <div class="col-sm-9"><xsl:value-of select="@Description" disable-output-escaping="yes" /></div>
       </div>
       
       <p class="text-center p-2"><button type="button" class="btn btn-primary" data-digifi-action="Edit" data-digifi-id="{@Id}"><i class="fa fa-pencil"></i> Edit</button></p>
    </xsl:template>
</xsl:stylesheet>
