<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Events">      
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Event</th>
            <th class="text-center">Date</th>
            <th class="text-center">Active</th>
            <th class="text-center">Invited</th>
            <th class="text-center">Accepted</th>
            <th class="text-center">Maybe</th>
            <th class="text-center">Declined</th>            
            <th><span class="sr-only">Actions</span></th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Event">
            <tr>
              <td><i class="fa fa-calendar fa-fw fa-2x text-warning"></i> <a href="/adminevents/{@Id}"><xsl:value-of select="@Name" /></a></td>
              <td class="text-center">
                <xsl:value-of select="@StartDateTime" />   <br />
                <small><i class="fa fa-clock-o mr-1"></i> <time class="timeago" datetime="{@Starting}"><xsl:value-of select="@Starting" /></time></small>
              </td>
              <td class="text-center"><xsl:choose>
                <xsl:when test="@IsActive = 0"><i class="fa fa-fw fa-times text-danger" title="This event is not yet available to the public"></i></xsl:when>
                <xsl:otherwise><i class="fa fa-fw fa-check text-success" text="This event is viewable by the public (or invitees)"></i></xsl:otherwise>
              </xsl:choose></td>
              <td class="text-center"><xsl:value-of select="@Invited" /></td>
              <td class="text-center"><xsl:value-of select="@Accepted" /></td>
              <td class="text-center"><xsl:value-of select="@Maybe" /></td>
              <td class="text-center"><xsl:value-of select="@Declined" /></td>              
              <td class="text-right">
                <a href="/events/{@UrlSegment}" title="Go to the public view of this event"><i class="fa fa-globe fa-2x text-info"></i></a>
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
