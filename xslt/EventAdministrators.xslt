<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Administrators">
    
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th>User</th>
            <th><span class="sr-only">Actions</span></th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Administrator">
            <tr>
              <td><xsl:choose>
                <xsl:when test="@AvatarUrl = ''"><i class="fa fa-fw fa-user fa-2x pull-left mr-2"></i> </xsl:when>
                <xsl:otherwise><img src="{@AvatarUrl}" width="32" height="32" class="pull-left mr-2" /> </xsl:otherwise>
              </xsl:choose> <a href="/adminusers/{@Id}"><xsl:value-of select="@DisplayName" /></a><br /><small><xsl:value-of select="@Location" /></small></td>
              <td class="text-right">
                <button type="button" class="btn btn-sm btn-danger" data-digifi-action="Delete" data-digifi-id="{@EventAdministratorId}" aria-label="Remove {@DisplayName} from the list of administrators" title="Remove {@DisplayName} from the list of administrators"><i class="fa fa-trash"></i></button>              
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
