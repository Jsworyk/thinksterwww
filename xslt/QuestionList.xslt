<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Questions">      
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th data-digifi-sortcolumn="p.Question">Question</th>
            <th class="text-center" data-digifi-sortcolumn="p.IsRequired">Required</th>
            <th class="text-center" data-digifi-sortcolumn="p.IsAvailable">Active</th>
            <th class="text-center">Responses</th>
            <th><span class="sr-only">Actions</span></th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Question">
            <tr>
              <td><i >
                <xsl:attribute name="class">fa fa-fw mr-1 <xsl:choose>
                  <xsl:when test="@IconClass != ''"> <xsl:value-of select="@IconClass" /></xsl:when>
                  <xsl:when test="@EventId != ''"> fa-calendar text-primary</xsl:when>
                  <xsl:otherwise> fa-question text-warning</xsl:otherwise>
                </xsl:choose></xsl:attribute>
              </i> <a href="/adminquestions/{@Id}"><xsl:value-of select="@Question" /></a>
                <xsl:if test="@Filtered = 1"><span title="This question has a filtered applied to it"><i class="fa fa-filter text-info ml-2"></i></span></xsl:if>
                <div class="small ml-4"><xsl:value-of select="@Category" /></div>                
              </td>
              <td class="text-center"><xsl:choose>
                <xsl:when test="@IsRequired = 0"><i class="fa fa-fw fa-times text-danger" title="This question is not required to be answered before any other questions"></i></xsl:when>
                <xsl:otherwise><i class="fa fa-fw fa-check text-success" text="This question is required before answering other questions"></i></xsl:otherwise>
              </xsl:choose></td>
              <td class="text-center"><xsl:choose>
                <xsl:when test="@IsAvailable = 0"><i class="fa fa-fw fa-times text-danger" title="This question is not currently available"></i></xsl:when>
                <xsl:when test="@StartDate != ''">
                </xsl:when>
                <xsl:when test="@EndDate != ''"></xsl:when>
                <xsl:otherwise><i class="fa fa-fw fa-check text-success" text="This question is available"></i></xsl:otherwise>
              </xsl:choose></td>
              <td class="text-center"><xsl:value-of select="@ResponseCount" /></td>
              <td class="text-right">
                <span class="btn-group btn-group-sm">
                  <a class="btn btn-info" href="/poll/{@UrlSegment}" title="Go to the public view of this question"><i class="fa fa-globe"></i></a>
                  <button type="button" class="btn btn-danger" data-digifi-action="Delete" data-digifi-id="{@Id}"><i class="fa fa-trash"></i></button>
                </span>
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
