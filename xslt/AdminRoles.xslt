<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Roles">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Role</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Role">
            <tr>
              <td><a href="/devroles/{@RoleId}"><xsl:value-of select="Name" /></a></td>
              <td class="text-right">
              	<div class="btn-group btn-group-sm" role="group" aria-label="Edit actions for role {Name}">
              	   <button type="button" class="btn btn-sm btn-primary" aria-label="Edit {Name}" title="Edit {@RoleId}" data-digifi-action="Edit" data-digifi-id="{@RoleId}"><i class="fa fa-pencil fa-fw"></i></button>  
                   <button type="button" class="btn btn-sm btn-danger" aria-label="Delete {Name}" title="Delete {@RoleId}" data-digifi-action="Delete" data-digifi-delete-prompt="Are you sure you want to delete role {Name} and all it's member Profiles?" data-digifi-id="{@RoleId}"><i class="fa fa-trash fa-fw"></i></button>
                </div>
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
