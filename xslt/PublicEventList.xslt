<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" indent="no"/>
  <xsl:template match="Events">          
    <xsl:for-each select="Event">
      
      <div class="card">
        <div class="card-block">
          <span class="pull-left text-center mr-2">
            <i class="fa fa-calendar fa-2x"></i><br />
            <xsl:value-of select="@CalDate" />
          </span>
          <h4 class="card-title"><a href="/events/{@UrlSegment}"><xsl:value-of select="@Name" /></a></h4>          
        </div>
      </div>
    
    <!--<tr>
        <td><i class="fa fa-calendar fa-fw fa-2x text-warning"></i> <a href="/adminevents/{@Id}"><xsl:value-of select="@Name" /></a></td>
        <td class="text-center"><xsl:value-of select="@StartDateTime" /></td>
        <td class="text-center"><xsl:choose>
          <xsl:when test="@IsActive = 0"><i class="fa fa-fw fa-times text-danger" title="This event is not yet available to the public"></i></xsl:when>
          <xsl:otherwise><i class="fa fa-fw fa-check text-success" text="This event is viewable by the public (or invitees)"></i></xsl:otherwise>
        </xsl:choose></td>
        <td class="text-center"><xsl:value-of select="@Invited" /></td>
        <td class="text-center"><xsl:value-of select="@Accepted" /></td>
        <td class="text-center"><xsl:value-of select="@Maybe" /></td>
        <td class="text-center"><xsl:value-of select="@Declined" /></td>              
        <td class="text-right">
          <a href="/events/{@UrlSegment}" title="Go to the public view of this event"><i class="fa fa-globe fa-2x text-info"></i></a>
        </td>
      </tr>              -->
    </xsl:for-each>
      
    
  </xsl:template>
</xsl:stylesheet>
