<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Sessions">     
      <table class="table table-striped">
        <thead>
          <tr>
            <th>IP Address</th>
            <th>Start</th>
            <th>End</th>
            <th>Last Activity</th>
            <th>Agent</th>                        
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Session">
            <tr>
              <td><xsl:value-of select="@IPAddress" /></td>
              <td><xsl:value-of select="@Started" /></td>
              <td><xsl:value-of select="@Ended" /></td>
              <td><xsl:value-of select="@LastActivity" /></td>
              <td><xsl:value-of select="@Agent" /></td>              
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
