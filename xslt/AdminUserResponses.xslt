<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Responses">
    
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Date</th>
            <th>Question/Answer</th>
            <th class="text-center"><i class="fa fa-comment" aria-label="Comment Count"></i><span class="sr-only">Comment Count</span></th>
            <th class="text-center"><i class="fa fa-thumbs-up" aria-label="Reaction Count"></i><span class="sr-only">Reaction Count</span></th>            
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Response">
            <tr>
              <td><xsl:value-of select="@ResponseDate" /></td>
              <td>Q: <a href="/poll/{@Url}"><xsl:value-of select="@Question" /></a><br />
                A: <xsl:value-of select="@Answer" />
              </td>
              <td class="text-center"><xsl:value-of select="@Comments" /></td>
              <td class="text-center"><xsl:value-of select="@Reactions" /></td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
