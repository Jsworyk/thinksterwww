<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Users">
    
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th data-digifi-sortcolumn="p.DisplayName">User</th>
            <th data-digifi-sortcolumn="p.Age" class="text-center">Age</th>
            <th data-digifi-sortcolumn="p.IsActive" class="text-center">Active</th>
            <th data-digifi-sortcolumn="`@ResponseCount`" class="text-center">Responses</th>            
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="User">
            <tr>
              <td><xsl:choose>
                <xsl:when test="@AvatarUrl = ''"><i class="fa fa-fw fa-user fa-2x pull-left mr-2"></i> </xsl:when>
                <xsl:otherwise><img src="{@AvatarUrl}" width="32" height="32" class="pull-left mr-2" /> </xsl:otherwise>
              </xsl:choose> <a href="/adminusers/{@Id}"><xsl:value-of select="@DisplayName" /></a><br /><small><xsl:value-of select="@Location" /></small></td>
              <td class="text-center"><xsl:value-of select="@Age" /></td>
              <td class="text-center">
                <xsl:choose>
                  <xsl:when test="@IsActive = 1"><i class="fa fa-fw fa-check text-success"></i></xsl:when>
                  <xsl:otherwise><i class="fa fa-fw fa-times text-danger"></i></xsl:otherwise>
                </xsl:choose>
              </td>
              <td class="text-center"><xsl:value-of select="@ResponseCount" /></td>              
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
