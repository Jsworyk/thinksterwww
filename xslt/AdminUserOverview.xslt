<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Overview">
      <xsl:for-each select="Profile">                
        <img src="{AvatarMicrothumbUrl}" class="float-left mr-1" alt="Avatar for {DisplayName}" />
        <h3 class="clearfix"><xsl:value-of select="DisplayName" /></h3>
        
        <div class="row">
          <div class="col-3"><strong>Profile</strong></div>
          <div class="col-9"><a href="/profile/{ProfileName}"><xsl:value-of select="ProfileName" /></a></div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Gender</strong></div>
          <div class="col-9"><xsl:value-of select="GenderCode" /></div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Birthdate</strong></div>
          <div class="col-9"><xsl:value-of select="Birthdate" /> (<xsl:value-of select="Age" /> years old)</div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Location</strong></div>
          <div class="col-9"><xsl:value-of select="Location" /></div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Joined</strong></div>
          <div class="col-9"><xsl:value-of select="Joined" /></div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Active</strong></div>
          <div class="col-9">
            <xsl:call-template name="Flag">
              <xsl:with-param name="Value" select="IsActive" />
            </xsl:call-template>
            
            <button type="button" title="Switch the active state of this user" data-digifi-action="ToggleActive" class="ml-2 btn btn-sm btn-warning">
              Toggle Active            
            </button>
          </div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Followable</strong></div>
          <div class="col-9">
            <xsl:call-template name="Flag">
              <xsl:with-param name="Value" select="IsFollowable" />
            </xsl:call-template>
          </div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Followers</strong></div>
          <div class="col-9"><xsl:value-of select="Followers" /></div>
        </div>
        
        <div class="row">
          <div class="col-3"><strong>Following</strong></div>
          <div class="col-9"><xsl:value-of select="Following" /></div>
        </div>
        
      </xsl:for-each>
      
      <hr />
      
      <h4>Accounts</h4>
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Type</th>
            <th>Name</th>
            <th>Email</th>
            <th class="text-center">Primary</th>
          </tr>          
        </thead>
        <tbody>
          <xsl:for-each select="Accounts/Account">
            <tr>
              <td><xsl:value-of select="AccountType" /></td>
              <td><xsl:value-of select="DisplayName" /></td>
              <td><a href="mailto:{EmailAddress}"><xsl:value-of select="EmailAddress" /></a></td>
              <td class="text-center"><xsl:choose>
                <xsl:when test="IsPrimary = 1"><i class="fa fa-star text-warning"></i></xsl:when>
                <xsl:otherwise><i class="fa fa-star text-secondary"></i></xsl:otherwise>
              </xsl:choose></td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
      <hr />
      
      <h4>Blocks</h4>
      
      <div class="row">
        <div class="col-3"><strong>Blocking</strong></div>
        <div class="col-9">
          <xsl:if test="count(Blocking/Profile) = 0"><em>None</em></xsl:if>
          <xsl:for-each select="Blocking/Profile">
            <xsl:if test="position() > 1">, </xsl:if>
            <a href="/adminusers/{ProfileId}" title="As of {Started}"><xsl:value-of select="DisplayName" /></a>
          </xsl:for-each>
        </div>
      </div>
      
      <div class="row">
        <div class="col-3"><strong>Blocked By</strong></div>
        <div class="col-9">
        <xsl:if test="count(BlockedBy/Profile) = 0"><em>None</em></xsl:if>
          <xsl:for-each select="BlockedBy/Profile">
            <xsl:if test="position() > 1">, </xsl:if>
            <a href="/adminusers/{ProfileId}" title="As of {Started}"><xsl:value-of select="DisplayName" /></a>
          </xsl:for-each>
        </div>
      </div>
                                    
    </xsl:template>
    
    <xsl:template name="Flag">
      <xsl:param name="Value" />
        
      <xsl:choose>
        <xsl:when test="$Value = 1"><i class="fa fa-check text-success"></i></xsl:when>
        <xsl:otherwise><i class="fa fa-times text-danger"></i></xsl:otherwise>
      </xsl:choose>
      
    </xsl:template>
</xsl:stylesheet>
