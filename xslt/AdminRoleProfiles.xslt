<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Profiles">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Profile Display Name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Profile">
            <tr>
              <td><xsl:value-of select="@Name" /></td>
              <td class="text-right">
              	<div class="btn-group btn-group-sm" role="group" aria-label="Actions for Profile {@Name}">
                   <button type="button" class="btn btn-sm btn-danger" aria-label="Delete Profile {@Name} From This Role" title="Delete {@Name}" data-digifi-action="Delete" data-digifi-id="{@Id}"><i class="fa fa-trash fa-fw"></i></button>
                </div>
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
