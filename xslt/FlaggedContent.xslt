<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Flags">      
      
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Object</th>
            <th class="text-center">Times Flagged</th>              
            <th><span class="sr-only">Actions</span></th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="Flag">
            <tr>
              <td>
                <xsl:value-of select="@Created" />   <br />
                <small><i class="fa fa-clock-o mr-1"></i> <time class="timeago" datetime="{@Timestamp}"><xsl:value-of select="@Timestamp" /></time></small>
              </td>
              <td><xsl:value-of select="@FlagType" /></td>              
              <td><xsl:value-of select="@TargetType" /> (<xsl:value-of select="@TargetId" />)<br />
                <div class="small"><xsl:value-of select="@Details" /></div>
              </td>          
              <td class="text-center"><xsl:value-of select="@FlagCount" /></td>
              <td class="text-right">
                <xsl:choose>
                  <xsl:when test="@TargetTypeCode = 'ME'">
                    <a href="{@Url}" title="View this media" target="_blank"><i class="fa fa-eye"></i></a>
                  </xsl:when>
                </xsl:choose>
                <!--<a href="/events/{@UrlSegment}" title="Go to the public view of this event"><i class="fa fa-globe fa-2x text-info"></i></a>-->
              </td>
            </tr>
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
