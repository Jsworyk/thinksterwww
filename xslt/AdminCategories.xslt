<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Categories">        
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Category</th>
            <th>Background Color</th>
            <th><span class="sr-only">Actions</span></th>            
          </tr>
        </thead>                
        <tbody>
          <xsl:for-each select="Category">
            <tr>
              <td><i class="fa-fw {@IconClass} mr-1" /><xsl:value-of select="@Name" /></td>
              <td><span style="width: 32px; height: 100%; display: inline-block; background-color: {@BackgroundColor}; border: 1px solid #000;" class="mr-1">&#160;</span> <xsl:value-of select="@BackgroundColor" /></td>
              <td class="text-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-primary" 
                    title="Edit {@Name}" 
                    aria-title="Edit {@Name}"
                    data-digifi-action="Edit"
                    data-digifi-id="{@Id}"><i class="fa fa-fw fa-pencil"></i></button>
                </div>
              </td>
            </tr>            
          </xsl:for-each>
        </tbody>
      </table>
      
    </xsl:template>
</xsl:stylesheet>
