<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="no"/>
    <xsl:template match="Contents">
      
      <xsl:for-each select="Content">
        <div class="card">       
          <div class="card-body">                       
            <h4 class="card-title">            
              <i class="fa fa-pull-left fa-file-o"></i>
              <span class="pull-right digifi-actions">
                <button type="button" class="btn btn-sm btn-info" data-digifi-action="Edit" data-digifi-id="{@Id}"><i class="fa fa-pencil"></i></button>
              </span>
              <xsl:value-of select="@Title" />
            </h4>          
            <p class="card-text"><xsl:value-of select="@Description" /></p>
            <xsl:if test="@StaticContentTypeCode = 'PG'">
            <p class="card-text"><a href="/{@UrlSegment}">View this Page</a></p>
            </xsl:if>
          </div>
        </div>
      </xsl:for-each>
      
    </xsl:template>
</xsl:stylesheet>
