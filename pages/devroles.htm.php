<?php


class devroles {

  /** ['Access' => 'Administrator'] */
  public function GetRole($id) {
    $r = new Data('Role', $id);

    return $r->toJSON();     
  } 
    
  /** ['Access' => 'Administrator'] */
  public function GetRoles() { 
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT r.RoleId '@RoleId', r.Name
    		 FROM Role r;
    			";

    $xml = $q->executeXml('Roles', 'Role');
    
    return WebPage::Transform($xml, 'AdminRoles.xslt', '<p><em>No roles have been defined</em></p>');
  }

  /** ['Access' => 'Administrator'] */
  public function SaveRole($d) {
    $r = new Data('Role');

    $r->load($d);
    
    $r->update($this->CurrentUser->ProfileId);     
  } 
  
  /** ['Access' => 'Administrator'] */
  public function GetRoleProfiles() {
  
  	$q = new DatabaseQuery();
	    
	$q->sql = "SELECT pr.ProfileRoleId '@Id', p.DisplayName '@Name'
	    	   FROM ProfileRole pr
	    	   JOIN Profile p ON p.ProfileId = pr.ProfileId 
	    	   WHERE pr.RoleId = ?ai_roleid;";
    
    $q->addParameter("ai_roleid",$_GET['param']);
  
  	$xml = $q->executeXml('Profiles', 'Profile');
      
    return WebPage::Transform($xml, 'AdminRoleProfiles.xslt', '<p><em>No profiles have been added to this role</em></p>');
  
  }
 
  /** ['Access' => 'Administrator'] */
  public function SaveRoleProfile($d) {
	  $pr = new Data('ProfileRole');

	  $pr->load(array('ProfileId' => $d->ProfileId));
	  $pr->RoleId = $_GET['param'];

	  $pr->update($this->CurrentUser->ProfileId);     
  } 
  
  /** ['Access' => 'Administrator'] */
  public function DeleteRole($RoleId) {
  
  	$q = new DatabaseQuery();
  	
  	//First Delete All Profiles from this Role
  	
  	$q->sql = "DELETE FROM ProfileRole WHERE RoleId = ?ai_roleid;";
  	
  	$q->addParameter("ai_roleid",$RoleId);
  	
  	$q->executeNonQuery();
  
  	//Now Delete the Role
  	
  	$q = new DatabaseQuery();
  	
  	$q->sql = "DELETE FROM Role WHERE RoleId = ?ai_roleid;";
  	
  	$q->addParameter("ai_roleid",$RoleId);
	  	
	$q->executeNonQuery();
  }
  
}

?>