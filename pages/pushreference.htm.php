<?php

class pushreference {

  /** ['Access' => 'Developer'] */
  public function GetEndpoint($id) {
    $ep = new Data('PushEndpoint', $id);

    return $ep->toJSON();     
  } 
    
  /** ['Access' => 'Developer'] */
  public function SaveEndpoint($d) {
    $pe = new Data('PushEndpoint');

    $pe->load($d);
    
    $pe->update($this->CurrentUser->ProfileId);     
  } 
   
}

?>