<?php

class follow_requests {
  /** ['Access' => 'Everyone'] */
  public function GetRequests() {
    $pf = new PollFunctions();
    
    $result = $pf->GetFollowRequests(array());       
      
    return json_encode($result);
  } 
}

?>