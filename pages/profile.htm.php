<?php 
class profile {
  public $Viewing;
  public $isMe = false;
  public $CanViewFriends = false;
  public $CanViewContact = false;
  public $CanViewLocation = false;
  public $CanViewBirthday = false;
  public $CanViewQuestions = false;
  
  public $WeAreFriends = false;
  
  public function __construct() {
    $pf = new PollFunctions();
    $result = null;
                       
    if(!isset($_GET['param']) || WebPage::$currentUser->ProfileName == $_GET['param']) {
      $this->isMe = true;
      $result = $pf->GetProfile(array('ProfileName' => WebPage::$currentUser->ProfileName)); 
    } else {
      $result = $pf->GetProfile(array('ProfileName' => $_GET['param']));
    
      
    }    
    
    if($result['Success']) {
      $this->Viewing = $result['Profile'];
      $this->SetCanView();         
    }                                                        
  }
  
  /** ['Access' => 'Everyone'] */
	public function BlockUser() {
		$pf = new PollFunctions();
		
		return json_encode($pf->Block(array('ProfileId' => $this->Viewing->ProfileId)));
	}
  
  /** ['Access' => 'Everyone'] */
	public function GetCompatibility() {
    //Anti-hacker security check!
    if($this->Viewing->ProfileId == $this->CurrentUser->ProfileId) return null;
    
    $pf = new PollFunctions();
  
    return json_encode($pf->GetCompatibility(array('ProfileId' => $this->Viewing->ProfileId)));
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function GetFollowers() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetFollowers(array('ProfileId' => $this->Viewing->ProfileId)));         
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function GetFollowing() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetFollowing(array('ProfileId' => $this->Viewing->ProfileId)));         
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetRecentImages() {
    $pf = new PollFunctions();
		
		return json_encode($pf->GetProfileImages(array('ProfileId' => $this->CurrentUser->ProfileId, 'Count' => 5)));
  }
  
  /** ['Access' => 'Everyone'] */
	public function GetUploadToken() {
    //Anti-hacker security check!
    if($this->Viewing->ProfileId != $this->CurrentUser->ProfileId) throw new Exception("Access denied");
  
    return getOneTimeToken(array('TargetTypeCode' => 'PR', 'TargetId' => WebPage::$currentUser->ProfileId, 'ExposureTypeCode' => 'PU'), WebPage::$currentUser->ProfileId);
  }
  	  	
	private function SetCanView() {
	
		$CanViewFriends = false;
    $CanViewContact = false;
    $CanViewLocation = false;
    $CanViewBirthday = false;
    $CanViewQuestions = false;
    
    if(!$this->Viewing) return;
    
		if($this->Viewing->FriendExposure == 'PU' || ($this->Viewing->FriendExposure == 'FR' && $this->Viewing->Following) || $this->isMe) {
			$this->CanViewFriends = true;
		}
		    
    if($this->Viewing->ContactInfoExposure == 'PU' || ($this->Viewing->ContactInfoExposure == 'FR' && $this->Viewing->Following) || $this->isMe) {
    	$this->CanViewContact = true;
    }
       	
   	if($this->Viewing->LocationExposure == 'PU' || ($this->Viewing->LocationExposure == 'FR' && $this->Viewing->Following) || $this->isMe) {
   		$this->CanViewLocation = true;
   	}
   	    
    if($this->Viewing->BirthdayExposure == 'PU' || ($this->Viewing->BirthdayExposure == 'FR' && $this->Viewing->Following) || $this->isMe) {
    	$this->CanViewBirthday = true;
    }
       
    if($this->Viewing->QuestionsExposure == 'PU' || ($this->Viewing->QuestionsExposure == 'FR' && $this->Viewing->Following) || $this->isMe) {
    	$this->CanViewQuestions = true;
    }
	}

 	/** ['Access' => 'Everyone'] */
	public function FriendRequest($profileId) {
		$pf = new PollFunctions();
		
		return json_encode($pf->AddFriend(array('ProfileId' => $this->Viewing->ProfileId)));
	}
  
  	/** ['Access' => 'Everyone'] */
  	public function FollowRequest() {
  		$pf = new PollFunctions();
  		
  		return json_encode($pf->Follow(array('ProfileId'=> $this->Viewing->ProfileId)));
  	}
  
  	/** ['Access' => 'Everyone'] */
	 public function UnfollowRequest() {
	  	$pf = new PollFunctions();
	  		
	  	return json_encode($pf->Unfollow(array('ProfileId'=> $this->Viewing->ProfileId)));
  	}
    
  /** ['Access' => 'Everyone'] */
  public function PostBanner($d) {
    $m = new Data('Media');
    
    $p = new Data('Profile', $this->Viewing->ProfileId);
    
    //TODO: Ensure admin    
  
    if(isset($d->ExistingMediaId) && $d->ExistingMediaId && is_numeric($d->ExistingMediaId)) {
      $m = new Data('Media', $d->ExistingMediaId);
    } else {
      $m->merge($d);
      
      $m->IsPublished = 1;
      
      $m->update($this->CurrentUser->ProfileId);      	
    }
             
    $p->BannerMediaId = $m->MediaId;          
            
    $p->update($this->CurrentUser->ProfileId);
  }
    
  /** ['Access' => 'Everyone'] */
  public function PostImage($d) {
    $m = new Data('Media');
    
    //Quick and dirty security test:
    if($this->Viewing->ProfileId != $this->CurrentUser->ProfileId) throw new Exception("Access denied");
  
    /*$m->merge($d);
      
    $m->IsPublished = 1;
      
    $m->update($this->CurrentUser->ProfileId);  */
    
    $pf = new PollFunctions();
    
    $results = $pf->PostImage(array('MediaId' => $d->MediaId, 
      'Name' => $d->Name,
      'Description' => $d->Description,
      'ExposureTypeCode' => $d->ExposureTypeCode,
      'UseAsAvatar' => false));
      
     return json_encode($results);           
  }
  
  	/** ['Access' => 'Everyone'] */
	public function SendMessage($d) {
		$pf = new PollFunctions();
		
    		return $pf->SendMessage(array('Destination' => $this->Viewing->ProfileId, 'Message' => $d->Message));
    
		//return json_encode($pf->AddFriend(array('ProfileId' => $profileId)));
	}
  
  /** ['Access' => 'Everyone'] */
  public function GetFollowingCompatibility($pollCategoryId) {
    $pf = new PollFunctions();
    
    $results = $pf->GetFollowingCompatibility(array('PollCategoryId' => $pollCategoryId));
    
    if($results['Success']) {
      return json_encode($results['Compatibility']); 
    }
		
		return json_encode($results); 
  }
  
  /** ['Access' => 'Everyone'] */
	public function GetImages($fromTimestamp) {
		$pf = new PollFunctions();
		
		return json_encode($pf->GetProfileImages(array('ProfileId' => $this->Viewing->ProfileId, 'FromTimestamp' => $fromTimestamp))); 
	}

	/** ['Access' => 'Everyone'] */
	public function GetTimeline($fromTimestamp) {
		$pf = new PollFunctions();
		
		return json_encode($pf->GetTimeline(array('ProfileId' => $this->Viewing->ProfileId, 'FromTimestamp' => $fromTimestamp))); 
	}

	/** ['Access' => 'Everyone'] */
	public function GetResponses($friendStatusCode) {
		$pf = new PollFunctions();
		
		//Default with public exposures
		$exposure = array("PU");
		
		//If I'm viewing my own page, I want to see all responses. Add friend and private
		if($this->isMe){
			array_push($exposure,"FR","PR");
		}
		elseif($friendStatusCode === "AC") {
			//If I'm viewing a friend's page, I want to see all public and friend exposure reponses
			array_push($exposure,"FR");
		}
		
		return json_encode($pf->GetPollResponses(array('PageNumber' => 1, 'PageSize' => 30,'ProfileId' => $this->Viewing->ProfileId,'ExposureType' =>$exposure)));
	}
  
  
	/** ['Access' => 'Everyone'] */
	public function GetResponseTimeline($fromTimestamp) {
		$pf = new PollFunctions();
		
		return json_encode($pf->GetProfileResponses(array('ProfileId' => $this->Viewing->ProfileId, 'FromTimestamp' => $fromTimestamp))); 
	}
  
  /** ['Access' => 'Everyone'] */
	public function UnblockUser() {
		$pf = new PollFunctions();
		
		return json_encode($pf->Unblock(array('ProfileId' => $this->Viewing->ProfileId)));
	}
  
  /** ['Access' => 'Everyone'] */
	public function UpdateLocation($d) {
		$pf = new PollFunctions();
    
    $result = $pf->InitResult();
    
    if(!$this->isMe) {
      $result['Success'] = false;
      $result['Message'] = "Access denied";
      return $result; 
    }
		
    return $pf->SetLocation(array('CountryCode' => $d->CountryCode, 'PostalCode' => $d->PostalCode));
    
		//return json_encode($pf->AddFriend(array('ProfileId' => $profileId)));
	}

}

?>