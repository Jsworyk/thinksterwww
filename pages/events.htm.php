<?php

class events {

  public $Viewing;
  
  public function __construct() {
    if(isset($_GET['param'])) { 
      $pf = new PollFunctions();
    
      $e = new Data('Event');
    
      $e->load(array('UrlSegment' => $_GET['param']));
      
      $result = $pf->GetEvent(array('EventId' => $e->EventId));
      
      if($result['Success']) {
        $this->Viewing = $result['Event']; 
      }    
    }
  
  }
  
  /** ['Access' => 'Everyone'] */
  public function CheckIn() {
    $pf = new PollFunctions();
    
    $result = $pf->CheckInToEvent(array('EventId' => $this->Viewing->EventId));
  
    return $result; 
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetAttendees($attendaceTypeCode) {
    $pf = new PollFunctions();
    
    $result = $pf->GetEventAttendees(array('AttendanceTypeCode' => $attendaceTypeCode, 'EventId' => $this->Viewing->EventId));
  
    return json_encode($result); 
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetComments() {
    $pf = new PollFunctions();
    
    $result = $pf->GetComments(array('TargetTypeCode' => 'EV', 'TargetId' => $this->Viewing->EventId));
  
    return json_encode($result); 
  }

  /** ['Access' => 'Everyone'] */
	public function GetImages($fromTimestamp) {
		$pf = new PollFunctions();
		
		return json_encode($pf->GetEventImages(array('EventId' => $this->Viewing->EventId, 'FromTimestamp' => $fromTimestamp))); 
	}
  
  /** ['Access' => 'Everyone'] */
  public function GetRecentImages() {
    $pf = new PollFunctions();
		
		return json_encode($pf->GetEventImages(array('EventId' => $this->Viewing->EventId, 'Count' => 5)));
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetNextQuestion() {
    $pf = new PollFunctions();

    return json_encode($pf->NextQuestion(array('EventId' => $this->Viewing->EventId)));

  }
  
  /** ['Access' => 'Everyone'] */
	public function GetUploadToken() {
    return getOneTimeToken(array('TargetTypeCode' => 'EV', 'TargetId' => $this->Viewing->EventId, 'ExposureTypeCode' => 'PU'), WebPage::$currentUser->ProfileId);
  }
  
  /** ['Access' => 'Everyone'] */
  public function PostBanner($d) {
    $m = new Data('Media');
    
    $e = new Data('Event', $this->Viewing->EventId);
    
    //TODO: Ensure admin    
  
    if(isset($d->ExistingMediaId) && $d->ExistingMediaId && is_numeric($d->ExistingMediaId)) {
      $m = new Data('Media', $d->ExistingMediaId);
    } else {
      $m->merge($d);
      
      $m->IsPublished = 1;
      
      $m->update($this->CurrentUser->ProfileId);      	
    }
             
    $e->BannerMediaId = $m->MediaId;          
            
    $e->update($this->CurrentUser->ProfileId);
  }
  
  /** ['Access' => 'Everyone'] */
  public function PostImage($d) {
    /*$m = new Data('Media');
    
    //Quick and dirty security test:
    //if($this->Viewing->ProfileId != $this->CurrentUser->ProfileId) throw new Exception("Access denied");
  
    $m->merge($d);
      
    $m->IsPublished = 1;
      
    $m->update($this->CurrentUser->ProfileId);      */
    $pf = new PollFunctions();
    
    return $pf->PostImage(array('MediaId' => $d->MediaId,
      'Name' => $d->Name,
      'Description' => $d->Description,
      'ExposureTypeCode' => $d->ExposureTypeCode,
      'UseAsAvatar' => false));            
  }
  
  /** ['Access' => 'Everyone'] */
  public function SetAttendance($attendanceTypeCode) { 
    $pf = new PollFunctions();
    
    return $pf->SetEventAttendance(array('EventId' => $this->Viewing->EventId, 'AttendanceTypeCode' => $attendanceTypeCode));
  }
 
}

?>