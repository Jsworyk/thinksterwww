<?php

class newsfeed {

  /** ['Access' => 'Everyone'] */
  public function GetFeed($fromTimestamp) {
    $pf = new PollFunctions();
    
  
    //return "Under construction";
    return json_encode($pf->GetTimeline(array('FromTimestamp' => $fromTimestamp, 'IncludeImages' => true)));  
  }  
  
  /** ['Access' => 'Everyone'] */
  public function GetFollowing() {
    $pf = new PollFunctions();
    
  
    //return "Under construction";
    return json_encode($pf->GetFollowing(array()));  
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetTrending() {
    $pf = new PollFunctions();

    return json_encode($pf->GetTrendingQuestions(array()));
  }
  
  /** ['Access' => 'Everyone'] */
  public function Unfollow($profileId) {

	$pf = new PollFunctions();
	  		
  	return json_encode($pf->Unfollow(array('ProfileId'=> $profileId)));
  
  }
}

?>