<?php

class adminevents {
  
  /** ['Access' => 'Administrator'] */
  public function AddAdministrator($d) {
    $ea = new Data('EventAdministrator');
    
    $ea->load(array('ProfileId' => $d->ProfileId, 'EventId' => $_GET['param']));
    
    if($ea->isNewRow()) {
      $ea->update($this->CurrentUser->ProfileId); 
    }        
  }

  /** ['Access' => 'Administrator'] */
  public function GetActiveStatus() {
    return "asdf asdf sTesting"; 
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetAdministrators() {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT ea.ProfileId `@Id`, ea.EventAdministratorId `@EventAdministratorId`, p.DisplayName `@DisplayName`, p.AvatarMicrothumbUrl `@AvatarUrl` 
      FROM EventAdministrator ea
        JOIN Profile p ON p.ProfileId = ea.ProfileId
      WHERE ea.EventId = ?ai_eventid
      ORDER BY p.DisplayName";
      
    $q->addParameter('ai_eventid', $_GET['param']);
      
    $xml = $q->executeXml('Administrators', 'Administrator');
  
    return WebPage::Transform($xml, 'EventAdministrators.xslt', '<div class="alert alert-info">No event administrators have been added to this event</div>');
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetAttendees() {
    return "People go here"; 
  }
         
  /** ['Access' => 'Administrator'] */
  public function GetEvent($eventId) {
    /*$e = new Data('Event', $eventId);
     
    return $e->toJSON();  */
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT EventId, Name, UrlSegment, Description, AttendanceRestrictionTypeCode, StartDateTime, EndDateTime FROM Event WHERE EventId = ?ai_eventid";
    $q->addParameter('ai_eventid', $eventId);
    
    $events = $q->executeObjects();
    
    return json_encode($events[0]);  
  }

  /** ['Access' => 'Administrator'] */
  public function GetEventDetails() {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT e.EventId `@Id`, 
	e.Name `@Name`,
    REPLACE(e.Description, '\n', '<br />') `@Description`,
    art.Name `@AttendanceRestriction`,
    art.Description `@ARDescription`,
    DATE_FORMAT(e.StartDateTime, '%Y-%m-%dT%TZ') `@StartDate`,
	  DATE_FORMAT(e.StartDateTime, '%Y-%m-%dT%TZ') `@Starting` 
FROM Event e
	JOIN AttendanceRestrictionType art ON art.AttendanceRestrictionTypeCode = e.AttendanceRestrictionTypeCode
WHERE e.EventId = ?ai_eventid";

    $q->addParameter('ai_eventid', $_GET['param']);
    
    $xml = $q->executeXml('Events', 'Event');
  

    return WebPage::Transform($xml, "AdminEventDetails.xslt"); 
  }

  /** ['Access' => 'Administrator'] */
  public function GetEventList() {
    $q = new DatabaseQuery();
     
    $q->sql = "SELECT e.EventId `@Id`, 
	e.Name `@Name`, 
	e.Location `@Location`,
	e.StartDateTime `@StartDateTime`,
  DATE_FORMAT(e.StartDateTime, '%Y-%m-%dT%TZ') `@Starting`, 
  e.UrlSegment `@UrlSegment`,
  e.IsActive `@IsActive`,
    COUNT(IF(ea.AttendanceTypeCode = 'IN', 1, NULL)) `@Invited`,
    COUNT(IF(ea.AttendanceTypeCode = 'YA', 1, NULL)) `@Accepted`,
    COUNT(IF(ea.AttendanceTypeCode = 'MA', 1, NULL)) `@Maybe`,
    COUNT(IF(ea.AttendanceTypeCode = 'NO', 1, NULL)) `@Declined`
FROM Event e
	LEFT JOIN EventAttendee ea ON ea.EventId = e.EventId    
GROUP BY e.EventId, e.Name, e.Location, e.StartDateTime
ORDER BY e.StartDateTime";
    
    $xml = $q->executeXml('Events', 'Event');       
  
    return WebPage::Transform($xml, 'EventList.xslt', '<div class="alert alert-info" role="alert"><p>No events have been defined in the system yet</p></div>');
  }           
  
  /** ['Access' => 'Administrator'] */
  public function GetLocationData() {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT Address, AttendanceRestrictionTypeCode, ST_ASWKT(Location) Location, ST_AsText(GeoBoundary) GeoBoundary FROM Event WHERE EventId = ?ai_eventid";
    $q->addParameter('ai_eventid', $_GET['param']);
    
    $location = $q->executeObjects();
    
    $results = array('Address' => $location[0]->Address, 'AttendanceRestrictionTypeCode' => $location[0]->AttendanceRestrictionTypeCode, 'Location' => null, 'GeoBoundary' => null);
    
    if($location[0]->Location) {
      preg_match_all('/(-?\d+(\.\d+)?)/', $location[0]->Location, $matches);
    
      $results['Location'] = array('latitude' => floatval($matches[0][1]), 'longitude' => floatval($matches[0][0]));
    }
    
    if($location[0]->AttendanceRestrictionTypeCode == 'GEO' && $location[0]->Location) {      
      
      if(!$location[0]->GeoBoundary) {
        //Predefine the boundary as 500m in either direction of location point:
        $deltaLong = 500.0 / (111320.0 * cos(deg2rad($results['Location']['latitude'])));
        
        $results['GeoBoundary'] = array('north' => $results['Location']['latitude'] + 0.00452185866478557346211586810643, 
                                        'south' => $results['Location']['latitude'] - 0.00452185866478557346211586810643,
                                        'east' => $results['Location']['longitude'] + $deltaLong,
                                        'west' => $results['Location']['longitude'] - $deltaLong);
      } else {
        //Convert the WKT polygon to a rectangle bounds compatible with Google maps:
        preg_match_all('/(-?\d+(\.\d+)?)/', $location[0]->GeoBoundary, $matches);
        
        $results['GeoBoundary'] = array('north' => floatval($matches[0][1]), 
                                        'south' => floatval($matches[0][5]),
                                        'east' => floatval($matches[0][2]),
                                        'west' => floatval($matches[0][0]));  	
          
      }
       
    }
    
    return json_encode($results);
  }
  
  /** ['Access' => 'Administrator'] */
  public function RemoveAdministrator($eventAdministratorId) {
    $ea = new Data('EventAdministrator', $eventAdministratorId);
    
    $ea->delete($this->CurrentUser->ProfileId);  
  }

  
  /** ['Access' => 'Administrator'] */
  public function SaveEvent($d) {
    $e = new Data('Event');
    
    $e->load($d);
    
    $e->update($this->CurrentUser->ProfileId);
    
    return $e->EventId;
  }
  
  /** ['Access' => 'Administrator'] */
  public function SetGeoBoundary($bounds) {
    $q = new DatabaseQuery();
    
    $q->sql = "UPDATE Event SET GeoBoundary = ST_PolyFromText('POLYGON(({$bounds->west} {$bounds->north}, {$bounds->east} {$bounds->north}, {$bounds->east} {$bounds->south}, {$bounds->west} {$bounds->south}, {$bounds->west} {$bounds->north}))') WHERE EventId = ?ai_eventid";
      
    $q->addParameter('ai_eventid', $_GET['param']);  
    
    $q->executeNonQuery();    
    
    $push = array('bounds' => $bounds);
    
    $pf = new PollFunctions();
        
    $pf->Push(['e' . $_GET['param']], 'GeoBoundaryChanged', $push); 
  }
  
  /** ['Access' => 'Administrator'] */
  public function SaveLocation($d) {
    global $config;
  
    $e = new Data('Event', $_GET['param']);
    
    //$e->load($d);
    $e->Address = $d->Address;
    
    $geocoding = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($d->Address) . "&key=" . $config->GoogleMapsAPIKey), true);
              
    if($geocoding && $geocoding['status'] == 'OK') {
      $e->Location  = new staticValue("ST_GeomFromText('POINT(" . $geocoding['results'][0]['geometry']['location']['lng'] . ' ' . $geocoding['results'][0]['geometry']['location']['lat'] . ")')");
      $e->GeoBoundary = null;  
    }  
    
    
    $e->update($this->CurrentUser->ProfileId);     
    
    $pf = new PollFunctions();
        
    $pf->Push(['e' . $_GET['param']], 'LocationChanged', null);   
  }
  
  /** ['Access' => 'Administrator'] */
  public function ToggleAttribute($attribute) {
    $e = new Data('Event', $_GET['param']);
    
    $e->{$attribute} = !$e->{$attribute};
            
    $e->update($this->CurrentUser->ProfileId);
    
    $push = array('Attribute' => $attribute, 'Value' => $e->{$attribute});
    
    $pf = new PollFunctions();
        
    $pf->Push(['e' . $_GET['param']], 'AttributeToggled', $push); 
    
    return $e->{$attribute};
  }
 
}


?>