<?php

class media {

  public $Viewing = null;
  
  public function __construct() {
    if(isset($_GET['selector'])) {
      $this->Viewing = new Data('Media');
  
      $this->Viewing->load(array('Selector' => $_GET['selector']));
    }  
  }
  
  /** ['Access' => 'Everyone'] */
  public function DeleteImage() {
    $pf = new PollFunctions();
    
    $result = $pf->DeleteImage(array('MediaId' => $this->Viewing->MediaId));
  
    return json_encode($result);      
  }

  /** ['Access' => 'Everyone'] */
  public function GetComments($lastCommentId) {
    $pf = new PollFunctions();
    
    $result = $pf->GetComments(array('TargetTypeCode' => 'ME', 'TargetId' => $this->Viewing->MediaId, 'LastCommentId' => $lastCommentId));
  
    return json_encode($result); 
  }
 
}

?>