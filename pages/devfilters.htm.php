<?php


class devfilters {

  /** ['Access' => 'Administrator'] */
  public function GetFilter($id) {
    $f = new Data('Filter', $id);

    return $f->toJSON();     
  } 
  
  /** ['Access' => 'Administrator'] */
  public function GetParameter($id) {
    $fp = new Data('FilterParameter', $id);

    return $fp->toJSON();     
  } 

  /** ['Access' => 'Administrator'] */
  public function GetFilterCategory($id) {
    $fc = new Data('FilterCategory', $id);

    return $fc->toJSON();     
  } 


  /** ['Access' => 'Administrator'] */
  public function GetList() { 
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT fc.FilterCategoryId `@FilterCategoryId`, 
	fc.Name `@Category`,
    fc.IconClass `@IconClass`,
    f.FilterId `@FilterId`,
    f.Name `@Filter`,
    f.FilterDisplay `@FilterDisplay`,
    IFNULL(CONCAT('[', GROUP_CONCAT(CONCAT('{\"Name\":\"', fp.Name, '\",\"Id\":', fp.FilterParameterId,'}') SEPARATOR ', '), ']'), 'null') `@Parameters`
FROM FilterCategory fc
LEFT JOIN Filter f ON f.FilterCategoryId = fc.FilterCategoryId
LEFT JOIN FilterParameter fp ON fp.FilterId = f.FilterId
GROUP BY fc.FilterCategoryId, fc.Name, fc.IconClass, f.FilterId, f.Name, f.FilterDisplay
ORDER BY fc.Name, f.Name";

    $xml = $q->executeXml('Filters', 'Filter');
    
    return WebPage::Transform($xml, 'AdminFilters.xslt', '<p><em>No filters have been defined</em></p>');
  }

  /** ['Access' => 'Administrator'] */
  public function SaveFilter($d) {
    $f = new Data('Filter');

    $f->load($d);
    
    $f->update($this->CurrentUser->ProfileId);     
  }  
  
  /** ['Access' => 'Administrator'] */
  public function SaveFilterCategory($d) {
    $fc = new Data('FilterCategory');

    $fc->load($d);
    
    $fc->update($this->CurrentUser->ProfileId);     
  }
  
  /** ['Access' => 'Administrator'] */
  public function SaveParameter($d) {
    $fp = new Data('FilterParameter');

    $fp->load($d);
    
    $fp->update($this->CurrentUser->ProfileId);     
  }
 
}

?>