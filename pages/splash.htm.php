<?php

class splash {

  /** ['Access' => 'Everyone'] */
  public function FacebookLogin() {
    global $config;
    $result = "";
    
    $pf = new PollFunctions();
    
    $client = new GuzzleHttp\Client;    
    
    $fb = new Facebook\Facebook([
      'http_client_handler' => new Guzzle6HttpClient($client),
      'app_id' => $config->FBAppId,
      'app_secret' => $config->FBSecret,
      'default_graph_version' => 'v2.8',
    ]);
    
    $helper = $fb->getJavaScriptHelper();
    
    $result = $pf->FacebookLogin(array('AccessToken' => $helper->getAccessToken()));
    
    return json_encode($result);    
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetResetCode($profileName) {
    $pf = new PollFunctions();
    
    $result = $pf->GetPasswordResetCode(array('ProfileName' => $profileName));
    
    return json_encode($result);
  }

  /** ['Access' => 'Everyone'] */
  public function Login($em, $pw) {
    global $dbsession;
  
    $pf = new PollFunctions();
    
    $result = $pf->ThinksterLogin(array('EmailAddress' => $em, 'Password' => $pw));
    
    if($result['Success']) {
      $dbsession->start_session_record($result['ProfileId']); 
    }
   
    return json_encode($result);
  }
  
  /** ['Access' => 'Everyone'] */
  public function ResetPassword($clientCode, $resetCode, $newPassword) {
    $pf = new PollFunctions();
    
    $result = $pf->ResetPassword(array('ClientCode' => $clientCode, 'ResetCode' => $resetCode, 'NewPassword' => $newPassword));
    
    return json_encode($result); 
  }

  /** ['Access' => 'Everyone'] */
  public function SignUp($d) {        
    global $dbsession;
  
   /* $headers = 'From: webmaster@pollanalytics' . "\r\n" .
    'Reply-To: webmaster@pollanalytics' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    mail("clayton@digifi.ca", "testing from poll", "Hi Clayton, you're awesome!", $headers);
  
    throw new Exception($d->EmailAddress);                                         */
    
    $pf = new PollFunctions();
    
    //$d->BirthDate = $d->BirthYear . '-' . $d->BirthMonth . '-' . $d->BirthDay; 
    
    $result = $pf->SignUp((array) $d);
    
    if($result['Success']) {   
      $dbsession->start_session_record($result['ProfileId']); 
    }
    
    return $result;
  }
 
}

?>