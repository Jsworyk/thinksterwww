<?php

class admincategories {
  
  
  /** ['Access' => 'Administrator'] */
  public function GetCategories() {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT PollCategoryId `@Id`, Name `@Name`, BackgroundColor `@BackgroundColor`, IconClass `@IconClass`
      FROM PollCategory 
      ORDER BY Name";
      
    $xml = $q->executeXml('Categories', 'Category');
    
    return WebPage::Transform($xml, 'AdminCategories.xslt', '<p><em>No categories have been defined yet</em></p>'); 
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetCategory($pollCategoryId) {
    $pc = new Data('PollCategory', $pollCategoryId);
           
    return $pc->toJSON();
  }
  
  /** ['Access' => 'Administrator'] */
  public function SaveCategory($d) {
    $pc = new Data('PollCategory');
           
    $pc->load($d);
    
    $pc->update($this->CurrentUser->AccountId);
  }
 
}

?>