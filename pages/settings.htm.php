<?php 
class settings {

	public $Account;
	
	public function __construct() {
	
	 	$q = new DatabaseQuery();
	  
    	$q->sql = "SELECT pa.ProfileAccountId,
    					  pa.Password FROM ProfileAccount pa 
		      	  WHERE ProfileId = ?ai_profileId";
		      
		if(!isset($_GET['param']) || WebPage::$currentUser->ProfileId == $_GET['param']) {
		    $q->addParameter('ai_profileId', WebPage::$currentUser->ProfileId);
		}
		else
		{
			$q->addParameter('ai_profileId',$_GET['param']);
		}

		$profile = $q->executeObjects();
		    
    	if($profile) $this->Account = $profile[0];
	}
  
  /** ['Access' => 'Everyone'] */
  public function ChangePassword($d) {
    $pf = new PollFunctions();
    
    $result = array('Success' => true);
  
    if($d->NewPassword != $d->ConfirmPassword) {   
      $result['Success'] = false;
			$result['Message'] = "New Password and confirm do not match.";
		} else {
      $result = $pf->ChangeThinksterPassword(array('CurrentPassword' => $d->CurrentPassword, 'NewPassword' => $d->NewPassword));	
    }        
    
    if(!$result['Success']) {
      throw new Exception(json_encode($result));
    }
    
		return $result;
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetBlockedUsers() { 
    $pf = new PollFunctions();
    
    return json_encode($pf->GetBlockedUsers(array()));
  }
  
  /** ['Access' => 'Everyone'] */
	public function GetProfileAccounts() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetProfileAccounts(array()));
  } 
  
  /** ['Access' => 'Everyone'] */
  public function GetRecentImages() {
    $pf = new PollFunctions();
		
		return json_encode($pf->GetProfileImages(array('ProfileId' => $this->CurrentUser->ProfileId, 'Count' => 5)));
  }
  
  /** ['Access' => 'Everyone'] */
	public function GetUploadToken() {
    return getOneTimeToken(array('TargetTypeCode' => 'PR', 'TargetId' => WebPage::$currentUser->ProfileId, 'ExposureTypeCode' => 'PU'), WebPage::$currentUser->ProfileId);
  }
  
  /** ['Access' => 'Everyone'] */
  public function PostAvatar($d) {
    $m = new Data('Media');
    
    $p = new Data('Profile', $this->CurrentUser->ProfileId);
    $pa = new Data('ProfileAccount', $d->ProfileAccountId);
    
    //Quick and dirty security test:
    if($pa->ProfileId != $this->CurrentUser->ProfileId) throw new Exception("Access denied");
  
    if($d->ExistingMediaId && is_numeric($d->ExistingMediaId)) {
      $m = new Data('Media', $d->ExistingMediaId);
    } else {
      $m->merge($d);
      
      $m->IsPublished = 1;
      
      $m->update($this->CurrentUser->ProfileId);      	
    }
             
    $pf = new PollFunctions();
    
    $pf->SetAvatar(array('MediaId' => $m->MediaId));             
             
    /*$pa->AvatarUrl = $m->Url;
    $pa->AvatarThumbnailUrl = $m->ThumbnailUrl;
    $pa->AvatarMicrothumbUrl = $m->MicrothumbUrl;
    $pa->AvatarMediaId = $m->MediaId;
    
    $pa->update($this->CurrentUser->ProfileId);
    
    //If this account is the primary account, we need to update the profile:
    if($pa->IsPrimary) { 
      $p->AvatarUrl = $m->Url;
      $p->AvatarThumbnailUrl = $m->ThumbnailUrl;
      $p->AvatarMicrothumbUrl = $m->MicrothumbUrl;
      $p->AvatarMediaId = $m->MediaId;
      
      $p->update($this->CurrentUser->ProfileId);
    }  */
    
  }
    
 	/** ['Access' => 'Everyone'] */
	public function VerifyThinksterPassword($opw) {

		$ph_options = ['cost' => 11];
		
		if(password_verify($opw,$this->Account->Password)) {
		
			$result['Success'] = true;
			$result['Message'] = "";
		}
		else
		{
			$result['Success'] = false;
			$result['Message'] = "Current Password does not match the one on file.";
		}
		
		return json_encode($result);
	}
	
	/** ['Access' => 'Everyone'] */
	public function ChangeThinksterPassword($npw) {
		
		$profileAccount = new Data('ProfileAccount',$this->Account->ProfileAccountId);
		
		$ph_options = ['cost' => 11];
		
		$profileAccount->Password = password_hash($npw, PASSWORD_BCRYPT, $ph_options);
	
		$profileAccount->update($this->CurrentUser->ProfileId);
	
	}
	
	/** ['Access' => 'Everyone'] */
	public function FacebookLogin() {
		global $config;
		$result = "";

		$pf = new PollFunctions();

		$fb = new Facebook\Facebook([
		  'app_id' => "1702928873333290",
		  'app_secret' => "078d1a2e186a28d2b03a104c924c42d0",
		  'default_graph_version' => 'v2.8',
		]);

		$helper = $fb->getJavaScriptHelper();

		$result = $pf->FacebookLogin(array('AccessToken' => $helper->getAccessToken()));

		return json_encode($result);
	}
	
	/** ['Access' => 'Everyone'] */
	public function SaveSettings($args) {
		
		$profile = new Data('Profile',$this->CurrentUser->ProfileId);
		
	  	$profile->IsFollowable = $args->IsFollowable;
	  	
	  	$profile->update($this->CurrentUser->ProfileId);
	  	
	  	return "Settings Updated";
		
		}

	/** ['Access' => 'Everyone'] */
	public function SaveExposure($column,$value) {

		$profile = new Data('Profile',$this->CurrentUser->ProfileId);
    
    //TODO: Add security check to only allow certain columns to be set

		$profile->$column = $value;

		$profile->update($this->CurrentUser->ProfileId);
			
		}
    
  /** ['Access' => 'Everyone'] */
	public function SetPrimaryAccount($profileAccountId) {    
    $pf = new PollFunctions();
    
    $pf->SetPrimaryAccount(array('ProfileAccountId' => $profileAccountId));
			                                 
	}
  
  /** ['Access' => 'Everyone'] */
	public function UnblockUser($profileId) {
		$pf = new PollFunctions();
		
		return json_encode($pf->Unblock(array('ProfileId' => $profileId)));
	}
}

?>