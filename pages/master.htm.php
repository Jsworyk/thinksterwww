<?php

include_once('../inc/Guzzle6HttpClient.php');

class master {

  /** ['Access' => 'LoggedIn'] */
  public function AcceptFollowRequest($profileId) {
    $pf = new PollFunctions();
    
    $result = $pf->AcceptFollowRequest(array('ProfileId' => $profileId));
    
    return json_encode($result);  
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function Comment($targetTypeCode, $targetId, $replyToCommentId, $commentText) { 
    $pf = new PollFunctions();
    
    $pf->Comment(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId, 'ReplyToCommentId' => $replyToCommentId, 'CommentText' => $commentText));
  }
        
  
  /** ['Access' => 'LoggedIn'] */
  public function DeclineFollowRequest($profileId) {
    $pf = new PollFunctions();
    
    $result = $pf->DeclineFollowRequest(array('ProfileId' => $profileId));
    
    return json_encode($result);  
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function DeleteNotification($notificationId) { 
    $pf = new PollFunctions();
    
    return $pf->DeleteNotification(array('NotificationId' => $notificationId));    
  }
    
  /** ['Access' => 'Everyone'] */
  public function FacebookLogin() {
    global $config;
    $result = "";
    
    $pf = new PollFunctions();
    
    $client = new GuzzleHttp\Client;    
    
    $fb = new Facebook\Facebook([
      'http_client_handler' => new Guzzle6HttpClient($client),
      'app_id' => $config->FBAppId,
      'app_secret' => $config->FBSecret,
      'default_graph_version' => 'v2.8',
    ]);
    
    $helper = $fb->getJavaScriptHelper();
    
    $result = $pf->FacebookLogin(array('AccessToken' => $helper->getAccessToken()));
    
    return json_encode($result);    
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function FlagContent($targetTypeCode, $targetId, $flagTypeCode, $details) {
    $pf = new PollFunctions();
    
    return $pf->FlagContent(array('TargetTypeCode' => $targetTypeCode,
        'TargetId' => $targetId,
        'FlagTypeCode' => $flagTypeCode,
        'Details' => $details));
  } 

  /** ['Access' => 'LoggedIn'] */
  public function GetChatPreviews() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetChatRooms(array('PageSize' => 5)));     
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function GetNotificationPreviews() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetNotifications(array('Count' => 4)));     
  }  
  
  /** ['Access' => 'LoggedIn'] */
  public function GetFollowRequestsPreview() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetFollowRequests(array('PageSize' => 5)));         
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function GetFollowers() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetFollowers(array()));         
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function GetFollowing() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetFollowing(array()));         
  }
      
  /** ['Access' => 'Everyone'] */
  public function GetTargetComments($targetTypeCode, $targetId, $replyToCommentId, $lastCommentId) {
    $pf = new PollFunctions();
    
    $result = $pf->GetComments(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId, 'ReplyToCommentId' => $replyToCommentId, 'LastCommentId' => $lastCommentId));
  
    return json_encode($result); 
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function GetFriendPreviews() {
    $pf = new PollFunctions();
    
    return json_encode($pf->PendingFriendRequests(array('MaxResults' => 5))); 
  }
  

  
  /** ['Access' => 'Everyone'] */
  public function GetPushToken() {    
    return getOneTimeToken(array('ProfileId' => $this->CurrentUser->ProfileId, 'Rooms' => (isset($this->Settings['PushRooms']) ? $this->Settings['PushRooms'] : [])), $this->CurrentUser->ProfileId);  
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetReactors($targetTypeCode, $targetId) {
    $pf = new PollFunctions();
    
    $result = $pf->GetReactors(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId));
  
    return json_encode($result); 
  }  

  
  
  /** ['Access' => 'Everyone'] */
  public function Login($em, $pw) {
    global $dbsession;
  
    $pf = new PollFunctions();
    
    $result = $pf->ThinksterLogin(array('EmailAddress' => $em, 'Password' => $pw));
    
    if($result['Success']) {
      $dbsession->start_session_record($result['ProfileId']); 
    }
  
    /*$result = array('Success' => false);
    
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT * FROM ProfileAccount WHERE AccountTypeCode = 'PO' AND LOWER(EmailAddress) = TRIM(LOWER(?as_email))";
    
    $q->addParameter('as_email', $em);
    
    $acct = $q->executeObjects();
    
    if(count($acct) > 0) {
      //Set the password hash options:
      $ph_options = ['cost' => 11];
      
      if(password_verify ($pw, $acct[0]->Password)) {
        $result['Success'] = true;   
        
        //Initialize the session  
        $dbsession->start_session_record($acct[0]->ProfileId);                   
      } else {
        //TODO: Use some sort of multi-language table?
        $result['Message'] = 'Your login was not successful. Please check your credentials and try again.';
      }                
    } else {
      $result['Message'] = 'Your login was not successful. Please check your credentials and try again.';
    }     */
   
    return json_encode($result);
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function MarkNotificationRead($notificationId) { 
    $pf = new PollFunctions();
    
    return $pf->MarkNotificationRead(array('NotificationId' => $notificationId));    
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function MarkNotificationUnread($notificationId) { 
    $pf = new PollFunctions();
    
    return $pf->MarkNotificationUnread(array('NotificationId' => $notificationId));    
  }
  
  /** ['Access' => 'Developer'] */
  public function NewObject($type)
  {
    $d = new Data($type);
    
    return $d->toJSON();
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function React($targetTypeCode, $targetId, $reactionTypeCode) { 
    $pf = new PollFunctions();
    
    $pf->React(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId, 'ReactionTypeCode' => $reactionTypeCode));
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function RemoveReaction($targetTypeCode, $targetId) { 
    $pf = new PollFunctions();
    
    $pf->RemoveReaction(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId));
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function HeaderSearch($searchString) { 
    $pf = new PollFunctions();
    
    return $pf->Search(array('SearchString' => $searchString, 'Offset' => 0, 'Count' => 8));
  }
  
}

?>