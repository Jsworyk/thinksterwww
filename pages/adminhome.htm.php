<?php

class adminhome {

  /** ['Access' => 'Administrator'] */
  public function GetHeatMapPoints() {
    
    $q = new DatabaseQuery();
  
    $q->sql = "SELECT COUNT(*) `Weight`, l.Latitude / 10000.0 `Latitude`, l.Longitude  / 10000.0 `Longitude`
    FROM Profile p
    JOIN Location l ON l.LocationId = p.LocationId
    WHERE p.IsActive = 1
    GROUP BY l.Latitude / 10000.0, l.Longitude  / 10000.0";
  
    $locations = $q->executeObjects();    
    
    /*foreach($locations as $location) {
      if(!$first) echo ",";
      
      echo "{location: new google.maps.LatLng(" . $location->Latitude . ", " . $location->Longitude . "), weight: " . $location->Total * 100 . "}";
      
      $first = false;
    }     */
    return json_encode($locations);
  }
  
  
  /** ['Access' => 'Administrator'] */
  public function GetAnswerCounts() {
    
    $q = new DatabaseQuery();
  
    $q->sql = "SELECT dd.Date `Date`, DATE_FORMAT(dd.Date, '%M %d') `Label`, COUNT(pr.PollResponseId) `Total`
FROM DateDimension dd
LEFT JOIN PollResponse pr ON CONVERT(Created, date) = dd.Date
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date, DATE_FORMAT(dd.Date, '%M %d')
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $chart = array('type' => 'line', 'data' => array('labels' => [], 'datasets' => []), 'options' => array('title' => array('display' => true, 'text' => 'Site Activity Last 7 Days')));
    
    $dataset = array('label' => 'Responses', 'borderColor' => "rgba(255, 206, 86, 1)", 'backgroundColor' => "rgba(255, 206, 86, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($chart['data']['labels'], $row->Label);
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);      
    
    //Accounts created:
    $q->sql = "SELECT dd.Date, COUNT(p.ProfileId) `Total`
FROM DateDimension dd
LEFT JOIN Profile p ON CONVERT(p.Joined, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'New Accounts', 'borderColor' => "rgba(255, 99, 132, 1)", 'backgroundColor' => "rgba(255, 99, 132, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
    
    
    //Chat Messages created:
    $q->sql = "SELECT dd.Date, COUNT(crm.ChatRoomMessageId) `Total`
FROM DateDimension dd
LEFT JOIN ChatRoomMessage crm ON CONVERT(crm.Created, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'Chats Sent', 'borderColor' => "rgba(75,192,1921)", 'backgroundColor' => "rgba(75,192,192,0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
    
    //Reactions:
    $q->sql = "SELECT dd.Date, COUNT(r.ReactionId) `Total`
FROM DateDimension dd
LEFT JOIN Reaction r ON CONVERT(r.Created, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'Reactions', 'borderColor' => "rgba(153, 102, 255, 1)", 'backgroundColor' => "rgba(153, 102, 255, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
    
    //Comments:
    $q->sql = "SELECT dd.Date, COUNT(c.CommentId) `Total`
FROM DateDimension dd
LEFT JOIN Comment c ON CONVERT(c.Created, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'Comments', 'borderColor' => "rgba(102, 153, 102, 1)", 'backgroundColor' => "rgba(102, 153, 102, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
    
    
    //Follows initiated:
    $q->sql = "SELECT dd.Date, COUNT(pf.ProfileFollowerId) `Total`
FROM DateDimension dd
LEFT JOIN ProfileFollower pf ON pf.Approved IS NOT NULL AND CONVERT(pf.Approved, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'Follows', 'borderColor' => "rgba(202, 53, 92, 1)", 'backgroundColor' => "rgba(202, 53, 92, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
    
    
    //Unfollows initiated:
    $q->sql = "SELECT dd.Date, COUNT(pf.ProfileFollowerId) `Total`
FROM DateDimension dd
LEFT JOIN ProfileFollower pf ON pf.Ended IS NOT NULL AND CONVERT(pf.Ended, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'Unfollows', 'borderColor' => "rgba(52, 28, 202, 1)", 'backgroundColor' => "rgba(52, 28, 202, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
    
    
    //Passes:
    $q->sql = "SELECT dd.Date, COUNT(pp.PassedProfileId) `Total`
FROM DateDimension dd
LEFT JOIN PassedProfile pp ON CONVERT(pp.Modified, date) = dd.Date 
WHERE dd.Date BETWEEN CONVERT(DATE_ADD(NOW(), interval -7 day), date) AND CONVERT(NOW(), date)
GROUP BY dd.Date
ORDER BY dd.Date";
  
    $chartData = $q->executeObjects();
    
    $dataset = array('label' => 'Passes', 'borderColor' => "rgba(114, 114, 114, 1)", 'backgroundColor' => "rgba(114, 114, 114, 0.4)", 'data' => []);
           
    foreach($chartData as $row) {
      array_push($dataset['data'], $row->Total);
    }
    
    array_push($chart['data']['datasets'], $dataset);
        
    return json_encode($chart);
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetBlockedUsers() {
    $q = new DatabaseQuery(); 
    
    $results = '';
    
    $q->sql = "SELECT p.ProfileId `@Id`,
	p.ProfileName `@ProfileName`,
    p.DisplayName `@DisplayName`,
    p.AvatarMicrothumbUrl `@Avatar`,
    COUNT(b.BlockedId) `@BlockCount`
FROM Profile p
	JOIN Blocked b ON b.BlockedProfileId = p.ProfileId AND b.Ended IS NULL
WHERE p.IsActive != 0 
GROUP BY p.ProfileId, p.ProfileName, p.DisplayName, p.AvatarMicrothumbUrl
HAVING COUNT(b.BlockedId) > 5	
ORDER BY COUNT(b.BlockedId) DESC";

    $xml = $q->executeXml('Users', 'User');
    
    $results = WebPage::Transform($xml, "BlockedUsers.xslt", '<div class="alert alert-success"><p><i class="fa fa-pull-left fa-2x fa-smile-o mr-2"></i>There are no active users currently blocked by 5 or more others</p></div>');
    
    return $results;  
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetFlaggedObjects() {
    $q = new DatabaseQuery();
    $results = '';
    
    $q->sql = "SELECT cf.Created `@Created`,
    DATE_FORMAT(CONVERT_TZ(cf.Created,'America/Winnipeg','GMT'), '%Y-%m-%dT%TZ') `@Timestamp`,
	  cf.ContentFlagId `@Id`,               
    tt.Name `@TargetType`,
	  cf.TargetTypeCode `@TargetTypeCode`,
    cf.TargetId	`@TargetId`,
    tt.TableName `@TableName`,
    tt.SelectorColumn `@SelectorColumn`,
    tt.DisplayColumn `@DisplayColumn`,
    cf.Details `@Details`,
    ft.Name  `@FlagType`,
    CASE 
      WHEN cf.TargetTypeCode = 'ME' THEN
        (SELECT CONCAT('/media/', Selector) FROM Media WHERE MediaId = cf.TargetId)
      ELSE 
        '' 
    END `@Url`,
    (SELECT COUNT(*) FROM ContentFlag cf2 
      WHERE cf2.TargetTypeCode = cf.TargetTypeCode 
        AND cf2.TargetId = cf.TargetId) `@FlagCount`
FROM ContentFlag cf
	JOIN TargetType tt ON tt.TargetTypeCode = cf.TargetTypeCode
  JOIN FlagType ft ON ft.FlagTypeCode = cf.FlagTypeCode  
WHERE cf.ReviewedDate IS NULL
ORDER BY cf.Created DESC ";

    $xml = $q->executeXml('Flags', 'Flag');
    
    $results = WebPage::Transform($xml, "FlaggedContent.xslt", '<div class="alert alert-info"><p>There is no flagged content</p></div>');
    
    return $results;     
  }
  
}

?>