<?php
class embed2 {
  public $Viewing = null;
  
  public function __construct() {
    if(isset($_GET['param']) && !preg_match("/event(\d+)/", $_GET['param'])) {
      $pf = new PollFunctions();
      $p = new Data('Poll');
      
      $requestorProfileId = null;
      
      if(isset($_GET['requestor'])) $requestorProfileId = $_GET['requestor'];
  
      $p->load(array('UrlSegment' => strtolower($_GET['param'])));

      $this->Viewing = $pf->GetPoll(array('PollId' => $p->PollId, 'RequestorProfileId' => $requestorProfileId)); 
    }  
  } 
  
  /** ['Access' => 'Everyone'] */
  public function AnswerQuestion($pollChoiceId, $visibility, $latitude, $longitude) {
    $pf = new PollFunctions();
    
    return $pf->SubmitAnswer(array('PollId' => $this->Viewing['Question']->PollId, 'Response' => $pollChoiceId, 'ResponseVisibility' => $visibility, 'Latitude' => $latitude, 'Longitude' => $longitude)); 
  }  
  
  /** ['Access' => 'Everyone'] */
  public function GetComments($lastCommentId) {
    $pf = new PollFunctions();
    
    $result = $pf->GetComments(array('TargetTypeCode' => 'PO', 'TargetId' => $this->Viewing['Question']->PollId, 'LastCommentId' => $lastCommentId));
  
    return json_encode($result); 
  } 
  
  /** ['Access' => 'Everyone'] */
  public function GetFriendResponses() {
    $pf = new PollFunctions();
    
    $result = $pf->GetFriendResponses(array('PollId' => $this->Viewing['Question']->PollId));
  
    return json_encode($result); 
  }   
  
  /** ['Access' => 'Everyone'] */
  public function GetMapData($bounds, $zoom, $visualizationTypeCode, $omitRegions, $filters) {
    $pf = new PollFunctions();
    
    $results = $pf->GetPollMapData(array('Bounds' => array('north' => $bounds->north, 'south' => $bounds->south, 'east' => $bounds->east, 'west' => $bounds->west), 
      'Zoom' => $zoom, 
      'PollId' => $this->Viewing['Question']->PollId,
      'VisualizationTypeCode' => $visualizationTypeCode, 
      'OmitRegions' => $omitRegions, 
      'Filters'  => $filters));
    
    return json_encode($results);
  } 
  
  
}

?>