<?php

class friends {

  public $Viewing;
  public $isMe = false;
  public $WeAreFriends = false;
  public $CanViewFriends = false;
  
  public function __construct() {
 
    $pf = new PollFunctions(); 
    
  if(!isset($_GET['param']) || WebPage::$currentUser->ProfileName == $_GET['param']) {
    $this->isMe = true;
		$this->Viewing = $pf->GetProfile(array('ProfileName' => WebPage::$currentUser->ProfileName));
	}
	else
	{
		$this->Viewing = $pf->GetProfile(array('ProfileName' => $_GET['param']));
	}
    
   	// $this->Viewing = $pf->GetProfileCurrentPage();
    //$this->isMe = isset($_GET['param']) &&  WebPage::$currentUser->ProfileName == $_GET['param'];

    if(!$this->isMe) {
    
		$q = new DatabaseQuery();

	    $q->sql = "SELECT p.ProfileId,
               p.FriendsExpTypeCode
               FROM Profile p
               WHERE p.ProfileId = ?ai_profileid;";       
               
		$q->addParameter('ai_profileid',$this->Viewing->ProfileId);               
		
		$ExpCode = $q->executeObjects();
		
    	$this->WeAreFriends = $pf->AreWeFriends(array('ProfileId' => WebPage::$currentUser->ProfileId, 'FriendId' => $this->Viewing->ProfileId));
    	
    	$CanViewFriends = false;
		if($ExpCode[0]->FriendsExpTypeCode == 'PU' || ($ExpCode[0]->FriendsExpTypeCode == 'FR' && $this->WeAreFriends)) {
			$this->CanViewFriends = true;
		}
    } else {
    	$this->CanViewFriends = true;
    }
    
  }

  /** ['Access' => 'Everyone'] */
  public function GetFriendList() {
    $pf = new PollFunctions();    
    
    return json_encode($pf->FriendList(array('ProfileId' => $this->CurrentUser->ProfileId,'PageNumber' => 1, 'PageSize' => 30)));
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetIncomingRequests() {
    $pf = new PollFunctions();
    
    return json_encode($pf->PendingFriendRequests(array('MaxResults' => 10)));    
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetOutgoingRequests() {
    $pf = new PollFunctions();
    
    return json_encode($pf->PendingOutgoingRequests(array('MaxResults' => 10)));
  }

  /** ['Access' => 'Everyone'] */
  public function SuggestFriends() {
    $pf = new PollFunctions();

    return json_encode($pf->SuggestFriends(array('ProfileId'=> $this->Viewing->ProfileId,"MaxResults" => 10)));
  }
 
 /** ['Access' => 'Everyone'] */
  public function GetCommonFriends() {
    $pf = new PollFunctions();
    
    return json_encode($pf->CommonFriendsList($this->Viewing->ProfileId,array('PageNumber' => 1, 'PageSize' => 30)));
  }
}

?>