<?php


class embed {

  public $Viewing = null;
  
  public function __construct() {
    if(isset($_GET['param'])) {
      $this->Viewing = new Data('Poll');
  
      $this->Viewing->load(array('UrlSegment' => strtolower($_GET['param'])));
    }  
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetMapData($bounds, $zoom, $visualizationTypeCode, $omitRegions, $filters) {
    $pf = new PollFunctions();
    
    $results = $pf->GetPollMapData(array('Bounds' => array('north' => $bounds->north, 'south' => $bounds->south, 'east' => $bounds->east, 'west' => $bounds->west), 
      'Zoom' => $zoom, 
      'PollId' => $this->Viewing->PollId,
      'VisualizationTypeCode' => $visualizationTypeCode, 
      'OmitRegions' => $omitRegions, 
      'Filters'  => $filters));
    
    return json_encode($results);
  }
  
  /** ['Access' => 'Everyone'] */
  public function SignUp($d) {        
    global $dbsession;  
    
    $pf = new PollFunctions();
    
    $d->BirthDate = $d->BirthYear . '-' . $d->BirthMonth . '-' . $d->BirthDay; 
    
    $result = $pf->SignUp((array) $d);
    
    if($result['Success']) {   
      $dbsession->start_session_record($result['ProfileId']); 
    }
    
    return $result;
  }

}

?>