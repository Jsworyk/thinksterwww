<?php

class master {

  /** ['Access' => 'Everyone'] */
  public function GetPushToken() {
    return getOneTimeToken(array('ProfileId' => $this->CurrentUser->ProfileId, 'Rooms' => (isset($this->Settings['PushRooms']) ? $this->Settings['PushRooms'] : [])), $this->CurrentUser->ProfileId);  
  }

  /** ['Access' => 'Everyone'] */
  public function NewObject($type)
  {
    $d = new Data($type);
    
    return $d->toJSON();
  }
}  

?>