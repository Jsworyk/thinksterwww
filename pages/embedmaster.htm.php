<?php

class master {

  /** ['Access' => 'LoggedIn'] */
  public function Comment($targetTypeCode, $targetId, $replyToCommentId, $commentText) { 
    $pf = new PollFunctions();
    
    $pf->Comment(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId, 'ReplyToCommentId' => $replyToCommentId, 'CommentText' => $commentText));
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetReactors($targetTypeCode, $targetId) {
    $pf = new PollFunctions();
    
    $result = $pf->GetReactors(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId));
  
    return json_encode($result); 
  }  
  
  /** ['Access' => 'Everyone'] */
  public function GetTargetComments($targetTypeCode, $targetId, $replyToCommentId, $lastCommentId) {
    $pf = new PollFunctions();
    
    $result = $pf->GetComments(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId, 'ReplyToCommentId' => $replyToCommentId, 'LastCommentId' => $lastCommentId));
  
    return json_encode($result); 
  }

  /** ['Access' => 'Everyone'] */
  public function GetPushToken() {
    return getOneTimeToken(array('ProfileId' => $this->CurrentUser->ProfileId, 'Rooms' => (isset($this->Settings['PushRooms']) ? $this->Settings['PushRooms'] : [])), $this->CurrentUser->ProfileId);  
  }     
  
  /** ['Access' => 'Everyone'] */
  public function Login($em, $pw) {
    global $dbsession;
  
    $pf = new PollFunctions();
    
    $result = $pf->ThinksterLogin(array('EmailAddress' => $em, 'Password' => $pw));
    
    if($result['Success']) {
      $dbsession->start_session_record($result['ProfileId']); 
    }
  
    return json_encode($result);
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function React($targetTypeCode, $targetId, $reactionTypeCode) { 
    $pf = new PollFunctions();
    
    $pf->React(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId, 'ReactionTypeCode' => $reactionTypeCode));
  }
  
  /** ['Access' => 'LoggedIn'] */
  public function RemoveReaction($targetTypeCode, $targetId) { 
    $pf = new PollFunctions();
    
    $pf->RemoveReaction(array('TargetTypeCode' => $targetTypeCode, 'TargetId' => $targetId));
  }
  
}


?>