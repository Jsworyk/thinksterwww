<?php

class devdeploy {


  /** ['Access' => 'Developer'] */
  public function GetDeltas() {
    $items = array('Files' => array());
    
    $this->recursiveSearch('', $items['Files']);
  
    return json_encode($items); 
  }
  
  function Another_is_dir ($file)
  {
      if ((fileperms("$file") & 0x4000) == 0x4000)
          return TRUE;
      else
          return FALSE;
  }
    
  
  function recursiveSearch($subfolder, &$items) {
    $devFolder = '/var/pollsite/dev' . $subfolder;
    $prodFolder = '/var/pollsite/prod'. $subfolder;
    
    $files = scandir($devFolder);
    
    foreach($files as $file) {
      if(substr($file, 0, 1) != '.') {
        if(is_dir($devFolder . '/' . $file)) {
          $folder = array('Type' => 'Folder', 'Name' => $file, 'Files' => array());
          
          $this->recursiveSearch($subfolder . '/' . $file, $folder['Files']); 
          
          $folder['FileCount'] = count($folder['Files']);
        
          if(count($folder['Files']) != 0) array_push($items, $folder); 
        } else {
          if(!file_exists($prodFolder . '/'. $file) || (filemtime($prodFolder . '/'. $file) < filemtime($devFolder . '/'. $file))) {
            array_push($items, array('Type' => 'File', 'Name' =>  $file, 'FullPath' => $subfolder . '/' . $file));
          }          	
        }                                      
         
      } 
    }
   
  }
  
  /*function touchFile($file) {
  fclose(fopen($file, 'a'));    
}  */
  
  /** ['Access' => 'Developer'] */
  public function DeployFiles($fileList) {
    foreach($fileList as $file) {
      try {
        if(!file_exists(dirname('/var/pollsite/prod' . $file))) {
          mkdir(dirname('/var/pollsite/prod' . $file), 0777, true);
        }
        copy('/var/pollsite/dev' . $file, '/var/pollsite/prod' . $file);
        fclose(fopen('/var/pollsite/prod' . $file, 'a'));
      }catch(Exception $ex) {
         throw new Exception($file . ': ' . $ex->getMessage());
      }     
    }    
  }
 
}

?>