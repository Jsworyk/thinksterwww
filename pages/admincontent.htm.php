<?php

class admincontent {

  /** ['Access' => 'Administrator'] */
  public function GetContent($staticContentId) {
    $sc = new Data('StaticContent', $staticContentId);
    
    return $sc->toJSON();
  } 

  /** ['Access' => 'Administrator'] */
  public function GetContentList() {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT sc.StaticContentId '@Id',
       sc.Title '@Title',
       sc.ShortName '@ShortName',
       sct.Name '@Type',
       sc.Description '@Description',
       sc.StaticContentTypeCode '@StaticContentTypeCode',
       sc.UrlSegment '@UrlSegment'       
    FROM StaticContent sc
      JOIN StaticContentType sct ON sct.StaticContentTypeCode = sc.StaticContentTypeCode 
    ORDER BY sc.Title";
    
    $xml = $q->executeXml('Contents', 'Content');
     
    return WebPage::Transform($xml, 'StaticContent.xslt', '<div class="alert alert-info"><p>No content has been defined in the system</p></div>');
  } 
  
  /** ['Access' => 'Administrator'] */
  public function SaveContent($d) {
    $sc = new Data('StaticContent');
    
    $sc->load($d);
    
    $sc->update($this->CurrentUser->ProfileId);
  }
  
}

?>