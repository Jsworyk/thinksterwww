<?php

class adminusers {
 
  /** ['Access' => 'Administrator'] */
  public function GetOverview() {
    $q = new DatabaseQuery();
    
    $q->sql = "
SELECT p.ProfileId,
  p.ProfileName,
  p.DisplayName,
  p.AvatarMicrothumbUrl,
  p.Birthdate,
  p.Joined,
  p.Age,
  CONCAT(l.Name, ', ', l.City, ', ', l.State) Location, 
  p.GenderCode,
  p.IsActive,
  p.IsFollowable,
  (SELECT COUNT(*) FROM ProfileFollower pf WHERE pf.ProfileId = ?ai_profileid AND Approved IS NOT NULL AND Ended IS NOT NULL) `Followers`,
  (SELECT COUNT(*) FROM ProfileFollower pf WHERE pf.FollowerId = ?ai_profileid AND Approved IS NOT NULL AND Ended IS NOT NULL) `Following`
FROM Profile p
  JOIN Location l ON l.LocationId = p.LocationId    
WHERE p.ProfileId = ?ai_profileid";

    $q->addParameter('ai_profileid', $_GET['param']);
    
    $xml = $q->executeXml('Overview', 'Profile');
    
    $q->sql = "SELECT pa.DisplayName,
	pa.EmailAddress,
    ac.Name `AccountType`,
    pa.IsPrimary
FROM ProfileAccount pa
	JOIN AccountType ac ON ac.AccountTypeCode = pa.AccountTypeCode
WHERE ProfileId = ?ai_profileid";

    $q->executexml('Accounts', 'Account', $xml);
    
    $q->sql = "SELECT p.ProfileId, p.DisplayName, p.ProfileName, b.Started
FROM Blocked b
JOIN Profile p ON p.ProfileId = b.BlockedProfileId
WHERE b.ProfileId = ?ai_profileid
	AND b.Ended IS NULL";

    $q->executexml('Blocking', 'Profile', $xml);
    
    $q->sql = "SELECT p.ProfileId, p.DisplayName, p.ProfileName, b.Started
FROM Blocked b
JOIN Profile p ON p.ProfileId = b.ProfileId
WHERE b.BlockedProfileId = ?ai_profileid
	AND b.Ended IS NULL";

    $q->executexml('BlockedBy', 'Profile', $xml);    
  
    return WebPage::Transform($xml, 'AdminUserOverview.xslt');    
  }
 
 /** ['Access' => 'Administrator'] */
  public function GetResponses($pageNumber) {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT SQL_CALC_FOUND_ROWS pr.ResponseDate `@ResponseDate`,
    p.UrlSegment `@Url`,
	p.Question `@Question`,
	pc.Choice `@Answer`,
    pr.CommentCount `@Comments`,
    pr.ReactionCount `@Reactions`
FROM PollResponse pr
	JOIN PollChoice pc ON pc.PollChoiceId = pr.Response
	JOIN Poll p ON p.PollId = pc.PollId
WHERE pr.ProfileId = ?ai_profileid
ORDER BY pr.ResponseDate DESC
LIMIT " . (($pageNumber - 1) * 10) . ", 10";

    $q->addParameter('ai_profileid', $_GET['param']);
      
    $xml = $q->executeXml('Responses', 'Response');
    
    return WebPage::Transform($xml, 'AdminUserResponses.xslt', '<p><em>This user hasn\'t answered any polls yet</em></p>') . GeneratePageNumbers($pageNumber, 10, $q->found_rows); 
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetSessions($pageNumber) {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT SQL_CALC_FOUND_ROWS Started `@Started`, Ended `@Ended`, 'Mobile' `@Agent`, LastActivity `@LastActivity`, 'N/A' `@IPAddress`
FROM ApplicationSession
WHERE ProfileId = ?ai_profileid
UNION SELECT s.StartDate, s.EndDate, ua.Name, s.LastActivity, s.IPAddress 
FROM Session s
	JOIN UserAgent ua ON ua.UserAgentId = s.UserAgentId
WHERE s.ProfileId = ?ai_profileid
ORDER BY `@LastActivity` DESC
LIMIT " . (($pageNumber - 1) * 10) . ", 10";

    $q->addParameter('ai_profileid', $_GET['param']);
      
    $xml = $q->executeXml('Sessions', 'Session');
    
    return WebPage::Transform($xml, 'AdminUserSessions.xslt', '<p><em>This user hasn\'t logged in yet</em></p>') . GeneratePageNumbers($pageNumber, 10, $q->found_rows); 
  }
 
  /** ['Access' => 'Administrator'] */
  public function GetUsers($name, $showInactive, $pageNumber, $sortColumn, $sortDesc) {
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT SQL_CALC_FOUND_ROWS p.ProfileId `@Id`, p.AvatarUrl `@AvatarUrl`,
      p.Age `@Age`,
      p.ProfileName `@ProfileName`, p.DisplayName `@DisplayName`,
      CONCAT(l.Name, IFNULL(CONCAT(', ', l.City, ', ', l.State), '')) `@Location`,
      p.IsActive `@IsActive`,
      (SELECT COUNT(*) FROM PollResponse pr WHERE pr.IsCurrent = 1 AND pr.ProfileId = p.ProfileId) `@ResponseCount`
      FROM Profile p 
        LEFT JOIN Location l ON l.LocationId = p.LocationId
      WHERE p.IsActive = ?ai_active
        AND (?as_name = '' OR p.ProfileName LIKE CONCAT('%', ?as_name, '%')
           OR p.DisplayName LIKE CONCAT('%', ?as_name, '%'))
      ORDER BY " . $sortColumn . ($sortDesc ? ' DESC' : '') . "
      LIMIT " . (($pageNumber - 1) * 20) . ", 20";
      
    $q->addParameter('as_name', $name);
    $q->addParameter('ai_active', !$showInactive);
      
    $xml = $q->executeXml('Users', 'User');       
    
    return WebPage::Transform($xml, 'AdminUserList.xslt', '<p><em>No users match your criteria</em></p>') . GeneratePageNumbers($pageNumber, 20, $q->found_rows);         
  }
  
  /** ['Access' => 'Administrator'] */
  public function ToggleActive() { 
    if(!isset($_GET['param'])) return;
    
    $q = new DatabaseQuery();
    
    $q->sql = "UPDATE Profile SET IsActive = !IsActive, Modified = CURRENT_TIMESTAMP, Modifier = ?ai_me WHERE ProfileId = ?ai_profileid";
    
    $q->addParameter('ai_me', $this->CurrentUser->ProfileId);
    $q->addParameter('ai_profileid', $_GET['param']);
    
    $q->executeScalar();
  }
   
}

?>