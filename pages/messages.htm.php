<?php

class messages {
  
  /** ['Access' => 'Everyone'] */
  public function DeleteChatRoomMessage($chatRoomMessageId) {
    $pf = new PollFunctions();
    
    return json_encode($pf->DeleteChatRoomMessage(array('ChatRoomMessageId' => $chatRoomMessageId))); 
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetChatRooms() {
    $pf = new PollFunctions();
    
    return json_encode($pf->GetChatRooms(array())); 
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetMessages($chatRoomId) {
    /*$q = new DatabaseQuery();
    
    $result = array('Success' => true, 'Messages' => array());
    
    $q->sql = "select MIN(ChatRoomMessageId) from (SELECT ChatRoomMessageId FROM ChatRoomMessage 
WHERE ChatRoomId = ?ai_chatroomid
ORDER BY ChatRoomMessageId DESC
LIMIT 15) as tbl";

    $q->addParameter('ai_chatroomid', $chatRoomId);
    
    $startingPoint = $q->executeScalar();
    
    $q->sql = "SELECT crm.ChatRoomMessageId,
	crm.ProfileId,
    p.DisplayName,
    p.AvatarUrl,
    crm.Message,
    crm.Created,
    CASE WHEN IFNULL(crp.LastRead, '1900-01-01') < crm.Created AND crm.ProfileId != ?ai_profileid THEN 1 ELSE 0 END Unread
FROM ChatRoomMessage crm
JOIN Profile p ON p.ProfileId = crm.ProfileId
JOIN ChatRoomParticipant crp ON crp.ChatRoomId = crm.ChatRoomid AND crp.ProfileId = ?ai_profileid
WHERE crm.ChatRoomId = ?ai_chatroomid
AND crm.ChatRoomMessageId >= ?ai_startingpoint
ORDER BY crm.Created";
    
    $q->addParameter('ai_profileid', $this->CurrentUser->ProfileId);     
    $q->addParameter('ai_startingpoint', $startingPoint);
    
    $result['Messages'] = $q->executeObjects();
    
    return json_encode($result);    */
    $pf = new PollFunctions();
    
    return json_encode($pf->GetChatMessages(array('ChatRoomId' => $chatRoomId, 'NumberOfMessages' => 15))); 
  }
  
  /** ['Access' => 'Everyone'] */
  public function JoinChatRoom($chatRoomId) {
    $pf = new PollFunctions();
    
    return json_encode($pf->AcceptRoomRequest(array('ChatRoomId' => $chatRoomId))); 
  }
  
  /** ['Access' => 'Everyone'] */
  public function SendMessage($chatRoomId, $message) { 
    $pf = new PollFunctions();
    
    return json_encode($pf->SendMessage(array('Destination' => $chatRoomId, 'Message' => $message)));
  }
  
  /** ['Access' => 'Everyone'] */
  public function UpdateReadDate($chatRoomId) {
    /*$crp = new Data('ChatRoomParticipant');
    
    $crp->load(array('ChatRoomId' => $chatRoomId, 'ProfileId' => $this->CurrentUser->ProfileId));
    $crp->LastRead = new staticValue('CURRENT_TIMESTAMP');
    $crp->update($this->CurrentUser->ProfileId);*/
  
    $pf = new PollFunctions();
    
    return json_encode($pf->UpdateReadDate(array('ChatRoomId' => $chatRoomId))); 
  }
} 

?>