<?php

class dashboard {

  /** ['Access' => 'Everyone'] */
  public function AnswerQuestion($pollChoiceId, $visibility) {
    $pf = new PollFunctions();
    
    $pf->SubmitAnswer(array('PollChoiceId' => $pollChoiceId, 'ResponseVisibility' => $visibility)); 
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetNewsfeed() {
    $pf = new PollFunctions();
    
    //return "Under construction";
    return json_encode($pf->GetTimeline(array('Count' => 3)));  
  }

  /** ['Access' => 'Everyone'] */
  public function GetNextQuestion() {
    $pf = new PollFunctions();

    return json_encode($pf->NextQuestion(array()));

  }
  
  /** ['Access' => 'Everyone'] */
  public function GetTrending() {
    $pf = new PollFunctions();

    return json_encode($pf->GetTrendingQuestions(array()));
  }

  /** ['Access' => 'Everyone'] */
  public function SuggestFollowers($pollCategoryId) {
    $pf = new PollFunctions();

    return json_encode($pf->SuggestFollowers(array("MaxResults" => 4, 'PollCategoryId' => $pollCategoryId)));
  }
   

}

?>