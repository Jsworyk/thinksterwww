<?php

class adminquestions {

  /** ['Access' => 'Administrator'] */
  public function DeleteChoice($pollChoiceId) {
    $pc = new Data('PollChoice', $pollChoiceId);
    
    if($pc->PollId != $_GET['param']) throw new Exception("Access Denied");
    
    $pc->delete($this->CurrentUser->ProfileId); 
  }

  /** ['Access' => 'Administrator'] */
  public function DeleteQuestion($pollId) {
    $q = new DatabaseQuery();
    
    $q->addParameter('ai_pollid', $pollId);
    
    $q->sql = "DELETE FROM FilteredOut WHERE PollId = ?ai_pollid";
    $q->executeNonQuery();    
    
    $q->sql = "DELETE pr.* FROM PollResponse pr JOIN PollChoice pc ON pr.Response = pc.PollChoiceId WHERE pc.PollId = ?ai_pollid";
    $q->executeNonQuery(); 
    
    $q->sql = "DELETE FROM PollChoice WHERE PollId = ?ai_pollid";
    $q->executeNonQuery();     
    
    $q->sql = "DELETE FROM Poll WHERE PollId = ?ai_pollid";
    $q->executeNonQuery();       
    
    $q->executeNonQuery();
   
  }

  /** ['Access' => 'Administrator'] */
  public function GetAnswerList() {
    $q = new DatabaseQuery();
    $p = new Data('Poll', $_GET['param']);
    $at = new Data('AnswerType', $p->AnswerTypeCode);
    $results = '';
    
    $results .= '<h5>Answer Type: ' . $at->Name . ' <button type="button" class="ml-1 btn btn-info btn-sm" id="at_description"  data-trigger="focus" data-container="body" data-placement="top" data-toggle="popover" data-content="' . htmlentities($at->Description) . '"><i class="fa fa-info-circle"></i></span></h5>';
    $results .= '<input type="hidden" id="answer_type_code" value="' . htmlentities($p->AnswerTypeCode) . '" />';
    
    switch($p->AnswerTypeCode) {
      case 'RA':
        $results .= 'These options will be displayed as a horizontal rating strip of some kind (e.g. stars)<br />';
      
      case 'MC':
        $q->sql = "SELECT pc.PollChoiceId, 
            pc.Choice, 
            pc.Icon, 
            pc.SelectionCount,
            pc.ChartColour,
            pc.GradientLight,
            pc.GradientDark,
            m.Url,
            m.ThumbnailUrl,
            m.MicrothumbUrl           
          FROM PollChoice pc
            LEFT JOIN Media m ON m.MediaId = pc.MediaId 
          WHERE pc.PollId = ?ai_pollid 
          ORDER BY pc.DisplayOrder";
    
        $q->addParameter('ai_pollid', $_GET['param']);
    
        $xml = $q->executeXml('Choices', 'Choice');
    
        $results .= WebPage::Transform($xml, 'PollChoices.xslt', '<p><em>This question has no options yet</em></p>') . '<hr /><div class="text-center"><button type="button" class="btn btn-success" data-digifi-action="Add"><i class="fa fa-plus"></i> Add Choice</button></div>';
      break; 
      
      case 'FO':
        $results .= '<div class="alert alert-info">This question only accepts a ProfileId of a person followed by the current user as an answer.</div>';
      break;
    }      
       
    
    return $results;
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetChoice($pollChoiceId) {
    $pc = new Data('PollChoice', $pollChoiceId);
     
    return $pc->toJSON();
   
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetFilteredOutExpiry($dummy) {
    $q = new DatabaseQuery();
     
    $q->sql = "SELECT FilterExpiryValue, FilterExpiryUnit FROM Poll WHERE PollId = ?ai_pollid";
    $q->addParameter('ai_pollid', $_GET['param']);
    
    return json_encode($q->executeObjects()[0]);   
  }
  
  /** ['Access' => 'Everyone'] */
  public function GetRecentImages() {
    $pf = new PollFunctions();
    
    $results = array('Succes' => true);
    $results['Images'] = $pf->InternalimageList(array('TargetTypeCode' => 'PO', 'Count' => 5));
		
		return json_encode($results);
  }
  
  /** ['Access' => 'Everyone'] */
	public function GetUploadToken() {
    return getOneTimeToken(array('TargetTypeCode' => 'PO', 'TargetId' => $_GET['param'], 'ExposureTypeCode' => 'PU'), WebPage::$currentUser->ProfileId);
  }
  
  /** ['Access' => 'Everyone'] */
  public function FilterSearch($filterId, $paramName, $searchText) {
    $q = new DatabaseQuery();
    
    $fp = new Data('FilterParameter');
    
    $fp->Load(array('FilterId' => $filterId, 'Name' => $paramName));
    
    $q->sql = $fp->SearchQuery;
    
    $q->addParameter('text', $searchText);
    
    $results = $q->executeObjects();
  
    return json_encode($results);
  }
  
  private function FiltersToSql($filters, $prefix = ' AND ') { 
    if($filters == null || count($filters) == 0) {
      return ''; 
    }
    
    $q = new DatabaseQuery();
    
    $q->sql = "SELECT fp.FilterId, fp.Name, ft.ShortName
FROM FilterParameter fp
JOIN FilterType ft ON ft.FilterTypeId = fp.FilterTypeId";

    $obj = $q->executeObjects();
    
    $parameters = [];
    
    foreach($obj as $obj) {
      $parameters[$obj->FilterId . '_' . $obj->Name] = $obj->ShortName; 
    }
    
    $sql = '';    
    
    foreach($filters as $filter) {
      if($sql != '') $sql .= $prefix;
    
      switch($filter->Op) {
        case 'And':        
        case 'Or':          
        case 'None':
        
          if($filter->Op == 'None') {
            $sql .= ' NOT ';
          }
          
          $sql .= '(';
          
          $first = true;
          
          $sql .= $this->FiltersToSql($filter->Filters, ($filter->Op == 'Or' ? ' OR ' : ' AND '));
                   
          /*foreach($filters as $filter) {
            if(!$first) $sql .= ($filter->Op == 'Or' ? ' OR ' : ' AND ');
            
            $sql .= $this->FiltersToSql($filter);
            
            $first = false;
          }   */
        
          $sql .= ')';
        break;
        
        default:
          $f = new Data('Filter', $filter->FilterId);
          
          preg_match_all("|\\[\\[([^\\]]+)\\]\\]|U", $f->SQL, $matches, PREG_OFFSET_CAPTURE);
        
          $sql .= '('; // . $f->SQL . ")\n";
          
          
          $lastIndex = 0;
              
          if($matches && count($matches) > 0) {
            for($m = 0; $m < count($matches[0]); $m++) {                  
              if($lastIndex < $matches[0][$m][1]) {                                      
                $sql .= substr($f->SQL, $lastIndex, $matches[0][$m][1] - $lastIndex);                                
                
                
                if($filter->Values && property_exists($filter->Values, $matches[1][$m][0]) && $filter->Values->{$matches[1][$m][0]}) {
                  if($parameters[$filter->FilterId . '_' . $matches[1][$m][0]] == 'GEOFENCE') {
                    $geom = json_decode(html_entity_decode($filter->Values->{$matches[1][$m][0]}), false, 512, JSON_BIGINT_AS_STRING);
                    
                    //$sql .= print_r($geom, true);
                    $sql .= "GeomFromText('Polygon(($geom->west $geom->north, $geom->east $geom->north, $geom->east $geom->south, $geom->west $geom->south, $geom->west $geom->north))')";                                       
                                                              
                  } else if(is_numeric($filter->Values->{$matches[1][$m][0]})) {
                    $sql .= $q->escape($filter->Values->{$matches[1][$m][0]}); 
                  } else {
                    $sql .= "'" . $q->escape($filter->Values->{$matches[1][$m][0]}) . "'";
                    //$sql .= print_r($filter, true);
                    //$sql .= $parameters[$filter->FilterId . '_' . $matches[1][$m][0]]; 
                  }                                           
                } else {                       
                  $sql .= ' NULL ';
                }
                
                //$sql .= '?' . $matches[1][$m][0];
                  
                $lastIndex = $matches[0][$m][1] + strlen($matches[0][$m][0]);
              } 
            }
            
            if($lastIndex < strlen($f->SQL)) {
              $sql .= substr($f->SQL, $lastIndex); 
            }
              
          }
            
          $sql .= ")\n";
        break;
      }
    }
    
    return $sql;
  
  }
  
  private function FiltersToHtml($filters, $topLevel = false) {
    $html = '';
    
    $q = new DatabaseQuery();
  
    if($filters == null || count($filters) == 0) {
      if($topLevel) { 
        $html = '<div class="alert alert-info poll-no-filters">No filters are enabled for this question; all users will be asked it</div>';
      } else {
        $html = '<div class="alert alert-warning poll-no-filters">There are no filters defined here</div>'; 	
      }       
    } else {      
    
      try {
        foreach($filters as $filter) {
          $containerName = '';
        
          switch($filter->Op) {
            case 'And':
              //if(!$containerName) $containerName = '<button type="button" class="btn btn-secondary" title="Click to change condition" data-digifi-action="ChangeCondition">All</button> of the Following:';
              $containerName = 'All';
            case 'Or':
              //if(!$containerName) $containerName = '<button type="button" class="btn btn-secondary" title="Click to change condition" data-digifi-action="ChangeCondition">Any</button> of the Following:';
              if(!$containerName) $containerName = 'Any';
            case 'None':
              //if(!$containerName) $containerName = '<button type="button" class="btn btn-secondary" title="Click to change condition" data-digifi-action="ChangeCondition">None</button> of the Following:';
              if(!$containerName) $containerName = 'None';
            
              $html .= '<div class="poll-filter-op" data-op="' . htmlentities($filter->Op) . '"><div class="pull-right"><button type="button" class="btn btn-sm btn-danger" title="Remove this Filter" data-digifi-action="RemoveFilter"><i class="fa fa-trash"></i></button></div><h4><button type="button" class="btn btn-outline-success" title="Click to change condition" data-digifi-action="ChangeCondition">' 
                . $containerName . '</button> of the Following:</h4><div class="poll-filter-container">';          
              
              $html .= $this->FiltersToHtml($filter->Filters);
              
              $html .= '<p><button type="button" class="btn btn-sm btn-success poll-add-filter-btn" data-digifi-action="AddFilter"><i class="fa fa-fw fa-plus"></i> Add Filter</button></p>';
              
              $html .= '</div></div>';
            break; 
            
            default:
              $pf = new Data('Filter', $filter->FilterId);
              
              $html .= '<p class="poll-filter-op" data-op="Filter" data-id="' . $pf->FilterId . '"><span class="pull-right"><button type="button" class="btn btn-sm btn-danger" title="Remove this Filter" data-digifi-action="RemoveFilter"><i class="fa fa-trash"></i></button></span>';
              
              //$pattern = '/\\[\\[([^\\]]+)\\]\\]/U';
              
              //preg_match_all($pattern, $pf->FilterDisplay, $matches, PREG_OFFSET_CAPTURE);          
              
              preg_match_all("|\\[\\[([^\\]]+)\\]\\]|U", $pf->FilterDisplay, $matches, PREG_OFFSET_CAPTURE);
              
              //preg_match_all($pattern, $pf->FilterDisplay, $matches, PREG_OFFSET_CAPTURE);
              
              $lastIndex = 0;
              
              $value = "";
              
              if($matches && count($matches) > 0) {
                for($m = 0; $m < count($matches[0]); $m++) {                  
                  if($lastIndex < $matches[0][$m][1]) {                                      
                    $html .= substr($pf->FilterDisplay, $lastIndex, $matches[0][$m][1] - $lastIndex);
                    
                    $buttonText = '';
                    $buttonOutline = 'btn-outline-danger';
                    
                    $q->sql = "SELECT ft.HtmlType FROM FilterType ft JOIN FilterParameter fp ON fp.FilterTypeId = ft.FilterTypeId WHERE fp.Name = ?as_name AND fp.FilterId = ?ai_filterid";
                    $q->addParameter('as_name', $matches[1][$m][0]);
                    $q->addParameter('ai_filterid', $filter->FilterId);
                    
                    $htmlType = $q->executeScalar();
                                        
                    
                    if($filter->Values && property_exists($filter->Values, $matches[1][$m][0]) && $filter->Values->{$matches[1][$m][0]}) {                                          
                      $buttonText = htmlentities($filter->Values->{$matches[1][$m][0]});                                        
                      
                      if($htmlType == 'geofence') {
                        $buttonText = html_entity_decode($filter->Values->{$matches[1][$m][0]});
                        $json_test = json_decode($buttonText);
                      
                        //if(preg_match('/\d+/i', $filter->Values->{$matches[1][$m][0]}) != 0) {
                        if($json_test != null) {
                          $buttonOutline = 'btn-outline-success';
                          $value = $buttonText;
                          $buttonText = sprintf("(%0.2f, %0.2f) - (%0.2f, %0.2f)", $json_test->north, $json_test->west, $json_test->south, $json_test->east);
                          //$buttonText = '<pre>' . json_encode($json_test, JSON_PRETTY_PRINT) . '</pre>'; 
                        } else {                        
                          $buttonText = 'Bounds';
                          $value = '';
                          $buttonOutline = 'btn-outline-danger'; 
                        }                        
                      } else if($htmlType == 'number' && is_numeric($filter->Values->{$matches[1][$m][0]})) {
                        $buttonOutline = 'btn-outline-success'; 
                      } else if($htmlType != 'number') {
                        $buttonOutline = 'btn-outline-success';
                      }                                           
                    } else {                       
                      $buttonText = $matches[1][$m][0];
                      $value = $buttonText;
                    }                                       
                    
                    if($htmlType == 'search') {
                      if(property_exists($filter->Values, $matches[1][$m][0])) { 
                      
                        $value = $filter->Values->{$matches[1][$m][0]};
                      
                        $fp = new Data('FilterParameter');
                      
                        $fp->Load(array('FilterId' => $filter->FilterId, 'Name' => $matches[1][$m][0]));
                      
                        $q->sql = $fp->DisplayQuery;
                        $q->addParameter('val', $value);
                        
                        $buttonText = $q->executeScalar();
                      } else { 
                        $buttonText = $matches[1][$m][0];
                      }
                      
                      //$buttonText = $filter->Values->{$matches[1][$m][0]}; 
                    }
                    
                    
                    $html .= '<button type="button" class="btn ' . $buttonOutline . ' btn-sm poll-filter-value" data-value-type="' . $htmlType . '" data-filter-value="' . htmlentities($value) . '" data-filter-key="' . $matches[1][$m][0] .  '" data-digifi-action="EditFilterValue">' . $buttonText . '</button>';
                      
                    $lastIndex = $matches[0][$m][1] + strlen($matches[0][$m][0]);
                  } 
                }
               
                /*foreach($matches[1] as $match) {
                  if($lastIndex < $match[1]) {                    
                    $html .= substr($pf->FilterDisplay, $lastIndex, $match[1] - $lastIndex);  
                    $lastIndex = $match[1] + strlen($match[0]);
                  }
                }  */                             
              }
              
              if($lastIndex < strlen($pf->FilterDisplay)) {
                $html .= substr($pf->FilterDisplay, $lastIndex); 
              }
              
            
              //$html .= '<p>' . htmlentities($pf->FilterDisplay) . '<br />';
              
              $html .= '</p>';
            break;
          } 
        }
      } catch(Exception $ex) {
        $html .= '<p>' . $ex->getMessage() . "<br />trace:<br />" . $ex->getTraceAsString() . '</p><pre><code>' . print_r($filters, true) . '</code></pre>'; 
      }                  
    }
    
    if($topLevel && ($filters == null || count($filters) == 0)) { 
      $html .= '<p class="text-center"><button type="button" class="btn btn-sm btn-success poll-add-filter-btn" data-digifi-action="EnableFilters"><i class="fa fa-fw fa-filter"></i> Enable Filtering</button></p>';
    } else if(!$topLevel) {      
    }
    
    return $html; 
  }
  
  /** ['Access' => 'Developer'] */
  public function GetFilterSql($filters) { 
    return '<pre><code>' . $this->FiltersToSql($filters) . '</code></pre>';
  }
  
  /** ['Access' => 'Administrator'] */
  public function UpdateFilters($filters) {
    $q = new DatabaseQuery();
    
    $q->sql = "UPDATE Poll SET FilterJSON = ?as_json, FilterSQL = ?as_sql, Modified = CURRENT_TIMESTAMP WHERE PollId = ?ai_pollid";
         
    $q->addParameter('as_json', json_encode($filters));
    $q->addParameter('as_sql', $this->FiltersToSql($filters) );
    $q->addParameter('ai_pollid', $_GET['param']);
    
    $q->executeNonQuery();
  
    return '<div class="poll-filter-container poll-filter-container-top">' . $this->FiltersToHtml($filters, true) . '</div>';  
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetFilters() { 
    $q = new DatabaseQuery();
     
    $q->sql = "SELECT IFNULL(FilterJSON, 'null') FROM Poll WHERE PollId = ?ai_pollid";
    $q->addParameter('ai_pollid', $_GET['param']);
    
    $filters = json_decode($q->executeScalar());
    
    return '<div class="poll-filter-container poll-filter-container-top">' . $this->FiltersToHtml($filters) . '</div>';
  }
                                      
  /** ['Access' => 'Administrator'] */
  public function GetQuestionInfo() {
    $poll = new Data('Poll', $_GET['param']);
      
    $results = '<span class="pull-right"><button type="button" class="btn btn-sm btn-primary" data-digifi-action="Edit" data-digifi-id="' . $_GET['param'] . '"><i class="fa fa-pencil"></i> Edit Question</button></span>' . "<h3>" . htmlentities($poll->Question) . "</h3>";
    
    if($poll->EventId) {  
      $event = new Data('Event', $poll->EventId);
      
      $results .= '<p class="small">This questions is available only to attendees of <a href="/adminevents/' . $event->EventId . '">' . htmlentities($event->Name) . '</a></p>'; 
    }
    
    return $results; 
  }
  
  /** ['Access' => 'Administrator'] */
  public function GetQuestion($pollId) {
    $p = new Data('Poll', $pollId);
     
    return $p->toJSON();
   
  }

  /** ['Access' => 'Administrator'] */
  public function GetQuestions($filterCategoryId, $filterText, $showInactive, $sortColumn, $sortDesc) {
    $q = new DatabaseQuery();
     
    $q->sql = "SELECT PollId `@Id`, Question `@Question`, 
      pc.Name `@Category`,
      pc.IconClass `@IconClass`,
      ResponseCount `@ResponseCount`, 
      UrlSegment `@UrlSegment`, 
      IsAvailable `@IsAvailable`, 
      IsRequired `@IsRequired`,
      StartDate `@StartDate`, 
      EventId `@EventId`,
      EndDate `@EndDate`,
      CASE WHEN RTRIM(IFNULL(FilterSQL, '')) = '' THEN 0 ELSE 1 END `@Filtered`
    FROM Poll p
      JOIN PollCategory pc ON pc.PollCategoryId = p.PollCategoryId
    WHERE (LOWER(?as_filtertext) = '' OR p.Question LIKE CONCAT('%', LOWER(?as_filtertext), '%'))
    AND (?ab_showinactive = 1 OR p.IsAvailable = 1)
    AND (?ai_filtercategoryid = 0 OR p.PollCategoryId = ?ai_filtercategoryid)
    ORDER BY " . $sortColumn . ($sortDesc ? ' DESC' : '');
    
    $q->addParameter('ai_filtercategoryid', $filterCategoryId == '' ? 0 : $filterCategoryId);
    $q->addParameter('as_filtertext', $filterText);
    $q->addParameter('ab_showinactive', $showInactive);
    
    $xml = $q->executeXml('Questions', 'Question');       
  
    return WebPage::Transform($xml, 'QuestionList.xslt', '<div class="alert alert-info" role="alert"><p>No questions have been defined in the system</p></div>');  
  } 
  
  /** ['Access' => 'Everyone'] */
  public function PostChoiceImage($d) {
    $m = new Data('Media');
    
    $pc = new Data('PollChoice', $d->PollChoiceId);
    
    if($d->ExistingMediaId && is_numeric($d->ExistingMediaId)) {
      $m = new Data('Media', $d->ExistingMediaId);
    } else {
      $m->merge($d);
      
      $m->IsPublished = 1;
      
      $m->update($this->CurrentUser->ProfileId);      	
    }
             
    $pc->MediaId = $m->MediaId;
    
    $pc->update($this->CurrentUser->ProfileId);   
    
  }
  
  /** ['Access' => 'Administrator'] */
  public function SaveChoice($d) {
    $q = new DatabaseQuery();
    $pc = new Data('PollChoice');
    
    $pc->load($d);
    
    if($pc->IsNewRow()) {
      $q->sql = "SELECT IFNULL(MAX(DisplayOrder) + 1, 1) FROM PollChoice WHERE PollId = ?ai_pollid";
      $q->addParameter('ai_pollid', $_GET['param']);
    
      $pc->PollId = $_GET['param'];
      
      $pc->DisplayOrder = $q->executeScalar(); 
    }
    
    $pc->update($this->CurrentUser->ProfileId);  
  }
  
  /** ['Access' => 'Administrator'] */
  public function SaveQuestion($d) {
    $p = new Data('Poll');
    
    $p->load($d);
    
    if($p->EventId == '') {
      $p->EventId = null;
    }
    
    $p->update($this->CurrentUser->ProfileId);
    
    return $p->PollId;
  }
  
  /** ['Access' => 'Administrator'] */
  public function SetFilteredOutExpiry($d) {
    $q = new DatabaseQuery();
    
    $val = $d->FilterExpiryValue;
    $unit = $d->FilterExpiryUnit;
    
    if($val == '' || $unit == '' || $val == null || $unit == null) {
      $val = null;
      $unit = null; 
    }
          
    $q->sql = "UPDATE Poll SET FilterExpiryValue = ?ai_value, FilterExpiryUnit = ?as_unit, Modified = CURRENT_TIMESTAMP, Modifier = ?ai_profileid WHERE PollId = ?ai_pollid";
    $q->addParameter('ai_value', $val);
    $q->addParameter('as_unit', $unit);
    $q->addParameter('ai_profileid', $this->CurrentUser->ProfileId);
    $q->addParameter('ai_pollid', $_GET['param']);
    
    $q->executeNonQuery();
    
    if($val == null) {
      return "System Default"; 
    } else {
      return $val . ' ' . $unit . ($val == 1 ? '' : 'S');
    }   
  }
  
  /** ['Access' => 'Administrator'] */
  public function ToggleAttribute($attribute) {
    $p = new Data('Poll', $_GET['param']);
    
    $p->{$attribute} = !$p->{$attribute};
            
    $p->update($this->CurrentUser->ProfileId);
    
    return $p->{$attribute};
  }
}

?>