(function ($)
{
    var bootstrap3_enabled = (typeof $().emulateTransitionEnd == 'function');
    
    $.fn.formRequired = function(value) {
      if(typeof value == 'undefined')
      {
        var required = false;
        
        $(this).each(function() { 
          required |= $(this).hasClass('required');
        });
        
        return required;
      }
      
      return $(this).each(function() {
        if(value)
        {
          $(this).addClass('required').parents('tr:first').find('th').addClass('required');
        }
        else
        {
          $(this).removeClass('required').parents('tr:first').find('th').removeClass('required');           
        }
      });    
    };
    
    $.fn.formShow = function(value) { 
      if(typeof value == 'undefined')
      {
        var visible = false;
        $(this).each(function() { 
          visible |= $(this).is(":visible");
         });
         
        return visible;
      }
      
      return $(this).each(function() {
        if(value)
        {        
          $(this).parents('tr:first').show();
        }
        else
        {
          $(this).parents('tr:first').hide();           
        }
      });            
    };
    
    $.fn.newObject = function (type)
      {
          return $(this).each(function ()
          {
              var popup = this;
      
              Master.NewObject(type, /*function (json)
              {
                  var p = jQuery.parseJSON(json);
                  $(popup).dataBind(p);
                  if(popup.show) popup.show();
              }*/objectLoaded, alertResult, popup);
          });
      };

    $.fn.initializeDigifiUI = function ()
    {
      /*******************************************
        AUTO-LAYOUT
      *******************************************/
      $(this).find('.auto-layout').each(function() {
        if(bootstrap3_enabled) {
          var container = $('<div />');
          $(this).find('>*').each(function() {
            var ctrl = this;
            
            if(!$(ctrl).is(':input')) {   
              container.append(ctrl);
            }
            else if($(this).attr('type') == 'checkbox') {
              var cb = $('<div class="checkbox" />');
              var label = $('<label />');
              
              cb.append(label);
              
              label.append(ctrl);
              
              if($(ctrl).attr('title') != '') label.append($(ctrl).attr('title'));
              
              container.append(cb);
                
            }
            else if($(this).attr('type') != 'button' && $(this).attr('type') != 'submit') { 
              
              fg = $('<div class="form-group" />');
              if($(ctrl).attr('title') != '')
              {
                label = $('<label />').attr('for', ctrl.id).html($(ctrl).attr('title'));
                fg.append(label);
              }
              
              $(ctrl).addClass('form-control');
              
              fg.append(ctrl);
              
              if($(ctrl).data('help') != '')
              {
                helptext = $('<p class="help-block" />').html($(ctrl).data('help'));
                fg.append(helptext);
              }
             
              container.append(fg); 
            }           
            else
            {
              container.append(ctrl);
            }
          });
          
          $(this).attr('role', 'form').html(container.html());
        } 
        else {
          var container = $('<div class="panel-content" />');
          var table = $('<table class="digifi-form" />');
          $(this).find('>*').each(function() {
            var ctrl = this;
            var tr = $('<tr />');
            
            if($(this).is('h2'))
            {
              var td = $('<td colspan="2" class="digifi-group-header" />').append($(ctrl).detach());
              $(tr).append(th).append(td);
              $(table).append(tr);    
            }
            else
            {        
              var th = $('<th />');
              var td = $('<td />').append($(ctrl).detach());
              
              if($(this).attr('type') == 'checkbox')
              {
                var label = $('<label />').html($(this).attr('title') || '');
                $(label).attr('for', ctrl.id);
                $(td).append(label);
              }
              else if($(this).attr('title') && $(this).attr('title') != ''
                  && $(this).attr('type') != 'button')
              {
                $(th).html($(this).attr('title') + ':');
              }
              
              if($(ctrl).hasClass('required')) $(th).addClass('required');
              
              $(tr).append(th).append(td);
              $(table).append(tr);          
              
              if($(ctrl).attr('data-help') && $(ctrl).attr('data-help') != '')
              {
                tr = $('<tr id="' + ctrl.id + '_help" />').append('<th />');
                $(tr).append($('<td class="help-text" />').html($(ctrl).attr('data-help')));
                $(table).append(tr);                        
              }
            }
          });
          $(container).prepend(table); 
          $(this).html(container);
        }
      });
    
      /*******************************************
        TABS
      *******************************************/
      $(this).find('.digifi-tabs').each(function() { 
      });
      
      /*******************************************
        PANEL
      *******************************************/
      $(this).find('.digifi-panel').each(function() { 
        var title = $(this).attr('title') || '';
        var header = null;
        var content = null;
        
        if(bootstrap3_enabled)
        {
          $(this).addClass('panel').addClass('panel-default');
          header = $('<h3 class="panel-title" />');
          content = $('<div class="panel-body" />');
        }
        else
        {
          header = $('<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-default ui-corner-top ui-accordion-icons" />');
          content = $('<div class="digifi-panel-content ui-widget-content ui-helper-reset ui-corner-bottom" />');
          $(this).addClass('ui-widget').addClass('ui-helper-reset').addClass('ui-accordian');
        }
                               
        $(header).html(title);
        
        $(this).attr('title', '');
        
        if($(this).find('.data').length == 0 && $(this).attr('data-source') && $(this).attr('data-source') != '')
        {
          $(content).append($('<div class="data" />'));                     
        }               
        
        $(this).wrapInner(content);
            
        if($(this).hasClass('refresh'))
        {
          var refresh = null;                   
          
          if(bootstrap3_enabled)
          {
            refresh = $('<button type="button" class="action btn btn-default btn-xs" data-action="Refresh"><span class="glyphicon glyphicon-refresh"><img class="hidden refresh-throbber" src="http://www.digifi.ca/common/Images/refreshing.gif" width="16" height="16" /></span></button>');
          }
          else
          {
            refresh = $('<span class="action" title="Refresh" data-action="Refresh"><span class="digifi-icon ui-icon ui-icon-refresh"><img class="hidden refresh-throbber" src="http://www.digifi.ca/common/Images/refreshing.gif" width="16" height="16" /></span></span>');
          }
          
          $(this).bind('content-loaded.digifi', function() { 
            if(bootstrap3_enabled)
            {
              $(refresh).find('span').addClass('glyphicon glyphicon-refresh').find('img').hide();
            }
            else
            {        
              $(refresh).find('span').addClass('ui-icon ui-icon-refresh').find('img').hide();
            }
          });
          
          $(this).bind('action.digifi', function(e, a) {
            if(a.action == 'Refresh')
            {           
              e.stopImmediatePropagation();
              $(this).trigger('refresh.digifi');   
              if(bootstrap3_enabled)
              {
                $(refresh).find('span').removeClass('glyphicon glyphicon-refresh').find('img').show();
              }
              else
              {
                $(refresh).find('span').removeClass('ui-icon ui-icon-refresh').find('img').show();//.css('background-image', 'http://www.digifi.ca/common/Images/refreshing.gif');
              }
            } 
          });                      
          
          $(header).append(refresh);
        }
        
        if($(this).hasClass('add'))
        {
          
          var add = null;
          
          if(bootstrap3_enabled)
          {
            add = $('<button type="button" class="action btn btn-default btn-xs" data-action="Add"><span class="glyphicon glyphicon-plus"></span></button>');
          }
          else
          {
            add = $('<span class="action" title="Add Item" data-action="Add"><span class="digifi-icon ui-icon ui-icon-circle-plus"></span></span>');
          }
          
          $(header).append(add);
        }      
        
        if(bootstrap3_enabled)
        {
          bsheader = $('<div class="panel-heading" />');
          bsheader.append(header);
          
          header = bsheader;
        }  
        
        
        $(this).prepend(header);               
      });
      
      /*******************************************
        POPUPS
      *******************************************/
      $(this).find('.digifi-popup').each(function() { 
        var dlg = this;
        var saveButton = "Save";
        var cancelButton = "Cancel";
        var b = {};
                
        $(dlg).data('edit-title', $(dlg).attr('title'));
        
        switch($(this).data('button-style'))
        {
          case "yes-no":
            saveButton = "Yes";
            cancelButton = "No";
          break;
          
          case "done":
            saveButton = '';
            cancelButton = 'Done';
          break;
        }

        if ($(this).attr('data-save-button')) saveButton = $(this).attr('data-save-button');
        if ($(this).attr('data-cancel-button')) cancelButton = $(this).attr('data-cancel-button');               

        if (saveButton != '') {
            b[saveButton] = function () {
                $(this).parent('.ui-dialog:first').find('.ui-dialog-buttonpane button:first').addClass('ui-state-disabled').prop('disabled', true);

                var errors = [];
                var event = jQuery.Event("validate.digifi");

                /*if ($(this).data('object'))*/ $(this).dataValidate(errors);

                event.errors = errors;

                $(this).trigger(event);

                if (errors.length > 0) {
                  var message = '';
                  for(var x = 0; x < errors.length; x++)
                  {                     
                    message += errors[x].message + ' ';
                  }
                  this.saveError(message);
                  return;
                }

                if ($(dlg).attr('data-update') && $(dlg).attr('data-update') != '') {
                    var fn = $(dlg).attr('data-update');

                    if (!fn.match(/\(/ig)) {
                        fn += '(';
                    }

                    fn += '$("#' + dlg.id + '").data("object"),$("#' + dlg.id + '")[0].saveComplete, $("#' + dlg.id + '")[0].saveError,"' + dlg.id + '");';
                    eval(fn);
                }
                else {
                    $(this).triggerHandler('save.digifi');
                    
                    if($(dlg).hasClass('file-upload')) {
                      var uploadForm = '#' + dlg.id + '_form_upload';

                      if ($(uploadForm).length == 0)
                      {
                          $(dlg).parent('.ui-dialog').wrap('<form id="' + uploadForm.substring(1) + '" method="post" target="_digifi_async_upload" enctype="multipart/form-data" />');
  
                          $(uploadForm).append('<input type="hidden" name="__DIGIFI_UPLOADER" value="' + dlg.id + '" />');
                      }
  
                      //Submit the form:
                      $(uploadForm).attr('action', window.location).submit();
                    }
                }
            };
        }

        b[cancelButton] = function () {
            $(this).trigger('cancel.digifi');
            $(this).dialog("close");
        };

        $(dlg).dialog({ autoOpen: false, modal: true, resizable: false,
            buttons: b
        });

        $(this).dialog("option", "width", $(this).css('width'));

        $(this).enterKey(function (e) {
            $(dlg).parent('.ui-dialog:first').find('.ui-dialog-buttonpane button:first').click();
        });               

        this.saveComplete = function (result) {
            $(dlg).data('save-result', result);
            $(dlg).dialog("close");
            $(dlg).parent('.ui-dialog:first').find('.ui-dialog-buttonpane button').removeClass('ui-state-disabled').prop('disabled', false);
            $('*[data-editor="' + dlg.id + '"]').trigger('refresh.digifi');
            $(dlg).trigger('saved.digifi');
        }
        this.saveError = function (msg) { if (msg) alert(msg); $(dlg).parent('.ui-dialog:first').find('.ui-dialog-buttonpane button').removeClass('ui-state-disabled').prop('disabled', false); }
        $(this).dialog('option', 'title', $(this).attr('title'));
        this.show = function () { $(dlg).parent('.ui-dialog:first').find('.ui-dialog-buttonpane button:first').removeClass('ui-state-disabled').prop('disabled', false); $(this).dialog('open'); };
        this.hide = function () { $(dlg).dialog('close'); };
  
      });           
      
      /*******************************************
        SLIDESHOW
      *******************************************/
      $(this).find('.digifi-slideshow').each(function() {
        var count = $(this).find('ul > li').length;
        var navBar = $('<div class="digifi-slideshow-nav" />');
        var slideShow = this;
        var timeout = 10000;
        var intervalId;
        
        if($(this).data('timeout') && !isNaN($(this).data('timeout'))) timeout = parseInt($(this).data('timeout'));
        
        $(slideShow).css('height', $(this).find('ul > li').height() + 'px');
        $(this).find('ul > li').addClass('digifi-slideshow-panel');
            
        $(slideShow).data('activeIndex', 0);
        
        $(this).find('ul').addClass('no-list');
        $(this).find('ul > li:not(:first)').hide(); 
        
        $(navBar).css('margin-top', (parseInt($(this).height()) - 25) + 'px').click(function(e) { e.stopPropagation(); });
        
        var gotoSlide = function(index) {
          if(index < 0 || index >= count) index = 0;          
          $(navBar).find('img').attr('src', 'http://www.digifi.ca/common/Images/slideshow-t.png');
          $(navBar).find('img:nth-child(' + (index + 1) + ')').attr('src', 'http://www.digifi.ca/common/Images/slideshow-w.png');
          $($(slideShow).find('ul > li')[$(slideShow).data('activeIndex')]).fadeOut();
          $(slideShow).data('activeIndex', index);          
          $($(slideShow).find('ul > li')[$(slideShow).data('activeIndex')]).fadeIn();
        };
        
        for(var x = 0; x < count; x++)
        {
          var button = $('<img src="http://www.digifi.ca/common/Images/slideshow-' + (x == 0 ? 'w' : 't') + '.png" width="16" height="16" alt="Slideshow Button" />');
          $(button).data('index', x);
          
          $(navBar).append(button);
          
          $(button).click(function(e) {
            e.stopPropagation();
            
            clearInterval(intervalId);
            
            gotoSlide($(this).data('index'));
            
            intervalId = setInterval(function() { gotoSlide($(slideShow).data('activeIndex') + 1); }, timeout);
          });
        }      
        
        $(this).append(navBar);
        
        intervalId = setInterval(function() { gotoSlide($(slideShow).data('activeIndex') + 1); }, timeout);         
      });
          
      /*******************************************
        DATA-SOURCE
      *******************************************/
      $(this).find('*[data-source]').each(function() {
        var ds = this;
                      
        $(this).bind('refresh.digifi', function(e) {
           e.stopPropagation();
           
           if($(this).hasClass('digifi-panel')) {
              $(this).find('h3 span[data-action="Refresh"] span').removeClass('ui-icon ui-icon-refresh').find('img').show();
           }                      
        
           var fn = $(ds).data('source');
           var id = this.id;
           
           if(!fn.match(/\(/ig)) fn += '(';
           
           if($(this).is('select'))
           {
              $(this).loading();
              fn += ($(this).data('selected-value') || 'null') + ',';
           }
           else if($(this).hasClass('digifi-panel'))
           {
            id += ' .data';
            
            if($(this).find('.data').html() == '')
            {              
              $(this).find('.data').loading();
            }
           }
           
           if($(ds).attr('data-page') && $(ds).attr('data-page') != '')
           {
            fn += $(ds).attr('data-page') + ", ";
           } 
           
           if($(ds).attr('data-sortcolumn') && $(ds).attr('data-sortcolumn') != '')
           {
            fn += "'" + $(ds).attr('data-sortcolumn') + "', " + ($(ds).attr('data-sortdesc') || 'false') + ', ';          
           } 
           
           fn += 'setContent, setContent, "#' + id + '");';                     
           
           eval(fn);
        });
      });
      
      /*******************************************
        DEFAULT ACTION HANDLERS
      *******************************************/
      $(this).find('*[data-editor]').bind('action.digifi', function(e, a) {
        var editor = $('#' + $(this).attr('data-editor'));
        var container = this;
              
        switch(a.action)
        {
          case 'Add':
            e.stopPropagation();
            
            var dt = $(container).attr('data-type');
            
            if(!dt || dt.trim() == '')
            {
              alert('data-type not set for addable container with a data-editor attribute.');
              return;
            }
            
            if(editor.attr('data-add-title') && editor.attr('data-add-title') != '')
            {
              editor.dialog('option', 'title', editor.attr('data-add-title'));
            }
            
            editor.newObject(dt);
            //Master.NewObject(dt, objectLoaded, alertResult, editor.attr('id'));
            
            
            //editor[0].show();          
          break;
          
          case 'Delete':
            e.stopPropagation();
            
            var deletePrompt = 'Are you sure you want to delete this item?';
            
            if($(container).data('delete-prompt') && $(container).data('delete-prompt') != '')
            {
              deletePrompt = $(container).data('delete-prompt');              
            } 
            
            if(!confirm(deletePrompt)) return;
            
            if($(container).data('delete') && $(container).data('delete') != '')
            {
              var fn = $(container).data('delete');                
                         
              if(!fn.match(/\(/ig)) fn += '(';
             
              fn += a.id + ',function() { $("#' + container.id + '").trigger("refresh.digifi"); }, alertResult);';     
              
              eval(fn);
            } 
            
            $(this).trigger('delete.digifi');
          break;
          
          case 'Edit':
            e.stopPropagation();
                                    
            if(editor.hasClass('digifi-popup')) editor.dialog('option', 'title', editor.data('edit-title'));
            
            editor.loadData(a.id);
            
            /*var fn = $(editor).attr('data-source');
            
            if(!fn || fn == '')
            {
              alert('No data source specified for ' + $(this).attr('data-editor'));
              return;
            }
                       
            if(!fn.match(/\(/ig)) fn += '(';
           
            fn += a.id + ',objectLoaded, alertResult, "#' + $(this).attr('data-editor') + '");';                     
           
            eval(fn);   */                  
          break;
          
          case 'Refresh':
            e.stopPropagation();
            $(this).trigger('refresh.digifi');
          break;
        } 
      });
      
      /*******************************************
        TITLE MENUS
      *******************************************/
      $('.digifi-title-menu > ul > li ul').each(function() {
        var menu = this;
        
        var mouseOver = function(e) {          
        
          //$(menu).append(   $(menu).parents('ul').length);
          if($(menu).parents('ul').length == 1)
          {            
            $(menu).css('left',  $(this).offset().left + 'px').css('top', ($(this).offset().top + $(this).height() - 1) + 'px');
          }
          else
          {
            $(menu).html($(menu).parent().offset().top);
            $(menu).css('left', ($(this).width() + 4) + 'px').css('top', '20px');
            //$(menu).offset({left: $(menu).parent().offset().left + $(menu).parent().width(), top: $(menu).parent().offset().top });
          }
        
          $(menu).show(); 
        };
        
        var mouseOut = function(e) {
          $(menu).hide(); 
        };
        
        $(this).parent().hover(mouseOver, mouseOut); 
      });
      
      $('.digifi-title-menu li').hover(function() { $(this).addClass('hover'); } , function() { $(this).removeClass('hover'); });
      
      /*******************************************
        DATE, DATE-TIME
      *******************************************/
      $(this).find('input.date').each(function() {
        var dp = this;
        $(this).datepicker({
          dateFormat: 'yy-mm-dd',
          minDate: $(dp).attr('data-min-value'),
          maxDate: $(dp).attr('data-max-value'),     
          showOn: "button",          
          buttonImage: "http://www.digifi.ca/common/Images/calendar.png",        
          buttonImageOnly: true          
        });

       
      });
      
      /*******************************************
        Paging & Sorting
      *******************************************/
      $(this).find('.digifi-page-number').click(function() {
        var ds = $(this).parents('*[data-page]')[0];
        
        $(ds).find('.digifi-page-number').removeClass('current-page');
        $(this).addClass('current-page');
        
        $(ds).attr('data-page', $(this).text());
        $(ds).trigger('refresh.digifi');
      });
      
      $(this).find('th[data-sortcolumn]').each(function() {
        var ds = $(this).parents('*[data-page]')[0];
      
        $(this).addClass('digifi-sortable').addClass('hoverable');
        
        if($(this).attr('data-sortcolumn') == $(ds).attr('data-sortcolumn'))
        {
          var i = $('<img class="sort-arrow" alt="Sort Direction" width="16" height="16" />');
          
          i.attr('src', '//www.digifi.ca/common/Images/sort' + (eval($(ds).attr('data-sortdesc')) ? 'Up' : 'Down') + '.png');
          
          $(this).append(i);
          //alert($(this).data('sortcolumn'));
        } 
        
        $(this).click(function() {
          if($(this).attr('data-sortcolumn') == $(ds).attr('data-sortcolumn'))
          {
            $(ds).attr('data-sortdesc', !eval($(ds).attr('data-sortdesc')));
          }
          else
          {
            $(ds).attr('data-sortcolumn', $(this).data('sortcolumn'));
            $(ds).attr('data-sortdesc', 'false');
          }
          
          $(ds).trigger('refresh.digifi');
        });
      });
      
      /*******************************************
        ACTIONS
      *******************************************/
      $(this).find('*[data-action]').each(function() {
        var o = this;
                
        $(o).addClass('ui-corner-all ui-state-default data-action'); //.parent().addClass('ui-widget');         
        
        $(o).hover(function() { $(o).addClass('ui-state-active').removeClass('ui-state-default'); }, function() { $(o).removeClass('ui-state-active').addClass('ui-state-default'); });
        
        $(o).click(function() {
          var action = {action: $(this).attr('data-action'), id: $(this).attr('data-id'), element: o };        
          $(o).trigger('action.digifi', action);
        });
         
      });      
      
      $(this).find('menu > nav > ul').menu({ position: { my: "left top", at: "left bottom+5" }, 
              icons: { submenu: "ui-icon-circle-triangle-s" } });
    
      /*******************************************
        MISC.
      *******************************************/
      $(this).find('.accordian').accordion({ collapsible: true });

      $(this).find('.click-url').click(function() { window.location = $(this).attr('data-url'); });
      
      $(this).find('textarea').enterKey(function(e) { e.stopPropagation(); });
      
      $('.hoverable').hover(function() { $(this).addClass('hover'); } , function() { $(this).removeClass('hover'); });
      
      $(this).delegate('textarea.tabbing', 'keydown', function(e) {
        var keyCode = e.keyCode || e.which;
      
        if (keyCode == 9) {
          e.preventDefault();
          var start = $(this).get(0).selectionStart;
          var end = $(this).get(0).selectionEnd;
      
          // set textarea value to: text before caret + tab + text after caret
          $(this).val($(this).val().substring(0, start)
                      + "\t"
                      + $(this).val().substring(end));
      
          // put caret at right position again
          $(this).get(0).selectionStart = $(this).get(0).selectionEnd = start + 1;
        }
      });
      
      
      
      return this;
    };
    
    $(document).ready(function() { 
       $(document).initializeDigifiUI();
       
       $('*[data-source]:not(select):not(.digifi-popup):not(.no-initial-load)').trigger('refresh.digifi');
    });
})(jQuery);


/***************************************************
Asynchronous Support
***************************************************/
function __async(path, method, args, params, onSuccess, onFailure, context)
{
    var delim = path.match(/\?/ig) ? '&' : '?';

    if (args.length < params.length)
    {
        onFailure(method + ' requires ' + params.length + ' parameters');
        return;
    }

    for (var a = 0; a < params.length; a++)
    {
        var nullable = false;
        var type = params[a];

        if (type.match(/\?$/))
        {
            nullable = true;
            type = type.substr(0, type.length - 1);
        }        

        switch (type)
        {
            case 'DateTime':
                if (args[a] == null && !nullable) {
                    if (onFailure) onFailure('You must provide a date/time value to parameter #' + a + ' in method ' + method);
                    return;
                }

                if (args[a] == '' && nullable) {
                    args[a] = null;
                }

                break;

            case 'Double':
            case 'Single':
            case 'Int16':
            case 'Int64':
            case 'Int32':

                if (args[a] == null && !nullable)
                {
                    if (onFailure) onFailure('You must provide a numeric value to parameter #' + a + ' in method ' + method);
                    return;
                }

                if (args[a] != null && isNaN(args[a]))
                {
                    if (onFailure) onFailure('You must provide a numeric value to parameter #' + a + ' in method ' + method);
                    return;
                }

                break;

            case 'Boolean':
                if (args[a] == null && !nullable)
                {
                    if (onFailure) onFailure('You must provide a boolean value to parameter #' + a + ' in method ' + method);
                    return;
                }

                if (typeof (args[a]) != 'boolean')
                {
                    if (onFailure) onFailure('You must provide a boolean value to parameter #' + a + ' in method ' + method);
                    return;
                }

                break;
        }
    }
        
    $.ajax({ type: 'POST',
        url: path + delim + '__asyncmethod=' + method,
        dataType: 'json',
        data: JSON.stringify(args).replace(/\%/g, "%25").replace(/\=/g, "%3D").replace(/\&/g, "%26"),
        success: function (result, status, method) { 
        if (result.status == 1) { if(onSuccess) onSuccess(result.result, context, method); 
        } else {
        if (onFailure) onFailure(result.result, context, method); 
        } },
        error: function (request, status, errorThrown) {
        if(onFailure) onFailure(errorThrown + ': ' + request.responseText, context, status); 
        }
    }
        );
}

jQuery.fn.enterKey = function (op) {
    return $(this).keydown(function (e) { if (e.which == 13) { op(e); e.stopPropagation(); return !e.isDefaultPrevented(); } });
};

jQuery.fn.displayErrors = function(errors) {
  if(!errors || errors.length == 0) return $(this);
  
  var ul = $('<ul>');
  
  for(var x = 0; x < errors.length; x++)
  {
    var li = $('<li>').html(errors[x].message);
    ul.append(li);
  } 
  
  return $(this).html(ul);
};

jQuery.fn.dataValidate = function(errors) {
  var obj = $(this).data('object');
  
  if(!obj) 
  {
    obj = {};
    $(this).data('object', obj);
  }
  
  return $(this).find(':input[data-column]').each(function() {
    var val = $(this).val().trim();
    
    if(val != '')
    {
      if($(this).attr('type') == 'checkbox')
      {
        val = $(this).prop('checked');        
      }
      else if($(this).hasClass('date'))
      {
      }
      else if($(this).hasClass('money'))
      {
        if(!val.match(/^(?!0\.00)\d{1,3}(,\d{3})*(\.\d\d)?$/ig))
        {
          errors[errors.length] = {message: $(this).attr('title') + ' must be a valid monetary amount.', element: this};
        }
        else
        {
          val = parseFloat(val);
        }
      }
      else if($(this).hasClass('integer'))
      {
        if(!val.match(/\d+/ig))
        {
          errors[errors.length] = {message: $(this).attr('title') + ' must be a valid integer.', element: this}; 
        }
        else
        {
          val = parseInt(val);
        }                
      }           
    }
    else if($(this).hasClass('required'))
    {
      errors[errors.length] = {message: $(this).attr('title') + ' is required.', element: this};
    }
        
    obj[$(this).attr('data-column')] = val === '' ? null : val;
    
  }); 
};

jQuery.fn.loading = function (msg)
{
    if (!msg) msg = 'Loading...';

    return $(this).each(function ()
    {
        if (this.tagName && this.tagName == 'SELECT')
        {
            $(this).html('<option value="">' + msg + '</option>', true);
        }
        else if($(this).attr('data-source') && $(this).attr('data-source') != '')
        { 
          $(this).find('>.data').loading();
        }
        else
        {
            $(this).html('<img src="images/nloading.gif" alt="Loading" align="absmiddle" title="' + msg.replace(/"/g, "&quot;") + '" />');
        }
    });
};

String.prototype.trim = function () {
    a = this.replace(/^\s+/, '');
    return a.replace(/\s+$/, '');
};

function setContent(result, context) {     
      $(context).html(result).initializeDigifiUI();
      $(context).trigger('content-loaded.digifi');
      if($(context).is('select')) $(context).val($(context).data('selected-value'));
      if($(context).is(':input')) $(context).change();
    };
    
    function alertResult(result, context) {     
      alert(result);
    }; 
    
    function objectLoaded(result, context) { 
      var obj = jQuery.parseJSON(result);      
      
      $(context).data('object', obj).find('*[data-column]').each(function() {
        var ctrl = this;
        var val = obj[$(ctrl).attr('data-column')];
        
        if($(ctrl).attr('type') == 'checkbox')
        {
          $(ctrl).prop('checked', val == 'Y' || val == '1' || val == 'true');
        }
        else if($(ctrl).is('select') && $(ctrl).data('source') && $(ctrl).data('source') != '')
        {
          $(ctrl).data('selected-value', val).loading().trigger('refresh.digifi');
        }
        else if($(ctrl).is(':input'))
        {
          $(ctrl).val(val);
        }         
        else
        {
          $(ctrl).html(val);
        }
        
        $(ctrl).change();
      });

      if($(context).hasClass('digifi-popup')) $(context)[0].show();
      
      $(context).trigger('object-loaded.digifi');
    };
    
jQuery.fn.loadData = function(id) {
  return $(this).each(function() { 
      var fn = $(this).attr('data-source');
      
      if(!fn || fn == '')
      {
        alert('No data source specified for ' + this.id);
        return;
      }
                 
      if(!fn.match(/\(/ig)) fn += '("';
      
     fn += id.toString().replace(/"/g, '\\"') + '",objectLoaded, alertResult, "#' + this.id + '");';                                        
      
      eval(fn);
      });
    };    