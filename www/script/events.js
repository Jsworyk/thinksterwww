$(document).ready(function() {

  $('.attendance-selector').change(function() {
    events.SetAttendance($(this).val(), function(result){
      if(!result.Success) {
        alert(result.Message); 
      }
    }, alert);     
  });
  
  $('#admin_change_header_button').click(function() {
    $('#banner_uploader').modal('show'); 
  });
  
  //if($('#live_event_container').length > 0) getNextQuestion();
  
  $('#image_upload').fileupload({
      dataType: 'json',
      add: function(e, data) {
        events.GetUploadToken(function(result) {
          data.formData = {token: result};
          data.submit(); 
        }, alert);         
      },
      done: function (e, data) {
        if(data) {
          $('#image_upload_button, #image_list').attr('hidden', 'hidden');
          $('#image_upload_step_2').attr('hidden', null);
          
          var row = $('<div class="row" />');
          var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
          var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result.Files[0].Name).attr('src', data.result.Files[0].ThumbnailUrl));
          var block = $('<div class="card-block"></div>');
          var buttonContainer = $('<p class="text-center m-2"></p>');
          var postButton = $('<button type="button" class="btn btn-success">Post this Image</button>'); 
          
          block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result.Files[0].MediaId));
                
          block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result.Files[0].Name)));
          block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result.Files[0].Description)));
          block.append($('<div class="form-group"></div>').append($('<select class="mdb-select" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result.Files[0].ExposureTypeCode)));
          
          row.append(col);
          col.append(card);
          
          card.append(block);                   
          
          buttonContainer.append(postButton);
          
          $('#image_upload_step_2').html(row).append('<hr />', buttonContainer);
          
          postButton.click(function() { 
            var data = $('#image_upload_step_2').digifiUnbind();
            
            events.PostImage(data, function() { 
              $('#image_upload_button, #image_list').attr('hidden', null);
              $('#image_upload_step_2').attr('hidden', 'hidden');
              
              //TODO: remove this and replace with push:
              //$('#image_list').trigger('digifi.refresh');
            }, function(result) {
              alert('A problem happened during posting. Please try again later.'); 
            });
            
           
          });
          
          
          //$('#avatar_uploader .modal-footer > .btn-success').text('Post').prop('disabled', false);
        } else {
          alert('A problem happened during posting. Please try again later.'); 
        }
      }
  });
  
  $('#event_upload').fileupload({
      dataType: 'json',
      add: function(e, data) {
        events.GetUploadToken(function(result) {
          data.formData = {token: result};
          data.submit(); 
        }, alert);         
      },
      done: function (e, data) {
        if(data) {
          $('#event_image_step_1').attr('hidden', 'hidden');
          $('#event_image_step_2').attr('hidden', null);
          
          var row = $('<div class="row" />');
          var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
          var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result.Files[0].Name).attr('src', data.result.Files[0].ThumbnailUrl));
          var block = $('<div class="card-block"></div>');
          var buttonContainer = $('<p class="text-center m-2"></p>');
          var postButton = $('<button type="button" class="btn btn-success">Post this Image</button>'); 
          
          block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result.Files[0].MediaId));
                
          block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result.Files[0].Name)));
          block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result.Files[0].Description)));
          block.append($('<div class="form-group"></div>').append($('<select class="form-control" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result.Files[0].ExposureTypeCode)));
          
          row.append(col);
          col.append(card);
          
          card.append(block);                   
          
          buttonContainer.append(postButton);
          
          $('#event_image_step_2').html(row).append('<hr />', buttonContainer);
          
          postButton.click(function() { 
            var data = $('#event_image_step_2').digifiUnbind();
            
            events.PostBanner(data, function() { 
              $('#event_image_step_1').attr('hidden', null);
              $('#event_image_step_2').attr('hidden', 'hidden');
              
              //TODO: remove this and replace with push:
              $('#image_list').trigger('digifi.refresh');
            }, function(result) {
              alert('A problem happened during posting. Please try again later.'); 
            });
            
           
          });
          
          
          //$('#avatar_uploader .modal-footer > .btn-success').text('Post').prop('disabled', false);
        } else {
          alert('A problem happened during posting. Please try again later.'); 
        }
      }
  });
  
  $('#checkin_button').click(function() {
    if(!confirm('Are you sure you to check in to this event?')) return;
    events.CheckIn(function(result) {      
      if(result.Success) { 
        window.location = window.location;
      } else {
        alert(result.Message); 
      } 
    }, alert);
  });
 
});

thinkster.transform.recentImages = function(container, data) {  
  if(data.Images.length == 0) {
    $(container).html('<div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>you have not uploaded any images yet</p></div></div>');
  } else {
    
    var row = $('<div class="row m-1" />');
    
    for(var x = 0; x < data.Images.length; x++) {
      var col = $('<div class="col-lg-4 col-sm-3"></div>');
      var imageDiv = $('<div class="mt-2 text-center" />');
      var img = $('<img width="220" height="200" />').data('id',  data.Images[x].MediaId).attr('src', data.Images[x].ThumbnailUrl).attr('title', data.Images[x].Name + ' - ' + data.Images[x].Description);
      
      imageDiv.append(img);
      
      img.click(function() {
        $(container).find('img.rounded').removeClass('rounded poll-selected');
        $(this).addClass('rounded poll-selected');
        
        $('#au_existing').val($(this).data('id'));
        
        $('#avatar_uploader .modal-footer > .btn-success').text('Change').prop('disabled', false);
      }); 
      
      col.append(imageDiv);
      
      row.append(col); 
    }
  
    $(container).html($('<p class="small text-center">...or select a recent image from your gallery</p>')).append(row);
  }
};

thinkster.transform.attendees = function(container, data) {
  var showEmpty = function() {
    $(container).html($('<div class="empty-message text-muted"></div>').text($(container).attr('data-poll-empty-message'))); 
  };
  
  if(!data || data.People.length == 0) {
    showEmpty();
  } else {
    
    var row = $(container).find('.row');
    
    if(row.length == 0) { 
      row = $('<div class="row m-1" />');
      $(container).html(row);
    }
    
    for(var x = 0; x < data.People.length; x++) {
      var ic = $('<div class="mr-1" style="display: inline-block;"></div>');
      var link = $('<a></a>');
      
      link.attr('href', '/profile/' + data.People[x].ProfileName);
      
      ic.attr('data-profile-id', data.People[x].ProfileId);
      
      var av = $('<img class="poll-avatar" width="32" height="32" />');
      
      if(data.People[x].CheckedIn) {
        av.addClass('poll-checked-in');
      }
      
      if(data.People[x].AvatarMicrothumbUrl && data.People[x].AvatarMicrothumbUrl != '') {
        av.attr('src', data.People[x].AvatarMicrothumbUrl); 
      } else { 
        av.attr('src', '/images/default_avatar.png');
      }
      
      av.attr('title', data.People[x].DisplayName + (data.People[x].CheckedIn ? ' (checked in)' : ''));             
      
      link.append(av);
      
      if(data.People[x].CheckedIn) {        
        link.prepend('<i class="fa fa-check poll-checkmark"></i>');
      }
      
      ic.append(link);
      
      row.prepend(ic);
    }      
    
    if(row.find('div').length == 0) {
      showEmpty();
    } else {
      $(container).find('.empty-message').remove();
    }
  }
};