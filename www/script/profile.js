$(document).ready(function(){

	$('#btn_add_friend').click(function() {

		//Get the data from the button to see if the user wants to add the person as a friend
		var data = $('#btn_add_friend').attr('data-button');
		var profile_id = $('#btn_add_friend').attr('data-profileid');

		if(data === "") {
			//They want to add as a friend, let's do it!
			profile.FriendRequest(profile_id, function(result) {

				var test = JSON.parse(result);

				//Change the button to show waiting for approval
				if(test.Success) {
					$('#btn_add_friend').attr("class","btn btn-secondary");
					$('#btn_add_friend').attr("data-button", "WA");
					$('#btn_add_friend').html("<i class=\"fa fa-user-plus\"></i>&#160;Waiting for Approval");

					//Check to see if they don't already follow this person. If not, follow.
					if($('#btn_follow').attr("data-button") === "") {
						followUser();
					}
				}
			}, alert);
		}

	});
  
  $('#comaptibility_categories :input').change(function() {
    $('#following_compatibility').css('min-height', $('#following_compatibility').height());    
    $('#following_compatibility').html($('<p class="text-center"><i class="fa fa-refresh fa-spin text-danger fa-2x"></i> loading...</p>')).trigger('digifi.refresh');     
  });

  $('#btn_change_location').click(function() {
    $('#edit_location').modal('show');
  });

  $('#edit_location').bind('digifi.save-complete', function(e, result) {
    $('.profile-location-name').html(result.LocationName);
  });

	$('#btn_follow').click (function () {
			followUser();
		});

	$('#btn_add_friend #btn_follow').hover(function() {
		$(this).css('cursor','pointer');
	});
  
  $('#admin_change_header_button').click(function(e) {
    e.stopPropagation();
    $('#banner_uploader').modal('show');
    return false;
  });  
  
  $('.block-user-button').click(function() {
    if(!confirm('Are you sure you want to block this person?'));
    
    profile.BlockUser(function() {
      window.location = window.location; 
    }, alert); 
  });
  
  $('.unblock-user-button').click(function() {
    if(!confirm('Are you sure you want to unblock this person? They will once again be able to see your profile and any public posts.'));
    
    profile.UnblockUser(function() {
      window.location = window.location; 
    }, alert); 
  });
  
  $('#image_upload').fileupload({
      dataType: 'json',
      add: function(e, data) {
        profile.GetUploadToken(function(result) {
          data.formData = {token: result};
          data.submit(); 
        }, alert);         
      },
      done: function (e, data) {
        if(data) {
          $('#image_upload_button, #image_list').attr('hidden', 'hidden');
          $('#image_upload_step_2').attr('hidden', null);
          
          var row = $('<div class="row" />');
          var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
          var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result.Files[0].Name).attr('src', data.result.Files[0].ThumbnailUrl));
          var block = $('<div class="card-block"></div>');
          var buttonContainer = $('<p class="text-center m-2"></p>');
          var postButton = $('<button type="button" class="btn btn-success">Post this Image</button>'); 
          
          block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result.Files[0].MediaId));
                
          block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result.Files[0].Name)));
          block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result.Files[0].Description)));
          block.append($('<div class="form-group"></div>').append($('<select class="form-control" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result.Files[0].ExposureTypeCode)));
          
          row.append(col);
          col.append(card);
          
          card.append(block);                   
          
          buttonContainer.append(postButton);
          
          $('#image_upload_step_2').html(row).append('<hr />', buttonContainer);
          
          postButton.click(function() { 
            var data = $('#image_upload_step_2').digifiUnbind();
            
            profile.PostImage(data, function(json) {
              var result = jQuery.parseJSON(json);
              
              if(result.Success) {
                $('#image_upload_button, #image_list').attr('hidden', null);
                $('#image_upload_step_2').attr('hidden', 'hidden'); 
              } else {
                alert(result.Message); 
              }                         
              
              //TODO: remove this and replace with push:
              //$('#image_list').trigger('digifi.refresh');
            }, function(result) {
              alert('A problem happened during posting. Please try again later.'); 
            });
            
           
          });
          
          
          //$('#avatar_uploader .modal-footer > .btn-success').text('Post').prop('disabled', false);
        } else {
          alert('A problem happened during posting. Please try again later.'); 
        }
      }
  });
  
  $('#event_upload').fileupload({
      dataType: 'json',
      add: function(e, data) {
        $('#banner_uploader .fileinput-button i').removeClass('fa-plus').addClass('fa-refresh fa-spin');
        $('#banner_uploader .fileinput-button span').prop('disabled', true).text('Uploading...');
        profile.GetUploadToken(function(result) {          
          data.formData = {token: result};
          data.submit(); 
        }, function(result) {
          alert(result);
          $('#banner_uploader .fileinput-button i').addClass('fa-plus').removeClass('fa-refresh fa-spin');
          $('#event_upload .fileinput-button span').prop('disabled', false).text('Uploading Image...');
        });         
      },
      done: function (e, data) {
        $('#banner_uploader .fileinput-button i').addClass('fa-plus').removeClass('fa-refresh fa-spin');
        $('#banner_uploader .fileinput-button span').prop('disabled', false).text('Uploading Image...');
        if(data) {
          $('#event_image_step_1').attr('hidden', 'hidden');
          $('#event_image_step_2').attr('hidden', null);
          
          var row = $('<div class="row" />');
          var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
          var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result.Files[0].Name).attr('src', data.result.Files[0].ThumbnailUrl));
          var block = $('<div class="card-block"></div>');
          var buttonContainer = $('<p class="text-center m-2"></p>');
          var postButton = $('<button type="button" class="btn btn-success">Post this Image</button>'); 
          
          block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result.Files[0].MediaId));
                
          block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result.Files[0].Name)));
          block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result.Files[0].Description)));
          block.append($('<div class="form-group"></div>').append($('<select class="form-control" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result.Files[0].ExposureTypeCode)));
          
          row.append(col);
          col.append(card);
          
          card.append(block);                   
          
          buttonContainer.append(postButton);
          
          $('#event_image_step_2').html(row).append('<hr />', buttonContainer);
          
          postButton.click(function() { 
            var data = $('#event_image_step_2').digifiUnbind();
            
            profile.PostBanner(data, function() {
              $('#banner_uploader').modal('hide'); 
              $('#event_image_step_1').attr('hidden', null);
              $('#event_image_step_2').attr('hidden', 'hidden');
              
              //TODO: remove this and replace with push:
              window.location = window.location;
            }, function(result) {
              alert('A problem happened during posting. Please try again later.'); 
            });
            
           
          });
          
          
          //$('#avatar_uploader .modal-footer > .btn-success').text('Post').prop('disabled', false);
        } else {
          alert('A problem happened during posting. Please try again later.'); 
        }
      }
  });
  
  $('#select_response_tab').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    $('#responses_tab').tab('show'); 
  });
  
  $('#display_followers').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    $('#follower_list .modal-body').trigger('digifi.refresh');
    
    $('#follower_list').modal('show');
  });
  
  $('#display_following').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    $('#following_list .modal-body').trigger('digifi.refresh');
        
    $('#following_list').modal('show');
  });
  
  thinkster.connection.on('FollowerCount', function(data) {
    if(thinkster.CurrentPage.ProfileId == data.ProfileId) $('#display_followers').text(data.Count); 
  });
  
  thinkster.connection.on('FollowingCount', function(data) {
    if(thinkster.CurrentPage.ProfileId == data.ProfileId) $('#display_following').text(data.Count); 
  });
  
  
  thinkster.connection.on('BannerChanged', function(data) {    
    $('#profile_banner_image').css('background-image', 'url(' + data.Url + ')'); 
  });


  /*var canvas = document.getElementById('compatibility_chart');
  var ctx = canvas.getContext('2d');
  
  var options = {
      scale: {
          // Hides the scale
          display: false
      }
  };
  
  var compatibilityChart = new Chart(ctx, {
      type: 'radar',
      data: {
        labels: ['Running', 'Swimming', 'Eating', 'Cycling'],
        datasets: [{
            data: [20, 10, 4, 2]
        }]
      },
      options: options
  }); */
    
});

thinkster.transform.personalCompatibility = function(context, data) {  
  //if($(context).find('div').length == 0) $(context).html(JSON.stringify(data));
  
  //$(context).html(JSON.stringify(data));
  
  var table = $('<table class="table table-striped w-100"><thead class="thead-inverse"><tr><th>Category</th><th class="text-right">Compatibility</th></tr></thead><tbody></tbody></table>');
  
  for(var x = 0; x < data.Compatibility.length; x++) {
    var row = $('<tr />');
    var th = $('<th />');
    var td = $('<td class="text-right" />');
    
    th.text(data.Compatibility[x].Name);
    
    if(data.Compatibility[x].sameQuestionAnswered == 0) {
      td.html('<em>insufficient data</em>'); 
    } else {
      td.attr('title', data.Compatibility[x].matchingAnswer + ' out of ' + data.Compatibility[x].sameQuestionAnswered + ' question(s) answered').text((data.Compatibility[x].matchingAnswer * 100.00 / data.Compatibility[x].sameQuestionAnswered).toFixed(2) + '%');
    }        
    
    row.append(th, td);
    
    table.find('tbody').append(row);           
  } 
  
  $(context).html(table);  
};

thinkster.transform.compatibility = function(context, data) {  
  if($(context).find('div').length == 0) $(context).html('');

  var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  var match;
  var lastTimestamp = 0;
  
  if(data.length == 0) {
    $(context).html('<div class="alert alert-info">Either you do not have followers, or you haven\'t answered enough questions to calculate compatibility yet</div>'); 
  }

  for(var x = 0; x < data.length; x++) {
	//var card = $('<div class="card border-0">');
	//var cardBlock = $('<div class="card-block border-0" />');
    var item = $('<div class="media mt-1" style="/*font-size: 1.6rem;*/" />');
    var body = $('<div class="media-body d-flex align-self-center" />');
    var text = $('<div />');
    var lastIndex = 0;

    if(data[x].AvatarMicrothumbUrl) {
      item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" width="64" height="64" />').attr('src', data[x].AvatarMicrothumbUrl));
    } else {
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x fa-user" />'));
    }

    item.append(body);
    
    var link = $('<a />');
    
    link.attr('href', '/profile/' + data[x].ProfileName);
    link.text(data[x].DisplayName);
    
   // var percentage = $('<span class="ml-3 badge badge-pill"></span>');
    var val = (data[x].Agree / data[x].Total * 100.0).toFixed(2); 
    
    var percentage = $('<span class="min-chart"><span class="percent"></span></span>').attr('id', 'compatibility_' + data[x].ProfileId).attr('data-percent', val);

    
   /*percentage.text(val + '%');
    
    if(val >= 75) {
      percentage.addClass('badge-success'); 
    } else if (val >= 50) { 
      percentage.addClass('badge-warning');
    } else if (val < 50) {  
      percentage.addClass('badge-danger'); 
    }
                               */

    text.append(link, percentage);

    body.append(text);
	//cardBlock.append(item);
	//card.append(cardBlock);

    $(context).append(item, $('<hr />'));
        
    percentage.easyPieChart({
        barColor: "#4caf50", 
        size: 48,
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
    
  }

};

function followUser()
{
	var data = $('#btn_follow').attr('data-button');
  
  $('#btn_follow').prop('disabled', true);

	if(data === "") {
		//They want to add as a friend, let's do it!
		profile.FollowRequest(function(result) {
      console.log(result);

			var test = JSON.parse(result);

			//Change the button to show waiting for approval
			if(test.Success) {
				$('#btn_follow').attr("data-button", "F");
        $('#btn_follow').html($('<i />').addClass(test.FollowStatusIcon));
				$('#btn_follow').append(" " + test.FollowStatus);
			}
		}, alert, function() {
      $('#btn_follow').prop('disabled', false); 
    });
	} else {

		if(data === "F") {

			//They want to unfollow
			profile.UnfollowRequest(function(result) {

			var test = JSON.parse(result);

			//Change the button to show waiting for approval
			if(test.Success) {
				$('#btn_follow').attr("data-button", "");
				$('#btn_follow').html("<i class=\"fa fa-bullseye\"></i> Follow");
			}
		}, alert, function() {
      $('#btn_follow').prop('disabled', false); 
    });

		}
	}
}

thinkster.transform.recentImages = function(container, data) {  
  if(data.Images.length == 0) {
    $(container).html('<div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>you have not uploaded any images yet</p></div></div>');
  } else {
    
    var row = $('<div class="row m-1" />');
    
    for(var x = 0; x < data.Images.length; x++) {
      var col = $('<div class="col-lg-4 col-sm-3"></div>');
      var imageDiv = $('<div class="mt-2 text-center" />');
      var img = $('<img width="220" height="200" />').data('id',  data.Images[x].MediaId).attr('src', data.Images[x].ThumbnailUrl).attr('title', data.Images[x].Name + ' - ' + data.Images[x].Description);
      
      imageDiv.append(img);
      
      img.dblclick(function() {
        /*$(container).find('img.rounded').removeClass('rounded poll-selected');
        $(this).addClass('rounded poll-selected');
        
        $('#au_existing').val($(this).data('id'));
        
        $('#avatar_uploader .modal-footer > .btn-success').text('Change').prop('disabled', false);*/
        profile.PostBanner({ExistingMediaId: $(this).data('id')}, function() {
          
          $('#banner_uploader').modal('hide'); 
        }, alert);
      }); 
      
      col.append(imageDiv);
      
      row.append(col); 
    }
  
    $(container).html($('<p class="small text-center">...or select a recent image from your gallery</p>')).append(row);
  }    
};