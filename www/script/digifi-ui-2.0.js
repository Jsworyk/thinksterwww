var digifi = digifi || {};

(function ($, digifi)
{  
  digifi.async = {};

  digifi.contextStack = [];
  digifi.helperFunctions = {};
  digifi.templates = {};
  
  digifi.registerHelper = function(name, fn) {
    digifi.helperFunctions[name] = fn; 
  };
  
  digifi.registerTemplate = function(name, fn) {
   digifi.templates[name] = fn;
  };
  
  digifi.resolvePath = function(path) {
    if(!this) return null;
    
    return this[path.trim()]; 
  };
  
  digifi.transform = function(template, context) {
    return digifi.templates[template].call(context); 
  };


  digifi.registerHelper('each', function(context, options) {
    var ret = '';
    
    var current = digifi.resolvePath.call(this, context);
        
    for(var x = 0; x < current.length; x++) {
      ret += options.fn.call(current[x]);
    }
    
    return ret; 
  });
  
  digifi.registerHelper('if', function(context, options) {
    var ret = '';        
    var current = digifi.resolvePath.call(this, context);
    
    if(current || (current && current.length != 0)) {
      ret += options.fn.call(this); 
    } else {
      if(options.elsefn) {
        ret += options.elsefn.call(this);
      } 
    }
    
    /*    
    for(var x = 0; x < current.length; x++) {
      ret += options.fn.call(current[x]);
    }   */
    
    return ret; 
  });
  
  digifi.registerHelper('with', function(context, options) {
    var current = digifi.resolvePath.call(this, context);
    
    if(!current) return '';
    
    return options.fn.call(current);     
  });
  
  digifi.fn = {};        
  
  digifi.fn.nextId = (function() {
    var i = 0;
    
    return function() { return ++i; }; 
  })();
  
  digifi.fn.alertResult = function(result) { alert(result); };
  
  digifi.fn.invokeAsync = function(path, method, params, onSuccess, onFailure, onComplete, context) {
    var transmitParams = {};

    for (var p = 0; p < params.length; p++) {
        var nullable = false;
        var type = params[p].type;

        transmitParams[params[p].name] = params[p].value;

        var fail = function (param, dataType) {
            var message = 'You must provide a ' + dataType + ' value to parameter ' + param.name + ' in method ' + method + '. Value "' + param.value + '" is invalid.';
            if (onFailure) {
                onFailure(message, context, method);
            } else {
                alert(message);
            }
        };

        if (type.match(/\?$/)) {
            nullable = true;
            type = type.substring(0, type.length - 1);
        }

        switch (type) {
            case 'DateTime':
                //TODO: Date validation stuff
                if ((typeof params[p].value == 'undefined' || params[p].value == null) && !nullable) {
                    fail(params[p], 'date-time');
                    return;
                }
                break;

            case 'Double':
            case 'Single':
            case 'Int16':
            case 'Int64':
            case 'Int32':

                if ((typeof params[p].value == 'undefined' || params[p].value == null) && !nullable ) {
                    fail(params[p], 'numeric');
                    return;
                }

                if (params[p].value != null && isNaN(params[p].value)) {
                    fail(params[p], 'numeric');
                    return;
                }

                break;

            case 'Boolean':
                if ((typeof params[p].value == 'undefined' || params[p].value == null) && !nullable) {
                    fail(params[p], 'boolean');
                    return;
                }

                if (typeof (args[a]) != 'boolean') {
                    fail(params[p], 'boolean');
                    return;
                }

                break;
        }
    }

    $.ajax({
        type: 'POST',
        url: path,
        data: { AsyncMethod: method, Parameters: JSON.stringify(transmitParams) },
        dataType: 'json',
        success: function (result, status, other) {
            if (result.Success == 1) {
                if (onSuccess) onSuccess(result.Result, context, method);
            } else {
                if (onFailure) onFailure(result.Message, context, method);
            }            
        },
        error: function (request, status, errorThrown) { 
        if (onFailure) { 
          onFailure(errorThrown + '\n' + request.responseText, context, status); 
        } },
        complete: function (status) {
            if (onComplete) onComplete(status, context, method);
        }


    });  
  };
  
  digifi.invokeReferencedMethod = function(name, arguments, onSuccess, onFailure, onComplete) {
    var pieces = name.split(".");
    var internalArgs = [].concat(arguments);
    
    if(pieces.length != 2) {
      console.log('Invalid method: ' + name);
      return;
    }  
    
    internalArgs.push(onSuccess);
    internalArgs.push(onFailure);
    internalArgs.push(onComplete);  
    
    if(!window[pieces[0]]) {
      onFailure('Unable to locate ' + pieces[0]);
      return;
    }
    
    if(!window[pieces[0]][pieces[1]]) {
      onFailure('Unable to locate ' + pieces[0] + '.' + pieces[1]);
      return;
    }
    
    window[pieces[0]][pieces[1]].apply(window[pieces[0]], internalArgs);     
  }; 
   
  
  digifi.initialize = function(target) {
    if(jQuery.fn.timeago) {
      $('time.timeago').timeago();
    }
  
    $(target).find('.digifi-auto-layout').each(function() {
      var form = $('<div class="form" />');
      
      $(this).children().each(function() {        
        var formGroup = $('<div></div>').addClass(($(this).is(':checkbox') || $(this).is(':radio') ? 'form-check' : 'form-group'));
        var label = $('<label />');
        
        if($(this).is(':checkbox') || $(this).is(':radio')) {
          label.addClass('form-check-label');
          
          $(this).addClass('form-check-input');
                                                 
          $(label).prepend(this);
          formGroup.append(label); 
        } else {
          label.attr('for', this.id);           
          
          formGroup.append(label, this);          
          
        }               
        
        label.append(' ', $(this).attr('title'));
        
        if($(this).attr('required')) formGroup.addClass('digifi-required');

        if($(this).is(':input:not(:checkbox):not(:radio)')) $(this).addClass('form-control');                
        
        if($(this).attr('data-digifi-help') && $(this).attr('data-digifi-help') != '') {  
          var helpId = '_digifi_' + digifi.fn.nextId(); 
          var help = $('<small class="form-text text-muted"></small>').attr('id', helpId).html($(this).attr('data-digifi-help'));
          
          $(this).attr('aria-describedby', helpId);
          
          formGroup.append(help); 
        }
                                
        form.append(formGroup); 
      }); 
      
      $(this).html(form);
    });
    
    $(target).find('.digifi-panel').each(function() {
      var original = this;      
      var panel = $('<div class="card digifi-panel"></div>');
      var header = $('<div class="card-header"></div>');
      var block = $('<div class="card-body"></div>');
      
      header.text($(this).attr('title'));
      
      if($(this).attr('data-digifi-buttons') && $(this).attr('data-digifi-buttons') != '') {
        var headerButtons = $('<span class="pull-right digifi-actions" />');
        var actions = eval('(' + $(this).attr('data-digifi-buttons') + ')');
        
        for(var a = 0; a < actions.length; a++) {
          if(typeof actions[a] === 'string' || actions[a] instanceof String) {
            var btn = $('<button type="button" class="btn btn-sm" />');
            
            switch(actions[a]) {
              case 'Add':
                btn.addClass('btn-success');
                btn.attr('data-digifi-action', 'Add').data('target', original);
                btn.append('<i class="fa fa-plus" />');
              break;
              
              case 'Refresh':
                btn.addClass('btn-warning');                
                btn.attr('data-digifi-action', 'Refresh').data('target', original);
                btn.append('<i class="fa fa-refresh" />');
              break; 
            }
            
            headerButtons.append(btn);           
          } else { 
          } 
        }
        
        header.append(headerButtons); 
      }
      
      $(this).attr('title', null);
      
      panel.append(header);
      panel.append(block);
      
      $(this).after(panel);
      
      $(this).remove();        
      
      block.append(this);      
    });
    
    $(target).find('.digifi-popup').each(function() {
       var popup = this;
       var dialog = $('<div class="modal-dialog modal-lg" role="document"></div>');
       var content = $('<div class="modal-content"></div>');
       var header = $('<div class="modal-header"></div>');
       var title = $('<h4 class="modal-title"></h4>');
       var body = $('<div class="modal-body"></div>');
       var footer = $('<div class="modal-footer"></div>');
       var buttons = [{Text: 'Save', Action: 'Save'},{Text: 'Cancel', Action: 'Cancel'}];
       
       if($(popup).attr('data-digifi-buttons') && $(popup).attr('data-digifi-buttons') != '') {
          buttons = eval('(' + $(popup).attr('data-digifi-buttons') + ')'); 
       }
       
       for(var b = 0; b < buttons.length; b++) {
          var button = $('<button type="button" class="btn" />');
          var icon = null;
          
          if(buttons[b].Icon) {
            icon = $('<i />').addClass(buttons[b].Icon); 
          }
          
          button.attr('data-digifi-action', buttons[b].Action);
          button.append(icon, buttons[b].Text);
          
          switch(buttons[b].Action) { 
            case 'Save':
              button.addClass('btn-success');
              button.attr('data-digifi-submits', popup.id);
            break;
            
            case 'Cancel':
              button.addClass('btn-warning');
            break;
          } 
          
          footer.append(' ', button);
       }
       
       title.html($(popup).attr('title'));
       
       $(popup).attr('title', null);
       
       header.append(title, $('<button type="button" class="close" data-dismiss="modal" title="Close this popup" aria-label="Close"><span aria-hidden="true">&times;</span></button>'));
       body.append($(popup).html());
       
       content.append(header, body, footer);
       dialog.append(content);
       
       popup.digifi = {};
       
       popup.digifi.newObject = function(objectType, initialValues) {
        if($(popup).attr('data-digifi-add-title') && $(popup).attr('data-digifi-add-title') != '') {
          title.html($(popup).attr('data-digifi-add-title'));
        }
        Master.NewObject(objectType, function(result) {
          var obj = jQuery.parseJSON(result);
          
          if(initialValues) {
            for (var property in initialValues) {
                if (initialValues.hasOwnProperty(property)) {
                  obj[property] = initialValues[property];                    
                }
            }
          }
          
          $(popup).digifiDatabind(obj);
          $(popup).modal('show');
        });         
       };
       
       popup.digifi.editObject = function(editId) {
        title.html($(popup).attr('title'));
        if($(popup).data('digifi-source') && $(popup).data('digifi-source') != '') {
          var args = [editId];
        
          if($(popup).attr('data-digifi-source-args') && $(popup).attr('data-digifi-source-args') != '') {
            args = eval($(popup).attr('data-digifi-source-args'));         
          }
          
          //Invoke the method specified in data-digifi-source:
          digifi.invokeReferencedMethod($(popup).data('digifi-source'), args,     
            function(result) { 
              var obj = jQuery.parseJSON(result);
              $(popup).digifiDatabind(obj);
              $(popup).modal('show');
            }, function(result) {
              alert(result);    
            }, function() {                
              $(popup).trigger('digifi.load-complete'); 
            });
          } else {
            alert('Missing/invalid source on popup ' + popup.id);
          }
         
       };
       
       $(popup).addClass('modal fade').attr('tabindex', '-1').attr('role', 'dialog').html(dialog);
       
       $(popup).modal( {show: false });
       
       $(popup).bind('digifi.action', function(e, a) { 
        switch(a.action) { 
          case 'Save':
            
          break;
          
          case 'Cancel':
            $(popup).modal('hide');
          break;
        }
       });
    });  
  
  
    $(target).find('*[data-digifi-source]').bind('digifi.refresh', function (e, options) {
      var destination = this;
      
      //e.stopImmediatePropagation();
      e.stopPropagation();
      
      if($(this).is('select')) {
        $(this).html('<option value="">Loading...</option>').prop('disabled', true); 
      } else if(!$(this).is('.modal') && ($(this).html() == '')) { // || !$(this).attr('data-digifi-json-transform'))) {
        $(this).html('<p class="text-xs-center"><i class="fa fa-5x fa-refresh fa-spin text-danger"></i></p>'); 
      }           
    
      if ($(this).attr('data-digifi-actions') && $(this).attr('data-digifi-actions') != '') {
          $(this).find('*[data-digifi-action]').prop('disabled', true);
          $('#' + $(this).attr('data-digifi-actions') + ' *[data-digifi-action]').prop('disabled', true);
          $('#' + $(this).attr('data-digifi-actions') + ' *[data-digifi-action="Refresh"]').addClass('btn-danger').find('.fa').addClass('fa-spin');
      }
      
      var args = [];
      
      if(options && options.args) {
        args = options.args; 
      } else {
        if($(this).attr('data-digifi-source-args') && $(this).attr('data-digifi-source-args') != '') {
          args = eval($(this).attr('data-digifi-source-args'));         
        }
      }
      
      if($(this).attr('data-digifi-sortcolumn') && $(this).attr('data-digifi-sortcolumn') != '') {
        args[args.length] = $(this).attr('data-digifi-sortcolumn');
        args[args.length] = $(this).attr('data-digifi-sortdesc');
      }
      
      //Invoke the method specified in data-digifi-source:
      digifi.invokeReferencedMethod($(this).data('digifi-source'), args,     
        function(result) { 
          if($(destination).data('digifi-json-transform') && $(destination).data('digifi-json-transform') != '') {
            try {
              var transform = eval($(destination).data('digifi-json-transform'));
              
              if(transform) {
                transform(destination, jQuery.parseJSON(result)); 
              }
            } catch(ex) {
              $(destination).html($('<div class="alert alert-danger" />').html(ex.message)); 
            }
          } else {
            $(destination).html(result);
            digifi.initialize(destination);
          }                     
        }, function(result) {
          $(destination).html(result);   
          digifi.initialize(destination);    
        }, function() {             
          if($(destination).attr('data-digifi-sortcolumn') && $(destination).attr('data-digifi-sortcolumn') != '') {
            $(destination).find('th[data-digifi-sortcolumn="' + $(destination).attr('data-digifi-sortcolumn') + '"]').addClass('active' + ($(destination).attr('data-digifi-sortdesc') == 1 ? ' descending' : ''));
          }   
          $(destination).trigger('digifi.load-complete'); 
        });
      
    }).bind('digifi.load-complete', function () {      
      if ($(this).attr('data-digifi-actions') && $(this).attr('data-digifi-actions') != '') {        
          $('#' + $(this).attr('data-digifi-actions') + ' *[data-digifi-action]').prop('disabled', false);
          //See https://github.com/FortAwesome/Font-Awesome/issues/7572
          var focused = $(':focus');
          $('#' + $(this).attr('data-digifi-actions') + ' *[data-digifi-action="Refresh"]').removeClass('btn-danger').find('.fa').removeClass('fa-spin').focus();
          focused.focus();
      }
      
      if($(this).is('select')) {
        $(this).prop('disabled', false); 
      }
      
    }).bind('digifi.action', function(e, a) {      
      switch(a.action) {
        case 'Add':
          if($(this).attr('data-digifi-editor') && $(this).attr('data-digifi-editor') != '') {
            var editor = $('#' + $(this).attr('data-digifi-editor'))[0]; 
            editor.digifi.newObject($(this).attr('data-digifi-type'));
            $(editor).modal('show');
          }
        break;
        
        case 'Edit':
          if($(this).attr('data-digifi-editor') && $(this).attr('data-digifi-editor') != '') {          
            var editor = $('#' + $(this).attr('data-digifi-editor'))[0]; 
            if(editor) {
              editor.digifi.editObject(a.params);
            } else {
              alert('Unknown editor: ' + $(this).attr('data-digifi-editor'));
            }          
          }   
        break;
        
        case 'Delete':
          var args = [];
          var destination = this;
                                                       
          if($(a.element).data('digifi-delete-prompt') && $(a.element).data('digifi-delete-prompt') != '') {
            if(!confirm($(a.element).data('digifi-delete-prompt'))) return; 
          } else if($(destination).data('digifi-delete-prompt') && $(destination).data('digifi-delete-prompt') != '') {
            if(!confirm($(destination).data('digifi-delete-prompt'))) return;
          }                   
          
          if(a.params) {
            args = a.params;
          }
      
          if($(destination).attr('data-digifi-delete-args') && $(destination).attr('data-digifi-delete-args') != '') {
            args = eval($(destination).attr('data-digifi-delete-args'));         
          }
          
          //Invoke the method specified in data-digifi-source:
          digifi.invokeReferencedMethod($(destination).data('digifi-delete'), args,     
            function(result) {  
              $(destination).trigger('digifi.refresh');  
            }, function(result) {
              alert(result);    
            });      
        break;
        
        case 'SetPageNumber':          
          e.stopPropagation();
          e.preventDefault();
          $(this).data('digifi-page-number', a.params).trigger('digifi.refresh');
        break;
      
        case 'Refresh':
          e.stopPropagation();
          
          $(this).trigger('digifi.refresh');
        break; 
      }
      
    }).each(function() {
      // Connect this source object to each action button:
      if ($(this).attr('data-digifi-actions') && $(this).attr('data-digifi-actions') != '') {
        $('#' + $(this).attr('data-digifi-actions') + ' *[data-digifi-action]').data('target', this);                  
      }
      
      if($(this).is('.modal')) {
        var modal = this;
        
        this.digifi = this.digifi || {};
        
        this.digifi.add = function($type) {
          $type = $type || $(modal).data('digifi-type') || '';
          
          if($type == '') {
            alert('Unable to display Add modal: missing data type');
            return;
          }
        
          $(modal).digifiResetForm().removeClass('digifi-edit').addClass('digifi-add');
          Master.NewObject($type, function(result) {
            $(modal).digifiDatabind($.parseJSON(result));
            $(modal).modal('show'); 
          }, function(result) {
            alert(result);
          });
        };
        
        this.digifi.edit = function($id) {
          $(modal).removeClass('digifi-add').addClass('digifi-edit');
        
          digifi.invokeReferencedMethod($(modal).data('digifi-source'), [$id],     
            function(result) {
              $(modal).digifiDatabind($.parseJSON(result));
              $(modal).modal('show');
            }, function(result) {
              alert(result);    
            }, function() {                
              $(modal).trigger('digifi.load-complete'); 
            }); 
        }; 
      }
    });                    
    
    $(target).find('*[data-digifi-action]').click(function() {
      var action = {};
      
      action.action = $(this).attr('data-digifi-action');
      if($(this).attr('data-digifi-id') && $(this).attr('data-digifi-id') != '')
      {
        action.params = $.parseJSON($(this).attr('data-digifi-id'));
      }
      action.element = this;
      
      $($(this).data('target') || this).trigger('digifi.action', action);
    });
    
    $(target).find('th[data-digifi-sortcolumn]').click(function() {
       if($(target).attr('data-digifi-sortcolumn') == $(this).attr('data-digifi-sortcolumn')) {
        $(target).attr('data-digifi-sortdesc', $(target).attr('data-digifi-sortdesc') == 1 ? 0 : 1);        
       } else {
        $(target).attr('data-digifi-sortcolumn', $(this).attr('data-digifi-sortcolumn'));        
        $(target).attr('data-digifi-sortdesc', 0);
       }
       $(target).trigger('digifi.refresh');
    });
    
    
    $(target).find('*[data-digifi-submits]').click(function() {
      var target = $('#' + $(this).attr('data-digifi-submits'));
      var obj = target.digifiUnbind(); //target.data('digifi.object') || {};
      var args = [];
      
      if(target.digifiValidate()) {
        /*target.find(':input[data-digifi-value]').each(function() {
          var val = $(this).val();
          
          if($(this).is(":checkbox")) {
            val = $(this).prop('checked'); 
          }
        
          obj[$(this).attr('data-digifi-value')] = val;      
        });     */
        
        target.find(':input,button').prop('disabled', true);
        
        if(target.data('digifi-update') && target.data('digifi-update') != '') {
          var pieces = target.data('digifi-update').split(".");
          
          var args = new Array();
          
          if(target.data('digifi-update-args') && target.data('digifi-update-args') != '') {
            args = eval(target.data('digifi-update-args'));      
          }      
          
          args[args.length] = obj;
          
          //Invoke the method specified in data-digifi-source:
          digifi.invokeReferencedMethod(target.data('digifi-update'), args,     
            function(result) { 
              target.modal('hide');          
              target.trigger('digifi.save-complete', result);
              if(target.data('digifi-refresh') && target.data('digifi-refresh') != '') {
                $(target.data('digifi-refresh')).trigger('digifi.refresh'); 
              } 
              
              $('*[data-digifi-editor="' + target.attr('id') + '"]').trigger('digifi.refresh');
            }, function(result) {
              alert(result);    
              target.trigger('digifi.save-failed', result);
            }, function() {                
              target.find(':input,button').prop('disabled', false);
            });
        }            
      } 
    }); 
    $(target).addClass('digifi-initialized');
  };   
  
  $(document).ready(function() {
    digifi.initialize(this);
    
    $('*[data-digifi-source]:not(.digifi-no-initial-load):not(.modal)').trigger('digifi.refresh'); 
  });

})(jQuery, digifi);




/***************************************************
Asynchronous Support
***************************************************/
function __async(path, method, args, params, onSuccess, onFailure, onComplete, context)
{
    var delim = path.match(/\?/ig) ? '&' : '?';

    if (args.length < params.length)
    {
        onFailure(method + ' requires ' + params.length + ' parameters');
        return;
    }

    for (var a = 0; a < params.length; a++)
    {
        var nullable = false;
        var type = params[a];

        if (type.match(/\?$/))
        {
            nullable = true;
            type = type.substr(0, type.length - 1);
        }        

        switch (type)
        {
            case 'DateTime':
                if (args[a] == null && !nullable) {
                    if (onFailure) onFailure('You must provide a date/time value to parameter #' + a + ' in method ' + method);
                    return;
                }

                if (args[a] == '' && nullable) {
                    args[a] = null;
                }

                break;

            case 'Double':
            case 'Single':
            case 'Int16':
            case 'Int64':
            case 'Int32':

                if (args[a] == null && !nullable)
                {
                    if (onFailure) onFailure('You must provide a numeric value to parameter #' + a + ' in method ' + method);
                    return;
                }

                if (args[a] != null && isNaN(args[a]))
                {
                    if (onFailure) onFailure('You must provide a numeric value to parameter #' + a + ' in method ' + method);
                    return;
                }

                break;

            case 'Boolean':
                if (args[a] == null && !nullable)
                {
                    if (onFailure) onFailure('You must provide a boolean value to parameter #' + a + ' in method ' + method);
                    return;
                }

                if (typeof (args[a]) != 'boolean')
                {
                    if (onFailure) onFailure('You must provide a boolean value to parameter #' + a + ' in method ' + method);
                    return;
                }

                break;
        }
    }
        
    $.ajax({ type: 'POST',
        url: path + delim + '__asyncmethod=' + method,
        dataType: 'json',
        data: JSON.stringify(args).replace(/\%/g, "%25").replace(/\=/g, "%3D").replace(/\&/g, "%26"),
        success: function (result, status, method) { 
        if (result.status == 1) { if(onSuccess) onSuccess(result.result, context, method); 
        } else {
        if (onFailure) onFailure(result.result, context, method); 
        } },
        complete: function(jqXHR, textStatus) { if(onComplete) onComplete(textStatus); },
        error: function (request, status, errorThrown) {
        if(onFailure) onFailure(errorThrown + ': ' + request.responseText, context, status); 
        }              
    }
        );
}

/*******************************
 * jQuery Extensions 
 ******************************/

jQuery.fn.digifiEnterKey = function (op) {
    return $(this).keydown(function (e) { if (e.which == 13) { op(e); e.stopPropagation(); return !e.isDefaultPrevented(); } });
};

jQuery.fn.digifiGetLabel = function() {
  var label = this.id;
  
  if($(this).attr('aria-described-by') != '') {
    label = $('#' + $(this).attr('aria-described-by')).text();  
  } 
  
  return label;
};

jQuery.fn.digifiResetForm = function() {
  $(this).find(':input[data-digifi-value]').val('');
  return this;   
};

jQuery.fn.digifiDatabind = function(obj) {
  var target = $(this);
  
  obj = obj || {};
  
  target.data('digifi.object', obj);
    
  return $(target).find('*[data-digifi-value]').each(function() {
    var val = obj[$(this).data('digifi-value')] || '';
  
    if($(this).is(':checkbox')) {
      $(this).prop('checked', val || val == 'Y' || val == '1' || val == 'true');
    } else if($(this).is('.digifi-codemirror')) { 
      $(this).data('CodeMirror').setValue(val);
    } else if($(this).is('input[type="date"]') || $(this).is('.digifi-date')) {
      var date = null;
          
      if(val.substr(1, 4) == 'Date') {     
        date = new Date(parseInt(val.substr(6)));
      } else {             
        var bits = val.split(/\D/);            
        date = new Date(bits[0], --bits[1], bits[2], bits[3] || null, bits[4] || null);
      }

      /*if ($(this).hasClass('date-time') || $(this).hasClass('date_time'))
      {
          $('#' + this.id + '_hours').val((date.getHours() < 10 ? '0' : '') + date.getHours().toString());
          $('#' + this.id + '_minutes').val((date.getMinutes() < 10 ? '0' : '') + date.getMinutes().toString());
      }    */

      if($.fn.datepicker && date && val) {
        val = $.fn.datepicker.DPGlobal.formatDate(date, 'yyyy-mm-dd', 'en');
        //val = $.datepicker.formatDate('yy-mm-dd', date);
      }
      
      $(this).val(val);       
    } else if($(this).is('select[data-digifi-source]')) {
      
    } else { 
      $(this).val(val).change();
    }           
  });   
};

jQuery.fn.digifiUnbind = function()  {
  var obj = $(this).data('digifi.object') || {};
  
  $(this).find(':input[data-digifi-value]').each(function() {
    var val = $(this).val();
    
    if($(this).is(":checkbox")) {
      val = $(this).prop('checked'); 
    } else if($(this).is('.digifi-codemirror')) {
      val = $(this).data('CodeMirror').getValue();
    }
  
    obj[$(this).attr('data-digifi-value')] = val;       
  });
  
  return obj;
};

jQuery.fn.digifiSetControlValidation = function(mode, message) {
  mode = mode.toLowerCase(); 
  
  if($.fn.tooltip.Constructor.VERSION && parseInt($.fn.tooltip.Constructor.VERSION.substring(0, 1)) < 4) {
    if(mode == 'danger') mode = 'error';
   
  }
  
  
  if($(this).is(':checkbox') || $(this).is(':radio')) {
    $(this).parent().parent().removeClass('has-success has-warning has-danger has-error').addClass('has-' + mode);
  } else {
    $(this).parent().removeClass('has-success has-warning has-danger has-error').addClass('has-' + mode);
  } 
  
  $(this).removeClass('form-control-success form-control-warning form-control-danger form-control-error').addClass('form-control-' + mode);
  
  if(mode == 'success') {
    if($(this).is(':checkbox') || $(this).is(':radio')) {
        $(this).attr('title', message);         
    } else {
      $(this).parent().find('.form-control-feedback').html(message);
    }
  } else { 
    if($(this).parent().find('.form-control-feedback').length > 0) {
       $(this).parent().find('.form-control-feedback').removeClass('hidden').html(message);
    } else { 
      var d = $('<div class="form-control-feedback" />');
      d.html(message);
      if($(this).is(':checkbox') || $(this).is(':radio')) {
        $(this).attr('title', message);         
      } else { 
        $(this).after(d);
      }
    }
  }
 
};

jQuery.fn.digifiValidate = function() {
  $(this).find(':input:not(button):not(:hidden),textarea.digifi-codemirror').each(function() {    
    var isValid = true;
    var message = '';
    var val = $(this).val().trim();
    var input = this;       
    
    if($(this).is('.digifi-codemirror')) {
      val = $(this).data('CodeMirror').getValue();
    }
    
    if (input.checkValidity() == false) {
      isValid = false;
      message = input.validationMessage || 'Invalid value';
    }
    
    if(eval($(this).attr('data-digifi-required')) || $(this).attr('required')) {                
      if($(this).is(':checkbox') && !$(this).prop('checked')) {
        isValid = false;
        message = $(this).attr('data-digifi-required-prompt');
        
        if(!message || message == '') {
          message = 'This field is required';  
        }
      } else if(!val || val.trim() == '') { 
        isValid = false;
        message = $(this).attr('data-digifi-required-prompt');
        
        if(!message || message == '') {
          message = 'This field is required';  
        }
      } 
    } 
    
    if (isValid && !this.hasOwnProperty('pattern') && this.pattern && this.pattern != ''          
         && this.value.search(this.pattern)) {
       
        isValid = false;
       
        message = $(this).attr('data-digifi-pattern-prompt');
        
        if(!message || message == '') {
          message = 'This field is not in the correct format';  
        }         
    }
    
    if(isValid && $(this).attr('data-digifi-match') && $(this).attr('data-digifi-match') != '') {
       if($('#' + $(this).attr('data-digifi-match')).val() != val) {
        isValid = false; 
        message = $(this).attr('data-digifi-match-prompt');
        
        if(!message || message == '') {
          message = $(this).digifiGetLabel() + ' must match ' + $('#' + $(this).attr('data-digifi-match')).digifiGetLabel();  
        }
       }
    }
    
    if(isValid && $(this).attr('data-digifi-min-length') && $(this).attr('data-digifi-min-length') != '') {
      if(parseInt($(this).attr('data-digifi-min-length')) > val.length) {
        isValid = false; 
        message = $(this).attr('data-digifi-min-length-prompt');
        
        if(!message || message == '') {
          message = $(this).digifiGetLabel() + ' must be a minimum of ' + $(this).attr('data-digifi-min-length') + ' character' + (parseInt($(this).attr('data-digifi-min-length')) != 1 ? 's' : '') + ' long';  
        }
       } 
    }
    
    //Do Async validation last:
    if(isValid && $(this).attr('data-digifi-async-validate') && $(this).attr('data-digifi-async-validate') != '') {
       var av = $(this).attr('data-digifi-async-validate');
       
       var f = av.split('.').reduce(function(o,i){ return o[i]; }, window);
       
       f(val, function(result) { 
        var obj = jQuery.parseJSON(result);
      
        if(obj.isValid) {
          $(input).digifiSetControlValidation('success', obj.Message);       
        } else {
          $(input).digifiSetControlValidation('danger', obj.Message);
        }
       }, function(result) {
        alert("failure: " + result); 
       });       
    } else if(!isValid) { 
      $(this).digifiSetControlValidation('danger', message);
    } else { 
      $(this).digifiSetControlValidation('success', message);
    }         
  }); 
  
  return ($(this).find(':input.form-control-danger, :input.form-control-error').length + $(this).find(':input.form-control-warning').length) == 0; 
};


String.prototype.trim = function () {
    a = this.replace(/^\s+/, '');
    return a.replace(/\s+$/, '');
};