$(document).ready(function() {

  $('#poll_sf_categories').bind('digifi.action', function(e, a) {
    //alert(a.action + ', ' + a.params); 
    $('#poll_sf_categories > button').html($(a.element).html());
    
    $('#poll_suggested_follow').trigger('digifi.refresh', {args: [a.params || null]});
  });

  thinkster.connection.on('FollowerCount', function(data) {
    if(data.ProfileId == thinkster.CurrentUser.ProfileId) {
      $('#db_follower_count').text(data.Count); 
    }
  });

  var questionFormatter = function(poll) {
    $('#poll_question_text').text(poll.Question.Question);
    $('#poll_question_choices').html('');
    
    //console.log(JSON.stringify(poll.Choices));

    for(var c = 0; c < poll.Choices.length; c++) {
      var li = $('<li class="text-center" />');
      //var label = $('<label />');
      var cb = $('<button type="button" class="btn btn-mdb w-75" name="poll_current_question"></button>');
      
      cb.css('background-color', poll.Choices[c].ChartColour);

      cb.val(poll.Choices[c].PollChoiceId);
      cb.text(poll.Choices[c].Choice);
      
      cb.click(function() { 
        dashboard.AnswerQuestion($(this).val(), $('#visibility_selector input:checked').val(), function() {
          getNextQuestion();
        }, alert);   
      });
      
      li.append(cb);

      //label.append(cb, '&nbsp;', $('<span />').text(poll.Choices[c].Choice));
      //li.append(label);

      $('#poll_question_choices').append(li);
      
      //$('#poll_submission').prop('disabled', false);

      $('#current_question').show();
    }
  };

  var getNextQuestion = function() {
    //$('#poll_submission').prop('disabled', true);
    
    dashboard.GetNextQuestion(function(json) {
      var result = jQuery.parseJSON(json);
      
      if(result['Success']) {      
        questionFormatter(result);
      } else {
        var error = $('<div class="alert alert-danger m-3"></div>').html(result['Message']);
      
        $('#current_question').html(error).show();
      }
    }, alert);
  };
  
  $('.poll-visibility-option').click(function() {
    var button = $(this);
    
    $('#visibility_selector').data('poll-value', button.val()).text(button.text()); 
  });
  
  thinkster.connection.on('BannerChanged', function(data) {      
    $('#profile_banner_image').attr('src', data.Url); 
  });
  
  /*$('#poll_submission').click(function() {
    if($('#poll_question_choices :radio:checked').length == 0) {
      alert('You must select an answer');
      return;       
    } else {
      dashboard.AnswerQuestion($('#poll_question_choices :radio:checked').val(), $('#visibility_selector').data('poll-value'), function() {
        getNextQuestion();
      }, alert);            
    }
  });*/

  getNextQuestion();
});

thinkster.transform.questionList = function(context, data) {
  if(data.Success) {
    var questions = data.Trending;
    var list = $('<div class="list-group list-group-flush"></div>');
    
    if(data.Trending.length == 0) {
      list = $('<div class="alert alert-info">No questions are currently trending in the system.</div>');
    } else {  
      for(var x = 0; x < data.Trending.length; x++) {
        var item = $('<a class="list-group-item justify-content-between"></a>');
        
        item.attr('href', '/poll/' + data.Trending[x].UrlSegment);
        item.text(data.Trending[x].Question);
        item.append($('<span class="badge badge-primary badge-pill pull-right"></span>').text(data.Trending[x].ResponseCount)); 
        //list.append($('<span class="badge badge-primary badge-pill pull-right"></span>').text(data.Trending[x].ResponseCount));
        
        list.append(item);
      }
    }
    
    $(context).html(list); 
  } else {
    $(context).html($('<div class="alert alert-danger"></div>').text(data.Message)); 
  }   
};