$(document).ready(function() {  
  var ableDeployButton = function() {
    $('#deploy_button').prop('disabled', $('#deploy_files :checkbox:checked').length == 0); 
  };


  thinkster.CurrentPage.deployFiles =  function(container, data) {
    //$(container).text(JSON.stringify(data));
    //return;
    
    if($(container).is('#deploy_files')) {
      $(container).html('');
      /*var toggleAll = $('<input type="checkbox" title="Toggle All" />');
      
      toggleAll.click(function() {
         $('.poll-deploy-file').prop('checked', $(this).prop('checked'));
      });
    
      $(container).append($('<div />').append(toggleAll));      */
    }
  
    //if($(container).find('ul').length == 0) $(container).html('');
    
    var ul = $('<ul class="list-unstyled pl-1 m-0 mt-2" />');
    
    for(var f = 0; f < data.Files.length; f++) {
      if(data.Files[f].Type == 'Folder') {
        var li = $('<li />').attr('title', data.Files[f].FileCount);
        
        var span = $('<span class="text-primary"></span>').text(data.Files[f].Name);
        
        li.append('<i class="fa fa-fw fa-folder text-warning" />');
        li.append(span);
        
        span.click(function() {
          $(this).parent().find(':checkbox').prop('checked', !$(this).parent().find(':checkbox').prop('checked'));
          ableDeployButton(); 
        });
        
        if(data.Files[f].Files.length > 0 ) {
          thinkster.CurrentPage.deployFiles(li, data.Files[f]);
          //li.append('<i class="fa fa-fw fa-user text-warning" />', data.Files[f].Files.length);
        }       
        
        ul.append(li);                       
      } else { 
      
        var li = $('<li class="form-check" />');
        var label = $('<label class="form-check-label" />');
        
        label.append($('<input type="checkbox" class="form-control-checkbox poll-deploy-file" />').attr('data-path', data.Files[f].FullPath).click(function() {
          ableDeployButton(); 
        }));          
        label.append('<i class="fa fa-fw fa-file-o text-primary" />');
        label.append(data.Files[f].Name);
        
        li.append(label);
        
        ul.append(li);                
      } 
    }
    
    
    $(container).append(ul);
  }; 
  
  
  $('#select_all_button').click(function() {
    $('#deploy_files .poll-deploy-file:checkbox').prop('checked', true);
    ableDeployButton(); 
  });
  
  
  $('#deploy_button').click(function() {
    var filesToDeploy = [];
    
    $('#deploy_files :checkbox:checked').each(function() {
      filesToDeploy[filesToDeploy.length] = $(this).attr('data-path');
    });
    
    //alert(JSON.stringify(filesToDeploy));
    devdeploy.DeployFiles(filesToDeploy, function() {
       $('#deploy_files').html('<i class="fa fa-spin fa-2x text-danger fa-refresh" />').trigger('digifi.refresh');
    }, alert);
  
  });
  
});