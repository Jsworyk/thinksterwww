var rectangle = null;
var map = null;
var marker = null;
var map_loaded = false;
var initialLocation = {lat: 44.5452, lng: -78.5389};

$(document).ready(function() {
  var qRefreshTimerId = null;
  
  $('#question_details').bind('digifi.load-complete', function() {  
    $('#at_description').popover();    
  });
  
  var qRefresh = function() {
     clearTimeout(qRefreshTimerId);
     
     qRefreshTimerId = setTimeout(function() {
        $('#question_list').trigger('digifi.refresh'); 
     }, 300);
  };
  
  $('#f_question').keydown(qRefresh).change(qRefresh);
  $('#f_showinactive').change(qRefresh);
  $('#f_categoryid').change(qRefresh);  


  var saveFilter = function() {
    thinkster.CurrentPage.PollFilters = [];      
    getFilterObject($('.poll-filter-container-top'), thinkster.CurrentPage.PollFilters);  
    $('#question_filters, #question_filters_sql').trigger('digifi.refresh'); 
  };
  
  var checkCanActivate = function() {
    var canActivate = true;
    var errors = [];
   
    if($('#answers tr').length < 2 && $('#answer_type_code').val() != 'FO') {
      canActivate = false;
      errors[errors.length] = "You must provide at least two choices for a question to be valid."; 
    }
   
    if($('#question_filters .poll-filter-value.btn-outline-danger').length > 0) {
      canActivate = false;
      errors[errors.length] = "There are one or more errors with your filters in Question Access. Please correct them.";
    }       
   
    if(errors.length == 0) {
      $('#activate_errors').hide();       
    } else {
      $('#activate_errors ul li').remove();
      
      for(var e = 0; e < errors.length; e++) {
        $('#activate_errors ul').append($('<li />').text(errors[e])); 
      }
    
      $('#activate_errors').show(); 
    }
   
    $('#activate_question').prop('disabled', !canActivate);
  };
  
  $('#answers, #question_filters').bind('digifi.load-complete', checkCanActivate);
  
  var createUrlSegment = function() {
    var value = $('#aq_question').val().trim();
    $('#aq_urlsegment').val(encodeURIComponent(value.toLowerCase().replace(/[\']+/gi, '').replace(/[^a-z0-9_-]+/gi, '_'))); 
  };
  
  
  $('#aq_question').change(createUrlSegment).keydown(createUrlSegment);
  
  $('#answers').bind('digifi.action', function(e, a) {
    
    switch(a.action) {
      case 'SetImage':
        $('.upload-step-1').attr('hidden', null);
        $('.upload-step-2').html('').attr('hidden', '');
      
        $('#recent_uploads').trigger('digifi.refresh');                    
       
        $('#image_uploader .modal-footer > .btn-success').prop('disabled', true);
        
        $('#iu_poll_choice_id').val(a.params);
        
        $('#image_uploader').modal('show');       
      break;           
    }
  }); 
  
  $('#image_uploader').bind('digifi.save-complete', function() {
    $('#answers').trigger('digifi.refresh');
  });


  $('#edit_filter_map').on('shown.bs.modal', function() {
    var bounds = {
          north: 44.55,
          south: 44.53,
          east: -78.50,
          west: -78.55
    };
    
    try {
      bounds = jQuery.parseJSON($('#filter_map_value').val());     
    } catch(ex) { 
    }          
    
    if(!map_loaded) {
      initMap();                            
    } 
  
    if(rectangle) rectangle.setMap(null);
    
   
    map.setCenter({lat: bounds.north + (bounds.south - bounds.north) / 2, lng: bounds.east + (bounds.west - bounds.east) / 2});
    
    
    rectangle = new google.maps.Rectangle({
      bounds: bounds,
      editable: true,
      draggable: true          
    });
    
    rectangle.setMap(map);   
    
    google.maps.event.addListener(rectangle, 'bounds_changed', function() {
      $('#filter_map_value').val(JSON.stringify(rectangle.bounds));      
      /*rectangle.setEditable(false);
      marker.setAnimation(google.maps.Animation.BOUNCE);
      
      adminevents.SetGeoBoundary(rectangle.bounds, function() {                        
        setTimeout(function() {
          marker.setAnimation(null); 
        }, 850);          
      }, function(result) {
        alert(result);
        marker.setAnimation(null);                   
      }, function() {
        rectangle.setEditable(true);
      });   */
    });     
  });

  $('#question_filters').bind('digifi.action', function(e, a) {
    switch(a.action) { 
      case 'AddFilter':
        $('#add_filter').data('source', a.element).modal('show');
      break;
      
      case 'ChangeCondition':   
        $('#edit_condition_operator').find(':input,button').prop('disabled', false);     
        $('#edit_condition_operator :radio[value="' + $(a.element).parents('.poll-filter-op').first().data('op') + '"]').prop('checked', true);
        $('#edit_condition_operator').data('op', $(a.element).parents('.poll-filter-op').first()).modal('show');
      break;
      
      case 'EditFilterValue':        
        
        $('#efv_geofence').css('display', 'none');
        
        switch($(a.element).data('value-type'))
        {
          case 'geofence':            
            $('#efv_geofence').css('display', 'block');
            
            $('#edit_filter_map').find(':input,button').prop('disabled', false);
            
            $('#filter_map_value').val($(a.element).attr('data-filter-value'));
            
            $('#edit_filter_map').data('field', a.element).modal('show');                        
            
          break;
        
          case 'search':            
            $('#filter_value_search').find(':input,button').prop('disabled', false);
            $('#filter_value_search label:nth-child(2)').text($(a.element).data('filter-key'));
            
            $('#fvs_current').html($('<div class="alert alert-info"><h5 class="alert-heading">Current Value:</h5></div>').append($('<p />').html($(a.element).text())));
            
            $('#fvs_results').html('');
            $('#fvs_searchtext').val('');
            
            $('#filter_value_search').data('field', a.element).modal('show');
          break;
          
          default:
            $('#edit_filter_value').find(':input,button').prop('disabled', false);
            $('#edit_filter_value label').text($(a.element).data('filter-key'));
          
            $('#efv_value').attr('type', $(a.element).data('value-type')).val($(a.element).text());
        
            $('#edit_filter_value').data('field', a.element).modal('show');
          break;
        }                                       
      break;
      
      case 'EnableFilters':      
        thinkster.CurrentPage.PollFilters = [{Op: 'And', Filters: []}];
        
        $('#question_filters, #question_filters_sql').trigger('digifi.refresh');
      break;
      
      case 'RemoveFilter':
        if(!confirm('Are you sure you want to remove this filter?')) return;
        
        $(a.element).parents('.poll-filter-op').first().remove();
        
        saveFilter();
      break;
    }
  });
  
  var filterSearch = function() {
    var searchText = $('#fvs_searchtext').val().trim();
                 
    if(searchText == '') {
    
      $('#fvs_results').html('<p class="text-info text-center">You must enter something to search for</p>'); 
    } else { 
      var field = $('#filter_value_search').data('field');
    
      $('#fvs_results').html('<p class="text-center"><i class="fa fa-spin text-danger fa-refresh"></i> Searching...</p>');
      adminquestions.FilterSearch($(field).parent().attr('data-id'), $(field).attr('data-filter-key'), searchText, function(json) {
        var results = jQuery.parseJSON(json);
        
        if(results.length == 0) {  
          $('#fvs_results').html('<p class="text-warning text-center">There were no results to your search</p>');      
        } else { 
          list = $('<div class="list-group mt-2"></div>');
          
          for(var r = 0; r < results.length; r++) {
            var item = $('<button class="list-group-item list-group-item-action"></button>');
            
            item.attr('data-id', results[r].Value);
            item.text(results[r].Text);
            
            item.click(function() {
              //alert($(this).attr('data-id'));
              $(field).attr('data-filter-value', $(this).attr('data-id'));
              $(field).text($(this).text());
              saveFilter();  
              $('#filter_value_search').modal('hide'); 
            });
            
            list.append(item); 
          }
          
          $('#fvs_results').html($('<div class="mt-2">Select an Option</div>')).append(list);
        }         
      }, alert);
      
    }
        
  };
  
  $('#fvs_searchtext').digifiEnterKey(filterSearch);
  $('#filter_search_button').click(filterSearch);
  
  $('#edit_filter_map').bind('digifi.action', function(e, a) {
  
    if(a.action == 'Save') { 
      var val = $('#filter_map_value').val().trim();
      
      if(val == '') {
        alert('You must provide a value');
        $('#edit_filter_map').find(':input,button').prop('disabled', false);
        $('#filter_map_value').focus();
        return;         
      }
      
      $('#edit_filter_map').find(':input,button').prop('disabled', false);
      
      $($('#edit_filter_map').data('field')).attr('data-filter-value', val).text('saving...');
      saveFilter();      
      $('#edit_filter_map').modal('hide');      
    } 
  });
  
  $('#edit_filter_value').bind('digifi.action', function(e, a) {
  
    if(a.action == 'Save') { 
      var val = $('#efv_value').val().trim();
      
      if(val == '') {
        alert('You must provide a value');
        $('#edit_filter_value').find(':input,button').prop('disabled', false);
        $('#efv_value').focus();
        return;         
      }
      
      $('#edit_filter_value').find(':input,button').prop('disabled', false);
      
      $($('#edit_filter_value').data('field')).attr('data-filter-value', val).text(val);
      saveFilter();      
      $('#edit_filter_value').modal('hide');      
    } 
  });
  
  $('#edit_condition_operator').bind('digifi.action', function(e, a) {
  
    if(a.action == 'Save') { 
      var op = $('#edit_condition_operator').data('op');
      var label = 'All';
      
      op.data('op', $('#edit_condition_operator :radio:checked').val()); 
      
      switch(op.data('op')) {
         case 'Or':
          label = 'Any';
         break;
         
         case 'None':
          label = 'None';
         break;
      }
      
      op.find('h4 > button').text(label);
      
      saveFilter();
    
      $('#edit_condition_operator').modal('hide');
    } 
  });
  
  $('#add_question').bind('digifi.save-complete', function(e, result) {
    alert(result); 
  });
  
  $('.colorpicker-default').colorpicker();
  
  var getFilterObject = function(parent, filterObject) {
    parent.children('.poll-filter-op').each(function() {
      var op = {Op: $(this).data('op')};
      
      filterObject[filterObject.length] = op;
    
      switch($(this).data('op')) { 
        case 'And':
        case 'Or':
        case 'None':
          op.Filters = [];
          
          getFilterObject($(this).children('.poll-filter-container'), op.Filters);                              
        break;
        
        default:
          op.FilterId = $(this).data('id');
          
          op.Values = {};
          
          $(this).children('.poll-filter-value').each(function() {
            op.Values[$(this).data('filter-key')] = $(this).attr('data-filter-value') || $(this).text();
          });
        break;
        
      } 
    });
       
  };
  
  
  $('#add_filter').bind('digifi.action', function(e, a) {
    if(a.action == 'AddFilter'){
      
      var div = $('<div class="poll-filter-op">Loading filter information...</div>');      
      
      switch(a.params[0]) {
        case 'And':
        case 'Or':
        case 'None':
          div.attr('data-op', a.params[0]);
        break;
        
        default:
          div.attr('data-op', 'Filter');
          div.attr('data-id', a.params[0]);
        break;                 
      }
      
      //var container = $($('#add_filter').data('parent')).parents('.poll-filter-container').first();           
      
      $($('#add_filter').data('source')).parent().find('> .poll-no-filters').remove();
                       
      $($('#add_filter').data('source')).parent().before(div);
      
      $('#add_filter').modal('hide');      
      
      saveFilter();  
    } 
  });
  
 
  $('.aq-attribute-toggle').click(function() {
    var target = $(this);
    
    target.prop('disabled', true).find('.fa').addClass('fa-spin');
    
    adminquestions.ToggleAttribute($(this).data('aq-attribute'), function(result) {
       target.prop('disabled', false).find('.fa').removeClass('fa-spin' + (result ? ' fa-ban text-danger' : ' fa-check-circle text-success')).addClass((!result ? 'fa-ban text-danger' : 'fa-check-circle text-success'));
    }, alert);
    
  });
  
  $('#foe_button').click(function() {
    $('#edit_foe_value')[0].digifi.editObject(1);
  });
  
  $('#edit_foe_value').bind('digifi.save-complete', function(e, result) {
    $('#foe_expiry').text(result);    
  });
  
  $('#image_upload').fileupload({
      dataType: 'json',
      add: function(e, data) {
        adminquestions.GetUploadToken(function(result) {
          //alert('file added. Add token ' + result + '? Auto submit!');
          data.formData = {token: result};
          data.submit(); 
        }, alert);         
      },
      done: function (e, data) {
          /*$.each(data.result.files, function (index, file) {
              $('<p/>').text(file.name).appendTo(document.body);
          });*/
          //alert(JSON.stringify(data.result));
          
          $('.upload-step-1').attr('hidden', 'hidden');
          $('.upload-step-2').attr('hidden', null);
          
          var row = $('<div class="row" />');
          var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
          var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result[0].Name).attr('src', data.result[0].ThumbnailUrl));
          var block = $('<div class="card-block"></div>');
          
          block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result[0].MediaId));
          //block.append($('<input type="hidden" data-digifi-value="ProfileAccountId" />').val($('#avatar_uploader').data('profileaccountid')));
          
        
          block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result[0].Name)));
          block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result[0].Description)));
          block.append($('<div class="form-group"></div>').append($('<select class="form-control" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result[0].ExposureTypeCode)));
          
          row.append(col);
          col.append(card);
          
          card.append(block);                   
          
          $('.upload-step-2').html(row);
          
          $('#iu_existing').val(null);
          
          $('#image_uploader .modal-footer > .btn-success').text('Post').prop('disabled', false);
      }
  });
  
  checkCanActivate(); 
});

thinkster.transform.recentImages = function(container, data) {  
  if(data.Images.length == 0) {
    $(container).html('<div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>you have not uploaded any images yet</p></div></div>');
  } else {
    
    var row = $('<div class="row m-1" />');
    
    for(var x = 0; x < data.Images.length; x++) {
      var col = $('<div class="col-lg-4 col-sm-3"></div>');
      var imageDiv = $('<div class="mt-2 text-center" />');
      var img = $('<img width="220" height="200" />').data('id',  data.Images[x].MediaId).attr('src', data.Images[x].ThumbnailUrl).attr('title', data.Images[x].Name + ' - ' + data.Images[x].Description);
      
      imageDiv.append(img);
      
      img.click(function() {
        $(container).find('img.rounded').removeClass('rounded poll-selected');
        $(this).addClass('rounded poll-selected');
        
        $('#iu_existing').val($(this).data('id'));
        
        $('#image_uploader .modal-footer > .btn-success').text('Change').prop('disabled', false);
      }); 
      
      col.append(imageDiv);
      
      row.append(col); 
    }
  
    $(container).html($('<p class="small text-center">...or select a recently-used image</p>')).append(row);
  }
};


function initMap() {                          
    
  //$('#event_location_details').trigger('digifi.refresh');
  map = new google.maps.Map(document.getElementById('location_map'), 
    {
      zoom:8,      
      center: initialLocation,
      mapTypeId: 'satellite'
    });     
  
  map_loaded = true;  

}