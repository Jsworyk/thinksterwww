var map = null; 

var resizeIFrame = function() {
  window.parent.iFrameSize(window, $('body').height()); 
};

$(document).ready(function() {
  var menuCounter = 0;
  var overlays = [];                   
  var filters = {Age: null, Gender: null };
  var currentLocation = {latitude: 49.899444, longitude: -97.139167};
  var liveUpdates = true;
  var liveLocation = false;
  var loader = $('<span class="p-1 alert alert-warning" style="font-size: 18px;"><i class="fa fa-refresh fa-spin fa-fw"></i> Refreshing Data...</span>');
  var idleTimeout = null;
  var question = jQuery.parseJSON($('#poll_object_json').text());
  
  resizeIFrame();   
  
  $('body').resize(function() { 
    resizeIFrame();
  });
  
  navigator.geolocation.getCurrentPosition(function(location) {
    currentLocation.latitude = location.coords.latitude; 
    currentLocation.longitude = location.coords.longitude;
    liveLocation = true;
  });

  $('.poll-question-choice').click(function() {
    //alert('You clicked ' + $(this).text() + ' (' + $(this).val() + ')' + ' with a privacy setting of ' + $('#visibility_selector input:checked').val());
    var answer = $(this).text();
    
    embed2.AnswerQuestion($(this).val(), $('#visibility_selector input:checked').val(),
      liveLocation ? currentLocation.latitude : null,
      liveLocation ? currentLocation.longitude : null,
      function(result) {
      if(result.Success) {
        if(!map) {
          initMap(); 
        } 
        
        $('#your_poll_answer').text('You answered: ' + answer);
      
        $('.poll-unanswered').attr('hidden', 'hidden');
        $('.poll-answered').attr('hidden', null);  
        setTimeout(resizeIFrame, 300);       
      } else {
        alert('Unable to record your answer: ' + result.Message); 
      }
      //alert(JSON.stringify(result));
    }, alert);  
  });  
  
  /*$('#data_test').click(function() { 
    var chart = $('.poll-chart-data').data('pie-chart');
    
    
    chart.data.datasets[0].data[data.Answer]++;
    chart.update();
    //alert(JSON.stringify(chart.data.datasets[0].data));
  }); */
  
  if(thinkster.connection.on) {    
    thinkster.connection.on("PollAnswer", function(data) {
      var chart = $('.poll-chart-data').data('pie-chart');
        
      chart.data.datasets[0].data[data.Answer]++;
      chart.update();
    });   
  }
  
  
  var refreshmap = function() {
    var omits = [];
    
    loader.show();
    
    for(var o = 0; o < overlays.length; o++) {
      if(map.getZoom() >= overlays[o].pollRegion.minZoom && map.getZoom() <= overlays[o].pollRegion.maxZoom) {
        overlays[o].setMap(map); 
      } else { 
        overlays[o].pollRegion.infowindow.close();
        overlays[o].setMap(null);
      }
    
      omits[omits.length] = overlays[o].pollRegion.id; 
    }
    
    embed2.GetMapData(map.getBounds(), map.getZoom(), 'GE', omits, filters, function(json) {
      //console.log('Result: ' + json);
    
      var results = jQuery.parseJSON(json);
      var obj = results.MapData;
      
      console.log(obj);
      
      for(var p = 0; p < obj.length; p++) {         
        var fillColor = '#444444', strokeColor = '#222233';
        var name = obj[p].Name;
        var bestAnswerIndex = 0;
        var tied = false;  
       
        for(var i = 0; i < obj[p].Polygons.length; i++) {  
          var ptsArray = [];
        
          for(var pt = 0; pt < obj[p].Polygons[i].length; pt+=2) {
            ptsArray.push(new google.maps.LatLng(obj[p].Polygons[i][pt],obj[p].Polygons[i][pt + 1]));
          }
          
          overlays[overlays.length] = new google.maps.Polygon({
            paths: ptsArray,
            //strokeColor: strokeColor,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            //fillColor: fillColor,
            fillOpacity: 0.5
          });
          
          var index = overlays.length - 1;
          
          overlays[index].pollRegion = { id: obj[p].Id, 
                name: obj[p].Name, 
                results: obj[p].Results, 
                overlay: overlays[index],
                responses: obj[p].Responses, 
                minZoom: obj[p].MinZoom,
                maxZoom: obj[p].MaxZoom,
                chart: null,
                setColor: function() {
                  var fillColor = "#444444", strokeColor = "#222233";
                  var bestAnswerIndex = 0;
                  var tied = false;
                  
                  for(var pc = 1; pc < this.results.length; pc++) {
                    if(this.results[pc] > this.results[bestAnswerIndex]) {
                      bestAnswerIndex = pc;
                      tied = false;
                    } else if (this.results[pc] == this.results[bestAnswerIndex]) {
                      tied = true; 
                    }
                  }
                  
                  if(tied && this.results[bestAnswerIndex] != 0) {
                    fillColor = "#ffffff";
                    strokeColor = "#aaaadd";
                  } else if(this.results[bestAnswerIndex] != 0) {
                    fillColor = question.Choices[bestAnswerIndex].ChartColour;
                    strokeColor = question.Choices[bestAnswerIndex].GradientDark;
                  } 
                  
                  //console.log("setting color to " + fillColor + ', ' + this.overlay.pollRegion.id);
                  
                  this.overlay.setOptions({fillColor: fillColor});  
                     
                  }
              };
              
          overlays[index].pollRegion.setColor();
          
          //overlays[index].pollRegion.infowindow = new google.maps.InfoWindow({content: '<strong>' + obj[p].Name + '</strong><br /><div class="text-center">' + obj[p].Responses + ' Response' + (obj[p].Responses == 1 ? '' : 's') + '</div><canvas id="poll_region_marker_' + obj[p].Id + '" width="96" height="96"></canvas>' });
                           
          
          overlays[index].pollRegion.infowindow = new google.maps.InfoWindow({content: 'test' });
          
          //overlays[index].pollRegion.infowindow.mapRegionId = obj[p].Id;
          
          google.maps.event.addListener(overlays[index].pollRegion.infowindow, 'domready', function() {
            //console.log('testing: ' + this.mapRegionId);
            
            //this.setContent('<strong>' + obj[p].Name + '</strong><br /><div class="text-center">' + obj[p].Responses + ' Response' + (obj[p].Responses == 1 ? '' : 's') + '</div><canvas id="poll_region_marker_' + obj[p].Id + '" width="96" height="96"></canvas>');
            //this.setContent('asd');
          });               
                   
        
          overlays[overlays.length - 1].addListener('click', function(event) {
            this.pollRegion.infowindow.setPosition(event.latLng);
            
            //Hide all other overlays:
            //Remove this to keep multiple overlays open
            for(var o = 0; o < overlays.length; o++) {              
              overlays[o].pollRegion.infowindow.close();               
            }
            
            //Create chart if it doesn't exist yet:
            //var ctx = document.getElementById("myChart");            
            if(!this.pollRegion.chart) {
              //var data = [];
              var color = [];
              var hover = [];
              var labels = [];
              
              var content = $('<div style="min-width: 120px; white-space: nowrap;"></div>').append($('<strong />').html(this.pollRegion.name), $('<div class="text-center poll-response-count"></div>').html(this.pollRegion.responses + ' Response' + (this.pollRegion.responses == 1 ? '' : 's')));
              var canvas = $('<canvas width="96" height="96" style="margin-left: auto; margin-right: auto;"></canvas>');
              
              content.append(canvas);
                
              for(var x = 0; x < this.pollRegion.results.length; x++) {
                labels[labels.length] = question.Choices[x].Text;
                //data[data.length] = this.pollRegion.results[x];
                color[color.length] = question.Choices[x].ChartColour; 
                hover[hover.length] = question.Choices[x].GradientDark;
              }     
                    
                
                  //alert(document.getElementById("poll_region_marker_" + this.pollRegion.id));
              this.pollRegion.chart = new Chart(canvas, {
                  type: 'pie',              
                  data: { 
                    labels: labels,
                    datasets: [{ data: this.pollRegion.results,
                    backgroundColor: color
                    }]
                  },
                  options: {
                       legend: {
                          display: false
                       }
                  }  
              });                       
              
              this.pollRegion.infowindow.setContent(content[0]);
              
              //console.log(this.pollRegion.infowindow.getContent()); 
            }
                     
            this.pollRegion.infowindow.open(map); 
          });
        
          overlays[overlays.length - 1].setMap(map); 
        }    
      }
      
         
    }, function(e) { 
      $("#map_debug").text(JSON.stringify(e));
    }, function() {
      loader.hide(); 
    });
    
  };
  
  var resetMap = function() {
     //Clear all previous overlays:
    for(var o = 0; o < overlays.length; o++) {
      overlays[o].setMap(null);
      overlays[o] = null; 
    } 
    
    overlays = [];
    
    var filterText = '';
    
    $('#map_buttons .dropdown-item.active').each(function() {
      if(filterText != '') filterText += ', ';  
      filterText += $(this).text();
    });
    
    if(filterText != '') {
      liveUpdates = false;
       
      clearButton = $('<button type="button" class="btn btn-danger btn-sm ml-2"><i class="fa fa-ban fa-fw"></i> Remove all Filters</button>');
      
      clearButton.click(function() {  
        filters.Age = null;
        filters.Gender = null;
        
        $('#map_buttons .dropdown-toggle.text-success').removeClass('text-success');
        $('#map_buttons .dropdown-item.active').removeClass('active');
        
        resetMap();
      });
    
      $('#filter_details').html('Filtered to ' + filterText + '.<br />Live updates disabled when filtering.').append(clearButton);
    } else {
      liveUpdates = true;
      $('#filter_details').html('');
    }
    
    refreshmap(); 
  };

  var createMapButtons = function(container, buttons, filter) {
    for(var x = 0; x < buttons.length; x++) {
      if(buttons[x].Children) { 
        var ddgroup = $('<div class="btn-group" role="group"></div>');
        var ddbtn = $('<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>').attr('id', 'gmap_cust_b_' + menuCounter);
        var ddmenu = $('<div class="dropdown-menu"></div>').attr('aria-labelledby', 'gmap_cust_b_' + menuCounter);
       
        ddbtn.attr('title', buttons[x].Text);
        ddbtn.append($('<i />').attr('class', buttons[x].Icon).addClass('mr-1'));
        
        ddgroup.append(ddbtn, ddmenu); 
        
        container.append(ddgroup);
        
        //Create "Don't" filter:
        var btn = $('<button type="button" class="btn btn-sm btn-secondary dropdown-item"></button>');
        
        btn.attr('title', "Don't " + buttons[x].Text);
        btn.append($('<i />').attr('class', 'fa fa-ban fa-fw text-danger mr-1'), "Don't " + buttons[x].Text);
        //btn.click(buttons[x].Click);               
        
        btn.on('click', null, buttons[x].Filter, function(e) {
          filters[e.data] = null;
          
          $(this).parents('.btn-group').first().find('.btn.dropdown-toggle').removeClass('btn-success fa-2x').addClass('btn-secondary');
                              
          $(this).parent().find('.active').removeClass('active');          
          
          resetMap(); 
        });
        
        ddmenu.append(btn);
        
        createMapButtons(ddmenu, buttons[x].Children, buttons[x].Filter);
      
      } else {
        var btn = $('<button type="button" class="btn btn-secondary"></button>');
        
        btn.attr('title', buttons[x].Text);
        btn.append($('<i />').attr('class', buttons[x].Icon).addClass('mr-1'));
        //btn.click(buttons[x].Click);
        
        btn.on('click', null, buttons[x], function(e) {          
          if(!filter && e.data()) return;
                    
          filters[filter] = {Id: e.data.Id, Parameters: e.data.Parameters };
          
          $(this).parents('.btn-group').first().find('.btn.dropdown-toggle').removeClass('btn-secondary').addClass('btn-success fa-2x');
          
          $(this).parent().find('.active').removeClass('active');
          $(this).addClass('active');
          
          resetMap(); 
        });
        
        /*var fn = buttons[x].Click; 
        alert(fn);
        btn.click(function() { alert(fn); fn.call(this); resetMap(); });*/
        
        if(container.is('.dropdown-menu')) {
          btn.addClass('dropdown-item');
          btn.append(buttons[x].Text);
        }
        
        container.append(btn);
      }
      
      menuCounter++;     
    }
   
  };


  var initMap = function() {
    var customButtons = [
      {Text: 'Return to your primary location', Icon: 'fa fa-home', Click: function() {
        map.panTo( {lat: currentLocation.latitude, lng: currentLocation.longitude} );
        return true;
      }}].concat(mapFilters); 
      
      
      /*,      
      {Text: 'Filter on Gender', Icon: 'fa fa-venus-mars', Filter: 'Gender', Children: [
        {Text: 'Men Only', Icon: 'fa fa-male fa-fw', Click: function() {
          filters.Gender = {Id: 5 };   
        }},
        {Text: 'Women Only', Icon: 'fa fa-female fa-fw', Click: function() {
          filters.Gender = {Id: 6 }; 
        }}
      ]},
      {Text: 'Filter on Age', Icon: 'fa fa-birthday-cake', Filter: 'Age', Children: [
        {Text: 'Ages 15-25', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {          
          filters.Age = {Id: 4, Parameters: {X: 15, Y: 25} };           
        }},
        {Text: 'Ages 25-35', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 4, Parameters: {X: 25, Y: 35} };          
        }},
        {Text: 'Ages 35-45', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 4, Parameters: {X: 35, Y: 45} };          
        }},
        {Text: 'Ages 45-55', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 4, Parameters: {X: 45, Y: 55} };          
        }},
        {Text: 'Ages 55+', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 2, Parameters: {Age: 55} };        
        }}
      ]} 
    ];        */
    
   
    map = new google.maps.Map(document.getElementById('poll_map'), 
    {
      zoom: 12,      
      center: {lat: currentLocation.latitude, lng: currentLocation.longitude},
      /*center: {lat: <?=$location[0]->Latitude / 10000.00; ?>, lng: <?=$location[0]->Longitude / 10000.00; ?>},  */
      mapTypeId: 'roadmap',
      styles: [{"featureType":"all","elementType":"all","stylers":[{"hue":"#e7ecf0"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#636c81"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#636c81"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#ff0000"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#f1f4f6"}]},{"featureType":"landscape","elementType":"labels.text.fill","stylers":[{"color":"#496271"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-70}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#c6d3dc"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#898e9b"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3eaf8"}]}]
    });
    
                  
    var centerControlDiv = document.createElement('div');
    var buttonGroup = $('<div class="btn-group" style="margin: 0 auto;" role="group" aria-label="Poll filtering functions"></div>'); 
    
    $(centerControlDiv).append(loader);
    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

    
    createMapButtons(buttonGroup, customButtons);
    
    $('#map_buttons').append(buttonGroup);              
    
    //Bind to the maps idle event:
    google.maps.event.addListener(map, 'idle', function(ev){      
        if(idleTimeout) window.clearTimeout(idleTimeout);
        
        idleTimeout = window.setTimeout(function() { idleTimeout = null; refreshmap(); }, 700);
        
    });
    
    google.maps.event.addListenerOnce(map, 'bounds_changed', function() {
     window.clearTimeout(idleTimeout);
     idleTimeout = null;
   });
    
    thinkster.connection.on("PollAnswer", function(data){    
      if(!liveUpdates) return;
      for(var x = 0; x < data.Regions.length; x++) {
        for(var o = 0; o < overlays.length; o++) {
          if(overlays[o].pollRegion.id == data.Regions[x]) {          
            var total = 0;                   
          
            overlays[o].pollRegion.responses++;
            overlays[o].pollRegion.results[data.Answer]++;
            overlays[o].pollRegion.setColor();
            
            for(var r = 0; r < overlays[o].pollRegion.results.length; r++) {
              total += overlays[o].pollRegion.results[r];  
            } 
                      
            $(overlays[o].pollRegion.infowindow.getContent()).find('.poll-response-count').html(total + ' Response' + (total == 1 ? '' : 's'));
            if(overlays[o].pollRegion.chart) {
              overlays[o].pollRegion.chart.update();
            } else {             
            }                    
          } 
        } 
      }    
    }); 
    
  };

  var getFilter = function() {
    var node = null;
  
    switch($(this).data("poll-filter")) {
      case 'gender':
        node = $('<ul class="list-group m-0"><li class="list-group-item m-0">Off</li><li class="list-group-item m-0">Male</li><li class="list-group-item m-0">Female</li></ul>');
      break;
      
      default:
        node = $('<ul><li>Testing</li><li>testing again</li></ul>');
      break; 
    }
  
    return node; 
  };
  
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    if($(e.target).attr('href') == '#poll_map_tab') {      
      if(!map) {      
        initMap(); 
      }       
    }
  });
  
  
  //{"datasets":[{"data":[24,77,47]}],"labels":["Boxers","Briefs","None - I prefer to go Commando!"]  }
  var pc = 0;
  $('.poll-chart-data').each(function() {
    var data = jQuery.parseJSON($(this).text());
    
    var size = $(this).is('.poll-full-size') ? '100%' : '250px';
     
    var chartContainer = $('<div class="chart-container" style="position: relative; height:auto; width:' + size + '"></div>');
    var canvas = $('<canvas id="something' + pc + '" style="background-color: #fff; width: ' + size + '; height: 400px;"></canvas>');
    
    pc++;
    
    chartContainer.append(canvas);
    $(this).parent().append(chartContainer);
    
    var ctxP = canvas[0].getContext('2d');        
    
    //var myChart = new Chart(ctxP, { type: 'pie', data: data }, options: {responsive: true });
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: data,
        options: {
            responsive: true,
            legend: { position: 'bottom' }
        }    
    });
    
    $(this).data('pie-chart', myPieChart);
    
    resizeIFrame();    
       
  }); 
});

thinkster.transform.FriendResponses = function(context, data) {
  for(var x = 0; x < data.Responses.length; x++) {
    var response = data.Responses[x]; 
    var item = $(context).find('div[data-profile-id=' + response.ProfileId + ']');
    
    var replies = $('<div class="poll-comments-replies" data-digifi-source="master.GetTargetComments" data-digifi-json-transform="thinkster.transform.comments" />').attr('data-poll-responseid', response.PollResponseId);    
    var commentLink = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
    var react = $('<span title="React to this response" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', response.ReactionCount).attr('data-poll-targettype', 'RE').attr('data-poll-targetid', response.PollResponseId);
    
    react.react(response.ReactionText, response.Emoticon);
    
    if(item.length == 0) { 
      item = $('<div class="media mb-3"></div>').attr('data-profile-id', data.Responses[x].ProfileId);
      
      if(data.Responses[x].AvatarThumbnailUrl) {
        item.append($('<img class="d-flex mr-3 rounded-circle" width="64" height="64" style="border-radius: 50%;">').attr('src', data.Responses[x].AvatarThumbnailUrl).attr('alt', data.Responses[x].DisplayName));
      } else {
        item.append($('<i class="d-flex mr-3 fa fa-fw fa-user fa-4x rounded-circle" style="width: 64px; height: 64px;" />')); 
      }
      var body = $('<div class="media-body"></div>');      
      body.append($('<span class="small text-muted pull-right" />').append($('<time class="timeago" />').attr('datetime', data.Responses[x].ResponseDate).text(data.Responses[x].ResponseDate)));
      body.append($('<h5 class="mt-0" />').html($('<a target="_top" />').attr('href', '/profile/' + data.Responses[x].ProfileName).text(data.Responses[x].DisplayName)));
      body.append(data.Responses[x].Choice, '<br />', react, commentLink, replies);       
      item.append(body);   
      
      item.thinksterInitialize();
         
    } else {
      item.remove(); 
    }
    
    $(context).prepend(item);
  }

  //$(context).html(JSON.stringify(data)); 
};