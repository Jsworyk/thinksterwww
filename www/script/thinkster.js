var thinkster = thinkster || {};

(function ($, thinkster) {
  jQuery.timeago.settings.allowFuture = true;

  var socket;
  
  thinkster.CurrentUser = {};
  thinkster.CurrentPage = {};

  thinkster.fn = {};    
  
  // Start of /var/pollsite/prod/inc/thinkster.js/functions/avatar.js
  
  /*******************************************************************************
  
   * avatar
  
   *
  
   * Good programmers would write comments about what this about
  
   *
  
   ******************************************************************************/
  
  thinkster.fn.avatar = function(avatarUrl) {
  
    if(!avatarUrl) {
  
      return "/images/default_avatar.png";    
  
    }
  
    
  
    return avatarUrl;
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/functions/avatar.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/functions/cancelFriendshipRequest.js
  
  /*******************************************************************************
  
   * cancelFriendshipRequest
  
   *
  
   * Good programmers would write comments about what this about
  
   *
  
   ******************************************************************************/
  
  thinkster.fn.cancelFriendshipRequest = function(profileId, onSuccess, onFailure, onComplete) {
  
    master.CancelFriendRequest(profileId, onSuccess, onFailure, onComplete);
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/functions/cancelFriendshipRequest.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/functions/confirmFriendshipRequest.js
  
  /*******************************************************************************
  
   * confirmFriendshipRequest
  
   *
  
   * Good programmers would write comments about what this about
  
   *
  
   ******************************************************************************/
  
  thinkster.fn.confirmFriendshipRequest = function(profileId, onSuccess, onFailure, onComplete) {
  
    master.ConfirmFriendRequest(profileId, onSuccess, onFailure, onComplete);
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/functions/confirmFriendshipRequest.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/functions/formatText.js
  
  /*******************************************************************************
  
   * formatText
  
   *
  
   * Takes text with Thinkster markup and converts it to links
  
   *
  
   ******************************************************************************/
  
  thinkster.fn.formatText = function(text, containerElement) {
  
    var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  
    var match;
  
    var lastIndex = 0;
  
    
  
    while ((match = re.exec(text)) !== null) {
  
      if(match.index > lastIndex) {
  
        containerElement.append(text.substring(lastIndex, match.index));
  
        //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
  
      } 
  
      
  
      //text += match[3];
  
      var anchor = $('<a />').html(match[3]);
  
      
  
      switch(match[1]) {
  
        case 'e':
  
          anchor.attr('href', '/events/' + match[2]);
  
        break;
  
      
  
        case 'u':
  
          anchor.attr('href', '/profile/' + match[2]);
  
        break;
  
        
  
        case 'p':
  
          anchor.attr('href', '/poll/' + match[2]);
  
        break; 
  
        
  
        case 'm':
  
          anchor.attr('href', '/media/' + match[2]);
  
        break;
  
      }
  
      
  
      containerElement.append(anchor);
  
      
  
      //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
  
      
  
      lastIndex = re.lastIndex;            
  
    }
  
    
  
    if(text.length > lastIndex) {
  
      containerElement.append(text.substring(lastIndex)); 
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/functions/formatText.js
  

  thinkster.transform = {};
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/cardTimeline.js
  
  thinkster.transform.cardTimeline = function(container, data) {
  
    if($(container).find('div').length == 0) $(container).html('');
  
    
  
    
  
    if($(container).find('div').length == 0 && data.Items.length == 0) $(container).html('<div class="m-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have not answered any polls yet! <a class="alert-link" href="/">Answer polls</a> so that your answers can be displayed here!</p></div></div>');
  
  
  
    $(container).find('.poll-load-more').remove();
  
  
  
    var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  
    var match;
  
    var lastTimestamp = 0;
  
  
  
    for(var x = 0; x < data.Items.length; x++) {
  
  	//var card = $('<div class="card border-0">');
  
  	//var cardBlock = $('<div class="card-block border-0" />');
  
      var item = $('<div class="media mb-4" style="font-size: 1.6rem;" />');
  
      var body = $('<div class="media-body d-flex align-self-center" />');
  
      var text = $('<div />');
  
      var lastIndex = 0;
  
  
  
      lastTimestamp = data.Items[x].Timestamp;
  
  
  
      if(data.Items[x].Image) {
  
        item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" width="64" height="64" />').attr('src', data.Items[x].Image));
  
      } else if(data.Items[x].Icon) {
  
        item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x" />').addClass(data.Items[x].Icon));
  
      } else {
  
        item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x fa-info-circle" />'));
  
      }
  
  
  
      item.append(body);
  
  
  
      while ((match = re.exec(data.Items[x].Text)) !== null) {
  
        if(match.index > lastIndex) {
  
          text.append(data.Items[x].Text.substring(lastIndex, match.index));
  
          //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
  
        }
  
  
  
        //text += match[3];
  
        var anchor = $('<a />').html(match[3]);
  
  
  
        switch(match[1]) {
  
          case 'e':
  
            anchor.attr('href', '/events/' + match[2]);
  
          break;
  
          
  
          case 'u':
  
            anchor.attr('href', '/profile/' + match[2]);
  
          break;
  
  
  
          case 'p':
  
            anchor.attr('href', '/poll/' + match[2]);
  
          break;
  
          
  
          case 'm':
  
            anchor.attr('href', '/media/' + match[2]);
  
          break;
  
        }
  
  
  
        text.append(anchor);
  
  
  
        //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
  
  
  
        lastIndex = re.lastIndex;
  
      }
  
  
  
      if(data.Items[x].Text.length > lastIndex) {
  
        text.append(data.Items[x].Text.substring(lastIndex));
  
      }
  
  
  
      text.append('<br />', $('<span class="small text-muted"><i class="fa fa-fw fa-clock-o"></i></span>').append($('<time class="timeago" />').attr('datetime', data.Items[x].Created).text(data.Items[x].Created)));
  
  
  
      body.append(text);
  
  	//cardBlock.append(item);
  
  	//card.append(cardBlock);
  
  
  
      $(container).append(item, $('<hr />'));
  
  
  
      item.find('time.timeago').timeago();
  
    }
  
  
  
    if(data.Items.length > 0) {
  
  
  
       var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
  
       var loadMoreButton = $('<button type="button" class="btn btn-primary mb-2">Load More History</button>');
  
  
  
       loadMoreContainer.append(loadMoreButton);
  
  
  
       loadMoreButton.click(function() {
  
        $(container).trigger('digifi.refresh', {args: [lastTimestamp]});
  
        //alert('hi');
  
       });
  
  
  
       $(container).append(loadMoreContainer);
  
    }
  
  
  
  
  
    //$(container).html(data.ChatRooms.length);
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/cardTimeline.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/chart.js
  
  thinkster.transform.chart = function(container, data) {
  
    var myChart = new Chart(container, data);
  
  };                                                                                      
  // End of /var/pollsite/prod/inc/thinkster.js/transform/chart.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/chatRoomDropdown.js
  
  thinkster.transform.chatRoomDropdown = function(container, data) {
  
    var lg = $(container);
  
    
  
    /*<a class="dropdown-item" href="#"><div class="alert alert-info clearfix">
  
        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
  
        <div class="noti-info">
  
            <a href="#"> Server #1 overloaded.</a>
  
        </div>
  
    </div></a>*/
  
    
  
    
  
    /*if(lg.length == 0) {
  
      lg = $('<div class="list-group mx-0"/>');
  
      $(container).html(lg); 
  
    }   */
  
    
  
    if(lg.find('.poll-chat-room').length == 0) lg.html('');
  
    
  
    for(var x = 0; x < data.ChatRooms.length; x++) {
  
      var chatRoom = data.ChatRooms[x];
  
      var room = $(lg).find('.poll-chat-room[data-id=' + chatRoom.ChatRoomId + ']');               
  
                                                            
  
      if(room.length == 0) {
  
        //room = $('<div class="poll-chat-room list-group-item list-group-item-action p-1"><div class="media mr-auto"><img src="" class="d-flex mr-3" width="64" height="64" /> <div class="media-body"><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></div></div><div class="dropdown align-self-start"></div></div>');
  
        room = $('<a class="poll-chat-room dropdown-item p-1 mb-1"><img src="" class="pull-left mr-3" width="32" height="32" /><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></a>');
  
  
  
        room.attr('data-id', chatRoom.ChatRoomId);          
  
        
  
        if(chatRoom.Unread) {
  
          room.addClass('poll-chat-unread');         
  
        }
  
        
  
            
  
        room.data('poll-chatroom', chatRoom);
  
            
  
        room.find('img').attr('src', chatRoom.ChatRoomAvatar);
  
                
  
        lg.append(room);
  
      }
  
      
  
      room.attr('href', '/messages/' + chatRoom.LinkId);
  
      room.find('img').attr('src', thinkster.fn.avatar(chatRoom.ChatRoomAvatar));
  
      room.find('h6').html(chatRoom.RoomName);
  
      room.find('.poll-chatroom-preview').html(chatRoom.ChatPreview);              
  
    }
  
    
  
    var unreadCount = lg.find('.poll-chat-room.poll-chat-unread').length;
  
    $('#chat_top_menu').find('.badge').html(unreadCount == 0 ? '' : unreadCount);
  
    
  
  
  
    //$(container).html(data.ChatRooms.length); 
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/chatRoomDropdown.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/chatRooms.js
  
  thinkster.transform.chatRooms = function(container, data) {
  
    var lg = $(container).find('div.list-group');
  
    
  
    if(lg.length == 0) {
  
      lg = $('<div class="list-group mx-0"/>');
  
      $(container).html(lg); 
  
    }
  
    
  
    for(var x = 0; x < data.ChatRooms.length; x++) {
  
      var chatRoom = data.ChatRooms[x];
  
      var room = $(lg).find('.list-group-item[data-id=' + chatRoom.ChatRoomId + ']');               
  
                                                            
  
      if(room.length == 0) {
  
        room = $('<div class="poll-chat-room list-group-item list-group-item-action p-1"><div class="media mr-auto"><img src="" class="d-flex rounded-circle mr-3" width="64" height="64" /> <div class="media-body"><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></div></div><time class="align-self-start timeago mr-1 mt-1 badge badge-pill"></time><div class="dropdown align-self-start"></div></div>');
  
  
  
        if(chatRoom.Unread) { 
  
          room.addClass('poll-chat-unread');
  
        }
  
        
  
        room.find('time').attr('datetime', chatRoom.LastMessage).text(chatRoom.LastMessage);      
  
  
  
        room.attr('data-id', chatRoom.ChatRoomId);          
  
        
  
        if(thinkster.CurrentPage.ActiveChatRoomId == chatRoom.LinkId) {
  
          room.addClass('active');
  
          
  
          //If we're on the messaging page (or any page that supports chat interfacing right on the page), this needs to be called:
  
          if(thinkster.CurrentPage.createChatInterface)  thinkster.CurrentPage.createChatInterface(chatRoom);            
  
        }
  
        
  
        room.data('poll-chatroom', chatRoom);
  
        
  
        var button = $('<button class="btn p-0 dropdown-toggle" type="buton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-fw fa-gear text-muted"></i></button>');
  
        
  
        //button.click(function(e) { e.preventDefault(); e.stopPropagation(); });
  
        
  
        button.attr('id', 'ddb' + chatRoom.ChatRoomId); 
  
        
  
        room.find('.dropdown').append(button, $('<div class="dropdown-menu dropdown-menu-right" />'))/*.click(function(e) {
  
          e.stopPropagation(); 
  
          e.preventDefault();
  
          alert('hi');
  
        })*/; 
  
        
  
        room.find('.dropdown-menu').attr('aria-labelledby', 'ddb' + chatRoom.ChatRoomId);
  
        
  
        var ddMarkAsRead = $('<button class="dropdown-item" type="button">Mark as Read</button>');
  
        
  
        ddMarkAsRead.click(function(e) { e.preventDefault(); } );
  
        
  
        room.find('.dropdown-menu').append(ddMarkAsRead);
  
        
  
        var ddReportConvo = $('<button class="dropdown-item poll-flag-button" data-poll-type="Conversation" data-poll-targettype="CH" type="button">Report Conversation</button>');
  
        
  
        ddReportConvo.attr('data-poll-targetid', chatRoom.ChatRoomId);
  
        
  
        room.find('.dropdown-menu').append(ddReportConvo);   
  
        
  
        room.find('img').attr('src', thinkster.fn.avatar(chatRoom.ChatRoomAvatar));
  
                
  
        lg.append(room);
  
        
  
        room.thinksterInitialize();
  
      }
  
      
  
      room.attr('href', '/messages/' + chatRoom.LinkId);
  
      room.find('img').attr('src', thinkster.fn.avatar(chatRoom.ChatRoomAvatar));
  
      room.find('h6').html(chatRoom.RoomName);
  
      room.find('.poll-chatroom-preview').html(chatRoom.ChatPreview);
  
      
  
      room.click(function(e) {
  
        if($(e.target).parents('.dropdown').length != 0) {
  
        console.log('returning');
  
          return;
  
        }
  
      
  
        e.preventDefault();
  
        e.stopPropagation(); 
  
        
  
        if($(this).hasClass('active')) return;
  
        
  
        var chatRoom = $(this).data('poll-chatroom');                                                        
  
        
  
        $('.poll-chat-room').removeClass('active');
  
        $(this).addClass('active');
  
        
  
        //$('.poll-chat-interface').addClass(');
  
        thinkster.CurrentPage.createChatInterface(chatRoom);
  
        
  
        document.title = chatRoom.RoomName;
  
        
  
        $('#chat_room_title').html(chatRoom.RoomName);
  
        
  
        window.history.pushState({
  
            "pageTitle":chatRoom.RoomName
  
          },
  
          "",
  
          '/messages/' + chatRoom.LinkId
  
        );
  
        
  
        $('#messaging_container').addClass('messaging-activechat');
  
  
  
      });      
  
      
  
    }
  
    
  
    $.timeago.settings.strings = {
  
          prefixAgo: null,
  
          prefixFromNow: null,
  
          suffixAgo: null,
  
          suffixFromNow: null,
  
          inPast: 'any moment now',
  
          seconds: "less than a minute",
  
          minute: "~1m",
  
          minutes: "%dm",
  
          hour: "~1h",
  
          hours: "%dh",
  
          day: "1d",
  
          days: "%dd",
  
          month: "~1 month",
  
          months: "%d months",
  
          year: "~1y",
  
          years: "%dy",
  
          wordSeparator: " ",
  
          numbers: []
  
        };
  
    
  
    $(container).find('time.timeago').each(function() {
  
      if(!$(this).data('ta_set')) {
  
        $(this).timeago().data('ta_set', true);
  
      } 
  
    });
  
  
  
    //$(container).html(data.ChatRooms.length); 
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/chatRooms.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/comments.js
  
  thinkster.transform.comments = function(container, data) {
  
    if($(container).find('div').length == 0) $(container).html('');   
  
    
  
    $(container).find('.poll-load-more').remove();
  
    
  
    if(data.Comments.length == 0 && $(container).html() == '') {
  
       $(container).html('<div class="poll-no-comments text-muted">No comments have been posted yet</div>');
  
    } else { 
  
      $(container).find('.poll-no-comments').remove();
  
    }    
  
    
  
    var lastCommentId = null;
  
     
  
    for(var x = 0; x < data.Comments.length; x++) {
  
      var comment = data.Comments[x];
  
      var item = $('<div class="media mb-4 mt-4" />');
  
      var body = $('<div class="media-body d-flex align-self-center" />');
  
      var text = $('<div />').text(comment.CommentText);
  
      var replies = $('<div class="poll-comments-replies" data-digifi-source="master.GetTargetComments" data-digifi-json-transform="thinkster.transform.comments" />').attr('data-poll-commentid', comment.CommentId);    
  
      var commentLink = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
  
      var react = $('<span title="React to this comment" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', comment.ReactionCount).attr('data-poll-targettype', 'CO').attr('data-poll-targetid', comment.CommentId);
  
      var flagLink = $('<span class="small poll-flag-button mr-4" data-poll-type="Comment" data-poll-targettype="CO"><i class="fa fa-flag"></i> Report</span>');
  
      
  
      lastCommentId = comment.CommentId;
  
      
  
      flagLink.attr('data-poll-targetid', comment.CommentId);
  
      
  
      react.react(comment.ReactionText, comment.Emoticon);
  
      
  
      /*var createCommentBox = function() { 
  
        if(!newComment) {
  
          newComment = $('<div />').attr('data-poll-targettype', comment.TargetTypeCode).attr('data-poll-targetid', comment.TargetId).attr('data-poll-replytoid', comment.CommentId);        
  
          
  
          newComment.commentbox();
  
          
  
          replies.append(newComment);                
  
        }
  
      };    */
  
      
  
      if(comment.CommentCount > 0) {
  
        var showLink = $('<div class="poll-show-comments" />').text('Show ' + comment.CommentCount + ' repl' + (comment.CommentCount == 1 ? 'y' : 'ies'));
  
        
  
        showLink.on('click', null, {Comment: comment, Replies: replies}, function(e) {
  
          $(this).remove();
  
          //master.GetComments(comment.TargetTypeCode, comment.TargetId, comment.CommentId, null
  
          e.data.Replies.trigger('digifi.refresh', {args: [e.data.Comment.TargetTypeCode, e.data.Comment.TargetId, e.data.Comment.CommentId, null]});   
  
        });           
  
        
  
        replies.append(showLink); 
  
      }
  
      
  
      var header = $('<div class="d-flex w-100 justify-content-between"></div>');
  
      
  
      header.append($('<h5 class="mt-0" />').append($('<a target="_top" />').attr('href', '/profile/' + comment.ProfileName).text(comment.DisplayName)), ' ');
  
      header.append($('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', comment.Created).text(comment.Created)));
  
      
  
      text.prepend(header); 
  
      
  
      item.append($('<img class="d-flex align-self-top mr-3 rounded-circle" width="64" height="64" style="border-radius: 50%;" />').attr('src', thinkster.fn.avatar(comment.AvatarMicrothumbUrl)));
  
      
  
      text.append('<br />', react, commentLink, flagLink, replies);  
  
      
  
      commentLink.on('click', null, {Comment: comment, Replies: replies}, function(e) {
  
        if(e.data.Replies.find('.poll-comment-box').length == 0) {
  
          var newComment = $('<div  class="poll-comment-box" />').attr('data-poll-targettype', e.data.Comment.TargetTypeCode).attr('data-poll-targetid', e.data.Comment.TargetId).attr('data-poll-replytoid', e.data.Comment.CommentId);        
  
          
  
          e.data.Replies.append(newComment); 
  
          
  
          newComment.commentbox().find('textarea').focus();                              
  
        } else { 
  
          e.data.Replies.find('.poll-comment-box textarea').focus();
  
        }    
  
      });
  
      
  
      //commentLink.click(createCommentBox);  
  
      
  
      item.append(body);    
  
        
  
      body.append(text);
  
    
  
      if($(container).is('.poll-comments-replies')) {
  
        $(container).last().before(item); 
  
      } else {
  
        $(container).append(item);
  
      }
  
      
  
      item.thinksterInitialize();
  
      
  
      item.find('time.timeago').timeago();
  
      
  
      digifi.initialize(text[0]);
  
    }
  
    
  
    if(data.TotalRecords > 20) {
  
       var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
  
       var loadMoreButton = $('<button type="button" class="btn btn-primary">Load More Comments</button>');
  
       
  
       loadMoreContainer.append(loadMoreButton);
  
       
  
       loadMoreButton.click(function() {
  
        $(container).trigger('digifi.refresh', {args: [lastCommentId]});
  
        //alert('hi');      
  
       });
  
       
  
       $(container).append(loadMoreContainer);
  
    }    
  
  
  
    //$(container).html('under construction');
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/comments.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/commonFriendsList.js
  
  thinkster.transform.commonFriendsList = function(container, data) {
  
    if(data.Friends.length == 0) {
  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have no friends, yet!</p><p>Check out the panel on the right or the "Find Friends" tab to get started!</p></div></div>');
  
    } else {
  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
      /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');
  
    
  
      deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));
  
    
  
      deleteButton.click(function() {
  
    	alert('Cancel: ' + friend.ProfileId);
  
      });
  
    
  
      container.append(deleteButton);*/
  
      });
  
    
  
      $(container).html(ul);
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/commonFriendsList.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/exposureDropdown.js
  
  thinkster.transform.exposureDropdown = function(container, data) {
  
    if(data.Exposures.length == 0) {
  
      $(container).html('');
  
    } else {
  
  
  
  		for(var e = 0; e < data.length; e++) {
  
  			options.append($('<option/>').attr('value',exp.ExposureTypeCode).text(exp.Name));
  
  		}
  
  
  
  	$(container).html(options);
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/exposureDropdown.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/followableUserList.js
  
  thinkster.transform.followableUserList = function(context, data) {
  
    if($(context).find('div').length == 0) $(context).html('');
  
    
  
    $(context).find('.poll-load-more').remove();
  
    
  
    var lg = $('<div class="list-group" />');
  
           
  
    for(var x = 0; x < data.Users.length; x++) {
  
      var item = $('<a class="list-group-item d-flex justify-content-between list-group-item-action" target="_top"></a>').attr('href', '/profile/' + data.Users[x].ProfileName).text(data.Users[x].DisplayName);                
  
      
  
      item.prepend($('<img class="pull-left align-self-top mr-3 rounded-circle" width="32" height="32" style="border-radius: 50%;" />').attr('src', thinkster.fn.avatar(data.Users[x].AvatarMicrothumbUrl)));
  
          
  
      if(data.Users[x].ProfileId != thinkster.CurrentUser.ProfileId) {
  
        var val = data.Users[x].Total == 0.0 ? 0.00 : (data.Users[x].Agree / data.Users[x].Total * 100.0).toFixed(2); 
  
        
  
        var percentage = $('<span class="min-chart"><span class="percent"></span></span>').attr('id', 'compatibility_' + data.Users[x].ProfileId).attr('data-percent', val);
  
    
  
        item.append(percentage);
  
        
  
        var followButton = $('<button type="button" class="btn btn-primary btn-rounded"></button>');
  
        
  
        if($(context).is('.poll-ful-sm')) {
  
          followButton.addClass('btn-sm');
  
          
  
          if(data.Users[x].Following) {      
  
            followButton.attr('title', 'Following');
  
            followButton.html($('<i class="fa fa-user-times"></i>'));
  
          } else {
  
            followButton.attr('title', 'Follow');
  
            followButton.html($('<i class="fa fa-user-plus"></i>')); 
  
          } 
  
        } else {
  
          if(data.Users[x].Following) {
  
             followButton.text('Following');
  
          } else {
  
            followButton.text('Follow'); 
  
          }
  
        }
  
        
  
        item.append(followButton);
  
                      
  
        percentage.easyPieChart({
  
            barColor: "#4caf50",        
  
            size:48,
  
            onStep: function (from, to, percent) {
  
                $(this.el).find('.percent').text(Math.round(percent));
  
            }        
  
        });
  
      } else {
  
        item.removeClass('justify-content-between'); 
  
      }
  
      
  
      lg.append(item);
  
  
  
    } 
  
    
  
    $(context).html(lg);
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/followableUserList.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/following.js
  
  thinkster.transform.following = function(container, data) {
  
    if(data.Following.length == 0) {
  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You aren\'t following anyone, yet!</p></div></div>');
  
    } else {
  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
        var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i> Unfollow</button>');
  
  
  
        deleteButton.append($('<span class="sr-only" />').text(friend.DisplayName));
  
  
  
        deleteButton.click(function() {
  
  
  
          newsfeed.Unfollow(friend.ProfileId, function(result) {
  
  
  
  			var retParse = JSON.parse(result);
  
  
  
  			if(retParse.Success) {
  
  				$('#following').trigger('digifi.refresh');
  
  			}
  
  		}, alert);
  
        });
  
  
  
        container.append(deleteButton);
  
      }, 'Following');
  
  
  
      $(container).html(ul);
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/following.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/followrequests.js
  
  thinkster.transform.followrequests = function(container, data, prepend) {
  
    //console.log(JSON.stringify(data));
  
    
  
    if($(container).find('div').length == 0) $(container).html('');   
  
    
  
    $(container).find('.poll-load-more').remove();
  
    
  
    if(data.FollowRequests.length == 0 && $(container).html() == '') {
  
       $(container).html('<div class="poll-no-requests text-muted">you have no requests</div>');
  
    } else { 
  
      $(container).find('.poll-no-requests').remove();
  
    }    
  
    
  
    var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  
    var match;
  
    var lastNotificationId = null;
  
     
  
    for(var x = 0; x < data.FollowRequests.length; x++) {
  
      var request = data.FollowRequests[x];
  
      
  
      var item = $('<div class="media p-4 poll-follow-request" />');
  
      var body = $('<div class="media-body d-flex align-self-center" />');
  
      var text = $('<div />');
  
      var buttonContainer = $('<span class="align-self-right"></span>');
  
      var acceptButton = $('<button type="button" class="btn btn-sm btn-success mr-2 poll-accept-follow-request"><i class="fa fa-fw fa-check"></i> Accept</button>');
  
      var declineButton = $('<button type="button" class="btn btn-sm btn-danger mr-2 poll-decline-follow-request"><i class="fa fa-fw fa-times"></i> Decline</button>');
  
      
  
      acceptButton.attr('data-poll-id', request.FollowerId);
  
      declineButton.attr('data-poll-id', request.FollowerId);
  
      
  
      buttonContainer.append(acceptButton, $('<br />'), declineButton);            
  
      
  
      item.attr('data-follower-id', request.FollowerId);
  
      
  
      if(request.Image) {
  
        item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" width="32" height="32" />').attr('src', request.Image)); 
  
      } else if(request.Icon) { 
  
        item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-2x" />').addClass(request.Icon));
  
      } else { 
  
        item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-2x fa-info-circle" />'));
  
      }
  
      
  
      body.append(text);
  
      item.append(body);
  
      
  
      item.append(buttonContainer);
  
      
  
      thinkster.fn.formatText(request.Text, text);
  
      
  
      if(prepend) { 
  
        $(container).prepend(item);
  
      } else {
  
        $(container).append(item);
  
      }
  
      
  
      item.find('time.timeago').timeago();
  
      buttonContainer.thinksterInitialize();
  
      
  
    }
  
    
  
    $('.poll-follow-request-count').text(data.UnreadCount ? data.UnreadCount : '');    
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/followrequests.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/friendList.js
  
  thinkster.transform.friendList = function(container, data) {
  
    if(data.Friends.length == 0) {
  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have no friends, yet!</p><p>Check out the panel on the right or the "Find Friends" tab to get started!</p></div></div>');
  
    } else {
  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
        /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');
  
  
  
        deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));
  
  
  
        deleteButton.click(function() {
  
          alert('Cancel: ' + friend.ProfileId);
  
        });
  
  
  
        container.append(deleteButton);*/
  
      });
  
  
  
      $(container).html(ul);
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/friendList.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/friendRequestDropdown.js
  
  thinkster.transform.friendRequestDropdown = function(container, data) {
  
    if(data.Friends.length == 0) {
  
      $(container).html('');
  
    } else {
  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
        /*var confirmButton = $('<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Confirm</button>');
  
        var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Decline</button>');
  
  
  
        confirmButton.append($('<span class="sr-only" />').text('Confirm ' + friend.DisplayName + ' as a friend'));
  
        deleteButton.append($('<span class="sr-only" />').text('Decline the friend request from ' + friend.DisplayName));
  
  
  
        confirmButton.click(function() {
  
          //alert('confirm: ' + friend.ProfileId);
  
          thinkster.fn.confirmFriendshipRequest(friend.ProfileId, function() {
  
             var itemToRemove = $(confirmButton).parents('li').first();
  
  
  
              itemToRemove.html($('<div class="alert alert-success">You are now friends with ' + friend.DisplayName + '!</div>'));
  
  
  
              setTimeout(function() {
  
                itemToRemove.hide('slow', function() {
  
                  $(this).remove();
  
                });
  
              }, 3000);
  
          }, alert);    
  
        });
  
  
  
        deleteButton.click(function() {
  
          alert('decline: ' + friend.ProfileId);
  
        });
  
  
  
        container.append(confirmButton, '&nbsp;', deleteButton);       */
  
      });
  
  
  
      $(container).html(ul);        
  
    }
  
  
  
    var unreadCount = $('#poll_menu_friends_preview li').length;
  
    $('#friends_top_menu').find('.badge').html(unreadCount == 0 ? '' : unreadCount); 
  
  
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/friendRequestDropdown.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/friendsListBase.js
  
  thinkster.transform.friendsListBase = function(data, addButtons, indexName) {
  
    var ul = $('<ul class="list-unstyled" />');
  
    indexName = indexName || 'Friends';
  
      
  
    for(var f = 0; f < data[indexName].length; f++) {
  
      var li = $('<li class="media my-4" />');
  
      var buttonContainer = $('<div class="d-flex mr-3 poll-friend-buttons"></div>');
  
      var body = $('<div class="media-body" />');
  
      var profileId = data[indexName][f].ProfileId;
  
  
  
      if(addButtons) addButtons(data[indexName][f], buttonContainer);
  
  
  
  
  
      li.append($('<img width="64" height="64" class="d-flex mr-3" />').attr('src', thinkster.fn.avatar(data[indexName][f].AvatarMicrothumbUrl)).attr('alt', 'Avatar').attr('title', 'Avatar for ' + data[indexName][f].DisplayName));
  
      body.append($('<a />').text(data[indexName][f].DisplayName).attr('href', '/profile/' + data[indexName][f].ProfileName));
  
      body.append($('<br />'), $('<span class="small text-muted" />').text(data[indexName][f].Location).append($('<br />'), data[indexName][f].FriendsInCommon > 0 ? data[indexName][f].FriendsInCommon + ' friend' + (data[indexName][f].FriendsInCommon == 1 ? '' : 's') + ' in common' : ''));
  
  
  
      li.append(body, buttonContainer);
  
      ul.append(li);
  
    }
  
  
  
    return ul;
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/friendsListBase.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/imageList.js
  
  thinkster.transform.imageList = function(container, data) {
  
    if(data.Images.length == 0) {
  
      $(container).html('<div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>No images to display</p></div></div>');
  
    } else {
  
      //var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
      /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');
  
    
  
      deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));
  
    
  
      deleteButton.click(function() {
  
    	alert('Cancel: ' + friend.ProfileId);
  
      });
  
    
  
      container.append(deleteButton);*/
  
      //});
  
      
  
      var row = $(container).find('.row');
  
      var prepend = true;
  
      
  
      if(row.length == 0) {
  
        prepend = false; 
  
        row = $('<div class="row m-1" />');
  
        $(container).html(row);
  
      }
  
      
  
      for(var x = 0; x < data.Images.length; x++) {
  
        var col = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
  
        var imageDiv = $('<div class="mt-4 text-center" />');
  
        var imageA = $('<a />').attr('href', '/media/' + data.Images[x].Selector);
  
        
  
        imageA.append($('<img width="220" height="200" />').attr('src', data.Images[x].ThumbnailUrl).attr('title', data.Images[x].Name + ' - ' + data.Images[x].Description));
  
        
  
        imageDiv.append(imageA); 
  
        
  
        col.append(imageDiv);
  
        
  
        if(prepend) { 
  
          row.prepend(col);
  
        } else {       
  
          row.append(col);
  
        } 
  
      }
  
       
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/imageList.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/newsfeedcards.js
  
  thinkster.transform.newsfeedCards = function(context, data, prepend) {
  
    if($(context).find('div').length == 0) $(context).html('');
  
    
  
    $(context).find('.poll-load-more').remove();
  
    
  
    var lastTimestamp = 0;
  
  
  
    for(var x = 0; x < data.Items.length; x++) {
  
      var item = data.Items[x];
  
    
  
      var card = $('<div class="card mb-4"></div>');
  
      var body = $('<div class="card-body"></div>');
  
      var header = $('<p class="card-text clearfix"></p>');
  
      var p = $('<p class="card-text"></p>');
  
      var avatarAnchor = $('<a class="pull-left mr-3" />');
  
      
  
      thinkster.fn.formatText(item.Text, p);
  
      
  
      lastTimestamp = item.Timestamp;
  
      
  
      if(item.Image) {
  
        avatarAnchor.append($('<img class="pull-left mr-3 poll-avatar" width="64" height="64" />').attr('src', item.Image)); 
  
      } else if(item.Icon) { 
  
        avatarAnchor.append($('<i class="fa fa-fw fa-3x" />').addClass(item.Icon));
  
      } else { 
  
        avatarAnchor.append($('<i class="fa fa-fw fa-3x fa-info-circle" />'));
  
      }
  
      
  
      avatarAnchor.attr('href', '/profile/' + item.ProfileName);
  
      
  
      header.append(avatarAnchor);
  
      
  
      var ta = $('<time class="timeago" />').attr('datetime', item.Created).text(item.Created)
  
      
  
      header.append($('<a />').attr('href', '/profile/' + item.ProfileName).text(item.DisplayName), $('<br />'), $('<div class="small"><i class="fa fa-clock-o fa-fw"></i></div>').append(ta));
  
      
  
      body.append(header);
  
      
  
      if(item.SecondaryImage) {    
  
        //var secondary = $('<img />').attr('src', item.SecondaryImage);
  
        var secondaryLink = $('<a />').attr('href', '/media/' + item.SecondaryImageSelector);
  
        var secondary = $('<div class="poll-newsfeed-image"></div>');
  
        
  
        secondary.css("background-image", "url(" + item.SecondaryImage + ")");
  
        
  
        secondaryLink.append(secondary);
  
        
  
        body.append(secondaryLink); 
  
      } else {
  
        body.append(p); 
  
      }        
  
        
  
      if(item.Responded) {   
  
        body.append($('<hr />'));
  
      
  
        var responseLine = $('<div class="poll-response-line"></div>');
  
        
  
        body.append(responseLine);
  
         
  
        var react = null;
  
        var commentLink = null;
  
  
  
        if(item.SecondaryImage) {
  
          react = $('<span title="React to this picture" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', item.ReactionCount).attr('data-poll-targettype', 'ME').attr('data-poll-targetid', item.TargetId);                 
  
        } else {
  
          react = $('<span title="React to this answer" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', item.ReactionCount).attr('data-poll-targettype', 'RE').attr('data-poll-targetid', item.TargetId);
  
        }
  
        
  
        comment = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
  
        
  
        react.attr('data-poll-reactioncount', item.ReactionCount);
  
        
  
        responseLine.append(react, comment);   
  
        
  
        if(item.CommentCount > 0) {
  
          var showLink = $('<div class="poll-show-comments" />').text('Show ' + item.CommentCount + ' repl' + (item.CommentCount == 1 ? 'y' : 'ies'));
  
          
  
         /* showLink.on('click', null, {Comment: comment, Replies: replies}, function(e) {
  
            $(this).remove();
  
            //master.GetComments(comment.TargetTypeCode, comment.TargetId, comment.CommentId, null
  
            e.data.Replies.trigger('digifi.refresh', {args: [e.data.Comment.TargetTypeCode, e.data.Comment.TargetId, e.data.Comment.CommentId, null]});   
  
          }); */          
  
          
  
          responseLine.append(showLink); 
  
        }             
  
        
  
        react.react(item.ReactionText, item.Emoticon);        
  
      }
  
      
  
      card.append(body);
  
      
  
      if(prepend) { 
  
        $(context).prepend(card);
  
      } else { 
  
        $(context).append(card);
  
      }
  
      
  
      ta.timeago();
  
    }   
  
    if(data.TotalRecords > data.PageSize)  {
  
      var moreButton = $('<button type="button" class="btn btn-primary">Load More</button>');
  
      $(context).append($('<p class="text-center"></p>').append(moreButton));
  
      
  
      moreButton.click(function() {
  
        moreButton.parent().replaceWith($('<p class="poll-load-more text-center"><i class="fa fa-refresh text-danger fa-2x fa-spin"></i> Loading...</p>'));
  
        $(context).trigger('digifi.refresh', {args: lastTimestamp});
  
      });
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/newsfeedcards.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/notifications.js
  
  thinkster.transform.notifications = function(container, data, prepend) {  
  
    if($(container).find('div').length == 0) $(container).html('');   
  
    
  
    $(container).find('.poll-load-more').remove();
  
    
  
    if(data.Notifications.length == 0 && $(container).html() == '') {
  
       $(container).html('<div class="poll-no-notifications text-muted">you have no notifications</div>');
  
    } else { 
  
      $(container).find('.poll-no-notifications').remove();
  
    }    
  
    
  
    var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  
    var match;
  
    var lastNotificationId = null;
  
     
  
    for(var x = 0; x < data.Notifications.length; x++) {
  
      var notification = data.Notifications[x];
  
      var item = $('<div class="media p-4 poll-notification" />');
  
      var body = $('<div class="media-body d-flex justify-content-between" />');
  
      var text = $('<div />');
  
      var lastIndex = 0;
  
  
  
      if(!notification.IsRead) {
  
        item.addClass('poll-unread');
  
      }
  
      
  
      item.attr('data-id', notification.NotificationId).attr('data-group', notification.GroupingValue);
  
      
  
      // We're rolling up:
  
      if(notification.GroupingCount) {
  
        $(container).find('.poll-notification[data-group="' + notification.GroupingValue + '"]').remove(); 
  
      }
  
      
  
      if(notification.AvatarMicrothumbUrl) {
  
        item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" />').attr('src', notification.AvatarMicrothumbUrl)); 
  
      } else if(notification.Icon) { 
  
        item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x" />').addClass(notification.Icon));
  
      } else { 
  
        item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x fa-info-circle" />'));
  
      }
  
      
  
      item.append(body);       
  
      
  
      while ((match = re.exec(data.Notifications[x].Text)) !== null) {
  
        if(match.index > lastIndex) {
  
          text.append(data.Notifications[x].Text.substring(lastIndex, match.index));
  
          //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
  
        } 
  
        
  
        //text += match[3];
  
        var anchor = $('<a />').html(match[3]);
  
        
  
        switch(match[1]) {
  
          case 'e':
  
            anchor.attr('href', '/events/' + match[2]);
  
          break;
  
        
  
          case 'u':
  
            anchor.attr('href', '/profile/' + match[2]);
  
          break;
  
          
  
          case 'p':
  
            anchor.attr('href', '/poll/' + match[2]);
  
          break; 
  
          
  
          case 'm':
  
            anchor.attr('href', '/media/' + match[2]);
  
          break;
  
        }
  
        
  
        text.append(anchor);
  
        
  
        //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
  
        
  
        lastIndex = re.lastIndex;            
  
      }
  
      
  
      if(data.Notifications[x].Text.length > lastIndex) {
  
        text.append(data.Notifications[x].Text.substring(lastIndex)); 
  
      }
  
      
  
      var time_container = $('<div class="d-flex align-self-bottom mr-2"></div>').append($('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', data.Notifications[x].NotificationDate).text(data.Notifications[x].NotificationDate)));
  
      
  
      if(data.Notifications[x].Emoticon) {    
  
        text.append('<br />', $('<span />').text(data.Notifications[x].Emoticon)); 
  
      }
  
      
  
      //text.append('<br />', $('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', data.Notifications[x].NotificationDate).text(data.Notifications[x].NotificationDate)));
  
        
  
      body.append(text);
  
      
  
      if(data.Notifications[x].SecondaryImage) {
  
        var secondaryLink = $('<a class="ml-4 mr-auto" />').attr('href', '/media/' + data.Notifications[x].SecondaryImageSelector);
  
        var secondary = $('<img />');
  
        
  
        secondary.attr("src", data.Notifications[x].SecondaryImage);
  
        
  
        secondaryLink.append(secondary);
  
        
  
        body.append(secondaryLink);  
  
      }                
  
      
  
      var menu_container = $('<div class="d-flex align-self-top ml-2"></div>');
  
      
  
      if($(container).is('#poll_menu_notification_preview')) {
  
        //body.removeClass('d-flex');
  
      }
  
      
  
      
  
      var menu_button = $('<div><div class="dropdown poll-notification-menu-btn"><i class="fa fa-ellipsis-v pl-1 pr-1 pb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i><div class="dropdown-menu dropdown-menu-right" style="left: 0px !important;"></div></div></div>');
  
      
  
      menu_button.find('i').attr('id', 'poll_nmnu_' + notification.NotificationId);
  
      menu_button.find('.dropdown-menu').attr('aria-labelledby', 'poll_nmnu_' + notification.NotificationId);
  
                                                                                  
  
      var remove_item_action = $('<button class="dropdown-item" type="button">Delete</button>');
  
      remove_item_action.data('id', notification.NotificationId); 
  
      remove_item_action.click(function() { 
  
        master.DeleteNotification($(this).data('id'), function(result) { 
  
        }); 
  
      });
  
      
  
      var read_toggle_action = $('<button class="dropdown-item poll-read-toggle-action" type="button">Mark as Read</button>');
  
  
  
      read_toggle_action.data('read', notification.IsRead).data('id', notification.NotificationId); 
  
      
  
      read_toggle_action.text(notification.IsRead ? 'Mark as Unread' : 'Mark as Read');        
  
      
  
      read_toggle_action.click(function() { 
  
        if($(this).data('read')) {
  
          master.MarkNotificationUnread($(this).data('id'), function(result) {
  
            $(this).data('read', false);
  
            $(this).text('Mark as Read');        
  
            //Do something on failure? 
  
          }); 
  
        } else {
  
          master.MarkNotificationRead($(this).data('id'), function(result) {
  
            $(this).data('read', true);
  
            $(this).text('Mark as Unread');        
  
            //Do something on failure? 
  
          });
  
        }
  
      });
  
         
  
      
  
      menu_button.find('.dropdown-menu').append(read_toggle_action, remove_item_action);
  
      
  
      menu_container.append(menu_button);
  
      
  
      item.append(time_container, menu_container);
  
    
  
      
  
      if(prepend) { 
  
        $(container).prepend(item);
  
      } else {
  
        $(container).append(item);
  
      }
  
      
  
      item.find('time.timeago').timeago();
  
      
  
    }
  
    
  
    if(data.TotalRecords > 20) {
  
       var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
  
       var loadMoreButton = $('<button type="button" class="btn btn-primary">Load More Notifications</button>');
  
       
  
       loadMoreContainer.append(loadMoreButton);
  
       
  
       loadMoreButton.click(function() {
  
        $(container).trigger('digifi.refresh', {args: [lastNotificationId]});
  
        //alert('hi');      
  
       });
  
       
  
       $(container).append(loadMoreContainer);
  
    }    
  
    
  
    //var unreadCount = $(container).find('.poll-unread').length;
  
    $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
  
                                                             
  
    //$(container).html('under construction');
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/notifications.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/outgoingFriends.js
  
  thinkster.transform.outgoingFriends = function(container, data) {
  
    if(data.Friends.length == 0) {
  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><p>You have no pending sent requests for friends right now.</p><p>If you\'re looking for more friends, check out the panel on the right!</p></div></div>');
  
    } else {
  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
        var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');
  
  
  
        deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));
  
  
  
        deleteButton.click(function() {
  
  
  
          thinkster.fn.cancelFriendshipRequest(friend.ProfileId, function() {
  
             var itemToRemove = $(deleteButton).parents('li').first();
  
  
  
              itemToRemove.html($('<div class="alert alert-warning">You have cancelled your friend request with ' + friend.DisplayName + '.</div>'));
  
  
  
              setTimeout(function() {
  
                itemToRemove.hide('slow', function() {
  
                  $(this).remove();
  
                });
  
              }, 3000);
  
          }, alert);
  
        });
  
  
  
        container.append(deleteButton);
  
      });
  
  
  
      $(container).html(ul);
  
    }
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/outgoingFriends.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/pendingFriends.js
  
  thinkster.transform.pendingFriends = function(container, data) {
  
    if(data.Friends.length == 0) {
  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><p>You have no outstanding friend requests right now.</p><p>If you\'re looking for more friends, check out the panel on the right!</p></div></div>');
  
    } else {
  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
  
        var confirmButton = $('<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Confirm</button>');
  
        var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Decline</button>');
  
  
  
        confirmButton.append($('<span class="sr-only" />').text('Confirm ' + friend.DisplayName + ' as a friend'));
  
        deleteButton.append($('<span class="sr-only" />').text('Decline the friend request from ' + friend.DisplayName));
  
  
  
        confirmButton.click(function() {
  
          //alert('confirm: ' + friend.ProfileId);
  
          thinkster.fn.confirmFriendshipRequest(friend.ProfileId, function() {
  
             var itemToRemove = $(confirmButton).parents('li').first();
  
  
  
              itemToRemove.html($('<div class="alert alert-success">You are now friends with ' + friend.DisplayName + '!</div>'));
  
  
  
              setTimeout(function() {
  
                itemToRemove.hide('slow', function() {
  
                  $(this).remove();
  
                  countRequests();
  
                });
  
              }, 3000);
  
          }, alert);
  
        });
  
  
  
        deleteButton.click(function() {
  
          alert('decline: ' + friend.ProfileId);
  
        });
  
  
  
        container.append(confirmButton, '&nbsp;', deleteButton);
  
      });
  
  
  
      $(container).html(ul);
  
    }
  
  
  
  
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/pendingFriends.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/pollFilters.js
  
  thinkster.transform.pollFilters = function(container, data) {
  
    var addFilterButton = function() { 
  
      var btn = $('<button type="button" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Add Filter</button>');
  
      
  
      btn.click(function() {
  
        $('#add_filter').modal('show');
  
      });
  
      
  
      return btn;
  
    };
  
  
  
    if(data.length) {
  
       
  
    } else {
  
      
  
    
  
      $(container).html($('<div class="alert alert-info">There are no filters defined here</div>')).append(addFilterButton(data)); 
  
    }   
  
  };      
  // End of /var/pollsite/prod/inc/thinkster.js/transform/pollFilters.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/pollResponses.js
  
  thinkster.transform.pollResponses = function(container, data) {
  
  	if(data.Responses.length == 0) {
  
  		$(container).html('<div class="m-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have not answered any polls yet! Answer polls so that your answers can be displayed here!</p></div></div>');
  
  	} else {
  
  
  
  		var cards=$('<div class="m-4"></div>');
  
  
  
  
  
  		for(var f = 0; f < data.Responses.length; f++) {
  
        var response = data.Responses[f];
  
  
  
  			var card=$('<div class="card"></div>');
  
  			var cardBlock=$('<div class="card-block"></div>');
  
  			var paraQuest=$('<p class="m-4">Poll Question: </p>');
  
  			var footer=$('<div class="card-footer text-muted"></div>');
  
  			var paraFoot=$('<p class="ml-4"></p>');
  
  			var sub=$('<span><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-heart">&#160;Like!</i></button>&#160;<button type="button" class="btn btn-sm btn-warning"><i class="fa fa-meh-o">&#160;Meh!</i></button>&#160;<button type="button" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-down">&#160;Nope!</i></button>&#160;<button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-thumbs-up">&#160;Yeah!</i></button></span>');
  
        
  
        paraQuest.append(data.Responses[f].Question + '<br />' + 'Answer: ' + '<em>' + data.Responses[f].Response + '</em>');
  
        
  
        /*var commentLink = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
  
        var react = $('<span title="React to this comment" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', comment.ReactionCount).attr('data-poll-targettype', 'PR').attr('data-poll-targetid', response.PollResponseId);
  
        var replies = $('<div class="poll-comments-replies" data-digifi-source="master.GetTargetComments" data-digifi-json-transform="thinkster.transform.comments" />').attr('data-poll-pollresponseid', response.PollResponseId);			
  
        
  
        paraQuest.append('<br />', react, commentLink, replies); 
  
         */
  
  			cardBlock.append(paraQuest);
  
  			card.append(cardBlock);
  
  			paraFoot.append('Answered on ' + data.Responses[f].ResponseDate + ' - ' + data.Responses[f].ExposureName, '<br/>' ,sub);
  
  			footer.append(paraFoot);
  
  			card.append(footer);
  
  			cards.append(card, '<br/>');
  
  		}
  
  
  
  		$(container).html(cards);
  
  	}
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/pollResponses.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/searchresults.js
  
  thinkster.transform.searchResults = function(container, data) { 
  
    if(data && data.length) {
  
      var div = $('<div class="list-group"></div>'); 
  
    
  
       for(var x = 0; x < data.length; x++) {
  
         var item = $('<a class="list-group-item b-0"></a>');
  
         
  
         item.css('display', 'block');
  
         
  
         if(data[x].ThumbnailUrl && data[x].ThumbnailUrl != '') {
  
          var img = $('<img class="pull-left mr-1 poll-avatar" width="32" height="32" />');
  
          
  
          img.attr('src', data[x].ThumbnailUrl);
  
          img.attr('alt', 'Search result');
  
          
  
          item.append(img); 
  
         } else if(data[x].Icon && data[x].Icon != '') {   
  
          var icn = $('<i class="fa-pull-left fa-2x mr-1 poll-avatar" />');
  
          
  
          icn.addClass(data[x].Icon);
  
          
  
          item.append(icn);
  
         }              
  
         
  
         var urlBase = '';
  
         
  
         switch(data[x].Type) {
  
          case 'u':
  
            urlBase = '/profile/';
  
            //item.append('<i class="fa fa-user mr-1" />');
  
          break;
  
          
  
          case 'p':
  
            urlBase = '/poll/';
  
            //item.append('<i class="fa fa-user mr-1" />');
  
          break;
  
          
  
          case 'm':
  
            urlBase = '/media/';
  
            //item.append('<i class="fa fa-image mr-1" />');
  
          break;
  
          
  
          case 'e':
  
            urlBase = '/events/';
  
          break; 
  
         }
  
         
  
         item.append(data[x].Name);
  
         item.append($('<br />'), $('<div class="small text-muted"></div>').text(data[x].Display));
  
         
  
         item.attr('href', urlBase + data[x].Selector);
  
         
  
         div.append(item);
  
       }
  
       
  
       $(container).html(div);
  
    } else {    
  
      $(container).html($('<div class="alert alert-info mt-1">There were no results to your search</div>')); 
  
    }   
  
  };      
  // End of /var/pollsite/prod/inc/thinkster.js/transform/searchresults.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/suggestedFriends.js
  
  thinkster.transform.suggestedFriends = function(container, data) {
  
    var ul = $('<ul class="list-unstyled" />');
  
  
  
    for(var f = 0; f < data.Friends.length; f++) {
  
      var li = $('<li class="media my-4" />').data('poll-id', data.Friends[f].ProfileId);
  
      var addButton = $('<button type="button" class="btn btn-success btn-sm d-flex mr-3"><i class="fa fa-user-plus"></i></button>');
  
      var body = $('<div class="media-body" />');
  
      var profileId = data.Friends[f].ProfileId;
  
  
  
      addButton.append($('<span class="sr-only" />').text('Add ' + data.Friends[f].DisplayName + ' as a friend')).data('poll-id', profileId);
  
  
  
      addButton.click(function() {
  
        var b = this;
  
  
  
        $(this).prop('disabled', true);
  
  
  
        master.AddFriend($(this).data('poll-id'), function(json) {
  
          var result = jQuery.parseJSON(json);
  
  
  
          if(result.Success) {
  
            var itemToRemove = $(b).parents('li').first();
  
  
  
            itemToRemove.html($('<div class="alert alert-success">friend request sent!</div>'));
  
  
  
            setTimeout(function() {
  
              itemToRemove.hide('slow', function() {
  
                $(this).remove();
  
  
  
                if(ul.find('li').length == 0) {
  
                  ul.parent().trigger('digifi.refresh');
  
                }
  
              });
  
            }, 3000);
  
  
  
  
  
          } else {
  
            alert('Unable to add friend at this time. Sorry about that.');
  
            $(this).prop('disabled', false);
  
          }
  
  
  
        }, alert);
  
      });
  
  
  
      li.append($('<img width="64" height="64" class="d-flex mr-3" />').attr('src', data.Friends[f].AvatarUrl).attr('alt', 'Avatar').attr('title', 'Avatar for ' + data.Friends[f].DisplayName));
  
      body.append($('<a />').text(data.Friends[f].DisplayName).attr('href', '/profile/' + data.Friends[f].ProfileName));
  
      body.append($('<br />'), $('<span class="small text-muted" />').text(data.Friends[f].Location).append($('<br />'), data.Friends[f].FriendsInCommon > 0 ? data.Friends[f].FriendsInCommon + ' friend' + (data.Friends[f].FriendsInCommon == 1 ? '' : 's') + ' in common' : ''));
  
  
  
      li.append(body, addButton);
  
      ul.append(li);
  
    }
  
  
  
     $(container).html(ul);
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/suggestedFriends.js
  
  // Start of /var/pollsite/prod/inc/thinkster.js/transform/timeline.js
  
  thinkster.transform.timeline = function(container, data, prepend) {
  
    if($(container).find('div').length == 0) $(container).html('');
  
    
  
    $(container).find('.poll-load-more').remove();
  
    
  
    var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  
    var match;
  
    var lastTimestamp = 0;
  
  
  
    for(var x = 0; x < data.Items.length; x++) {
  
      var item = $('<div class="media mb-4" />');
  
      var body = $('<div class="media-body d-flex align-self-center" />');
  
      var text = $('<div />');
  
      var lastIndex = 0;
  
      var avatarAnchor = $('<a class="d-flex align-self-center mr-3" />');
  
      
  
      lastTimestamp = data.Items[x].Timestamp;
  
            
  
      if(data.Items[x].Image) {
  
        avatarAnchor.append($('<img class="poll-avatar" width="64" height="64" />').attr('src', data.Items[x].Image)); 
  
      } else if(data.Items[x].Icon) { 
  
        avatarAnchor.append($('<i class="mr-3 fa fa-fw fa-3x" />').addClass(data.Items[x].Icon));
  
      } else { 
  
        avatarAnchor.append($('<i class="mr-3 fa fa-fw fa-3x fa-info-circle" />'));
  
      }
  
      
  
      item.append(avatarAnchor, body);       
  
      
  
      while ((match = re.exec(data.Items[x].Text)) !== null) {
  
        if(match.index > lastIndex) {
  
          text.append(data.Items[x].Text.substring(lastIndex, match.index));
  
          //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
  
        } 
  
        
  
        //text += match[3];
  
        var anchor = $('<a />').html(match[3]);
  
        
  
        switch(match[1]) {
  
          case 'e':
  
            anchor.attr('href', '/events/' + match[2]);
  
          break;
  
        
  
          case 'u':
  
            anchor.attr('href', '/profile/' + match[2]);
  
          break;
  
          
  
          case 'p':
  
            anchor.attr('href', '/poll/' + match[2]);
  
          break; 
  
          
  
          case 'm':
  
            anchor.attr('href', '/media/' + match[2]);
  
          break;
  
        }
  
        
  
        text.append(anchor);
  
        
  
        //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
  
        
  
        lastIndex = re.lastIndex;            
  
      }
  
      
  
      avatarAnchor.attr('href', anchor.attr('href'));
  
      
  
      if(data.Items[x].Text.length > lastIndex) {
  
        text.append(data.Items[x].Text.substring(lastIndex)); 
  
      }
  
      
  
      text.append('<br />', $('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', data.Items[x].Created).text(data.Items[x].Created)));
  
        
  
      body.append(text);
  
    
  
      if(prepend) { 
  
        $(container).prepend(item);
  
      } else {
  
        $(container).append(item);
  
      }
  
      
  
      item.find('time.timeago').timeago();
  
    }
  
    
  
    if(data.Items.length > 0 && !$(container).is('.poll-no-load-more')) {
  
       var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
  
       var loadMoreButton = $('<button type="button" class="btn btn-primary">Load More History</button>');
  
       
  
       loadMoreContainer.append(loadMoreButton);
  
       
  
       loadMoreButton.click(function() {
  
        $(container).trigger('digifi.refresh', {args: [lastTimestamp]});
  
        //alert('hi');      
  
       });
  
       
  
       $(container).append(loadMoreContainer);
  
    }
  
    
  
  
  
    //$(container).html(data.ChatRooms.length); 
  
  };  
  // End of /var/pollsite/prod/inc/thinkster.js/transform/timeline.js
  	                    

  thinkster.connect = function() {
    //var socket = io('https://devpush.thinkster.ca');
    var socket = io(thinkster.PushServer);

    socket.on('connect', function(token) {
      master.GetPushToken(function(token) {
        socket.emit('authenticate', token);   
      }, console.log);
    });
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/activeconnections.js
    
    socket.on('ActiveConnections', function(data){
    
      //$('#messages').append($('<li>').text(msg));
    
      
    
      var ul = $('<ul class="list-unstyled" />');
    
      
    
      ul.append($('<li><i class="fa fa-fw fa-question"></i> Unknown </li>').append(data.Unknown));
    
      ul.append($('<li><i class="fa fa-fw fa-mobile"></i> Mobile App </li>').append(data.Mobile));
    
      ul.append($('<li><i class="fa fa-fw fa-desktop"></i> Browsers </li>').append(data.Desktop));
    
      ul.append($('<li><i class="fa fa-fw fa-globe"></i> Total </li>').append(data.Total));
    
      
    
        
    
      $('.poll-active-connections').html(ul);    
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/activeconnections.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/allnotificationsread.js
    
    socket.on('AllNotificationsRead', function(data){
    
      $('.poll-notification.poll-unread').each(function() {
    
        $(this).removeClass('poll-unread');
    
        $(this).find('.poll-read-toggle-action').data('read', true).text('Mark as Unread'); 
    
      }).promise().done(function() {     
    
        $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
    
      });
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/allnotificationsread.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/chatmessage.js
    
    socket.on('ChatMessage', function(data){
    
      console.log(JSON.stringify(data));
    
      
    
      $('.poll-chat-room[data-id=' + data.ChatRoomId + ']').each(function() {
    
        var parent = $(this).parent();
    
        
    
        $(this).find('.poll-chatroom-preview').html(data.Messages[0].Message);
    
        
    
        if(data.Messages[0].ProfileId != thinkster.CurrentUser.ProfileId) {
    
          $(this).addClass('poll-chat-unread'); 
    
        }
    
        
    
        parent.prepend($(this).remove());     
    
        
    
        //var unreadCount = $('#poll_menu_chat_preview').find('.poll-chat-room.poll-chat-unread').length;
    
        if(data.UnreadDelta) { 
    
          var current = parseInt($('#chat_top_menu').find('.badge').text()) + data.UnreadDelta;
    
          $('#chat_top_menu').find('.badge').html(current == 0 ? '' : current);
    
        } else {
    
          $('#chat_top_menu').find('.badge').html(data.UnreadCount == 0 ? '' : data.UnreadCount); 
    
        }         
    
      }); 
    
      
    
      if(thinkster.CurrentPage.chatMessages) thinkster.CurrentPage.chatMessages('#ci' + data.ChatRoomId + ' .poll-message-container', data);
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/chatmessage.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/chatmessagedeleted.js
    
    socket.on('ChatMessageDeleted', function(data){
    
      for(var x = 0; x < data.Messages.length; x++) {
    
        $('.poll-message-container p[data-poll-id=' + data.Messages[x].ChatRoomMessageId + ']').each(function() {
    
          var parent = $(this).parents('.media').first();
    
         
    
          $(this).parent().hide('slow', function() { $(this).remove(); } );
    
          
    
          //Remove the entire media object if all chat messages for it have been removed: 
    
          if(parent.find('p').length == 0) parent.remove();
    
        }); 
    
      }
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/chatmessagedeleted.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/comment.js
    
    socket.on('Comment', function(data){
    
      console.log(JSON.stringify(data));
    
      
    
      
    
      if(data.ReplyToCommentId) { 
    
        thinkster.transform.comments($('.poll-comments[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .poll-comments-replies[data-poll-commentid="' + data.ReplyToCommentId + '"]')[0], {Comments: [data]});
    
      } else { 
    
        thinkster.transform.comments($('.poll-comments[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"]')[0], {Comments: [data]});
    
      }
    
      
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/comment.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/eventattenddelta.js
    
    socket.on('EventAttendanceDelta', function(data){
    
      if(data.OldAttendanceTypeCode != '' && $('.event-attendees-' + data.OldAttendanceTypeCode + ' div[data-profile-id=' + data.ProfileId + ']').length > 0) {
    
        var icon = $('.event-attendees-' + data.OldAttendanceTypeCode + ' div[data-profile-id=' + data.ProfileId + ']');
    
        
    
        icon.remove();
    
        
    
        if($('.event-attendees-' + data.OldAttendanceTypeCode + ' .row > div').length == 0) {
    
          thinkster.transform.attendees($('.event-attendees-' + data.OldAttendanceTypeCode)[0], null); 
    
        }
    
        
    
        if($('.event-attendees-' + data.NewAttendanceTypeCode + ' > .row').length == 0) {
    
          $('.event-attendees-' + data.NewAttendanceTypeCode).html('<div class="row m1"></div>'); 
    
        }
    
        
    
        $('.event-attendees-' + data.NewAttendanceTypeCode + ' > .row').prepend(icon);     
    
      } else { 
    
        thinkster.transform.attendees($('.event-attendees-' + data.NewAttendanceTypeCode)[0], {People: [data]});
    
      } 
    
        
    
      
    
      $('.poll-attendance-' + data.OldAttendanceTypeCode + '[data-event-id=' + data.EventId + ']').each(function() {
    
        var val = parseInt($(this).text()) || 0;
    
        
    
        val--; 
    
        
    
        $(this).text(val);
    
      });
    
      
    
      $('.poll-attendance-' + data.NewAttendanceTypeCode + '[data-event-id=' + data.EventId + ']').each(function() {
    
        var val = parseInt($(this).text()) || 0;
    
        
    
        val++; 
    
        
    
        $(this).text(val);
    
      });
    
      //var rcount = $('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text() || 0;
    
      
    
      
    
      //$('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text(rcount + data.Delta);
    
      
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/eventattenddelta.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/followrequest.js
    
    socket.on('FollowRequest', function(data){  
    
      /*$('.poll-follow-request[data-follower-id=' + data.FollowerId + ']').hide('slow', function() { 
    
        $(this).remove();
    
        $('.poll-follow-request-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount); 
    
      });     */
    
      thinkster.transform.followrequests($('.poll-follow-request-list'), data, true);
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/followrequest.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/imagepublished.js
    
    socket.on('ImagePublished', function(data){
    
      console.log(JSON.stringify(data));    
    
      
    
      
    
      $('.poll-image-list').each(function() { 
    
        thinkster.transform.imageList(this, {Images: [data]});
    
      });
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/imagepublished.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/notification.js
    
    socket.on('Notification', function(data){
    
      console.log(JSON.stringify(data));
    
      
    
      thinkster.transform.notifications('.poll-notification-list', data, true);
    
      
    
      //Client-side notification:
    
      if(window.Notification && Notification.permission!=="granted") {
    
        //if it is not supported request permission
    
    	 Notification.requestPermission(function(status){
    
        	if(Notification.permission!==status){
    
        	Notification.permission=status;
    
        	}
    
      	});  
    
      }
    
    
    
      
    
      if(window.Notification && Notification.permission=="granted") {
    
        var lastIndex = 0;
    
        var body = '';
    
        var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
    
        var match;
    
        var url = '';
    
        
    
        while ((match = re.exec(data.Text)) !== null) {
    
          if(match.index > lastIndex) {
    
            body += data.Text.substring(lastIndex, match.index);        
    
          } 
    
          
    
          //text += match[3];
    
          var anchor = $('<a />').html(match[3]);
    
          
    
          switch(match[1]) {
    
            case 'e':
    
              url = '/events/' + match[2];
    
            break;
    
          
    
            case 'u':
    
              url = '/profile/' + match[2];
    
            break;
    
            
    
            case 'p':
    
              url = '/poll/' + match[2];
    
            break; 
    
            
    
            case 'm':
    
              url = '/media/' + match[2];
    
            break;
    
          }
    
          
    
          body += match[3];
    
          
    
          //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
    
          
    
          lastIndex = re.lastIndex;            
    
        }
    
        
    
        if(data.Text && data.Text.length > lastIndex) {
    
          body += data.Text.substring(lastIndex); 
    
        }
    
        
    
      
    
        var title="Thinkster Notification";//title for notification
    
      	var options={//options for notification
    
      	body:body,
    
        icon:data.AvatarMicrothumbUrl,
    
        tag: data.GroupingValue              
    
      	
    
      	}
    
       noti = new Notification(title,options);//permission is granted, display notification
    
       noti.onclick = function() { window.location = url; };  
    
      }    
    
      
    
      /*
    
      $('.poll-notification[data-id=' + data.NotificationId + ']').each(function() {
    
        var parent = $(this).parent();
    
        
    
        $(this).find('.poll-chatroom-preview').html(data.Messages[0].Message);
    
        
    
        if(data.Messages[0].ProfileId != thinkster.CurrentUser.ProfileId) {
    
          $(this).addClass('poll-chat-unread'); 
    
        }
    
        
    
        parent.prepend($(this).remove());     
    
        
    
        var unreadCount = $('#poll_menu_chat_preview').find('.poll-chat-room.poll-chat-unread').length;
    
        $('#chat_top_menu').find('.badge').html(unreadCount == 0 ? '' : unreadCount); 
    
      }); 
    
      
    
      if(thinkster.CurrentPage.chatMessages) thinkster.transform.notifications('#ci' + data.NotificationId + ' .poll-message-container', data);
    
      */
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/notification.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/notificationread.js
    
    socket.on('NotificationRead', function(data){
    
      $('.poll-notification[data-id=' + data.NotificationId + ']').each(function() {
    
        $(this).removeClass('poll-unread');
    
        $(this).find('.poll-read-toggle-action').data('read', true).text('Mark as Unread'); 
    
      }).promise().done(function() {     
    
        $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
    
      });
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/notificationread.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/notificationremoved.js
    
    socket.on('NotificationRemoved', function(data){
    
      console.log(JSON.stringify(data));
    
      $('.poll-notification[data-id=' + data.NotificationId + ']').hide('slow', function() { 
    
        $(this).remove();
    
        $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount); 
    
      });
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/notificationremoved.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/notificationunread.js
    
    socket.on('NotificationUnread', function(data){
    
      $('.poll-notification[data-id=' + data.NotificationId + ']').each(function() {
    
        $(this).addClass('poll-unread');
    
        $(this).find('.poll-read-toggle-action').data('read', false).text('Mark as Read'); 
    
      }).promise().done(function() {     
    
        $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
    
      });
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/notificationunread.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/pollanswer.js
    
    socket.on('PollAnswer', function(data){
    
      //console.log(data);    
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/pollanswer.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/reaction.js
    
    socket.on('Reaction', function(data){
    
    //  console.log(JSON.stringify(data));
    
      
    
      var rcount = parseInt($('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text()) || 0;
    
      
    
      
    
      $('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text((rcount + data.Delta) == 0 ? '' : (rcount + data.Delta));
    
      
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/reaction.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/removefollowrequest.js
    
    socket.on('RemoveFollowRequest', function(data){  
    
      $('.poll-follow-request[data-follower-id=' + data.FollowerId + ']').hide('slow', function() { 
    
        $(this).remove();
    
        $('.poll-follow-request-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount); 
    
      });
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/removefollowrequest.js
    
    // Start of /var/pollsite/prod/inc/thinkster.js/socket/timeline.js
    
    socket.on('Timeline', function(data){
    
      console.log(JSON.stringify(data));
    
      
    
      $('.poll-personal-newsfeed').each(function() { 
    
        thinkster.transform.timeline(this, {Items: [data]}, true);
    
      });
    
      
    
    });    
    // End of /var/pollsite/prod/inc/thinkster.js/socket/timeline.js


    
    thinkster.connection = socket;
  };


  $(document).ready(function(){
    $(this).thinksterInitialize();
    
    $('.poll-comment-box').commentbox();        
    $('.poll-react').react();
    if($.fn.material_select) $('.mdb-select').material_select();
  });

})(jQuery, thinkster);

jQuery.fn.thinksterInitialize = function() {
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/chatareas.js
   $(this).find('textarea.poll-autosize').each(function() {
     var area = this;
     var textAreaAdjust = function() {
       area.style.height = "1px";
       area.style.height = (25+area.scrollHeight)+"px";
     }
     
     $(this).change(textAreaAdjust).keyup(textAreaAdjust);
       
   });   // End of /var/pollsite/prod/inc/thinkster.js/initialize/chatareas.js
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/doLogin.js
   var doLogin = function(e) {
     var em = $('#li_email').val().trim();
     var pw = $('#li_password').val().trim();
   
     e.stopPropagation();
   
     master.Login(em, pw, function(json) {
       var result = jQuery.parseJSON(json);
   
       if(result.Success) {
         window.location = window.location;
       } else if(result.Validation && result.Validation.length > 0) { 
         for(var v = 0; v < result.Validation.length; v++) {
           alert(result.Validation[v].Argument + ' ' + result.Validation[v].Message); 
         }            
       } else {
         alert(result.Message); 
       }
   
     }, alert);
   };
   
   $('#ts_login').digifiEnterKey(doLogin);
   $('#li_login').click(doLogin);
   
   
   $('.header-fb-login').click(function() {
     FB.login(function(response) {
       if(response.status == 'connected') {
   
         master.FacebookLogin(function(json) {
   
           var result = jQuery.parseJSON(json);
           if(result.Success) {
             window.location = window.location;
           } else {
             alert(result.Message);
           }
         }, alert);
   
       } else {
         alert('Sign in to Facebook failed or was cancelled.');
       }
       // handle the response
     }, {scope: 'public_profile,email'});
   });   // End of /var/pollsite/prod/inc/thinkster.js/initialize/doLogin.js
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/flagging.js
   $(this).find('.poll-flag-button').click(function() {
     var button = this;
     var modal = $(button).data('flag-modal');
     var uniqueId = $(button).attr('data-poll-targettype') +  $(button).attr('data-poll-targetid');  
     
     if(!modal) {
       //Create the modal:
       
       modal = $([
   '<div class="modal fade">', 
     '<div class="modal-dialog modal-lg modal-notify modal-danger" role="document"> ', 
       '<div class="modal-content"> ',                                                    
         '<div class="modal-header"> ',  
           '<h5 class="modal-title" style="color: #fff;">Modal title</h5> ',  
           '<button type="button" class="close"  style="color: #fff;" data-dismiss="modal" aria-label="Close"> ',  
             '<span aria-hidden="true">&times;</span>                                                             ',  
           '</button> ',  
         '</div> ',        
         '<div class="modal-body"> ',  
           '<div class="form">        ',  
             '<div class="form-group">  ',  
               '<label for="poll_flag_type' + uniqueId + '">Reason for this report<sup class="text-danger">*</sup></label> ',              
               '<select id="poll_flag_type' + uniqueId + '" class="form-control poll-flag-type" style="display: inherit !important;" id="exampleSelect1"> ',  
                 '<option value="">[make a selection]</option> ',  
                 '<option value="SO">Solicitation/Spam/bot</option> ',  
                 '<option value="CO">Copyright Infringement</option>   ',  
                 '<option value="IL">Links to illegal content or filesharing sites</option> ',  
                 '<option value="PO">Pornographic Content</option> ',  
                 '<option value="OT">Other (describe below)</option> ',  
               '</select> ',              
             '</div> ',  
             '<div class="form-group"> ',  
                 '<label for="poll_flag_details' + uniqueId + '">Details<sup class="text-danger">*</sup></label> ',  
                 '<textarea class="form-control poll-flag-details" id="poll_flag_details' + uniqueId + '" rows="5"></textarea> ',  
                 '<small id="poll_flag_details' + uniqueId + '_help" class="form-text text-muted">Provide a detailed description as to why you are reporting this ' + $(button).attr('data-poll-type') + '.</small> ',  
             '</div> ',  
           '</div> ',  
           '<p class="small text-danger"><i class="fa fa-warning"></i> Abuse of the reporting system may result in account termination</p> ',  
         '</div> ',  
         '<div class="modal-footer"> ',  
           '<button type="button" class="btn btn-primary">Report</button> ',  
           '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> ',  
         '</div> ',  
       '</div> ',  
     '</div> ',  
   '</div>'].join('\n'));
   
       if($(button).attr('data-poll-targettype') == 'PR') {
         modal.find('.modal-body').prepend('<p></p>'); 
       }
   
       modal.find('.mdb-select').material_select();
       modal.find('.modal-title, .modal-footer .btn-primary').text('Report ' + $(button).attr('data-poll-type'));
                 
       modal.data('flag-object', {'TargetTypeCode': $(button).attr('data-poll-targettype'), 'TargetId': $(button).attr('data-poll-targetid')});                                         
       modal.modal();
       
       $(button).data('flag-modal', modal);
       
       modal.find('.modal-footer .btn-primary').click(function() {
         var flagTypeCode = modal.find('.poll-flag-type').val();
         var details = modal.find('.poll-flag-details').val();
         
         if(flagTypeCode == '') {
           alert('You must select the reason for this report');
           modal.find('.poll-flag-type').focus();
           return; 
         }
         
         if(details == '') {
           alert('You must provide details for this report');
           modal.find('.poll-flag-details').focus();
           return; 
         }
         
         modal.find('.modal-footer .btn').prop('disabled', true);
         
         var fo = modal.data('flag-object');
         
         master.FlagContent(fo.TargetTypeCode, fo.TargetId, flagTypeCode, details, function(result) {
           if(result.Success) {
             alert('Your report has been sent. Thinkster administration will be reviewing it shortly.');
             modal.modal('hide');
           } else {
             alert(result.Message); 
           }
           modal.find('.modal-footer .btn').prop('disabled', false); 
         }, function(result) {
           alert(result);
           modal.find('.modal-footer .btn').prop('disabled', false); 
         }); 
       });
          
     }
     
     modal.find(':input').val('');
     
     modal.modal('show');
       
   });   // End of /var/pollsite/prod/inc/thinkster.js/initialize/flagging.js
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/followrequests.js
   $(this).find('.poll-accept-follow-request').click(function() {
     //Disable all buttons related to this request while it is processing:
     $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
     $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
     //alert('hi decline ' + $(this).attr('data-poll-id'));
     master.AcceptFollowRequest($(this).attr('data-poll-id'), function() {
       //Do nothing; push should take care of everything 
     }, alert, function() {
       //Re-enable all buttons related to this request while it is processing: 
       $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
       $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
     });
   });
   
   $(this).find('.poll-decline-follow-request').click(function() {
     //Disable all buttons related to this request while it is processing:
     $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
     $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
     //alert('hi decline ' + $(this).attr('data-poll-id'));
     master.DeclineFollowRequest($(this).attr('data-poll-id'), function() {
       //Do nothing; push should take care of everything 
     }, alert, function() {
       //Re-enable all buttons related to this request while it is processing: 
       $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
       $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
     });
   });   // End of /var/pollsite/prod/inc/thinkster.js/initialize/followrequests.js
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/overflowConceal.js
   $(this).find('.poll-overflow-conceal').each(function() {
     var container = this;
     $(this).data('expanded', false);
     if($(this).find('> div').height() > $(this).height()) {
       //Create the "more" link:
       var buttonContainer = $('<div class="mb-1 text-center"></div>');
       var button = $('<button type="button" class="btn btn-success btn-sm">== more ==</button>');
       
       buttonContainer.append(button); 
       
       button.click(function() {
         if($(container).data('expanded')) {
           $(container).css('height', '80px'); 
           $(this).removeClass('btn-warning').addClass('btn-success').text('== more ==');
         } else {
           $(container).css('height', $(container).find('> div').height());
           $(this).addClass('btn-warning').removeClass('btn-success').text('== less =='); 
         }                   
         
         $(container).data('expanded', !$(container).data('expanded'));
       });
       
       $(this).after(buttonContainer);
     }
   });   // End of /var/pollsite/prod/inc/thinkster.js/initialize/overflowConceal.js
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/search.js
   var searchDelayTimer = 0;
   
   var doSiteSearch = function() {
     var searchString = $('#poll_site_search').val().trim();
     
     if(searchString == '') {
       $('#poll_site_search').dropdown();
       $('#poll_menu_header_search_results').html(''); 
     } else {    
       master.HeaderSearch(searchString, function(results) {
         $('#poll_site_search').dropdown();
         
         thinkster.transform.searchResults('#poll_menu_header_search_results', results.SearchResults);
         
         //$('#poll_menu_header_search_results').html(JSON.stringify(results));   
       }, alert);
     } 
   };
   
   $('#poll_site_search').keyup(function() {
     clearTimeout(searchDelayTimer);
     setTimeout(doSiteSearch, 350); 
   }).digifiEnterKey(function() {
     clearTimeout(searchDelayTimer);
     doSiteSearch();
   }).focus(function() {
     if($('#poll_menu_header_search_results').html() != '') {
       $('#poll_site_search').dropdown(); 
     }  
   });   // End of /var/pollsite/prod/inc/thinkster.js/initialize/search.js
   // Start of /var/pollsite/prod/inc/thinkster.js/initialize/sfuploader.js
   /*$(this).find('.poll-sfuploader').each(function() {
     var target = this;
     
     $(target).fileupload({
         dataType: 'json',
         add: function(e, data) {
           adminquestions.GetUploadToken(function(result) {
             //alert('file added. Add token ' + result + '? Auto submit!');
             data.formData = {token: result};
             data.submit(); 
           }, alert);         
         },
         done: function (e, data) {
   
             
             $(target).find('.upload-step-1').attr('hidden', 'hidden');
             $(target).find('.upload-step-2').attr('hidden', null);
             
             var row = $('<div class="row" />');
             var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
             var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result[0].Name).attr('src', data.result[0].ThumbnailUrl));
             var block = $('<div class="card-block"></div>');
             
             block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result[0].MediaId));
                   
             block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result[0].Name)));
             block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result[0].Description)));
             block.append($('<div class="form-group"></div>').append($('<select class="form-control" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result[0].ExposureTypeCode)));
             
             row.append(col);
             col.append(card);
             
             card.append(block);                   
             
             $(target).find('.upload-step-2').html(row);
             
             $('#au_existing').val(null);
             
             $(target).find('.modal-footer > .btn-success').text('Post').prop('disabled', false);
         }
     });
    
   });                */   // End of /var/pollsite/prod/inc/thinkster.js/initialize/sfuploader.js

   
   return this;
};

// Start of /var/pollsite/prod/inc/thinkster.js/jquery/commentbox.js

/*******************************************************************************

 * commentbox

 *

 * Takes the given element and converts it into a comment entry box

 *

 ******************************************************************************/

jQuery.fn.commentbox = function() {

  return $(this).each(function() {

    var cbox = this;

    var id = 'comment_entry_' + $(cbox).attr('data-poll-targettype') + '_'+ $(cbox).attr('data-poll-targetid');

    var ta = $('<textarea class="form-control" style="overflow: hidden;" />').attr('id', id);

    var buttonBar = $('<div class="text-right"></div>');

    var attachButton = $('<button type="button" class="btn btn-md btn-secondary ml-3" title="Attach an image"><i class="fa fa-fw fa-picture-o text-muted"></i></button>'); 

    var emojiButton = $('<button type="button" class="btn btn-md btn-secondary ml-3" title="Insert Emoji"><i class="fa fa-fw fa-smile-o text-muted"></i></button>');

    var sendButton = $('<button type="button" class="btn btn-md btn-primary ml-3" title="Send Chat Message">Send</button>');

    

    var textAreaAdjust = function(e) {

      ta.css('height', '1px');

      ta.css('height', (ta.prop('scrollHeight') + 8) + 'px');

    }

    

    ta.change(textAreaAdjust).keyup(textAreaAdjust);   

    

    var sendComment = function() {

      var commentText = ta.val().trim();

      

      if(commentText == '') return;

       

      master.Comment($(cbox).attr('data-poll-targettype'), $(cbox).attr('data-poll-targetid'), $(cbox).attr('data-poll-replytoid') || null, commentText, function() {

        ta.val('').focus();  

      }, alert);

    }

     

    

    sendButton.click(sendComment);

    

    buttonBar.append(/*attachButton, emojiButton, */sendButton);

      

    $(this).addClass('rounded md-form').html(ta).append($('<label/>').attr('for', id).text('Write a comment...')).append(buttonBar);

    

    textAreaAdjust();

  });

  

};
// End of /var/pollsite/prod/inc/thinkster.js/jquery/commentbox.js

// Start of /var/pollsite/prod/inc/thinkster.js/jquery/react.js

/*******************************************************************************

 * commentbox

 *

 * Takes the given element and converts it into a comment entry box

 *

 ******************************************************************************/

jQuery.fn.react = function(reactionText, emoji) {

  return $(this).each(function() {

    var r = $(this);

    var showTimer = 0, hideTimer = 0;  

    

    var showReactionEmojis = function() {

      r.popover('show');

    }; 

    

    var hideReactionEmojis = function() {

      r.popover('hide');

    }; 

    

    var emojiList = $('#poll_reaction_list .poll-reaction-emojis').clone();

    

    digifi.initialize(emojiList);

    

    emojiList.on('mouseenter', function() { 

      clearTimeout(hideTimer);

    }).on('mouseleave', function() { 

      hideTimer = setTimeout(hideReactionEmojis, 700);

    }).bind('digifi.action', function(e, a) {

      e.stopPropagation();

      

      hideReactionEmojis();

      

      master.React(r.attr('data-poll-targettype'), r.attr('data-poll-targetid'), a.params[0], function() {

        r.find('.reaction-text').text($(a.element).attr('title')); 

        

        r.find('.emoji, .fa').replaceWith($('<span class="emoji mr-1"></span>').text($(a.element).text()));

      }, alert); 

    });    

    

    r.popover({content: emojiList[0], html: true, trigger: 'manual', placement: 'top', delay: 700});

    

    var rcount = r.attr('data-poll-reactioncount');

    var badge = $('<span class="badge badge-pill badge-primary ml-2 poll-reaction-count" aria-label="View who reacted"></span>').text(rcount <= 0 ? '' : rcount).click(function(e) {

      e.preventDefault();

      e.stopPropagation();

      

      /*var list = $(this).data('poplist');

      

      if(!list) {

         list = $(this).popover({content: '<div style="height: 150px;">Dynamic list should go here?</div>', title: function(el) { return ''; }, html: true, trigger: 'manual', placement: 'top', delay: 700});

         $(this).data('poplist', list);

      }    

      

      list.popover('show'); */

      

      var react_popup = $('#poll_reaction_modal');

      

      if(react_popup.length == 0) {

        react_popup = $('<div id="poll_reaction_modal" class="modal fade"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title">Reactions</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body" style="max-height: 280px; overflow: auto;">Reactions go here</div><div class="modal-footer"><button type="button" class="btn btn-warning" data-dismiss="modal">Close</button></div></div></div>');

        

        $('body').append(react_popup);

      }

      

      react_popup.find('.modal-body').html($('<div class="text-center"><i class="fa fa-spin fa-3x text-danger fa-refresh"></i></div>'));

      

      master.GetReactors($(this).parent().attr('data-poll-targettype'), $(this).parent().attr('data-poll-targetid'), function(json) {

        var data = jQuery.parseJSON(json);

                        

        if(data.Reactors.length == 0) { 

        } else {

          var groupingCode = '';

          var lg = $('<div class="list-group" />');

         

          for(var x = 0; x < data.Reactors.length; x++) {

            /*var dd = $('<dd class="mb-1 clearfix" />');  */

                                    

            if(data.Reactors[x].ReactionTypeCode != groupingCode) {

               //dl.append($('<dt class="mb-1" />').append($('<span class="emoji mr-1"></span>').text(data.Reactors[x].Emoticon), data.Reactors[x].Reaction));

               lg.append($('<h5 class="list-group-item strong" />').append($('<span class="emoji mr-1"></span>').text(data.Reactors[x].Emoticon), data.Reactors[x].Reaction));

               groupingCode = data.Reactors[x].ReactionTypeCode; 

            }    



            var item = $('<a class="list-group-item list-group-item-action d-flex justify-content-between" target="_top"></a>').attr('href', '/profiles/' + data.Reactors[x].ProfileName).text(data.Reactors[x].DisplayName);                

            

            item.prepend($('<img class="pull-left align-self-top mr-3 rounded-circle" width="32" height="32" style="border-radius: 50%;" />').attr('src', thinkster.fn.avatar(data.Reactors[x].AvatarMicrothumbUrl)));

            

            var val = (data.Reactors[x].Agree / data.Reactors[x].Total * 100.0).toFixed(2); 

    

            var percentage = $('<span class="min-chart small"><span class="percent"></span></span>').attr('id', 'compatibility_' + data.Reactors[x].ProfileId).attr('data-percent', val);



            item.append(percentage);

                

            lg.append(item);

            

            percentage.easyPieChart({

                barColor: "#4caf50",

                size: 48,

                onStep: function (from, to, percent) {

                    $(this.el).find('.percent').text(Math.round(percent));

                }

            });



          }

          

          react_popup.find('.modal-body').html(lg);

        }               

      }, alert);

      

      react_popup.modal('show');

      

      //alert('Create reaction popup here & load it');

    }).on('mouseenter', function(e) {

      hideTimer = setTimeout(hideReactionEmojis, 100); 

      clearTimeout(showTimer);

      e.preventDefault();

      e.stopPropagation(); 

    });  

    

    r.append(emoji ? $('<span class="emoji mr-1"></span>').text(emoji) : $('<i class="fa fa-thumbs-up mr-1"></i>'), $('<span class="reaction-text" />').text(reactionText ? reactionText : 'React'), badge);

    

    r.on('mouseenter', function() {

      clearTimeout(hideTimer);

      showTimer = setTimeout(showReactionEmojis, 700);       

    }).on('mouseleave', function() {

      clearTimeout(showTimer);

      hideTimer = setTimeout(hideReactionEmojis, 700); 

    }).click(function() {

      clearTimeout(showTimer);

      

      //If we haven't reacted yet, just like:

      if(r.find('.emoji').length == 0) { 

        master.React(r.attr('data-poll-targettype'), r.attr('data-poll-targetid'), 'LI', function() {

          r.find('.reaction-text').text('Like'); 

        

          r.find('.emoji, .fa').replaceWith($('<span class="emoji emoji-li"></span>'));

        }, alert);

      } else {       

        hideReactionEmojis();

        master.RemoveReaction(r.attr('data-poll-targettype'), r.attr('data-poll-targetid'));

        r.find('.reaction-text').text('React');         

        r.find('.emoji, .fa').replaceWith($('<i class="fa fa-thumbs-up mr-1"></i>')); 

      }            

    }); 

    

  });

  

};
// End of /var/pollsite/prod/inc/thinkster.js/jquery/react.js


 function iFrameSize(obj, height) {
    $('iframe').css('height', (height + 40) + 'px');
    //$(obj).height( $(obj).contents().find("html").height() );    
}