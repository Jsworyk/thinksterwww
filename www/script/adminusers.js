$(document).ready(function() {
  var refreshTimer = null;

  var setRefresh = function() {
     clearTimeout(refreshTimer);
     
     refreshTimer = setTimeout(function() {
      $('#user_list').trigger('digifi.refresh'); 
     }, 400);
  };


  $('#f_name').keyup(setRefresh).change(setRefresh);
  $('#f_showinactive').change(setRefresh);
  
  $('#profile_details').bind('digifi.action', function(e, a) {    
    switch(a.action) {
      case 'ToggleActive':
        if(!confirm('Are you sure you want to change the active state of this user?')) return;
        
        adminusers.ToggleActive(function() {
          $('#profile_details').trigger('digifi.refresh'); 
        }, alert);
      break; 
    } 
  });

});