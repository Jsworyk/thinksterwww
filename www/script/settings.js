$(document).ready(function(){

	$('#password_change').digifiEnterKey(changePassword);
	/*$('#submit').click(changePassword);
	$('#edit_button').click(clearPassword);   */

	$('input[type=checkbox]').change(function() {
    
		var value = (this.checked ? "1" : "0");
		settings.SaveExposure(this.id,value);
	});

	$("select.poll-exposure").change(function() {

		var value = $(this).val();
		settings.SaveExposure(this.id,value);
	});

	$('#fb_login').click(function() {
      FB.login(function(response) {
        if(response.status == 'connected') {

          settings.FacebookLogin(function(json) {
            var result = jQuery.parseJSON(json);
            if(result.Success) {
              window.location = window.location;
            } else {
              alert(result.Message);
            }
          }, alert);

        } else {
          alert('Sign in to Facebook failed or was cancelled.');
        }
        // handle the response
      }, {scope: 'public_profile,email'});
	 });
   
   /*$('#profile_accounts').bind('digifi.action', function(e, a) {
    switch(a.action) {
      case 'SetPrimaryAccount':
        if(!confirm('Are you sure you want to make this your primary account? This avatar and display name will be used for your Thinkster profile')) return;  
        settings.SetPrimaryAccount(a.params, function() {
          window.location = window.location; 
        }, alert);
      break; 
    } 
   });  */
   
   /*$('#password_change').bind('digifi.action', function(e, a) {
    if(a.action == 'Save') {  
      changePassword();  
    }
   }); */
});

thinkster.transform.recentImages = function(container, data) {  
  if(data.Images.length == 0) {
    $(container).html('<div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>you have not uploaded any images yet</p></div></div>');
  } else {
    
    var row = $('<div class="row m-1" />');
    
    for(var x = 0; x < data.Images.length; x++) {
      var col = $('<div class="col-lg-4 col-sm-3"></div>');
      var imageDiv = $('<div class="mt-2 text-center" />');
      var img = $('<img width="220" height="200" />').data('id',  data.Images[x].MediaId).attr('src', data.Images[x].ThumbnailUrl).attr('title', data.Images[x].Name + ' - ' + data.Images[x].Description);
      
      imageDiv.append(img);
      
      img.click(function() {
        $(container).find('img.rounded').removeClass('rounded poll-selected');
        $(this).addClass('rounded poll-selected');
        
        $('#au_existing').val($(this).data('id'));
        
        $('#avatar_uploader .modal-footer > .btn-success').text('Change').prop('disabled', false);
      }); 
      
      col.append(imageDiv);
      
      row.append(col); 
    }
  
    $(container).html($('<p class="small text-center">...or select a recent image from your gallery</p>')).append(row);
  }
};

thinkster.transform.blockedUsers = function(container, data) {
  if(data.BlockedUsers.length == 0) {
    $(container).html($('<div class="alert alert-success"><p>You are not currently blocking any users</p></div>'));    
  } else {
    var ul = $('<ul class="list-unstyled"></ul>');
     
    for(var x = 0; x < data.BlockedUsers.length; x++) {
      var li = $('<li class="mb-2"></li>');
      var undoButton = $('<button type="button" class="btn btn-secondary btn-sm pull-right"><i class="fa fa-undo"></i><span class="sr-only">k</span></button>');
      
      undoButton.attr('title', 'Unblock ' + data.BlockedUsers[x].DisplayName);
      undoButton.find('.sr-only').html('Unblock ' + data.BlockedUsers[x].DisplayName);
      undoButton.data('user', data.BlockedUsers[x]);
      
      undoButton.click(function() {
        if(!confirm('Are you sure you want to unblock ' + $(this).data('user').DisplayName + '?')) return;
        
        $(container).find('*[data-digifi-action]').prop('disabled', true);
        
        settings.UnblockUser($(this).data('user').ProfileId, function() {
           $(container).trigger('digifi.refresh');
        }, alert);
      });
        
      var link = $('<a></a>').text(data.BlockedUsers[x].DisplayName);
      
      link.attr('href', '/profile/' + data.BlockedUsers[x].ProfileName);
      
      li.append(undoButton, link, $('<br />'), $('<div class="small"></div>').html(data.BlockedUsers[x].ProfileName));
      
      ul.append(li);     
    }
    
    $(container).html(ul);
  } 
};

thinkster.transform.profileAccounts = function(container, data) {
  $(container).html('');
  
  for(var x = 0; x < data.Accounts.length; x++) {
    var card = $('<div class="card mb-4">');
    var cardBlock = $('<div class="card-block" />');
    var account = data.Accounts[x];
    var title = $('<h4 class="card-title"></h4>');
    var subtitle = $('<h6 class="card-subtitle mb-2 text-muted"></h6>');
    var text = $('<p class="card-text"></p>');
    
    subtitle.append(account.DisplayName, ' ', $('<small />').text(account.EmailAddress));
    
    if(account.IsPrimary) {
      title.append($('<i class="fa fa-star text-warning mr-2" title="This is your primary account. It sets your Thinkster profile\'s avatar and display name."></i>')); 
    } else { 
      var setPrimary = $('<i class="fa fa-star text-muted mr-2" title="Click here to make this your primary account and use its avatar and display name for your Thinkster profile."></i>').data('id', account.ProfileAccountId);
      
      setPrimary.click(function() {
        var icon = this;
        if(!confirm('Are you sure you want to make this your primary account? This avatar and display name will be used for your Thinkster profile')) return;  
        settings.SetPrimaryAccount($(this).data('id'), function() {
          $('#profile_accounts i.fa-star').prop('disabled', true);
          $(icon).removeClass('fa-star').addClass('fa-refresh fa-spin');
          $('#profile_accounts').trigger('digifi.refresh');
        }, alert);
      });
      
      title.append(setPrimary);
    }
    
    title.append(account.AccountType); 
    
    switch(account.AccountTypeCode) {
      case 'FB':
        var disconnect = $('<button type="button" class="btn btn-danger btn-sm" title="Disconnect your Facebook account from Thinkster">Disconnect</button>');
        
        disconnect.click(function() {
           alert('Disconnect account here, if not primary');
        });
        
        text.append(disconnect);
      break; 
      
      case 'PO':
        var changeAvatar = $('<button type="button" class="btn btn-primary btn-sm" title="Change your display picture">Avatar</button>');
        var edit = $('<button type="button" class="btn btn-info btn-sm" title="Edit your account details">Details</button>');
        var password = $('<button type="button" class="btn btn-warning btn-sm" title="Change your Thinkster password">Password</button>');
        var profileAccountId = account.ProfileAccountId;
        
        changeAvatar.click(function() {
          $('#avatar_step_1').attr('hidden', null);
          $('#avatar_step_2').html('').attr('hidden', '');
        
          $('#recent_uploads').trigger('digifi.refresh');                    
         
          $('#avatar_uploader .modal-footer > .btn-success').prop('disabled', true);
          
          $('#au_profile_account_id').val(profileAccountId);
          
          $('#avatar_uploader').data('profileaccountid', profileAccountId).modal('show');                   
                   
        });
        
        edit.click(function() {
          alert('to be added');
        });
        
        password.click(function() {
          $('#password_change').modal('show');
        });
        
        text.append($('<div class="btn-group" />').append(changeAvatar, edit, password));
      break;
    }
    
    cardBlock.append($('<img class="pull-left mr-2" width="110" height="100" />').attr('src', account.AvatarThumbnailUrl));
    cardBlock.append(title, subtitle, text);
    card.append(cardBlock);
    
    $(container).append(card);
    
    //item.find('time.timeago').timeago();
  }
  
  
  $('#avatar_upload').fileupload({
      dataType: 'json',
      add: function(e, data) {
        settings.GetUploadToken(function(result) {
          //alert('file added. Add token ' + result + '? Auto submit!');
          data.formData = {token: result};
          data.submit(); 
        }, alert);         
      },
      done: function (e, data) {
          /*$.each(data.result.files, function (index, file) {
              $('<p/>').text(file.name).appendTo(document.body);
          });*/
          //alert(JSON.stringify(data.result));
          
          $('#avatar_step_1').attr('hidden', 'hidden');
          $('#avatar_step_2').attr('hidden', null);
          
          var row = $('<div class="row" />');
          var col = $('<div class="col-sm-12 col-lg-6 offset-lg-3" />');
          var card = $('<div class="card"></div>').append($('<img class="card-img-top" width="220" height="200">').attr('alt', data.result.Files[0].Name).attr('src', data.result.Files[0].ThumbnailUrl));
          var block = $('<div class="card-block"></div>');
          
          block.append($('<input type="hidden" data-digifi-value="MediaId" />').val(data.result.Files[0].MediaId));
          //block.append($('<input type="hidden" data-digifi-value="ProfileAccountId" />').val($('#avatar_uploader').data('profileaccountid')));
          
        
          block.append($('<div class="form-group"></div>').append($('<input type="text" id="new_media_name" maxlength="500" data-digifi-value="Name" class="form-control" placeholder="Title" />').val(data.result.Files[0].Name)));
          block.append($('<div class="form-group"></div>').append($('<textarea class="form-control" placeholder="Description" data-digifi-value="Description"></textarea>').val(data.result.Files[0].Description)));
          block.append($('<div class="form-group"></div>').append($('<select class="form-control" data-digifi-value="ExposureTypeCode"><option value="PU">Public (everyone can see this)</option><option value="FR">Friends Only</option><option value="PR">Private (only you can see this)</option></select>').val(data.result.Files[0].ExposureTypeCode)));
          
          row.append(col);
          col.append(card);
          
          card.append(block);                   
          
          $('#avatar_step_2').html(row);
          
          $('#au_existing').val(null);
          
          $('#avatar_uploader .modal-footer > .btn-success').text('Post').prop('disabled', false);
      }
  });




  //$(container).html(data.ChatRooms.length);
};

function clearPassword() {

	$('#old_password').val('');
	$('#new_password').val('');
	$('#op_message').html('');
	$('#np_message').html('');
	$('#message').html('');
}

function changePassword() {    
	var opw = $('#pc_current').val().trim();
	var npw = $('#pc_new').val();
  var cpw = $('#pc_confirm').val();
  
  if($('#password_change').digifiValidate()) {
    alert('good'); 
  }

  /*
  var em = $('#password_change').attr('data-email').trim();
	$('#op_message').html('');
	$('#np_message').html('');
	$('#message').html('');

	if(opw == '') {
		$('#op_message').html("Please enter your current password");
	}
	else if(npw == '') {
		$('#np_message').html("Please enter your new password");
	}
	else {
		//Check that the old password is correct before changing the password
		settings.VerifyThinksterPassword(opw, function (results) {

			var password = JSON.parse(results);

			//Password correct, now change it with the new password they provided
			if(password.Success) {

				settings.ChangeThinksterPassword(npw, function() {

					$('#message').html('Your password has been successfully changed.');
					$('#old_password').val('');
					$('#new_password').val('');

					}, alert);
			}
			else
			{
				//Password not correct, blank out both inputs and post message
				$('#old_password').val('');
				$('#new_password').val('');
				$('#message').html(password.Message);
			}

		}, alert);
	}    */

}