$(document).ready(function() {
  $('#add_endpoint').click(function(e) {
    e.preventDefault();
    
    document.getElementById('edit_endpoint').digifi.newObject('PushEndpoint');
  }); 
  
  $('#edit_endpoint_button').click(function() {
    document.getElementById('edit_endpoint').digifi.editObject($(this).attr('data-digifi-id'));     
  });
  
  $('#edit_endpoint').bind('digifi.save-complete', function() {
    window.location = '/pushreference/' + $('#ee_name').val(); 
  });
});