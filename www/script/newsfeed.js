$(document).ready(function() {
  thinkster.connection.on('BannerChanged', function(data) {      
    $('#profile_banner_image').attr('src', data.Url); 
  }); 
});

thinkster.transform.questionList = function(context, data) {
  if(data.Success) {
    var questions = data.Trending;
    var list = $('<div class="list-group list-group-flush"></div>');
    
    if(data.Trending.length == 0) {
      list = $('<div class="alert alert-info">No questions are currently trending in the system.</div>');
    } else {  
      for(var x = 0; x < data.Trending.length; x++) {
        var item = $('<a class="list-group-item justify-content-between"></a>');
        
        item.attr('href', '/poll/' + data.Trending[x].UrlSegment);
        item.text(data.Trending[x].Question); 
        item.append($('<span class="badge badge-primary badge-pill"></span>').text(data.Trending[x].ResponseCount));
        //list.append($('<span class="badge badge-primary badge-pill pull-right"></span>').text(data.Trending[x].ResponseCount));
        
        list.append(item);
      }
    }
    
    $(context).html(list); 
  } else {
    $(context).html($('<div class="alert alert-danger"></div>').text(data.Message)); 
  }   
};