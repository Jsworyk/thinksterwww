$(document).ready(function() {
  var adjustLayout = function() {     
    $('.poll-message-container').each(function() {
      var h = $('body').height() - $('header > div').height() - $(this).parent().find('.poll-chat-footer').height();
    
      console.log($('#chat_interfaces').height() + ', ' + $(this).parent().find('.poll-chat-footer').height());
      $(this).css('height', h + 'px'); 
    });  
  };
  
  $(window).resize(adjustLayout);


  var pasteIntoInput = function(el, text) {
      el.focus();
      if (typeof el.selectionStart == "number"
              && typeof el.selectionEnd == "number") {
          var val = el.value;
          var selStart = el.selectionStart;
          el.value = val.slice(0, selStart) + text + val.slice(el.selectionEnd);
          el.selectionEnd = el.selectionStart = selStart + text.length;
      } else if (typeof document.selection != "undefined") {
          var textRange = document.selection.createRange();
          textRange.text = text;
          textRange.collapse(false);
          textRange.select();
      }
  }
  
  var internalSend = function() {
    var messageToSend = $(this).val().trim();
    var tb = this;
    
    if(messageToSend == '') {
      $(this).focus();
      return;
    }
    
    $(tb).prop('disabled', true);
    
    messages.SendMessage($(this).data('chatRoomId'), messageToSend, function(json) {
      //alert(json);
      $(tb).prop('disabled', false);            
    }, function(result) { 
      alert(result);
      $(tb).prop('disabled', false);
    }, function() { $(tb).prop('disabled', false).val('').focus(); });      
  }
  
  var handleEnter = function (evt) {      
      if (evt.keyCode == 13 && evt.shiftKey) {
          if (evt.type == "keypress") {
              //pasteIntoInput(this, "\n");
          }
          
      }  else if (evt.keyCode == 13) {
        evt.preventDefault();
        internalSend.call(this);
                 
                 
      }
  }

    thinkster.CurrentPage.createChatInterface = function(room) {        
      var chatInterface = $('#ci' + room.ChatRoomId);
      
      if(chatInterface.length == 0) {
         var id = room.ChatRoomId;
         //chatInterface = $('<div class="poll-chat-interface p-2 d-flex flex-column" style="height: 100%;"></div>');
         chatInterface = $('<div class="poll-chat-interface p-2"></div>');
         
         chatInterface.attr('id', 'ci' + id);
         
         //var message = $('<div class="poll-message-container d-flex flex-column" data-digifi-source="messages.GetMessages" data-digifi-json-transform="thinkster.CurrentPage.chatMessages"></div>');
         var message = $('<div class="poll-message-container" data-digifi-source="messages.GetMessages" data-digifi-json-transform="thinkster.CurrentPage.chatMessages"></div>');
         var textarea = $('<textarea class="form-control align-self-center mb-0" rows="3" cols="30"></textarea>');
         var chatbox = $('<div class="m-2 mt-auto poll-chat-footer d-flex justify-items-center poll-chatbox"></div>').append(textarea);
         var joinbox = $('<div class="p-2 mt-auto text-center poll-chat-footer"><p>You have been invited to chat</p></div>');
         var acceptChatButton = $('<button type="button" class="btn btn-secondary">Accept and Chat</button>');
         var sendButton = $('<span class="ml-2 mr-2 align-self-center poll-clickable"><i class="fa fa-paper-plane fa-2x text-primary"></i></span>');                  
         
         joinbox.append(acceptChatButton);
         
         acceptChatButton.click(function() { 
          messages.JoinChatRoom(room.ChatRoomId, function() {
            joinbox.remove();
            chatInterface.append(chatbox);
            
            chatbox.find('textarea').focus(); 
          }, thinkster.fn.alert); 
         });
         
         chatbox.append(sendButton);
         
         textarea.data('chatRoomId', id).keydown(handleEnter).keypress(handleEnter);
         sendButton.click(function() { internalSend.call(textarea); });
         
         textarea.focus(function() {
          messages.UpdateReadDate(room.ChatRoomId, function() {
            message.find('.poll-chat-unread').removeClass('poll-chat-unread');
            $('.poll-chat-room[data-id=' + room.ChatRoomId + ']').removeClass('poll-chat-unread');
            var unreadCount = $('#poll_menu_chat_preview').find('.poll-chat-room.poll-chat-unread').length;
            $('#chat_top_menu').find('.badge').html(unreadCount == 0 ? '' : unreadCount); 
          }, digifi.fn.alert);          
         });
         
         message.attr('data-digifi-source-args', id);
         
         chatInterface.append(message, room.HasJoined ? chatbox : joinbox);
         
         $('#chat_interfaces').append(chatInterface);
         
         //chatInterface.html('<div>Loading...</div>');
         
         digifi.initialize(chatInterface); //:not(.digifi-initialized)');
         
         chatInterface.find('div').trigger('digifi.refresh');                                                                       
      }
      
      $('#chat_interfaces .poll-chat-interface').addClass('d-none');
      chatInterface.removeClass('d-none');
      
      adjustLayout();     
      
      return chatInterface;       
    };
    
    
  thinkster.CurrentPage.chatMessages = function(container, data) {
    
    if($(container).find('div').length == 0) $(container).html('');
    
    var prev = $(container).find('> div').last();
    
    for(var x = 0; x < data.Messages.length; x++) {   
    
      if((x == 0 || data.Messages[x].Date != data.Messages[x - 1].Date) && $(container).find('.poll-chat-date').last().text() != data.Messages[x].Date) {
        $(container).append($('<div class="poll-chat-date"></div>').append($('<span></span>').text(data.Messages[x].Date))); 
      }   
      
      var div = $('<div class="d-flex"></div>');
      var messageContainer = $('<div></div>');            
      
      var message = $('<p class="rounded poll-chat-message mb-0">').attr('data-poll-id', data.Messages[x].ChatRoomMessageId).data('poll-message', data.Messages[x]).html(data.Messages[x].Message);
      
      if(data.Messages[x].ProfileId == thinkster.CurrentUser.ProfileId) {
        messageContainer.addClass('ml-auto');
        message.addClass('poll-chat-self');
      } else {
        messageContainer.addClass('mr-auto'); 
      }           
      
      messageContainer.append(message);

      var dt = new Date(data.Messages[x].Created);      
      var time = (dt.getHours() < 10 ? '0' : '') + dt.getHours() + ':' + (dt.getMinutes() < 10 ? '0' : '') + dt.getMinutes(); 
      
      messageContainer.append($('<div class="small text-info mb-2" />').append($('<time class="timeago" />').attr('datetime', data.Messages[x].Created).text(time)));
      
      div.append(messageContainer);

      if(x == data.Messages.length - 1 || data.Messages[x + 1].ProfileId != data.Messages[x].ProfileId) {      
        var img = $('<img class="d-flex rounded-circle" width="64" height="64">').attr('src', data.Messages[x].AvatarUrl).attr('alt', data.Messages[x].DisplayName);
        if(data.Messages[x].ProfileId == thinkster.CurrentUser.ProfileId) {
          img.addClass('ml-2');
          div.append(img);
        } else {
          img.addClass('mr-2');
          div.prepend(img);
        }                
      } else {
        var placeHolder = $('<span style="width: 64px;"></span>');
        
        if(data.Messages[x].ProfileId == thinkster.CurrentUser.ProfileId) {
          placeHolder.addClass('ml-2');
          div.append(placeHolder);
        } else {
          placeHolder.addClass('mr-2');
          div.prepend(placeHolder);
        } 
      }
      
      $(container).append(div);
      
      //messageContainer.find('time.timeago').timeago();
          
      
      /*if(prev.length != 0 && prev.data('poll-id') == data.Messages[x].ProfileId) {
        //div.append($('<i class="d-flex mr-3 fa fa-3x">&nbsp;</i>'));
        var messageContainer = $('<div class="ml-auto"></div>');
        
        var message = $('<p class="rounded">').attr('data-poll-id', data.Messages[x].ChatRoomMessageId).data('poll-message', data.Messages[x]).html(data.Messages[x].Message);
        
        if(data.Messages[x].ProfileId == thinkster.CurrentUser.ProfileId) {
          message.addClass('poll-chat-self');
        }
        
        if(data.Messages[x].Unread) {
          message.addClass('poll-chat-unread');
        } 
        
        messageContainer.append(message);
        
        prev.find('.media-body').append($('<div class="d-flex justify-content-center" />').append(messageContainer));
        
      } else {
        var div = $('<div class="media mt-auto mb-2" />');
      
        div.data('poll-id', data.Messages[x].ProfileId);
        
        var img = $('<img class="d-flex rounded-circle" width="64" height="64">').attr('src', data.Messages[x].AvatarUrl).attr('alt', data.Messages[x].DisplayName);
      
        //div.append($('<i class="d-flex mr-3 fa fa-user fa-3x" alt="avatar" />'));
        
        var message = $('<p class="rounded">').attr('data-poll-id', data.Messages[x].ChatRoomMessageId).data('poll-message', data.Messages[x]).html(data.Messages[x].Message);                
        
        if(data.Messages[x].Unread) {
          message.addClass('poll-chat-unread');
        } 
        
        var deleteLink = $('<i class="fa fa-times poll-message-delete" title="Delete"></i>').data('poll-id', data.Messages[x].ChatRoomMessageId);
        
        if(data.Messages[x].ProfileId == thinkster.CurrentUser.ProfileId) {
          img.addClass('ml-3');  
          deleteLink.addClass('mr-2');     
          message.addClass('poll-chat-self');
          div.append($('<div class="media-body poll-chat-self"></div>').append($('<div />').append(deleteLink, message)), img);           
        } else {
          img.addClass('mr-3');
          deleteLink.addClass('ml-2');
          div.append(img, $('<div class="media-body"></div>').append($('<div />').append(message, deleteLink))); 
        }   
        
        deleteLink.click(function() {
          messages.DeleteChatRoomMessage($(this).data('poll-id'));          
        });             
      
        $(container).find('.media').removeClass('mt-auto');
        
        if(data.Messages.length == 1) {
          $(container).append(div);
        } else { 
          $(container).append(div);
        }                
        
        prev = div;
      }  */                                         
    }
    
    setTimeout(function() {       
      $(container).scrollTo("max");
    }, 100);      
  };  
});