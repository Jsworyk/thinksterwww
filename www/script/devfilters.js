$(document).ready(function() {
  var sqlEditor = CodeMirror.fromTextArea(document.getElementById('fe_sql'), {
    mode: 'text/x-mysql',
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
    matchBrackets : true,
    autofocus: true,
    extraKeys: {"Ctrl-Space": "autocomplete"},
    hintOptions: {tables: {
      users: {name: null, score: null, birthDate: null},
      countries: {name: null, population: null, size: null}
    }}
  });
  
  $('#fe_sql').data('CodeMirror', sqlEditor);

  $('#categories').bind('digifi.action', function(e, a) {
    switch(a.action) {
      case 'AddFilter':
        document.getElementById('filter_editor').digifi.newObject('Filter', {FilterCategoryId: a.params});
      break; 
      
      case 'EditFilter':
        document.getElementById('filter_editor').digifi.editObject(a.params);
      break;
    } 
  }); 
  
  
  
  $('#filter_editor').on('shown.bs.modal', function (e) {
    sqlEditor.refresh();  
  }).bind('digifi.save-complete', function() {
    $('#categories').trigger('digifi.refresh'); 
  });
  
  $('#parameter_editor').bind('digifi.save-complete', function() {
    $('#categories').trigger('digifi.refresh'); 
  });
  
  $('#pe_filtertypeid').change(function() {
    if($('#pe_filtertypeid').val() == 3) {
      $('.poll-search-only').parents('.form-group').show(); 
    } else {
      $('.poll-search-only').parents('.form-group').hide(); 
    } 
  });
  
  $('#categories').bind('digifi.load-complete', function() {
    var re = /\[\[([^\]]+)\]\]/g;
    
    $('#categories .poll-filter-display').each(function() {
      var filterDisplay = $(this);
      var filterId = filterDisplay.attr('data-filter-id');
      var text = filterDisplay.text();      
      var parameters = jQuery.parseJSON($(this).attr('data-filter-parameters'));
      filterDisplay.html('');
      
      //Find all [[param]] and see if they've been       
      var match;
      var lastIndex = 0;
      
      while ((match = re.exec(text)) !== null) {
        var paramName = match[1];        
      
        if(match.index > lastIndex) {
          filterDisplay.append(text.substring(lastIndex, match.index));          
        } 
        
        var button = $('<button type="button" class="btn btn-sm" />').text(match[1]);
        var found = false;             
        
        if(parameters) {
          for(var p = 0; p < parameters.length; p++) { 
            if(parameters[p].Name == paramName) {
              found = true;              
              button.data('parameterId', parameters[p].Id);
              break; 
            }
          } 
        }   
        
        if(!found) {
          button.addClass('btn-danger').attr('aria-label', 'Edit the parameter details for ' + match[1]).attr('title', 'Edit the parameter details for ' + match[1]); 
        } else {
          button.addClass('btn-success'); 
        }
        
        button.click(function() {      
          if($(this).data('parameterId')) { 
            document.getElementById('parameter_editor').digifi.editObject($(this).data('parameterId'));
          } else {                         
            document.getElementById('parameter_editor').digifi.newObject('FilterParameter', {Name: $(this).text(), FilterId: filterId});
          }
        });
        
        filterDisplay.append(button);      
        
        lastIndex = re.lastIndex;            
      }
      
      if(text.length > lastIndex) {
        filterDisplay.append(text.substring(lastIndex)); 
      }  
    }); 
  });

  
});