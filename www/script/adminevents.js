var rectangle = null;
var map = null;
var marker = null;
var map_loaded = false;
var initialLocation = {lat: 44.5452, lng: -78.5389};
var pushChange = false;

$(document).ready(function() {
  $("#clayton_test").click(function(e) {
    //e.preventDefault();
    
    //alert('hi');
    
    $("#testing_ctr .poll-added-users").append($('<span class="btn btn-default mb-1 mr-1"> Clayton Rumley</span>')); 
  });


  var createUrlSegment = function() {
    var value = $('#ee_name').val().trim();
    $('#ee_urlsegment').val(encodeURIComponent(value.toLowerCase().replace(/[\']+/gi, '').replace(/[^a-z0-9_-]+/gi, '_'))); 
  };
  
  $('#ee_name').change(createUrlSegment).keyup(createUrlSegment);
  
  thinkster.connection.on("LocationChanged", function(data){
    $('#event_location_details').trigger('digifi.refresh');
  });
  
  thinkster.connection.on("GeoBoundaryChanged", function(data){    
    pushChange = true;
    
    if(rectangle) { 
      rectangle.setBounds(data.bounds);  
    } else { 
      /*rectangle = new google.maps.Rectangle({
          bounds: data.bounds,
          editable: true          
        });
        
      rectangle.setMap(map);      */
    }
    
    pushChange = false;
  }); 
  
  thinkster.connection.on("AttributeToggled", function(data){
    var target = $('.ae-attribute-toggle[data-ae-attribute="' + data.Attribute + '"]');
    var result = data.Value;
    
    target.find('.fa').removeClass('fa-spin' + (result ? ' fa-ban text-danger' : ' fa-check-circle text-success')).addClass((!result ? 'fa-ban text-danger' : 'fa-check-circle text-success'));            
  }); 
  
  $('.ae-attribute-toggle').click(function() {
    var target = $(this);
    
    target.prop('disabled', true).find('.fa').addClass('fa-spin');
    
    adminevents.ToggleAttribute($(this).data('ae-attribute'), function(result) {
       target.prop('disabled', false).find('.fa').removeClass('fa-spin' + (result ? ' fa-ban text-danger' : ' fa-check-circle text-success')).addClass((!result ? 'fa-ban text-danger' : 'fa-check-circle text-success'));
    }, alert);
    
  });
  
});

function initMap() {                  
       
  map = new google.maps.Map(document.getElementById('location_map'), 
    {
      zoom:13,
      center: initialLocation,
      mapTypeId: 'satellite'
    });     
  
  map_loaded = true;
  
  $(document).ready(function() {
    $('#event_location_details').trigger('digifi.refresh');
  });

}

thinkster.transform.adminEventLocation = function(container, data) {
  $(container).html('');
  //$(container).html($('<p />').html(JSON.stringify(data)));
  
  if(data.Address) {
    $(container).append($('<strong>Address</strong>'), $('<pre></pre>').text(data.Address));
  } else {
    $(container).append($('<div class="alert alert-info"><p>No address has been set for this event.</p></div>'));           
  }
  
  /*if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      map.setCenter({lat: position.coords.latitude, lng: position.coords.longitude});  
    });
      
  } */   
  
  if(data.Location) {
    initialLocation = {lat: data.Location.latitude, lng: data.Location.longitude};    
     
    map.setCenter(initialLocation);
      
    if(marker) { marker.setMap(null); }
    marker = new google.maps.Marker({position: initialLocation, map: map});
    
    if(data.AttendanceRestrictionTypeCode == 'GEO') {
      if(rectangle) rectangle.setMap(null);
  
      if(data.GeoBoundary) {
        /*var bounds = {
          north: 44.55,
          south: 44.53,
          east: -78.50,
          west: -78.55
        };*/
    
        // Define a rectangle and set its editable property to true.
        rectangle = new google.maps.Rectangle({
          bounds: data.GeoBoundary,
          editable: true          
        });
        
        rectangle.setMap(map);                
        
        google.maps.event.addListener(rectangle, 'bounds_changed', function() {
          //var newHeight = Math.abs(rectangle.getBounds().getNorthEast().lat() - rectangle.getBounds().getSouthWest().lat());
          //var newWidth = Math.abs(rectangle.getBounds().getNorthEast().lng() - rectangle.getBounds().getSouthWest().lng());
          
          if(pushChange) {
            return; 
          }
          
          rectangle.setEditable(false);
          marker.setAnimation(google.maps.Animation.BOUNCE);
          
          adminevents.SetGeoBoundary(rectangle.bounds, function() {                        
            setTimeout(function() {
              marker.setAnimation(null); 
            }, 850);          
          }, function(result) {
            alert(result);
            marker.setAnimation(null);                   
          }, function() {
            rectangle.setEditable(true);
          });
        });                                                     

      } 
    }
  }     
  
  
  var button = $('<button type="button" class="btn btn-primary"><i class="fa fa-marker"></i> Change Address</button>');
  
  button.click(function() {
    $('#el_address').val(data.Address || '');
    $('#edit_location').modal('show');
  });
  
  $(container).append($('<div class="m-1 text-center"></div>').append(button));    
};