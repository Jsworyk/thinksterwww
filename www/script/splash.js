$(document).ready(function() {    
  var signup = function() { 
    if($('#poll_signup').digifiValidate()) {
       var obj = $('#poll_signup').digifiUnbind();

       splash.SignUp(obj, function(result) {
          if(result.Success) {
            window.location = window.location;
          } else if(result.Validation && result.Validation.length > 0) { 
            for(var v = 0; v < result.Validation.length; v++) {
              alert(result.Validation[v].Argument + ' ' + result.Validation[v].Message); 
            }            
          } else {
            alert(result.Message); 
          }
       }, function(result) {
        alert(result); 
       });
    }
  };

  $('#poll_signup').digifiEnterKey(signup);
  $('#poll_signup_button').click(signup); 
  
  $('#su_birthdate').pickadate({
    format: 'yyyy-mm-dd',      
    formatSubmit: 'yyyy-mm-dd'
  });
  
  $('#forgot_pass_link').click(function(e) {
    e.stopPropagation();
    e.preventDefault();
    
    $('#fp_tab_toggle').tab('show');
  });
  
  $('#forgot_pass_1').digifiEnterKey(function() {
    $('#get_password_code').click(); 
  });   
  
  $('#get_password_code').click(function() {
    var profileName = $('#fp_profilename').val().trim();
    
    if(profileName == '') {
      $('#reset_code_problem').text('You must enter a profile name');
      $('#fp_profilename').focus();
      return; 
    } 
    
    $('#reset_code_problem').text('');
    
    $('#get_password_code').prop('disabled', true);
    
    splash.GetResetCode(profileName, function(result) {
      var obj = jQuery.parseJSON(result);
                                  
      if(obj.Success) {        
        $('#fp_clientcode').val(obj.ClientCode);
        $('#forgot_pass_1').hide();
        $('#forgot_pass_2').show();
        $('#fp_password').focus();
      } else {
        $('#reset_code_problem').text(obj.Message); 
      }            
    }, function(result) {
       $('#reset_code_problem').text(result);  
    }, function() {
      $('#get_password_code').prop('disabled', false); 
    });
  });
  
  $('#forgot_pass_2').digifiEnterKey(function() {
    $('#do_password_reset').click(); 
  }); 
  
  $('#do_password_reset').click(function() {
    var resetCode = $('#fp_resetcode').val().trim();
    var password = $('#fp_password').val().trim();
    var confirm = $('#fp_confirm_password').val().trim();          
    
    if(resetCode == '') {
      $('#reset_password_problem').text('You must enter the reset code');
      $('#fp_resetcode').focus();
      return; 
    }
    
    if(password == '') { 
      $('#reset_password_problem').text('You must enter a new password');
      $('#fp_password').focus();
      return; 
    }
    
    if(password != confirm) { 
      $('#reset_password_problem').text('New password and confirm do not match');
      $('#fp_confirm_password').focus();
      return; 
    }
    
    $('#do_password_reset').prop('disabled', true);
    
    splash.ResetPassword($('#fp_clientcode').val(), resetCode, password, function(result) {
      var obj = jQuery.parseJSON(result);
      
      if(obj.Success) {
        $('#forgot_pass_2').hide(); 
        $('#forgot_pass_3').show();
      } else {
        $('#reset_password_problem').html();
        if(obj.Code == 'ValidationFailure') {          
          for(var x = 0; x < obj.Validation.length; x++) {
            $('#reset_password_problem').append($('<p />').text(obj.Validation[x].Argument + ' ' + obj.Validation[x].Message));  
          } 
        } else {
          $('#reset_password_problem').text(obj.Message); 
        }         
      } 
    }, function(result) { 
      $('#reset_password_problem').text(result);
    }, function() { 
      $('#do_password_reset').prop('disabled', false);      
    });
           
  });
  
  $('#show_signin_tab').click(function() {
      $('#signin_tab_toggle').tab('show');
      $('#li_email').focus();
      $('#forgot_pass_2, #forgot_pass_3').hide(); 
      $('#forgot_pass_1').show();
  });
});