var map = null; 

$(document).ready(function() {
  var menuCounter = 0;
  var overlays = [];                   
  var filters = {Age: null, Gender: null };
  var currentLocation = {latitude: 49.899444, longitude: -97.139167};
  var liveUpdates = true;
  var loader = $('<span class="p-1 alert alert-warning" style="font-size: 18px;"><i class="fa fa-refresh fa-spin fa-fw"></i> Refreshing Data...</span>');
  var idleTimeout = null;
  
  navigator.geolocation.getCurrentPosition(function(location) {
    currentLocation.latitude = location.coords.latitude; 
    currentLocation.longitude = location.coords.longitude;
  });
  
  var refreshmap = function() {
    var omits = [];
    
    loader.show();
    
    for(var o = 0; o < overlays.length; o++) {
      if(map.getZoom() >= overlays[o].pollRegion.minZoom && map.getZoom() <= overlays[o].pollRegion.maxZoom) {
        overlays[o].setMap(map); 
      } else { 
        overlays[o].pollRegion.infowindow.close();
        overlays[o].setMap(null);
      }
    
      omits[omits.length] = overlays[o].pollRegion.id; 
    }
    
    embed.GetMapData(map.getBounds(), map.getZoom(), 'GE', omits, filters, function(json) {
      console.log('Result: ' + json);
    
      var results = jQuery.parseJSON(json);
      var obj = results.MapData;
      
      console.log(obj);
      
      for(var p = 0; p < obj.length; p++) {         
        var fillColor = '#444444', strokeColor = '#222233';
        var name = obj[p].Name;
        var bestAnswerIndex = 0;
        var tied = false;  
       
        for(var i = 0; i < obj[p].Polygons.length; i++) {  
          var ptsArray = [];
        
          for(var pt = 0; pt < obj[p].Polygons[i].length; pt+=2) {
            ptsArray.push(new google.maps.LatLng(obj[p].Polygons[i][pt],obj[p].Polygons[i][pt + 1]));
          }
          
          overlays[overlays.length] = new google.maps.Polygon({
            paths: ptsArray,
            //strokeColor: strokeColor,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            //fillColor: fillColor,
            fillOpacity: 0.5
          });
          
          var index = overlays.length - 1;
          
          overlays[index].pollRegion = { id: obj[p].Id, 
                name: obj[p].Name, 
                results: obj[p].Results, 
                overlay: overlays[index],
                responses: obj[p].Responses, 
                minZoom: obj[p].MinZoom,
                maxZoom: obj[p].MaxZoom,
                chart: null,
                setColor: function() {
                  var fillColor = "#444444", strokeColor = "#222233";
                  var bestAnswerIndex = 0;
                  var tied = false;
                  
                  for(var pc = 1; pc < this.results.length; pc++) {
                    if(this.results[pc] > this.results[bestAnswerIndex]) {
                      bestAnswerIndex = pc;
                      tied = false;
                    } else if (this.results[pc] == this.results[bestAnswerIndex]) {
                      tied = true; 
                    }
                  }
                  
                  if(tied && this.results[bestAnswerIndex] != 0) {
                    fillColor = "#ffffff";
                    strokeColor = "#aaaadd";
                  } else if(this.results[bestAnswerIndex] != 0) {
                    fillColor = question.Choices[bestAnswerIndex].ChartColour;
                    strokeColor = question.Choices[bestAnswerIndex].GradientDark;
                  } 
                  
                  //console.log("setting color to " + fillColor + ', ' + this.overlay.pollRegion.id);
                  
                  this.overlay.setOptions({fillColor: fillColor});  
                     
                  }
              };
              
          overlays[index].pollRegion.setColor();
          
          //overlays[index].pollRegion.infowindow = new google.maps.InfoWindow({content: '<strong>' + obj[p].Name + '</strong><br /><div class="text-center">' + obj[p].Responses + ' Response' + (obj[p].Responses == 1 ? '' : 's') + '</div><canvas id="poll_region_marker_' + obj[p].Id + '" width="96" height="96"></canvas>' });
                           
          
          overlays[index].pollRegion.infowindow = new google.maps.InfoWindow({content: 'test' });
          
          //overlays[index].pollRegion.infowindow.mapRegionId = obj[p].Id;
          
          google.maps.event.addListener(overlays[index].pollRegion.infowindow, 'domready', function() {
            //console.log('testing: ' + this.mapRegionId);
            
            //this.setContent('<strong>' + obj[p].Name + '</strong><br /><div class="text-center">' + obj[p].Responses + ' Response' + (obj[p].Responses == 1 ? '' : 's') + '</div><canvas id="poll_region_marker_' + obj[p].Id + '" width="96" height="96"></canvas>');
            //this.setContent('asd');
          });               
                   
        
          overlays[overlays.length - 1].addListener('click', function(event) {
            this.pollRegion.infowindow.setPosition(event.latLng);
            
            //Create chart if it doesn't exist yet:
            //var ctx = document.getElementById("myChart");
            console.log(this.pollRegion.chart);
            if(!this.pollRegion.chart) {
              //var data = [];
              var color = [];
              var hover = [];
              var labels = [];
              
              var content = $('<div style="min-width: 120px; white-space: nowrap;"></div>').append($('<strong />').html(this.pollRegion.name), $('<div class="text-center poll-response-count"></div>').html(this.pollRegion.responses + ' Response' + (this.pollRegion.responses == 1 ? '' : 's')));
              var canvas = $('<canvas width="96" height="96" style="margin-left: auto; margin-right: auto;"></canvas>');
              
              content.append(canvas);
                
              for(var x = 0; x < this.pollRegion.results.length; x++) {
                labels[labels.length] = question.Choices[x].Text;
                //data[data.length] = this.pollRegion.results[x];
                color[color.length] = question.Choices[x].ChartColour; 
                hover[hover.length] = question.Choices[x].GradientDark;
              }     
                    
                
                  //alert(document.getElementById("poll_region_marker_" + this.pollRegion.id));
              this.pollRegion.chart = new Chart(canvas, {
                  type: 'pie',              
                  data: { 
                    labels: labels,
                    datasets: [{ data: this.pollRegion.results,
                    backgroundColor: color
                    }]
                  },
                  options: {
                       legend: {
                          display: false
                       }
                  }  
              });                       
              
              this.pollRegion.infowindow.setContent(content[0]);
              
              console.log(this.pollRegion.infowindow.getContent()); 
            }
                     
            this.pollRegion.infowindow.open(map); 
          });
        
          overlays[overlays.length - 1].setMap(map); 
        }    
      }
      
         
    }, function(e) { 
      $("#map_debug").text(JSON.stringify(e));
    }, function() {
      loader.hide(); 
    });
    
  };
  
  var resetMap = function() {
     //Clear all previous overlays:
    for(var o = 0; o < overlays.length; o++) {
      overlays[o].setMap(null);
      overlays[o] = null; 
    } 
    
    overlays = [];
    
    var filterText = '';
    
    $('#map_buttons .dropdown-item.active').each(function() {
      if(filterText != '') filterText += ', ';  
      filterText += $(this).text();
    });
    
    if(filterText != '') {
      liveUpdates = false;
       
      clearButton = $('<button type="button" class="btn btn-danger btn-sm ml-2"><i class="fa fa-ban fa-fw"></i> Remove all Filters</button>');
      
      clearButton.click(function() {  
        filters.Age = null;
        filters.Gender = null;
        
        $('#map_buttons .dropdown-toggle.text-success').removeClass('text-success');
        $('#map_buttons .dropdown-item.active').removeClass('active');
        
        resetMap();
      });
    
      $('#filter_details').html('Filtered to ' + filterText + '.<br />Live updates disabled when filtering.').append(clearButton);
    } else {
      liveUpdates = true;
      $('#filter_details').html('');
    }
    
    refreshmap(); 
  };

  var createMapButtons = function(container, buttons) {
    for(var x = 0; x < buttons.length; x++) {
      if(buttons[x].Children) { 
        var ddgroup = $('<div class="btn-group" role="group"></div>');
        var ddbtn = $('<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>').attr('id', 'gmap_cust_b_' + menuCounter);
        var ddmenu = $('<div class="dropdown-menu"></div>').attr('aria-labelledby', 'gmap_cust_b_' + menuCounter);
       
        ddbtn.attr('title', buttons[x].Text);
        ddbtn.append($('<i />').attr('class', buttons[x].Icon));
        
        ddgroup.append(ddbtn, ddmenu); 
        
        container.append(ddgroup);
        
        //Create "Don't" filter:
        var btn = $('<button type="button" class="btn btn-sm btn-secondary dropdown-item"></button>');
        
        btn.attr('title', "Don't " + buttons[x].Text);
        btn.append($('<i />').attr('class', 'fa fa-ban fa-fw text-danger'), "Don't " + buttons[x].Text);
        //btn.click(buttons[x].Click);               
        
        btn.on('click', null, buttons[x].Filter, function(e) {
          filters[e.data] = null;
          
          $(this).parents('.btn-group').first().find('.btn.dropdown-toggle').removeClass('text-success fa-2x');
                              
          $(this).parent().find('.active').removeClass('active');          
          
          resetMap(); 
        });
        
        ddmenu.append(btn);
        
        createMapButtons(ddmenu, buttons[x].Children);
      
      } else {
        var btn = $('<button type="button" class="btn btn-secondary"></button>');
        
        btn.attr('title', buttons[x].Text);
        btn.append($('<i />').attr('class', buttons[x].Icon));
        //btn.click(buttons[x].Click);
        
        btn.on('click', null, buttons[x].Click, function(e) {          
          if(e.data()) return;
          
          $(this).parents('.btn-group').first().find('.btn.dropdown-toggle').addClass('text-success fa-2x');
          
          $(this).parent().find('.active').removeClass('active');
          $(this).addClass('active');
          
          resetMap(); 
        });
        
        /*var fn = buttons[x].Click; 
        alert(fn);
        btn.click(function() { alert(fn); fn.call(this); resetMap(); });*/
        
        if(container.is('.dropdown-menu')) {
          btn.addClass('dropdown-item');
          btn.append(buttons[x].Text);
        }
        
        container.append(btn);
      }
      
      menuCounter++;     
    }
   
  };


  var initMap = function() {
    var customButtons = [
      {Text: 'Return to your primary location', Icon: 'fa fa-home', Click: function() {
        map.panTo( {lat: currentLocation.latitude, lng: currentLocation.longitude} );
        return true;
      }},
      {Text: 'Filter on Gender', Icon: 'fa fa-venus-mars', Filter: 'Gender', Children: [
        {Text: 'Men Only', Icon: 'fa fa-male fa-fw', Click: function() {
          filters.Gender = {Id: 2 };   
        }},
        {Text: 'Women Only', Icon: 'fa fa-female fa-fw', Click: function() {
          filters.Gender = {Id: 3 }; 
        }}
      ]},
      {Text: 'Filter on Age', Icon: 'fa fa-birthday-cake', Filter: 'Age', Children: [
        {Text: 'Ages 15-25', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {          
          filters.Age = {Id: 1, Parameters: {X: 15, Y: 25} };           
        }},
        {Text: 'Ages 25-35', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 1, Parameters: {X: 25, Y: 35} };          
        }},
        {Text: 'Ages 35-45', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 1, Parameters: {X: 35, Y: 45} };          
        }},
        {Text: 'Ages 45-55', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 1, Parameters: {X: 45, Y: 55} };          
        }},
        {Text: 'Ages 55+', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          filters.Age = {Id: 4, Parameters: {Age: 55} };        
        }}
      ]}/*,
      {Text: 'Filter on Ethnicity', Icon: 'fa fa-flag', Children: [
        {Text: 'Ages 15-25', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 15-25'); 
        }},
        {Text: 'Ages 25-35', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 25-35'); 
        }},
        {Text: 'Ages 35-45', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 35-45'); 
        }},
        {Text: 'Ages 45-55', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 45-55'); 
        }},
        {Text: 'Ages 55+', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 55+'); 
        }}
      ]},
      {Text: 'Filter on Political Affiliate', Icon: 'fa fa-flag', Children: [
        {Text: 'Ages 15-25', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 15-25'); 
        }},
        {Text: 'Ages 25-35', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 25-35'); 
        }},
        {Text: 'Ages 35-45', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 35-45'); 
        }},
        {Text: 'Ages 45-55', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 45-55'); 
        }},
        {Text: 'Ages 55+', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 55+'); 
        }}
      ]},
      {Text: 'Filter on Religious Denomination', Icon: 'fa fa-flag', Children: [
        {Text: 'Ages 15-25', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 15-25'); 
        }},
        {Text: 'Ages 25-35', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 25-35'); 
        }},
        {Text: 'Ages 35-45', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 35-45'); 
        }},
        {Text: 'Ages 45-55', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 45-55'); 
        }},
        {Text: 'Ages 55+', Icon: 'fa fa-birthday-cake fa-fw', Click: function() {
          alert('filter 55+'); 
        }}  
      ]} */    
    ];
    
   
    map = new google.maps.Map(document.getElementById('poll_map'), 
    {
      zoom: 12,      
      center: {lat: currentLocation.latitude, lng: currentLocation.longitude},
      /*center: {lat: <?=$location[0]->Latitude / 10000.00; ?>, lng: <?=$location[0]->Longitude / 10000.00; ?>},  */
      mapTypeId: 'roadmap'
    });
    
                  
    var centerControlDiv = document.createElement('div');
    var buttonGroup = $('<div class="btn-group" style="margin: 0 auto;" role="group" aria-label="Poll filtering functions"></div>'); 
    
    $(centerControlDiv).append(loader);
    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);

    
    createMapButtons(buttonGroup, customButtons);
    
    $('#map_buttons').append(buttonGroup);              
    
    //Bind to the maps idle event:
    google.maps.event.addListener(map, 'idle', function(ev){      
        if(idleTimeout) window.clearTimeout(idleTimeout);
        
        idleTimeout = window.setTimeout(function() { idleTimeout = null; refreshmap(); }, 700);
        
    });
    
    google.maps.event.addListenerOnce(map, 'bounds_changed', function() {
     window.clearTimeout(idleTimeout);
     idleTimeout = null;
   });
    
    thinkster.connection.on("PollAnswer", function(data){    
      if(!liveUpdates) return;
      for(var x = 0; x < data.Regions.length; x++) {
        for(var o = 0; o < overlays.length; o++) {
          if(overlays[o].pollRegion.id == data.Regions[x]) {          
            var total = 0;                   
          
            overlays[o].pollRegion.responses++;
            overlays[o].pollRegion.results[data.Answer]++;
            overlays[o].pollRegion.setColor();
            
            for(var r = 0; r < overlays[o].pollRegion.results.length; r++) {
              total += overlays[o].pollRegion.results[r];  
            } 
                      
            $(overlays[o].pollRegion.infowindow.getContent()).find('.poll-response-count').html(total + ' Response' + (total == 1 ? '' : 's'));
            if(overlays[o].pollRegion.chart) {
              overlays[o].pollRegion.chart.update();
            } else {             
            }                    
          } 
        } 
      }    
    }); 
    
  };

  var getFilter = function() {
    var node = null;
  
    switch($(this).data("poll-filter")) {
      case 'gender':
        node = $('<ul class="list-group m-0"><li class="list-group-item m-0">Off</li><li class="list-group-item m-0">Male</li><li class="list-group-item m-0">Female</li></ul>');
      break;
      
      default:
        node = $('<ul><li>Testing</li><li>testing again</li></ul>');
      break; 
    }
  
    return node; 
  };


  $("#move_to_map").click(function() {
     $("#poll_question, #poll_results").animate({'left': '-100%'}, function() {
      if(!map) {
        initMap(); 
      } 
     });          
  });
  
  $("#move_to_poll").click(function() {
     $("#poll_question, #poll_results").animate({'left': '0'});
  });                        
  
  $('[data-toggle="popover"]').popover({
    trigger: 'focus',
    content: getFilter,            
    html: true
  });   
  
  $('#embed_login').click(function() {
    $('#login_popup').modal('show'); 
  });
  
  $('#embed_signup').click(function() {
    $('#signup_popup').modal('show');
  });           
  
  var signup = function() { 
    if($('#embed_signup').digifiValidate()) {
       var obj = $('#embed_signup').digifiUnbind();

       embed.SignUp(obj, function(result) {
          if(result.Success) {
            window.location = window.location;
          } else if(result.Validation && result.Validation.length > 0) { 
            for(var v = 0; v < result.Validation.length; v++) {
              alert(result.Validation[v].Argument + ' ' + result.Validation[v].Message); 
            }            
          } else {
            alert(result.Message); 
          }
       }, function(result) {
        alert(result); 
       });
    }
  };

  $('#embed_signup').digifiEnterKey(signup);
  $('#embed_signup_button').click(signup); 

});  
