<?php
error_reporting(-1);
ini_set('display_errors', 'On');

global $config;

$config = json_decode(file_get_contents('../inc/SiteSettings.json'));

date_default_timezone_set('America/Winnipeg');

require_once '/var/pollsite/vendor/autoload.php';

require '/var/pollsite/vendor/aws/aws-autoloader.php';

include_once('../inc/DatabaseQuery.php');
include_once('../inc/DatabaseSession.php');
include_once('../inc/Data.php');
include_once('../inc/Utilities.php');
include_once('../inc/DefaultConfig.php');
include_once('../inc/WebPage.php');
include_once('../inc/User.php');
//include_once('../inc/PollFunctions.php');
include_once('../inc/FileComposer.php');

include_once('../inc/' . FileComposer::Compose('PollFunctions'));

function handleException(Exception $e, $page)
{

  
  return $e->getMessage() . '<br />' . $e->getTraceAsString();
  //return "error!";
}

$current_user = new User();

$page = $current_user->IsLoggedIn() ? "dashboard" : "splash";

if(isset($_GET['page'])) $page = strtolower($_GET['page']);


ob_start();
try {

  //Check and see if the requested page is a URL segment of a Page type content:
  $q = new DatabaseQuery();
  
  $q->sql = "SELECT * FROM StaticContent WHERE UrlSegment = ?as_page AND StaticContentTypeCode = 'PG'";
  
  $q->addParameter('as_page', $page);
  
  $staticContent = $q->executeObjects();
  
  if($staticContent && $staticContent[0]) { 
    $page = "content";
  }

  //echo "hi Tere $page";
  //return;

  $webpage = new WebPage($page, $current_user);
} catch (Exception $e) {
  ob_end_clean();
  

    
  
  $failedErrorMessage = handleException($e, $page);  
  
  //displayErrorPage($e->getMessage());
  echo '<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<title>Oops! An error occurred!</title>
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
body {
	font-family: "Lucida Sans Unicode", "Lucida Grande", Arial, Verdana, Tahoma, garuda, georgia, sans-serif, helvetica, serif;
	font-size: 12px;
	text-decoration: none;	
}
</style>
</head>
<body>

<h1>Uh oh...something\'s wrong</h1>

<p>It looks like an error of some sort occurred. We\'re really sorry about that.</p>

<p ' . ($config->ShowErrors ? '' : 'style="display: none;"') . '><strong>' . $page .'</strong> '  . htmlentities($failedErrorMessage) . '</p>
<p>' . ($current_user->IsLoggedIn() ? 'logged in' : 'not logged in') . '</p>

</body>
</html>';
}


?>