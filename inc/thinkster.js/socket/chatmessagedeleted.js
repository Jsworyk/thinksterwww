socket.on('ChatMessageDeleted', function(data){
  for(var x = 0; x < data.Messages.length; x++) {
    $('.poll-message-container p[data-poll-id=' + data.Messages[x].ChatRoomMessageId + ']').each(function() {
      var parent = $(this).parents('.media').first();
     
      $(this).parent().hide('slow', function() { $(this).remove(); } );
      
      //Remove the entire media object if all chat messages for it have been removed: 
      if(parent.find('p').length == 0) parent.remove();
    }); 
  }
});