socket.on('EventAttendanceDelta', function(data){
  if(data.OldAttendanceTypeCode != '' && $('.event-attendees-' + data.OldAttendanceTypeCode + ' div[data-profile-id=' + data.ProfileId + ']').length > 0) {
    var icon = $('.event-attendees-' + data.OldAttendanceTypeCode + ' div[data-profile-id=' + data.ProfileId + ']');
    
    icon.remove();
    
    if($('.event-attendees-' + data.OldAttendanceTypeCode + ' .row > div').length == 0) {
      thinkster.transform.attendees($('.event-attendees-' + data.OldAttendanceTypeCode)[0], null); 
    }
    
    if($('.event-attendees-' + data.NewAttendanceTypeCode + ' > .row').length == 0) {
      $('.event-attendees-' + data.NewAttendanceTypeCode).html('<div class="row m1"></div>'); 
    }
    
    $('.event-attendees-' + data.NewAttendanceTypeCode + ' > .row').prepend(icon);     
  } else { 
    thinkster.transform.attendees($('.event-attendees-' + data.NewAttendanceTypeCode)[0], {People: [data]});
  } 
    
  
  $('.poll-attendance-' + data.OldAttendanceTypeCode + '[data-event-id=' + data.EventId + ']').each(function() {
    var val = parseInt($(this).text()) || 0;
    
    val--; 
    
    $(this).text(val);
  });
  
  $('.poll-attendance-' + data.NewAttendanceTypeCode + '[data-event-id=' + data.EventId + ']').each(function() {
    var val = parseInt($(this).text()) || 0;
    
    val++; 
    
    $(this).text(val);
  });
  //var rcount = $('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text() || 0;
  
  
  //$('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text(rcount + data.Delta);
  
});