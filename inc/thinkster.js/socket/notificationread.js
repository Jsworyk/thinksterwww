socket.on('NotificationRead', function(data){
  $('.poll-notification[data-id=' + data.NotificationId + ']').each(function() {
    $(this).removeClass('poll-unread');
    $(this).find('.poll-read-toggle-action').data('read', true).text('Mark as Unread'); 
  }).promise().done(function() {     
    $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
  });
});