socket.on('NotificationUnread', function(data){
  $('.poll-notification[data-id=' + data.NotificationId + ']').each(function() {
    $(this).addClass('poll-unread');
    $(this).find('.poll-read-toggle-action').data('read', false).text('Mark as Read'); 
  }).promise().done(function() {     
    $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
  });
});