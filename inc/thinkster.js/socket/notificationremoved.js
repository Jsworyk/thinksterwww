socket.on('NotificationRemoved', function(data){
  console.log(JSON.stringify(data));
  $('.poll-notification[data-id=' + data.NotificationId + ']').hide('slow', function() { 
    $(this).remove();
    $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount); 
  });
});