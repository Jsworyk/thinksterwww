socket.on('Reaction', function(data){
//  console.log(JSON.stringify(data));
  
  var rcount = parseInt($('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text()) || 0;
  
  
  $('.poll-react[data-poll-targettype="' + data.TargetTypeCode + '"][data-poll-targetid="' + data.TargetId + '"] .badge').text((rcount + data.Delta) == 0 ? '' : (rcount + data.Delta));
  
});