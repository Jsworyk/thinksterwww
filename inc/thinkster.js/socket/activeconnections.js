socket.on('ActiveConnections', function(data){
  //$('#messages').append($('<li>').text(msg));
  
  var ul = $('<ul class="list-unstyled" />');
  
  ul.append($('<li><i class="fa fa-fw fa-question"></i> Unknown </li>').append(data.Unknown));
  ul.append($('<li><i class="fa fa-fw fa-mobile"></i> Mobile App </li>').append(data.Mobile));
  ul.append($('<li><i class="fa fa-fw fa-desktop"></i> Browsers </li>').append(data.Desktop));
  ul.append($('<li><i class="fa fa-fw fa-globe"></i> Total </li>').append(data.Total));
  
    
  $('.poll-active-connections').html(ul);    
});