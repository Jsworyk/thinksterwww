socket.on('AllNotificationsRead', function(data){
  $('.poll-notification.poll-unread').each(function() {
    $(this).removeClass('poll-unread');
    $(this).find('.poll-read-toggle-action').data('read', true).text('Mark as Unread'); 
  }).promise().done(function() {     
    $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
  });
});