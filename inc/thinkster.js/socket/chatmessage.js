socket.on('ChatMessage', function(data){
  console.log(JSON.stringify(data));
  
  $('.poll-chat-room[data-id=' + data.ChatRoomId + ']').each(function() {
    var parent = $(this).parent();
    
    $(this).find('.poll-chatroom-preview').html(data.Messages[0].Message);
    
    if(data.Messages[0].ProfileId != thinkster.CurrentUser.ProfileId) {
      $(this).addClass('poll-chat-unread'); 
    }
    
    parent.prepend($(this).remove());     
    
    //var unreadCount = $('#poll_menu_chat_preview').find('.poll-chat-room.poll-chat-unread').length;
    if(data.UnreadDelta) { 
      var current = parseInt($('#chat_top_menu').find('.badge').text()) + data.UnreadDelta;
      $('#chat_top_menu').find('.badge').html(current == 0 ? '' : current);
    } else {
      $('#chat_top_menu').find('.badge').html(data.UnreadCount == 0 ? '' : data.UnreadCount); 
    }         
  }); 
  
  if(thinkster.CurrentPage.chatMessages) thinkster.CurrentPage.chatMessages('#ci' + data.ChatRoomId + ' .poll-message-container', data);
});