socket.on('Notification', function(data){
  console.log(JSON.stringify(data));
  
  thinkster.transform.notifications('.poll-notification-list', data, true);
  
  //Client-side notification:
  if(window.Notification && Notification.permission!=="granted") {
    //if it is not supported request permission
	 Notification.requestPermission(function(status){
    	if(Notification.permission!==status){
    	Notification.permission=status;
    	}
  	});  
  }

  
  if(window.Notification && Notification.permission=="granted") {
    var lastIndex = 0;
    var body = '';
    var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
    var match;
    var url = '';
    
    while ((match = re.exec(data.Text)) !== null) {
      if(match.index > lastIndex) {
        body += data.Text.substring(lastIndex, match.index);        
      } 
      
      //text += match[3];
      var anchor = $('<a />').html(match[3]);
      
      switch(match[1]) {
        case 'e':
          url = '/events/' + match[2];
        break;
      
        case 'u':
          url = '/profile/' + match[2];
        break;
        
        case 'p':
          url = '/poll/' + match[2];
        break; 
        
        case 'm':
          url = '/media/' + match[2];
        break;
      }
      
      body += match[3];
      
      //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
      
      lastIndex = re.lastIndex;            
    }
    
    if(data.Text && data.Text.length > lastIndex) {
      body += data.Text.substring(lastIndex); 
    }
    
  
    var title="Thinkster Notification";//title for notification
  	var options={//options for notification
  	body:body,
    icon:data.AvatarMicrothumbUrl,
    tag: data.GroupingValue              
  	
  	}
   noti = new Notification(title,options);//permission is granted, display notification
   noti.onclick = function() { window.location = url; };  
  }    
  
  /*
  $('.poll-notification[data-id=' + data.NotificationId + ']').each(function() {
    var parent = $(this).parent();
    
    $(this).find('.poll-chatroom-preview').html(data.Messages[0].Message);
    
    if(data.Messages[0].ProfileId != thinkster.CurrentUser.ProfileId) {
      $(this).addClass('poll-chat-unread'); 
    }
    
    parent.prepend($(this).remove());     
    
    var unreadCount = $('#poll_menu_chat_preview').find('.poll-chat-room.poll-chat-unread').length;
    $('#chat_top_menu').find('.badge').html(unreadCount == 0 ? '' : unreadCount); 
  }); 
  
  if(thinkster.CurrentPage.chatMessages) thinkster.transform.notifications('#ci' + data.NotificationId + ' .poll-message-container', data);
  */
});