socket.on('RemoveFollowRequest', function(data){  
  $('.poll-follow-request[data-follower-id=' + data.FollowerId + ']').hide('slow', function() { 
    $(this).remove();
    $('.poll-follow-request-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount); 
  });
});