$(this).find('.poll-overflow-conceal').each(function() {
  var container = this;
  $(this).data('expanded', false);
  if($(this).find('> div').height() > $(this).height()) {
    //Create the "more" link:
    var buttonContainer = $('<div class="mb-1 text-center"></div>');
    var button = $('<button type="button" class="btn btn-success btn-sm">== more ==</button>');
    
    buttonContainer.append(button); 
    
    button.click(function() {
      if($(container).data('expanded')) {
        $(container).css('height', '80px'); 
        $(this).removeClass('btn-warning').addClass('btn-success').text('== more ==');
      } else {
        $(container).css('height', $(container).find('> div').height());
        $(this).addClass('btn-warning').removeClass('btn-success').text('== less =='); 
      }                   
      
      $(container).data('expanded', !$(container).data('expanded'));
    });
    
    $(this).after(buttonContainer);
  }
});