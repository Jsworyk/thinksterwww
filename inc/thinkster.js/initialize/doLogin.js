 var doLogin = function(e) {
  var em = $('#li_email').val().trim();
  var pw = $('#li_password').val().trim();

  e.stopPropagation();

  master.Login(em, pw, function(json) {
    var result = jQuery.parseJSON(json);

    if(result.Success) {
      if(thinkster.CurrentPage.ReturnUrl) {
        window.location = thinkster.CurrentPage.ReturnUrl; 
      } else {  
        window.location = window.location;
      }
    } else if(result.Validation && result.Validation.length > 0) { 
      for(var v = 0; v < result.Validation.length; v++) {
        alert(result.Validation[v].Argument + ' ' + result.Validation[v].Message); 
      }            
    } else {
      alert(result.Message); 
    }

  }, alert);
};

$('#ts_login').digifiEnterKey(doLogin);
$('#li_login').click(doLogin);


$('.header-fb-login').click(function() {
  FB.login(function(response) {
    if(response.status == 'connected') {

      master.FacebookLogin(function(json) {

        var result = jQuery.parseJSON(json);
        if(result.Success) {
          window.location = window.location;
        } else {
          alert(result.Message);
        }
      }, alert);

    } else {
      alert('Sign in to Facebook failed or was cancelled.');
    }
    // handle the response
  }, {scope: 'public_profile,email'});
});