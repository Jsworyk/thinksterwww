var searchDelayTimer = 0;

var doSiteSearch = function() {
  var searchString = $('#poll_site_search').val().trim();
  
  if(searchString == '') {
    $('#poll_site_search').dropdown();
    $('#poll_menu_header_search_results').html(''); 
  } else {    
    master.HeaderSearch(searchString, function(results) {
      $('#poll_site_search').dropdown();
      
      thinkster.transform.searchResults('#poll_menu_header_search_results', results.SearchResults);
      
      //$('#poll_menu_header_search_results').html(JSON.stringify(results));   
    }, alert);
  } 
};

$('#poll_site_search').keyup(function() {
  clearTimeout(searchDelayTimer);
  setTimeout(doSiteSearch, 350); 
}).digifiEnterKey(function() {
  clearTimeout(searchDelayTimer);
  doSiteSearch();
}).focus(function() {
  if($('#poll_menu_header_search_results').html() != '') {
    $('#poll_site_search').dropdown(); 
  }  
});