$(this).find('.poll-accept-follow-request').click(function() {
  //Disable all buttons related to this request while it is processing:
  $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
  $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
  //alert('hi decline ' + $(this).attr('data-poll-id'));
  master.AcceptFollowRequest($(this).attr('data-poll-id'), function() {
    //Do nothing; push should take care of everything 
  }, alert, function() {
    //Re-enable all buttons related to this request while it is processing: 
    $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
    $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
  });
});

$(this).find('.poll-decline-follow-request').click(function() {
  //Disable all buttons related to this request while it is processing:
  $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
  $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', true);
  //alert('hi decline ' + $(this).attr('data-poll-id'));
  master.DeclineFollowRequest($(this).attr('data-poll-id'), function() {
    //Do nothing; push should take care of everything 
  }, alert, function() {
    //Re-enable all buttons related to this request while it is processing: 
    $('.poll-accept-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
    $('.poll-decline-follow-request[data-poll-id="' + $(this).attr('data-poll-id') + '"]').prop('disabled', false);
  });
});