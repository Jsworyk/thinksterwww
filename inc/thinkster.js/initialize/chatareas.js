$(this).find('textarea.poll-autosize').each(function() {
  var area = this;
  var textAreaAdjust = function() {
    area.style.height = "1px";
    area.style.height = (25+area.scrollHeight)+"px";
  }
  
  $(this).change(textAreaAdjust).keyup(textAreaAdjust);
    
});