$(this).find('.poll-flag-button').click(function() {
  var button = this;
  var modal = $(button).data('flag-modal');
  var uniqueId = $(button).attr('data-poll-targettype') +  $(button).attr('data-poll-targetid');  
  
  if(!modal) {
    //Create the modal:
    
    modal = $([
'<div class="modal fade">', 
  '<div class="modal-dialog modal-lg modal-notify modal-danger" role="document"> ', 
    '<div class="modal-content"> ',                                                    
      '<div class="modal-header"> ',  
        '<h5 class="modal-title" style="color: #fff;">Modal title</h5> ',  
        '<button type="button" class="close"  style="color: #fff;" data-dismiss="modal" aria-label="Close"> ',  
          '<span aria-hidden="true">&times;</span>                                                             ',  
        '</button> ',  
      '</div> ',        
      '<div class="modal-body"> ',  
        '<div class="form">        ',  
          '<div class="form-group">  ',  
            '<label for="poll_flag_type' + uniqueId + '">Reason for this report<sup class="text-danger">*</sup></label> ',              
            '<select id="poll_flag_type' + uniqueId + '" class="form-control poll-flag-type" style="display: inherit !important;" id="exampleSelect1"> ',  
              '<option value="">[make a selection]</option> ',  
              '<option value="SO">Solicitation/Spam/bot</option> ',  
              '<option value="CO">Copyright Infringement</option>   ',  
              '<option value="IL">Links to illegal content or filesharing sites</option> ',  
              '<option value="PO">Pornographic Content</option> ',  
              '<option value="OT">Other (describe below)</option> ',  
            '</select> ',              
          '</div> ',  
          '<div class="form-group"> ',  
              '<label for="poll_flag_details' + uniqueId + '">Details<sup class="text-danger">*</sup></label> ',  
              '<textarea class="form-control poll-flag-details" id="poll_flag_details' + uniqueId + '" rows="5"></textarea> ',  
              '<small id="poll_flag_details' + uniqueId + '_help" class="form-text text-muted">Provide a detailed description as to why you are reporting this ' + $(button).attr('data-poll-type') + '.</small> ',  
          '</div> ',  
        '</div> ',  
        '<p class="small text-danger"><i class="fa fa-warning"></i> Abuse of the reporting system may result in account termination</p> ',  
      '</div> ',  
      '<div class="modal-footer"> ',  
        '<button type="button" class="btn btn-primary">Report</button> ',  
        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> ',  
      '</div> ',  
    '</div> ',  
  '</div> ',  
'</div>'].join('\n'));

    if($(button).attr('data-poll-targettype') == 'PR') {
      modal.find('.modal-body').prepend('<p></p>'); 
    }

    modal.find('.mdb-select').material_select();
    modal.find('.modal-title, .modal-footer .btn-primary').text('Report ' + $(button).attr('data-poll-type'));
              
    modal.data('flag-object', {'TargetTypeCode': $(button).attr('data-poll-targettype'), 'TargetId': $(button).attr('data-poll-targetid')});                                         
    modal.modal();
    
    $(button).data('flag-modal', modal);
    
    modal.find('.modal-footer .btn-primary').click(function() {
      var flagTypeCode = modal.find('.poll-flag-type').val();
      var details = modal.find('.poll-flag-details').val();
      
      if(flagTypeCode == '') {
        alert('You must select the reason for this report');
        modal.find('.poll-flag-type').focus();
        return; 
      }
      
      if(details == '') {
        alert('You must provide details for this report');
        modal.find('.poll-flag-details').focus();
        return; 
      }
      
      modal.find('.modal-footer .btn').prop('disabled', true);
      
      var fo = modal.data('flag-object');
      
      master.FlagContent(fo.TargetTypeCode, fo.TargetId, flagTypeCode, details, function(result) {
        if(result.Success) {
          alert('Your report has been sent. Thinkster administration will be reviewing it shortly.');
          modal.modal('hide');
        } else {
          alert(result.Message); 
        }
        modal.find('.modal-footer .btn').prop('disabled', false); 
      }, function(result) {
        alert(result);
        modal.find('.modal-footer .btn').prop('disabled', false); 
      }); 
    });
       
  }
  
  modal.find(':input').val('');
  
  modal.modal('show');
    
});