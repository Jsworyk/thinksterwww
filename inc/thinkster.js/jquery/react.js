/*******************************************************************************
 * commentbox
 *
 * Takes the given element and converts it into a comment entry box
 *
 ******************************************************************************/
jQuery.fn.react = function(reactionText, emoji) {
  return $(this).each(function() {
    var r = $(this);
    var showTimer = 0, hideTimer = 0;  
    
    var showReactionEmojis = function() {
      r.popover('show');
    }; 
    
    var hideReactionEmojis = function() {
      r.popover('hide');
    }; 
    
    var emojiList = $('#poll_reaction_list .poll-reaction-emojis').clone();
    
    digifi.initialize(emojiList);
    
    emojiList.on('mouseenter', function() { 
      clearTimeout(hideTimer);
    }).on('mouseleave', function() { 
      hideTimer = setTimeout(hideReactionEmojis, 700);
    }).bind('digifi.action', function(e, a) {
      e.stopPropagation();
      
      hideReactionEmojis();
      
      master.React(r.attr('data-poll-targettype'), r.attr('data-poll-targetid'), a.params[0], function() {
        r.find('.reaction-text').text($(a.element).attr('title')); 
        
        r.find('.emoji, .fa').replaceWith($('<span class="emoji mr-1"></span>').text($(a.element).text()));
      }, alert); 
    });    
    
    r.popover({content: emojiList[0], html: true, trigger: 'manual', placement: 'top', delay: 700});
    
    var rcount = r.attr('data-poll-reactioncount');
    var badge = $('<span class="badge badge-pill badge-primary ml-2 poll-reaction-count" aria-label="View who reacted"></span>').text(rcount <= 0 ? '' : rcount).click(function(e) {
      e.preventDefault();
      e.stopPropagation();
      
      /*var list = $(this).data('poplist');
      
      if(!list) {
         list = $(this).popover({content: '<div style="height: 150px;">Dynamic list should go here?</div>', title: function(el) { return ''; }, html: true, trigger: 'manual', placement: 'top', delay: 700});
         $(this).data('poplist', list);
      }    
      
      list.popover('show'); */
      
      var react_popup = $('#poll_reaction_modal');
      
      if(react_popup.length == 0) {
        react_popup = $('<div id="poll_reaction_modal" class="modal fade"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title">Reactions</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body" style="max-height: 280px; overflow: auto;">Reactions go here</div><div class="modal-footer"><button type="button" class="btn btn-warning" data-dismiss="modal">Close</button></div></div></div>');
        
        $('body').append(react_popup);
      }
      
      react_popup.find('.modal-body').html($('<div class="text-center"><i class="fa fa-spin fa-3x text-danger fa-refresh"></i></div>'));
      
      master.GetReactors($(this).parent().attr('data-poll-targettype'), $(this).parent().attr('data-poll-targetid'), function(json) {
        var data = jQuery.parseJSON(json);
                        
        if(data.Reactors.length == 0) { 
        } else {
          var groupingCode = '';
          var lg = $('<div class="list-group" />');
         
          for(var x = 0; x < data.Reactors.length; x++) {
            /*var dd = $('<dd class="mb-1 clearfix" />');  */
                                    
            if(data.Reactors[x].ReactionTypeCode != groupingCode) {
               //dl.append($('<dt class="mb-1" />').append($('<span class="emoji mr-1"></span>').text(data.Reactors[x].Emoticon), data.Reactors[x].Reaction));
               lg.append($('<h5 class="list-group-item strong" />').append($('<span class="emoji mr-1"></span>').text(data.Reactors[x].Emoticon), data.Reactors[x].Reaction));
               groupingCode = data.Reactors[x].ReactionTypeCode; 
            }    

            var item = $('<a class="list-group-item list-group-item-action d-flex justify-content-between" target="_top"></a>').attr('href', '/profiles/' + data.Reactors[x].ProfileName).text(data.Reactors[x].DisplayName);                
            
            item.prepend($('<img class="pull-left align-self-top mr-3 rounded-circle" width="32" height="32" style="border-radius: 50%;" />').attr('src', thinkster.fn.avatar(data.Reactors[x].AvatarMicrothumbUrl)));
            
            var val = (data.Reactors[x].Agree / data.Reactors[x].Total * 100.0).toFixed(2); 
    
            var percentage = $('<span class="min-chart small"><span class="percent"></span></span>').attr('id', 'compatibility_' + data.Reactors[x].ProfileId).attr('data-percent', val);

            item.append(percentage);
                
            lg.append(item);
            
            percentage.easyPieChart({
                barColor: "#4caf50",
                size: 48,
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

          }
          
          react_popup.find('.modal-body').html(lg);
        }               
      }, alert);
      
      react_popup.modal('show');
      
      //alert('Create reaction popup here & load it');
    }).on('mouseenter', function(e) {
      hideTimer = setTimeout(hideReactionEmojis, 100); 
      clearTimeout(showTimer);
      e.preventDefault();
      e.stopPropagation(); 
    });  
    
    r.append(emoji ? $('<span class="emoji mr-1"></span>').text(emoji) : $('<i class="fa fa-thumbs-up mr-1"></i>'), $('<span class="reaction-text" />').text(reactionText ? reactionText : 'React'), badge);
    
    r.on('mouseenter', function() {
      clearTimeout(hideTimer);
      showTimer = setTimeout(showReactionEmojis, 700);       
    }).on('mouseleave', function() {
      clearTimeout(showTimer);
      hideTimer = setTimeout(hideReactionEmojis, 700); 
    }).click(function() {
      clearTimeout(showTimer);
      
      //If we haven't reacted yet, just like:
      if(r.find('.emoji').length == 0) { 
        master.React(r.attr('data-poll-targettype'), r.attr('data-poll-targetid'), 'LI', function() {
          r.find('.reaction-text').text('Like'); 
        
          r.find('.emoji, .fa').replaceWith($('<span class="emoji emoji-li"></span>'));
        }, alert);
      } else {       
        hideReactionEmojis();
        master.RemoveReaction(r.attr('data-poll-targettype'), r.attr('data-poll-targetid'));
        r.find('.reaction-text').text('React');         
        r.find('.emoji, .fa').replaceWith($('<i class="fa fa-thumbs-up mr-1"></i>')); 
      }            
    }); 
    
  });
  
};