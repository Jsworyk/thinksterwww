/*******************************************************************************
 * commentbox
 *
 * Takes the given element and converts it into a comment entry box
 *
 ******************************************************************************/
jQuery.fn.commentbox = function() {
  return $(this).each(function() {
    var cbox = this;
    var id = 'comment_entry_' + $(cbox).attr('data-poll-targettype') + '_'+ $(cbox).attr('data-poll-targetid');
    var ta = $('<textarea class="form-control" style="overflow: hidden;" />').attr('id', id);
    var buttonBar = $('<div class="text-right"></div>');
    var attachButton = $('<button type="button" class="btn btn-md btn-secondary ml-3" title="Attach an image"><i class="fa fa-fw fa-picture-o text-muted"></i></button>'); 
    var emojiButton = $('<button type="button" class="btn btn-md btn-secondary ml-3" title="Insert Emoji"><i class="fa fa-fw fa-smile-o text-muted"></i></button>');
    var sendButton = $('<button type="button" class="btn btn-md btn-primary ml-3" title="Send Chat Message">Send</button>');
    
    var textAreaAdjust = function(e) {
      ta.css('height', '1px');
      ta.css('height', (ta.prop('scrollHeight') + 8) + 'px');
    }
    
    ta.change(textAreaAdjust).keyup(textAreaAdjust);   
    
    var sendComment = function() {
      var commentText = ta.val().trim();
      
      if(commentText == '') return;
       
      master.Comment($(cbox).attr('data-poll-targettype'), $(cbox).attr('data-poll-targetid'), $(cbox).attr('data-poll-replytoid') || null, commentText, function() {
        ta.val('').focus();  
      }, alert);
    }
     
    
    sendButton.click(sendComment);
    
    buttonBar.append(/*attachButton, emojiButton, */sendButton);
      
    $(this).addClass('rounded md-form').html(ta).append($('<label/>').attr('for', id).text('Write a comment...')).append(buttonBar);
    
    textAreaAdjust();
  });
  
};