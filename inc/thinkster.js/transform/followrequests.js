thinkster.transform.followrequests = function(container, data, prepend) {
  //console.log(JSON.stringify(data));
  
  if($(container).find('div').length == 0) $(container).html('');   
  
  $(container).find('.poll-load-more').remove();
  
  if(data.FollowRequests.length == 0 && $(container).html() == '') {
     $(container).html('<div class="poll-no-requests text-muted">you have no requests</div>');
  } else { 
    $(container).find('.poll-no-requests').remove();
  }    
  
  var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  var match;
  var lastNotificationId = null;
   
  for(var x = 0; x < data.FollowRequests.length; x++) {
    var request = data.FollowRequests[x];
    
    var item = $('<div class="media p-4 poll-follow-request" />');
    var body = $('<div class="media-body d-flex align-self-center" />');
    var text = $('<div />');
    var buttonContainer = $('<span class="align-self-right"></span>');
    var acceptButton = $('<button type="button" class="btn btn-sm btn-success mr-2 poll-accept-follow-request"><i class="fa fa-fw fa-check"></i> Accept</button>');
    var declineButton = $('<button type="button" class="btn btn-sm btn-danger mr-2 poll-decline-follow-request"><i class="fa fa-fw fa-times"></i> Decline</button>');
    
    acceptButton.attr('data-poll-id', request.FollowerId);
    declineButton.attr('data-poll-id', request.FollowerId);
    
    buttonContainer.append(acceptButton, $('<br />'), declineButton);            
    
    item.attr('data-follower-id', request.FollowerId);
    
    if(request.Image) {
      item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" width="32" height="32" />').attr('src', request.Image)); 
    } else if(request.Icon) { 
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-2x" />').addClass(request.Icon));
    } else { 
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-2x fa-info-circle" />'));
    }
    
    body.append(text);
    item.append(body);
    
    item.append(buttonContainer);
    
    thinkster.fn.formatText(request.Text, text);
    
    if(prepend) { 
      $(container).prepend(item);
    } else {
      $(container).append(item);
    }
    
    item.find('time.timeago').timeago();
    buttonContainer.thinksterInitialize();
    
  }
  
  $('.poll-follow-request-count').text(data.UnreadCount ? data.UnreadCount : '');    
};