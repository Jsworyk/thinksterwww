thinkster.transform.notifications = function(container, data, prepend) {  
  if($(container).find('div').length == 0) $(container).html('');   
  
  $(container).find('.poll-load-more').remove();
  
  if(data.Notifications.length == 0 && $(container).html() == '') {
     $(container).html('<div class="poll-no-notifications text-muted">you have no notifications</div>');
  } else { 
    $(container).find('.poll-no-notifications').remove();
  }    
  
  var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  var match;
  var lastNotificationId = null;
   
  for(var x = 0; x < data.Notifications.length; x++) {
    var notification = data.Notifications[x];
    var item = $('<div class="media p-4 poll-notification" />');
    var body = $('<div class="media-body d-flex justify-content-between" />');
    var text = $('<div />');
    var lastIndex = 0;

    if(!notification.IsRead) {
      item.addClass('poll-unread');
    }
    
    item.attr('data-id', notification.NotificationId).attr('data-group', notification.GroupingValue);
    
    // We're rolling up:
    if(notification.GroupingCount) {
      $(container).find('.poll-notification[data-group="' + notification.GroupingValue + '"]').remove(); 
    }
    
    if(notification.AvatarMicrothumbUrl) {
      item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" />').attr('src', notification.AvatarMicrothumbUrl)); 
    } else if(notification.Icon) { 
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x" />').addClass(notification.Icon));
    } else { 
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x fa-info-circle" />'));
    }
    
    item.append(body);       
    
    while ((match = re.exec(data.Notifications[x].Text)) !== null) {
      if(match.index > lastIndex) {
        text.append(data.Notifications[x].Text.substring(lastIndex, match.index));
        //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
      } 
      
      //text += match[3];
      var anchor = $('<a />').html(match[3]);
      
      switch(match[1]) {
        case 'e':
          anchor.attr('href', '/events/' + match[2]);
        break;
      
        case 'u':
          anchor.attr('href', '/profile/' + match[2]);
        break;
        
        case 'p':
          anchor.attr('href', '/poll/' + match[2]);
        break; 
        
        case 'm':
          anchor.attr('href', '/media/' + match[2]);
        break;
      }
      
      text.append(anchor);
      
      //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
      
      lastIndex = re.lastIndex;            
    }
    
    if(data.Notifications[x].Text.length > lastIndex) {
      text.append(data.Notifications[x].Text.substring(lastIndex)); 
    }
    
    var time_container = $('<div class="d-flex align-self-bottom mr-2"></div>').append($('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', data.Notifications[x].NotificationDate).text(data.Notifications[x].NotificationDate)));
    
    if(data.Notifications[x].Emoticon) {    
      text.append('<br />', $('<span />').text(data.Notifications[x].Emoticon)); 
    }
    
    //text.append('<br />', $('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', data.Notifications[x].NotificationDate).text(data.Notifications[x].NotificationDate)));
      
    body.append(text);
    
    if(data.Notifications[x].SecondaryImage) {
      var secondaryLink = $('<a class="ml-4 mr-auto" />').attr('href', '/media/' + data.Notifications[x].SecondaryImageSelector);
      var secondary = $('<img />');
      
      secondary.attr("src", data.Notifications[x].SecondaryImage);
      
      secondaryLink.append(secondary);
      
      body.append(secondaryLink);  
    }                
    
    var menu_container = $('<div class="d-flex align-self-top ml-2"></div>');
    
    if($(container).is('#poll_menu_notification_preview')) {
      //body.removeClass('d-flex');
    }
    
    
    var menu_button = $('<div><div class="dropdown poll-notification-menu-btn"><i class="fa fa-ellipsis-v pl-1 pr-1 pb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i><div class="dropdown-menu dropdown-menu-right" style="left: 0px !important;"></div></div></div>');
    
    menu_button.find('i').attr('id', 'poll_nmnu_' + notification.NotificationId);
    menu_button.find('.dropdown-menu').attr('aria-labelledby', 'poll_nmnu_' + notification.NotificationId);
                                                                                
    var remove_item_action = $('<button class="dropdown-item" type="button">Delete</button>');
    remove_item_action.data('id', notification.NotificationId); 
    remove_item_action.click(function() { 
      master.DeleteNotification($(this).data('id'), function(result) { 
      }); 
    });
    
    var read_toggle_action = $('<button class="dropdown-item poll-read-toggle-action" type="button">Mark as Read</button>');

    read_toggle_action.data('read', notification.IsRead).data('id', notification.NotificationId); 
    
    read_toggle_action.text(notification.IsRead ? 'Mark as Unread' : 'Mark as Read');        
    
    read_toggle_action.click(function() { 
      if($(this).data('read')) {
        master.MarkNotificationUnread($(this).data('id'), function(result) {
          $(this).data('read', false);
          $(this).text('Mark as Read');        
          //Do something on failure? 
        }); 
      } else {
        master.MarkNotificationRead($(this).data('id'), function(result) {
          $(this).data('read', true);
          $(this).text('Mark as Unread');        
          //Do something on failure? 
        });
      }
    });
       
    
    menu_button.find('.dropdown-menu').append(read_toggle_action, remove_item_action);
    
    menu_container.append(menu_button);
    
    item.append(time_container, menu_container);
  
    
    if(prepend) { 
      $(container).prepend(item);
    } else {
      $(container).append(item);
    }
    
    item.find('time.timeago').timeago();
    
  }
  
  if(data.TotalRecords > 20) {
     var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
     var loadMoreButton = $('<button type="button" class="btn btn-primary">Load More Notifications</button>');
     
     loadMoreContainer.append(loadMoreButton);
     
     loadMoreButton.click(function() {
      $(container).trigger('digifi.refresh', {args: [lastNotificationId]});
      //alert('hi');      
     });
     
     $(container).append(loadMoreContainer);
  }    
  
  //var unreadCount = $(container).find('.poll-unread').length;
  $('.poll-notification-unread-count').text(data.UnreadCount == 0 ? '' : data.UnreadCount);
                                                           
  //$(container).html('under construction');
};