thinkster.transform.friendsListBase = function(data, addButtons, indexName) {
  var ul = $('<ul class="list-unstyled" />');
  indexName = indexName || 'Friends';
    
  for(var f = 0; f < data[indexName].length; f++) {
    var li = $('<li class="media my-4" />');
    var buttonContainer = $('<div class="d-flex mr-3 poll-friend-buttons"></div>');
    var body = $('<div class="media-body" />');
    var profileId = data[indexName][f].ProfileId;

    if(addButtons) addButtons(data[indexName][f], buttonContainer);


    li.append($('<img width="64" height="64" class="d-flex mr-3" />').attr('src', thinkster.fn.avatar(data[indexName][f].AvatarMicrothumbUrl)).attr('alt', 'Avatar').attr('title', 'Avatar for ' + data[indexName][f].DisplayName));
    body.append($('<a />').text(data[indexName][f].DisplayName).attr('href', '/profile/' + data[indexName][f].ProfileName));
    body.append($('<br />'), $('<span class="small text-muted" />').text(data[indexName][f].Location).append($('<br />'), data[indexName][f].FriendsInCommon > 0 ? data[indexName][f].FriendsInCommon + ' friend' + (data[indexName][f].FriendsInCommon == 1 ? '' : 's') + ' in common' : ''));

    li.append(body, buttonContainer);
    ul.append(li);
  }

  return ul;
};