thinkster.transform.outgoingFriends = function(container, data) {
  if(data.Friends.length == 0) {
    $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><p>You have no pending sent requests for friends right now.</p><p>If you\'re looking for more friends, check out the panel on the right!</p></div></div>');
  } else {
    var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
      var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');

      deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));

      deleteButton.click(function() {

        thinkster.fn.cancelFriendshipRequest(friend.ProfileId, function() {
           var itemToRemove = $(deleteButton).parents('li').first();

            itemToRemove.html($('<div class="alert alert-warning">You have cancelled your friend request with ' + friend.DisplayName + '.</div>'));

            setTimeout(function() {
              itemToRemove.hide('slow', function() {
                $(this).remove();
              });
            }, 3000);
        }, alert);
      });

      container.append(deleteButton);
    });

    $(container).html(ul);
  }
};