thinkster.transform.timeline = function(container, data, prepend) {
  if($(container).find('div').length == 0) $(container).html('');
  
  $(container).find('.poll-load-more').remove();
  
  var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  var match;
  var lastTimestamp = 0;

  for(var x = 0; x < data.Items.length; x++) {
    var item = $('<div class="media mb-4" />');
    var body = $('<div class="media-body d-flex align-self-center" />');
    var text = $('<div />');
    var lastIndex = 0;
    var avatarAnchor = $('<a class="d-flex align-self-center mr-3" />');
    
    lastTimestamp = data.Items[x].Timestamp;
          
    if(data.Items[x].Image) {
      avatarAnchor.append($('<img class="poll-avatar" width="64" height="64" />').attr('src', data.Items[x].Image)); 
    } else if(data.Items[x].Icon) { 
      avatarAnchor.append($('<i class="mr-3 fa fa-fw fa-3x" />').addClass(data.Items[x].Icon));
    } else { 
      avatarAnchor.append($('<i class="mr-3 fa fa-fw fa-3x fa-info-circle" />'));
    }
    
    item.append(avatarAnchor, body);       
    
    while ((match = re.exec(data.Items[x].Text)) !== null) {
      if(match.index > lastIndex) {
        text.append(data.Items[x].Text.substring(lastIndex, match.index));
        //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
      } 
      
      //text += match[3];
      var anchor = $('<a />').html(match[3]);
      
      switch(match[1]) {
        case 'e':
          anchor.attr('href', '/events/' + match[2]);
        break;
      
        case 'u':
          anchor.attr('href', '/profile/' + match[2]);
        break;
        
        case 'p':
          anchor.attr('href', '/poll/' + match[2]);
        break; 
        
        case 'm':
          anchor.attr('href', '/media/' + match[2]);
        break;
      }
      
      text.append(anchor);
      
      //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
      
      lastIndex = re.lastIndex;            
    }
    
    avatarAnchor.attr('href', anchor.attr('href'));
    
    if(data.Items[x].Text.length > lastIndex) {
      text.append(data.Items[x].Text.substring(lastIndex)); 
    }
    
    text.append('<br />', $('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', data.Items[x].Created).text(data.Items[x].Created)));
      
    body.append(text);
  
    if(prepend) { 
      $(container).prepend(item);
    } else {
      $(container).append(item);
    }
    
    item.find('time.timeago').timeago();
  }
  
  if(data.Items.length > 0 && !$(container).is('.poll-no-load-more')) {
     var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
     var loadMoreButton = $('<button type="button" class="btn btn-primary">Load More History</button>');
     
     loadMoreContainer.append(loadMoreButton);
     
     loadMoreButton.click(function() {
      $(container).trigger('digifi.refresh', {args: [lastTimestamp]});
      //alert('hi');      
     });
     
     $(container).append(loadMoreContainer);
  }
  

  //$(container).html(data.ChatRooms.length); 
};