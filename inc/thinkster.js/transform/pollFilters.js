thinkster.transform.pollFilters = function(container, data) {
  var addFilterButton = function() { 
    var btn = $('<button type="button" class="btn btn-sm btn-success"><i class="fa fa-fw fa-plus"></i> Add Filter</button>');
    
    btn.click(function() {
      $('#add_filter').modal('show');
    });
    
    return btn;
  };

  if(data.length) {
     
  } else {
    
  
    $(container).html($('<div class="alert alert-info">There are no filters defined here</div>')).append(addFilterButton(data)); 
  }   
};    