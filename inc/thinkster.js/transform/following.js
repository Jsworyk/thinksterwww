thinkster.transform.following = function(container, data) {
  if(data.Following.length == 0) {
    $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You aren\'t following anyone, yet!</p></div></div>');
  } else {
    var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
      var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i> Unfollow</button>');

      deleteButton.append($('<span class="sr-only" />').text(friend.DisplayName));

      deleteButton.click(function() {

        newsfeed.Unfollow(friend.ProfileId, function(result) {

			var retParse = JSON.parse(result);

			if(retParse.Success) {
				$('#following').trigger('digifi.refresh');
			}
		}, alert);
      });

      container.append(deleteButton);
    }, 'Following');

    $(container).html(ul);
  }
};