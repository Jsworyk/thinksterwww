thinkster.transform.chatRoomDropdown = function(container, data) {
  var lg = $(container);
  
  /*<a class="dropdown-item" href="#"><div class="alert alert-info clearfix">
      <span class="alert-icon"><i class="fa fa-bolt"></i></span>
      <div class="noti-info">
          <a href="#"> Server #1 overloaded.</a>
      </div>
  </div></a>*/
  
  
  /*if(lg.length == 0) {
    lg = $('<div class="list-group mx-0"/>');
    $(container).html(lg); 
  }   */
  
  if(lg.find('.poll-chat-room').length == 0) lg.html('');
  
  for(var x = 0; x < data.ChatRooms.length; x++) {
    var chatRoom = data.ChatRooms[x];
    var room = $(lg).find('.poll-chat-room[data-id=' + chatRoom.ChatRoomId + ']');               
                                                          
    if(room.length == 0) {
      //room = $('<div class="poll-chat-room list-group-item list-group-item-action p-1"><div class="media mr-auto"><img src="" class="d-flex mr-3" width="64" height="64" /> <div class="media-body"><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></div></div><div class="dropdown align-self-start"></div></div>');
      room = $('<a class="poll-chat-room dropdown-item p-1 mb-1"><img src="" class="pull-left mr-3" width="32" height="32" /><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></a>');

      room.attr('data-id', chatRoom.ChatRoomId);          
      
      if(chatRoom.Unread) {
        room.addClass('poll-chat-unread');         
      }
      
          
      room.data('poll-chatroom', chatRoom);
          
      room.find('img').attr('src', chatRoom.ChatRoomAvatar);
              
      lg.append(room);
    }
    
    room.attr('href', '/messages/' + chatRoom.LinkId);
    room.find('img').attr('src', thinkster.fn.avatar(chatRoom.ChatRoomAvatar));
    room.find('h6').html(chatRoom.RoomName);
    room.find('.poll-chatroom-preview').html(chatRoom.ChatPreview);              
  }
  
  var unreadCount = lg.find('.poll-chat-room.poll-chat-unread').length;
  $('#chat_top_menu').find('.badge').html(unreadCount == 0 ? '' : unreadCount);
  

  //$(container).html(data.ChatRooms.length); 
};