thinkster.transform.searchResults = function(container, data) { 
  if(data && data.length) {
    var div = $('<div class="list-group"></div>'); 
  
     for(var x = 0; x < data.length; x++) {
       var item = $('<a class="list-group-item b-0"></a>');
       
       item.css('display', 'block');
       
       if(data[x].ThumbnailUrl && data[x].ThumbnailUrl != '') {
        var img = $('<img class="pull-left mr-1 poll-avatar" width="32" height="32" />');
        
        img.attr('src', data[x].ThumbnailUrl);
        img.attr('alt', 'Search result');
        
        item.append(img); 
       } else if(data[x].Icon && data[x].Icon != '') {   
        var icn = $('<i class="fa-pull-left fa-2x mr-1 poll-avatar" />');
        
        icn.addClass(data[x].Icon);
        
        item.append(icn);
       }              
       
       var urlBase = '';
       
       switch(data[x].Type) {
        case 'u':
          urlBase = '/profile/';
          //item.append('<i class="fa fa-user mr-1" />');
        break;
        
        case 'p':
          urlBase = '/poll/';
          //item.append('<i class="fa fa-user mr-1" />');
        break;
        
        case 'm':
          urlBase = '/media/';
          //item.append('<i class="fa fa-image mr-1" />');
        break;
        
        case 'e':
          urlBase = '/events/';
        break; 
       }
       
       item.append(data[x].Name);
       item.append($('<br />'), $('<div class="small text-muted"></div>').text(data[x].Display));
       
       item.attr('href', urlBase + data[x].Selector);
       
       div.append(item);
     }
     
     $(container).html(div);
  } else {    
    $(container).html($('<div class="alert alert-info mt-1">There were no results to your search</div>')); 
  }   
};    