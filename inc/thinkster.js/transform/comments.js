thinkster.transform.comments = function(container, data) {
  if($(container).find('div').length == 0) $(container).html('');   
  
  $(container).find('.poll-load-more').remove();
  
  if(data.Comments.length == 0 && $(container).html() == '') {
     $(container).html('<div class="poll-no-comments text-muted">No comments have been posted yet</div>');
  } else { 
    $(container).find('.poll-no-comments').remove();
  }    
  
  var lastCommentId = null;
   
  for(var x = 0; x < data.Comments.length; x++) {
    var comment = data.Comments[x];
    var item = $('<div class="media mb-4 mt-4" />');
    var body = $('<div class="media-body d-flex align-self-center" />');
    var text = $('<div />').text(comment.CommentText);
    var replies = $('<div class="poll-comments-replies" data-digifi-source="master.GetTargetComments" data-digifi-json-transform="thinkster.transform.comments" />').attr('data-poll-commentid', comment.CommentId);    
    var commentLink = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
    var react = $('<span title="React to this comment" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', comment.ReactionCount).attr('data-poll-targettype', 'CO').attr('data-poll-targetid', comment.CommentId);
    var flagLink = $('<span class="small poll-flag-button mr-4" data-poll-type="Comment" data-poll-targettype="CO"><i class="fa fa-flag"></i> Report</span>');
    
    lastCommentId = comment.CommentId;
    
    flagLink.attr('data-poll-targetid', comment.CommentId);
    
    react.react(comment.ReactionText, comment.Emoticon);
    
    /*var createCommentBox = function() { 
      if(!newComment) {
        newComment = $('<div />').attr('data-poll-targettype', comment.TargetTypeCode).attr('data-poll-targetid', comment.TargetId).attr('data-poll-replytoid', comment.CommentId);        
        
        newComment.commentbox();
        
        replies.append(newComment);                
      }
    };    */
    
    if(comment.CommentCount > 0) {
      var showLink = $('<div class="poll-show-comments" />').text('Show ' + comment.CommentCount + ' repl' + (comment.CommentCount == 1 ? 'y' : 'ies'));
      
      showLink.on('click', null, {Comment: comment, Replies: replies}, function(e) {
        $(this).remove();
        //master.GetComments(comment.TargetTypeCode, comment.TargetId, comment.CommentId, null
        e.data.Replies.trigger('digifi.refresh', {args: [e.data.Comment.TargetTypeCode, e.data.Comment.TargetId, e.data.Comment.CommentId, null]});   
      });           
      
      replies.append(showLink); 
    }
    
    var header = $('<div class="d-flex w-100 justify-content-between"></div>');
    
    header.append($('<h5 class="mt-0" />').append($('<a target="_top" />').attr('href', '/profile/' + comment.ProfileName).text(comment.DisplayName)), ' ');
    header.append($('<span class="small text-muted" />').append($('<time class="timeago" />').attr('datetime', comment.Created).text(comment.Created)));
    
    text.prepend(header); 
    
    item.append($('<img class="d-flex align-self-top mr-3 rounded-circle" width="64" height="64" style="border-radius: 50%;" />').attr('src', thinkster.fn.avatar(comment.AvatarMicrothumbUrl)));
    
    text.append('<br />', react, commentLink, flagLink, replies);  
    
    commentLink.on('click', null, {Comment: comment, Replies: replies}, function(e) {
      if(e.data.Replies.find('.poll-comment-box').length == 0) {
        var newComment = $('<div  class="poll-comment-box" />').attr('data-poll-targettype', e.data.Comment.TargetTypeCode).attr('data-poll-targetid', e.data.Comment.TargetId).attr('data-poll-replytoid', e.data.Comment.CommentId);        
        
        e.data.Replies.append(newComment); 
        
        newComment.commentbox().find('textarea').focus();                              
      } else { 
        e.data.Replies.find('.poll-comment-box textarea').focus();
      }    
    });
    
    //commentLink.click(createCommentBox);  
    
    item.append(body);    
      
    body.append(text);
  
    if($(container).is('.poll-comments-replies')) {
      $(container).last().before(item); 
    } else {
      $(container).append(item);
    }
    
    item.thinksterInitialize();
    
    item.find('time.timeago').timeago();
    
    digifi.initialize(text[0]);
  }
  
  if(data.TotalRecords > 20) {
     var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
     var loadMoreButton = $('<button type="button" class="btn btn-primary">Load More Comments</button>');
     
     loadMoreContainer.append(loadMoreButton);
     
     loadMoreButton.click(function() {
      $(container).trigger('digifi.refresh', {args: [lastCommentId]});
      //alert('hi');      
     });
     
     $(container).append(loadMoreContainer);
  }    

  //$(container).html('under construction');
};