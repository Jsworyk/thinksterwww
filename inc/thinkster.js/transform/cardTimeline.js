thinkster.transform.cardTimeline = function(container, data) {
  if($(container).find('div').length == 0) $(container).html('');
  
  
  if($(container).find('div').length == 0 && data.Items.length == 0) $(container).html('<div class="m-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have not answered any polls yet! <a class="alert-link" href="/">Answer polls</a> so that your answers can be displayed here!</p></div></div>');

  $(container).find('.poll-load-more').remove();

  var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  var match;
  var lastTimestamp = 0;

  for(var x = 0; x < data.Items.length; x++) {
	//var card = $('<div class="card border-0">');
	//var cardBlock = $('<div class="card-block border-0" />');
    var item = $('<div class="media mb-4" style="font-size: 1.6rem;" />');
    var body = $('<div class="media-body d-flex align-self-center" />');
    var text = $('<div />');
    var lastIndex = 0;

    lastTimestamp = data.Items[x].Timestamp;

    if(data.Items[x].Image) {
      item.append($('<img class="d-flex align-self-center mr-3 poll-avatar" width="64" height="64" />').attr('src', data.Items[x].Image));
    } else if(data.Items[x].Icon) {
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x" />').addClass(data.Items[x].Icon));
    } else {
      item.append($('<i class="d-flex align-self-center mr-3 fa fa-fw fa-3x fa-info-circle" />'));
    }

    item.append(body);

    while ((match = re.exec(data.Items[x].Text)) !== null) {
      if(match.index > lastIndex) {
        text.append(data.Items[x].Text.substring(lastIndex, match.index));
        //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
      }

      //text += match[3];
      var anchor = $('<a />').html(match[3]);

      switch(match[1]) {
        case 'e':
          anchor.attr('href', '/events/' + match[2]);
        break;
        
        case 'u':
          anchor.attr('href', '/profile/' + match[2]);
        break;

        case 'p':
          anchor.attr('href', '/poll/' + match[2]);
        break;
        
        case 'm':
          anchor.attr('href', '/media/' + match[2]);
        break;
      }

      text.append(anchor);

      //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];

      lastIndex = re.lastIndex;
    }

    if(data.Items[x].Text.length > lastIndex) {
      text.append(data.Items[x].Text.substring(lastIndex));
    }

    text.append('<br />', $('<span class="small text-muted"><i class="fa fa-fw fa-clock-o"></i></span>').append($('<time class="timeago" />').attr('datetime', data.Items[x].Created).text(data.Items[x].Created)));

    body.append(text);
	//cardBlock.append(item);
	//card.append(cardBlock);

    $(container).append(item, $('<hr />'));

    item.find('time.timeago').timeago();
  }

  if(data.Items.length > 0) {

     var loadMoreContainer = $('<div class="poll-load-more text-center"></div>');
     var loadMoreButton = $('<button type="button" class="btn btn-primary mb-2">Load More History</button>');

     loadMoreContainer.append(loadMoreButton);

     loadMoreButton.click(function() {
      $(container).trigger('digifi.refresh', {args: [lastTimestamp]});
      //alert('hi');
     });

     $(container).append(loadMoreContainer);
  }


  //$(container).html(data.ChatRooms.length);
};