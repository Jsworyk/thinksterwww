thinkster.transform.pendingFriends = function(container, data) {
  if(data.Friends.length == 0) {
    $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><p>You have no outstanding friend requests right now.</p><p>If you\'re looking for more friends, check out the panel on the right!</p></div></div>');
  } else {
    var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
      var confirmButton = $('<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Confirm</button>');
      var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Decline</button>');

      confirmButton.append($('<span class="sr-only" />').text('Confirm ' + friend.DisplayName + ' as a friend'));
      deleteButton.append($('<span class="sr-only" />').text('Decline the friend request from ' + friend.DisplayName));

      confirmButton.click(function() {
        //alert('confirm: ' + friend.ProfileId);
        thinkster.fn.confirmFriendshipRequest(friend.ProfileId, function() {
           var itemToRemove = $(confirmButton).parents('li').first();

            itemToRemove.html($('<div class="alert alert-success">You are now friends with ' + friend.DisplayName + '!</div>'));

            setTimeout(function() {
              itemToRemove.hide('slow', function() {
                $(this).remove();
                countRequests();
              });
            }, 3000);
        }, alert);
      });

      deleteButton.click(function() {
        alert('decline: ' + friend.ProfileId);
      });

      container.append(confirmButton, '&nbsp;', deleteButton);
    });

    $(container).html(ul);
  }


};