thinkster.transform.chatRooms = function(container, data) {
  var lg = $(container).find('div.list-group');
  
  if(lg.length == 0) {
    lg = $('<div class="list-group mx-0"/>');
    $(container).html(lg); 
  }
  
  for(var x = 0; x < data.ChatRooms.length; x++) {
    var chatRoom = data.ChatRooms[x];
    var room = $(lg).find('.list-group-item[data-id=' + chatRoom.ChatRoomId + ']');               
                                                          
    if(room.length == 0) {
      room = $('<div class="poll-chat-room list-group-item list-group-item-action p-1"><div class="media mr-auto"><img src="" class="d-flex rounded-circle mr-3" width="64" height="64" /> <div class="media-body"><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></div></div><time class="align-self-start timeago mr-1 mt-1 badge badge-pill"></time><div class="dropdown align-self-start"></div></div>');

      if(chatRoom.Unread) { 
        room.addClass('poll-chat-unread');
      }
      
      room.find('time').attr('datetime', chatRoom.LastMessage).text(chatRoom.LastMessage);      

      room.attr('data-id', chatRoom.ChatRoomId);          
      
      if(thinkster.CurrentPage.ActiveChatRoomId == chatRoom.LinkId) {
        room.addClass('active');
        
        //If we're on the messaging page (or any page that supports chat interfacing right on the page), this needs to be called:
        if(thinkster.CurrentPage.createChatInterface)  thinkster.CurrentPage.createChatInterface(chatRoom);            
      }
      
      room.data('poll-chatroom', chatRoom);
      
      var button = $('<button class="btn p-0 dropdown-toggle" type="buton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-fw fa-gear text-muted"></i></button>');
      
      //button.click(function(e) { e.preventDefault(); e.stopPropagation(); });
      
      button.attr('id', 'ddb' + chatRoom.ChatRoomId); 
      
      room.find('.dropdown').append(button, $('<div class="dropdown-menu dropdown-menu-right" />'))/*.click(function(e) {
        e.stopPropagation(); 
        e.preventDefault();
        alert('hi');
      })*/; 
      
      room.find('.dropdown-menu').attr('aria-labelledby', 'ddb' + chatRoom.ChatRoomId);
      
      var ddMarkAsRead = $('<button class="dropdown-item" type="button">Mark as Read</button>');
      
      ddMarkAsRead.click(function(e) { e.preventDefault(); } );
      
      room.find('.dropdown-menu').append(ddMarkAsRead);
      
      var ddReportConvo = $('<button class="dropdown-item poll-flag-button" data-poll-type="Conversation" data-poll-targettype="CH" type="button">Report Conversation</button>');
      
      ddReportConvo.attr('data-poll-targetid', chatRoom.ChatRoomId);
      
      room.find('.dropdown-menu').append(ddReportConvo);   
      
      room.find('img').attr('src', thinkster.fn.avatar(chatRoom.ChatRoomAvatar));
              
      lg.append(room);
      
      room.thinksterInitialize();
    }
    
    room.attr('href', '/messages/' + chatRoom.LinkId);
    room.find('img').attr('src', thinkster.fn.avatar(chatRoom.ChatRoomAvatar));
    room.find('h6').html(chatRoom.RoomName);
    room.find('.poll-chatroom-preview').html(chatRoom.ChatPreview);
    
    room.click(function(e) {
      if($(e.target).parents('.dropdown').length != 0) {
      console.log('returning');
        return;
      }
    
      e.preventDefault();
      e.stopPropagation(); 
      
      if($(this).hasClass('active')) return;
      
      var chatRoom = $(this).data('poll-chatroom');                                                        
      
      $('.poll-chat-room').removeClass('active');
      $(this).addClass('active');
      
      //$('.poll-chat-interface').addClass(');
      thinkster.CurrentPage.createChatInterface(chatRoom);
      
      document.title = chatRoom.RoomName;
      
      $('#chat_room_title').html(chatRoom.RoomName);
      
      window.history.pushState({
          "pageTitle":chatRoom.RoomName
        },
        "",
        '/messages/' + chatRoom.LinkId
      );
      
      $('#messaging_container').addClass('messaging-activechat');

    });      
    
  }
  
  $.timeago.settings.strings = {
        prefixAgo: null,
        prefixFromNow: null,
        suffixAgo: null,
        suffixFromNow: null,
        inPast: 'any moment now',
        seconds: "less than a minute",
        minute: "~1m",
        minutes: "%dm",
        hour: "~1h",
        hours: "%dh",
        day: "1d",
        days: "%dd",
        month: "~1 month",
        months: "%d months",
        year: "~1y",
        years: "%dy",
        wordSeparator: " ",
        numbers: []
      };
  
  $(container).find('time.timeago').each(function() {
    if(!$(this).data('ta_set')) {
      $(this).timeago().data('ta_set', true);
    } 
  });

  //$(container).html(data.ChatRooms.length); 
};