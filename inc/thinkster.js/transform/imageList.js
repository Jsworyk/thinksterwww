thinkster.transform.imageList = function(container, data) {
  if(data.Images.length == 0) {
    $(container).html('<div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>No images to display</p></div></div>');
  } else {
    //var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
    /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');
  
    deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));
  
    deleteButton.click(function() {
  	alert('Cancel: ' + friend.ProfileId);
    });
  
    container.append(deleteButton);*/
    //});
    
    var row = $(container).find('.row');
    var prepend = true;
    
    if(row.length == 0) {
      prepend = false; 
      row = $('<div class="row m-1" />');
      $(container).html(row);
    }
    
    for(var x = 0; x < data.Images.length; x++) {
      var col = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
      var imageDiv = $('<div class="mt-4 text-center" />');
      var imageA = $('<a />').attr('href', '/media/' + data.Images[x].Selector);
      
      imageA.append($('<img width="220" height="200" />').attr('src', data.Images[x].ThumbnailUrl).attr('title', data.Images[x].Name + ' - ' + data.Images[x].Description));
      
      imageDiv.append(imageA); 
      
      col.append(imageDiv);
      
      if(prepend) { 
        row.prepend(col);
      } else {       
        row.append(col);
      } 
    }
     
  }
};