thinkster.transform.newsfeedCards = function(context, data, prepend) {
  if($(context).find('div').length == 0) $(context).html('');
  
  $(context).find('.poll-load-more').remove();
  
  var lastTimestamp = 0;

  for(var x = 0; x < data.Items.length; x++) {
    var item = data.Items[x];
  
    var card = $('<div class="card mb-4"></div>');
    var body = $('<div class="card-body"></div>');
    var header = $('<p class="card-text clearfix"></p>');
    var p = $('<p class="card-text"></p>');
    var avatarAnchor = $('<a class="pull-left mr-3" />');
    
    thinkster.fn.formatText(item.Text, p);
    
    lastTimestamp = item.Timestamp;
    
    if(item.Image) {
      avatarAnchor.append($('<img class="pull-left mr-3 poll-avatar" width="64" height="64" />').attr('src', item.Image)); 
    } else if(item.Icon) { 
      avatarAnchor.append($('<i class="fa fa-fw fa-3x" />').addClass(item.Icon));
    } else { 
      avatarAnchor.append($('<i class="fa fa-fw fa-3x fa-info-circle" />'));
    }
    
    avatarAnchor.attr('href', '/profile/' + item.ProfileName);
    
    header.append(avatarAnchor);
    
    var ta = $('<time class="timeago" />').attr('datetime', item.Created).text(item.Created)
    
    header.append($('<a />').attr('href', '/profile/' + item.ProfileName).text(item.DisplayName), $('<br />'), $('<div class="small"><i class="fa fa-clock-o fa-fw"></i></div>').append(ta));
    
    body.append(header);
    
    if(item.SecondaryImage) {    
      //var secondary = $('<img />').attr('src', item.SecondaryImage);
      var secondaryLink = $('<a />').attr('href', '/media/' + item.SecondaryImageSelector);
      var secondary = $('<div class="poll-newsfeed-image"></div>');
      
      secondary.css("background-image", "url(" + item.SecondaryImage + ")");
      
      secondaryLink.append(secondary);
      
      body.append(secondaryLink); 
    } else {
      body.append(p); 
    }        
      
    if(item.Responded) {   
      body.append($('<hr />'));
    
      var responseLine = $('<div class="poll-response-line"></div>');
      
      body.append(responseLine);
       
      var react = null;
      var commentLink = null;

      if(item.SecondaryImage) {
        react = $('<span title="React to this picture" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', item.ReactionCount).attr('data-poll-targettype', 'ME').attr('data-poll-targetid', item.TargetId);                 
      } else {
        react = $('<span title="React to this answer" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', item.ReactionCount).attr('data-poll-targettype', 'RE').attr('data-poll-targetid', item.TargetId);
      }
      
      comment = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
      
      react.attr('data-poll-reactioncount', item.ReactionCount);
      
      responseLine.append(react, comment);   
      
      if(item.CommentCount > 0) {
        var showLink = $('<div class="poll-show-comments" />').text('Show ' + item.CommentCount + ' repl' + (item.CommentCount == 1 ? 'y' : 'ies'));
        
       /* showLink.on('click', null, {Comment: comment, Replies: replies}, function(e) {
          $(this).remove();
          //master.GetComments(comment.TargetTypeCode, comment.TargetId, comment.CommentId, null
          e.data.Replies.trigger('digifi.refresh', {args: [e.data.Comment.TargetTypeCode, e.data.Comment.TargetId, e.data.Comment.CommentId, null]});   
        }); */          
        
        responseLine.append(showLink); 
      }             
      
      react.react(item.ReactionText, item.Emoticon);        
    }
    
    card.append(body);
    
    if(prepend) { 
      $(context).prepend(card);
    } else { 
      $(context).append(card);
    }
    
    ta.timeago();
  }   
  if(data.TotalRecords > data.PageSize)  {
    var moreButton = $('<button type="button" class="btn btn-primary">Load More</button>');
    $(context).append($('<p class="text-center"></p>').append(moreButton));
    
    moreButton.click(function() {
      moreButton.parent().replaceWith($('<p class="poll-load-more text-center"><i class="fa fa-refresh text-danger fa-2x fa-spin"></i> Loading...</p>'));
      $(context).trigger('digifi.refresh', {args: lastTimestamp});
    });
  }
};