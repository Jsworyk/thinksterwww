thinkster.transform.followableUserList = function(context, data) {
  if($(context).find('div').length == 0) $(context).html('');
  
  $(context).find('.poll-load-more').remove();
  
  var lg = $('<div class="list-group" />');
         
  for(var x = 0; x < data.Users.length; x++) {
    var item = $('<a class="list-group-item d-flex justify-content-between list-group-item-action" target="_top"></a>').attr('href', '/profile/' + data.Users[x].ProfileName).text(data.Users[x].DisplayName);                
    
    item.prepend($('<img class="pull-left align-self-top mr-3 rounded-circle" width="32" height="32" style="border-radius: 50%;" />').attr('src', thinkster.fn.avatar(data.Users[x].AvatarMicrothumbUrl)));
        
    if(data.Users[x].ProfileId != thinkster.CurrentUser.ProfileId) {
      var val = data.Users[x].Total == 0.0 ? 0.00 : (data.Users[x].Agree / data.Users[x].Total * 100.0).toFixed(2); 
      
      var percentage = $('<span class="min-chart"><span class="percent"></span></span>').attr('id', 'compatibility_' + data.Users[x].ProfileId).attr('data-percent', val);
  
      item.append(percentage);
      
      var followButton = $('<button type="button" class="btn btn-primary btn-rounded"></button>');
      
      if($(context).is('.poll-ful-sm')) {
        followButton.addClass('btn-sm');
        
        if(data.Users[x].Following) {      
          followButton.attr('title', 'Following');
          followButton.html($('<i class="fa fa-user-times"></i>'));
        } else {
          followButton.attr('title', 'Follow');
          followButton.html($('<i class="fa fa-user-plus"></i>')); 
        } 
      } else {
        if(data.Users[x].Following) {
           followButton.text('Following');
        } else {
          followButton.text('Follow'); 
        }
      }
      
      item.append(followButton);
                    
      percentage.easyPieChart({
          barColor: "#4caf50",        
          size:48,
          onStep: function (from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
          }        
      });
    } else {
      item.removeClass('justify-content-between'); 
    }
    
    lg.append(item);

  } 
  
  $(context).html(lg);
};