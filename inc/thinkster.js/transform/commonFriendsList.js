thinkster.transform.commonFriendsList = function(container, data) {
  if(data.Friends.length == 0) {
    $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have no friends, yet!</p><p>Check out the panel on the right or the "Find Friends" tab to get started!</p></div></div>');
  } else {
    var ul = thinkster.transform.friendsListBase(data, function(friend, container) {
    /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');
  
    deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));
  
    deleteButton.click(function() {
  	alert('Cancel: ' + friend.ProfileId);
    });
  
    container.append(deleteButton);*/
    });
  
    $(container).html(ul);
  }
};