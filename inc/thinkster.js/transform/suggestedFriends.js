thinkster.transform.suggestedFriends = function(container, data) {
  var ul = $('<ul class="list-unstyled" />');

  for(var f = 0; f < data.Friends.length; f++) {
    var li = $('<li class="media my-4" />').data('poll-id', data.Friends[f].ProfileId);
    var addButton = $('<button type="button" class="btn btn-success btn-sm d-flex mr-3"><i class="fa fa-user-plus"></i></button>');
    var body = $('<div class="media-body" />');
    var profileId = data.Friends[f].ProfileId;

    addButton.append($('<span class="sr-only" />').text('Add ' + data.Friends[f].DisplayName + ' as a friend')).data('poll-id', profileId);

    addButton.click(function() {
      var b = this;

      $(this).prop('disabled', true);

      master.AddFriend($(this).data('poll-id'), function(json) {
        var result = jQuery.parseJSON(json);

        if(result.Success) {
          var itemToRemove = $(b).parents('li').first();

          itemToRemove.html($('<div class="alert alert-success">friend request sent!</div>'));

          setTimeout(function() {
            itemToRemove.hide('slow', function() {
              $(this).remove();

              if(ul.find('li').length == 0) {
                ul.parent().trigger('digifi.refresh');
              }
            });
          }, 3000);


        } else {
          alert('Unable to add friend at this time. Sorry about that.');
          $(this).prop('disabled', false);
        }

      }, alert);
    });

    li.append($('<img width="64" height="64" class="d-flex mr-3" />').attr('src', data.Friends[f].AvatarUrl).attr('alt', 'Avatar').attr('title', 'Avatar for ' + data.Friends[f].DisplayName));
    body.append($('<a />').text(data.Friends[f].DisplayName).attr('href', '/profile/' + data.Friends[f].ProfileName));
    body.append($('<br />'), $('<span class="small text-muted" />').text(data.Friends[f].Location).append($('<br />'), data.Friends[f].FriendsInCommon > 0 ? data.Friends[f].FriendsInCommon + ' friend' + (data.Friends[f].FriendsInCommon == 1 ? '' : 's') + ' in common' : ''));

    li.append(body, addButton);
    ul.append(li);
  }

   $(container).html(ul);
};