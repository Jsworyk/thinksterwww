thinkster.transform.pollResponses = function(container, data) {
	if(data.Responses.length == 0) {
		$(container).html('<div class="m-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have not answered any polls yet! Answer polls so that your answers can be displayed here!</p></div></div>');
	} else {

		var cards=$('<div class="m-4"></div>');


		for(var f = 0; f < data.Responses.length; f++) {
      var response = data.Responses[f];

			var card=$('<div class="card"></div>');
			var cardBlock=$('<div class="card-block"></div>');
			var paraQuest=$('<p class="m-4">Poll Question: </p>');
			var footer=$('<div class="card-footer text-muted"></div>');
			var paraFoot=$('<p class="ml-4"></p>');
			var sub=$('<span><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-heart">&#160;Like!</i></button>&#160;<button type="button" class="btn btn-sm btn-warning"><i class="fa fa-meh-o">&#160;Meh!</i></button>&#160;<button type="button" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-down">&#160;Nope!</i></button>&#160;<button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-thumbs-up">&#160;Yeah!</i></button></span>');
      
      paraQuest.append(data.Responses[f].Question + '<br />' + 'Answer: ' + '<em>' + data.Responses[f].Response + '</em>');
      
      /*var commentLink = $('<span class="small poll-reply-link mr-4"><i class="fa fa-comment"></i> Reply</span>');
      var react = $('<span title="React to this comment" class="small poll-react mr-4"></span>').attr('data-poll-reactioncount', comment.ReactionCount).attr('data-poll-targettype', 'PR').attr('data-poll-targetid', response.PollResponseId);
      var replies = $('<div class="poll-comments-replies" data-digifi-source="master.GetTargetComments" data-digifi-json-transform="thinkster.transform.comments" />').attr('data-poll-pollresponseid', response.PollResponseId);			
      
      paraQuest.append('<br />', react, commentLink, replies); 
       */
			cardBlock.append(paraQuest);
			card.append(cardBlock);
			paraFoot.append('Answered on ' + data.Responses[f].ResponseDate + ' - ' + data.Responses[f].ExposureName, '<br/>' ,sub);
			footer.append(paraFoot);
			card.append(footer);
			cards.append(card, '<br/>');
		}

		$(container).html(cards);
	}
};