var thinkster = thinkster || {};

(function ($, thinkster) {
  jQuery.timeago.settings.allowFuture = true;

  var socket;
  
  thinkster.CurrentUser = {};
  thinkster.CurrentPage = {};

  thinkster.fn = {};    
  
  [[functions]]  

  thinkster.transform = {};
  
  [[transform]]  	                    

  thinkster.connect = function() {
    //var socket = io('https://devpush.thinkster.ca');
    var socket = io(thinkster.PushServer);

    socket.on('connect', function(token) {
      master.GetPushToken(function(token) {
        socket.emit('authenticate', token);   
      }, console.log);
    });
    
    [[socket]]

    
    thinkster.connection = socket;
  };


  $(document).ready(function(){
    $(this).thinksterInitialize();
    
    $('.poll-comment-box').commentbox();        
    $('.poll-react').react();
    if($.fn.material_select) $('.mdb-select').material_select();
  });

})(jQuery, thinkster);

jQuery.fn.thinksterInitialize = function() {
   [[initialize]]
   
   return this;
};

[[jquery]]

 function iFrameSize(obj, height) {
    $('iframe').css('height', (height + 40) + 'px');
    //$(obj).height( $(obj).contents().find("html").height() );    
}