/*******************************************************************************
 * confirmFriendshipRequest
 *
 * Good programmers would write comments about what this about
 *
 ******************************************************************************/
thinkster.fn.confirmFriendshipRequest = function(profileId, onSuccess, onFailure, onComplete) {
  master.ConfirmFriendRequest(profileId, onSuccess, onFailure, onComplete);
};