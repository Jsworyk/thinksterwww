/*******************************************************************************
 * formatText
 *
 * Takes text with Thinkster markup and converts it to links
 *
 ******************************************************************************/
thinkster.fn.formatText = function(text, containerElement) {
  var re = /\[\[([eupm]):([^:]+):([^\]]+)\]\]/g;
  var match;
  var lastIndex = 0;
  
  while ((match = re.exec(text)) !== null) {
    if(match.index > lastIndex) {
      containerElement.append(text.substring(lastIndex, match.index));
      //text += data.Items[x].Text.substring(lastIndex, match.index - re.lastIndex) + ', ' + re.lastIndex + ', ' + match.index + ', ' + match[0].length;
    } 
    
    //text += match[3];
    var anchor = $('<a />').html(match[3]);
    
    switch(match[1]) {
      case 'e':
        anchor.attr('href', '/events/' + match[2]);
      break;
    
      case 'u':
        anchor.attr('href', '/profile/' + match[2]);
      break;
      
      case 'p':
        anchor.attr('href', '/poll/' + match[2]);
      break; 
      
      case 'm':
        anchor.attr('href', '/media/' + match[2]);
      break;
    }
    
    containerElement.append(anchor);
    
    //text +=  match.index + ', ' + re.lastIndex + ', ' + match[0].length + ': ' + match[0];
    
    lastIndex = re.lastIndex;            
  }
  
  if(text.length > lastIndex) {
    containerElement.append(text.substring(lastIndex)); 
  }
};