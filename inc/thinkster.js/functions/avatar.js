/*******************************************************************************
 * avatar
 *
 * Good programmers would write comments about what this about
 *
 ******************************************************************************/
thinkster.fn.avatar = function(avatarUrl) {
  if(!avatarUrl) {
    return "/images/default_avatar.png";    
  }
  
  return avatarUrl;
};