/*******************************************************************************
 * cancelFriendshipRequest
 *
 * Good programmers would write comments about what this about
 *
 ******************************************************************************/
thinkster.fn.cancelFriendshipRequest = function(profileId, onSuccess, onFailure, onComplete) {
  master.CancelFriendRequest(profileId, onSuccess, onFailure, onComplete);
};