var thinkster = thinkster || {};

(function ($, thinkster) {

  var socket;
  
  thinkster.CurrentUser = {};
  thinkster.CurrentPage = {};

  thinkster.fn = {};

  
  // Start of /var/pollsite/dev/inc/thinkster.js/functions/cancelFriendshipRequest.js
  
  /*******************************************************************************  
   * cancelFriendshipRequest  
   *  
   * Good programmers would write comments about what this about  
   *  
   ******************************************************************************/  
  thinkster.fn.cancelFriendshipRequest = function(profileId, onSuccess, onFailure, onComplete) {  
    master.CancelFriendRequest(profileId, onSuccess, onFailure, onComplete);  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/functions/cancelFriendshipRequest.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/functions/confirmFriendshipRequest.js
  
  /*******************************************************************************  
   * confirmFriendshipRequest  
   *  
   * Good programmers would write comments about what this about  
   *  
   ******************************************************************************/  
  thinkster.fn.confirmFriendshipRequest = function(profileId, onSuccess, onFailure, onComplete) {  
    master.ConfirmFriendRequest(profileId, onSuccess, onFailure, onComplete);  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/functions/confirmFriendshipRequest.js
  

  thinkster.transform = {};

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/chatRooms.js
  
  thinkster.transform.chatRooms = function(container, data) {  
    var lg = $(container).find('div.list-group');  
      
    if(lg.length == 0) {  
      lg = $('<div class="list-group mx-0"/>');  
      $(container).html(lg);   
    }  
      
    for(var x = 0; x < data.ChatRooms.length; x++) {  
      var chatRoom = data.ChatRooms[x];  
      var room = $(lg).find('.list-group-item[data-id=' + chatRoom.ChatRoomId + ']');                 
                                                              
      if(room.length == 0) {  
        room = $('<a class="poll-chat-room list-group-item list-group-item-action p-1"><div class="media mr-auto"><img src="" class="d-flex mr-3" width="64" height="64" /> <div class="media-body"><h6 class="mt-0 mb-1"></h6><div class="poll-chatroom-preview"></div></div></div><div class="dropdown align-self-start"></div></a>');  
    
        room.attr('data-id', chatRoom.ChatRoomId);            
          
        if(thinkster.CurrentPage.ActiveChatRoomId == chatRoom.LinkId) {  
          room.addClass('active');  
            
          //If we're on the messaging page (or any page that supports chat interfacing right on the page), this needs to be called:  
          if(thinkster.CurrentPage.createChatInterface)  thinkster.CurrentPage.createChatInterface(data.ChatRooms[x].ChatRoomId);              
        }  
          
        room.data('poll-chatroom', data.ChatRooms[x]);  
          
        var button = $('<button class="btn p-0 dropdown-toggle" type="buton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-gear"></i></button>');  
          
        //button.click(function(e) { e.preventDefault(); e.stopPropagation(); });  
          
        button.attr('id', 'ddb' + chatRoom.ChatRoomId);   
          
        room.find('.dropdown').append(button, $('<div class="dropdown-menu" />'));  
          
        room.find('.dropdown-menu').attr('aria-labelledby', 'ddb' + chatRoom.ChatRoomId);  
          
        var ddMarkAsRead = $('<button class="dropdown-item" type="buton">Mark as Read</button>');  
          
        ddMarkAsRead.click(function(e) { e.preventDefault(); } );  
          
        room.find('.dropdown-menu').append(ddMarkAsRead);  
          
         /*  
        <div class="dropdown">  
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  
  Dropdown  
  </button>  
  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">  
  <button class="dropdown-item" type="button">Action</button>  
  <button class="dropdown-item" type="button">Another action</button>  
  <button class="dropdown-item" type="button">Something else here</button>  
  </div>  
  </div> */  
          
        room.find('img').attr('src', chatRoom.ChatRoomAvatar);  
                  
        lg.append(room);  
      }  
        
      room.attr('href', '/messages/' + chatRoom.LinkId);  
      room.find('img').attr('src', chatRoom.ChatRoomAvatar);  
      room.find('h6').html(chatRoom.RoomName);  
      room.find('.poll-chatroom-preview').html(chatRoom.ChatPreview);  
        
      room.click(function(e) {  
        e.preventDefault();  
        e.stopPropagation();   
          
        if($(this).hasClass('active')) return;  
          
        var chatRoom = $(this).data('poll-chatroom');                                                          
          
        $('.poll-chat-room').removeClass('active');  
        $(this).addClass('active');  
          
        //$('.poll-chat-interface').addClass(');  
        thinkster.CurrentPage.createChatInterface(chatRoom.ChatRoomId);  
          
        document.title = chatRoom.RoomName;  
          
        $('#chat_room_title').html(chatRoom.RoomName);  
          
        window.history.pushState({  
            "pageTitle":chatRoom.RoomName  
          },  
          "",  
          '/messages/' + chatRoom.LinkId  
        );  
    
      });        
        
    }  
      
    
    //$(container).html(data.ChatRooms.length);   
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/chatRooms.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/commonFriendsList.js
  
  thinkster.transform.commonFriendsList = function(container, data) {  
    if(data.Friends.length == 0) {  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have no friends, yet!</p><p>Check out the panel on the right or the "Find Friends" tab to get started!</p></div></div>');  
    } else {  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {  
      /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');  
      
      deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));  
      
      deleteButton.click(function() {  
    	alert('Cancel: ' + friend.ProfileId);  
      });  
      
      container.append(deleteButton);*/  
      });  
      
      $(container).html(ul);  
    }  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/commonFriendsList.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/friendList.js
  
  thinkster.transform.friendList = function(container, data) {  
    if(data.Friends.length == 0) {  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have no friends, yet!</p><p>Check out the panel on the right or the "Find Friends" tab to get started!</p></div></div>');  
    } else {  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {  
        /*var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');  
    
        deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));  
    
        deleteButton.click(function() {  
          alert('Cancel: ' + friend.ProfileId);  
        });  
    
        container.append(deleteButton);*/  
      });  
    
      $(container).html(ul);  
    }  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/friendList.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/friendsListBase.js
  
  thinkster.transform.friendsListBase = function(data, addButtons) {  
    var ul = $('<ul class="list-unstyled" />');  
    
    for(var f = 0; f < data.Friends.length; f++) {  
      var li = $('<li class="media my-4" />');  
      var buttonContainer = $('<div class="d-flex mr-3 poll-friend-buttons"></div>');  
      var body = $('<div class="media-body" />');  
      var profileId = data.Friends[f].ProfileId;  
    
      if(addButtons) addButtons(data.Friends[f], buttonContainer);  
    
    
      li.append($('<img width="64" height="64" class="d-flex mr-3" />').attr('src', data.Friends[f].AvatarUrl).attr('alt', 'Avatar').attr('title', 'Avatar for ' + data.Friends[f].DisplayName));  
      body.append($('<a />').text(data.Friends[f].DisplayName).attr('href', '/profile/' + data.Friends[f].ProfileName));  
      body.append($('<br />'), $('<span class="small text-muted" />').text(data.Friends[f].Location).append($('<br />'), data.Friends[f].FriendsInCommon > 0 ? data.Friends[f].FriendsInCommon + ' friend' + (data.Friends[f].FriendsInCommon == 1 ? '' : 's') + ' in common' : ''));  
    
      li.append(body, buttonContainer);  
      ul.append(li);  
    }  
    
    return ul;  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/friendsListBase.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/outgoingFriends.js
  
  thinkster.transform.outgoingFriends = function(container, data) {  
    if(data.Friends.length == 0) {  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><p>You have no pending sent requests for friends right now.</p><p>If you\'re looking for more friends, check out the panel on the right!</p></div></div>');  
    } else {  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {  
        var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Cancel</button>');  
    
        deleteButton.append($('<span class="sr-only" />').text('Cancel friend request to ' + friend.DisplayName));  
    
        deleteButton.click(function() {  
    
          thinkster.fn.cancelFriendshipRequest(friend.ProfileId, function() {  
             var itemToRemove = $(deleteButton).parents('li').first();  
    
              itemToRemove.html($('<div class="alert alert-warning">You have cancelled your friend request with ' + friend.DisplayName + '.</div>'));  
    
              setTimeout(function() {  
                itemToRemove.hide('slow', function() {  
                  $(this).remove();  
                });  
              }, 3000);  
          }, alert);  
        });  
    
        container.append(deleteButton);  
      });  
    
      $(container).html(ul);  
    }  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/outgoingFriends.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/pendingFriends.js
  
  thinkster.transform.pendingFriends = function(container, data) {  
    if(data.Friends.length == 0) {  
      $(container).html('<div class="mr-4"><div class="alert alert-info" role="alert"><p>You have no outstanding friend requests right now.</p><p>If you\'re looking for more friends, check out the panel on the right!</p></div></div>');  
    } else {  
      var ul = thinkster.transform.friendsListBase(data, function(friend, container) {  
        var confirmButton = $('<button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Confirm</button>');  
        var deleteButton = $('<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Decline</button>');  
    
        confirmButton.append($('<span class="sr-only" />').text('Confirm ' + friend.DisplayName + ' as a friend'));  
        deleteButton.append($('<span class="sr-only" />').text('Decline the friend request from ' + friend.DisplayName));  
    
        confirmButton.click(function() {  
          //alert('confirm: ' + friend.ProfileId);  
          thinkster.fn.confirmFriendshipRequest(friend.ProfileId, function() {  
             var itemToRemove = $(confirmButton).parents('li').first();  
    
              itemToRemove.html($('<div class="alert alert-success">You are now friends with ' + friend.DisplayName + '!</div>'));  
    
              setTimeout(function() {  
                itemToRemove.hide('slow', function() {  
                  $(this).remove();  
                });  
              }, 3000);  
          }, alert);  
        });  
    
        deleteButton.click(function() {  
          alert('decline: ' + friend.ProfileId);  
        });  
    
        container.append(confirmButton, '&nbsp;', deleteButton);  
      });  
    
      $(container).html(ul);  
    }  
    
    
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/pendingFriends.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/pollResponses.js
  
  thinkster.transform.pollResponses = function(container, data) {  
  	if(data.Responses.length == 0) {  
  		$(container).html('<div class="m-4"><div class="alert alert-info" role="alert"><i class="fa fa-from fa-pull-left fa-3x"></i><p>You have not answered any polls yet! Answer polls so that your answers can be displayed here!</p></div></div>');  
  	} else {  
    
  		var cards=$('<div class="m-4"></div>');  
    
    
  		for(var f = 0; f < data.Responses.length; f++) {  
    
  			var card=$('<div class="card"></div>');  
  			var cardBlock=$('<div class="card-block"></div>');  
  			var paraQuest=$('<p class="m-4">Poll Question: </p>');  
  			var footer=$('<div class="card-footer text-muted"></div>');  
  			var paraFoot=$('<p class="ml-4"></p>');  
  			var sub=$('<span><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-heart">&#160;Like!</i></button>&#160;<button type="button" class="btn btn-sm btn-warning"><i class="fa fa-meh-o">&#160;Meh!</i></button>&#160;<button type="button" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-down">&#160;Nope!</i></button>&#160;<button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-thumbs-up">&#160;Yeah!</i></button></span>');  
    
  			paraQuest.append(data.Responses[f].Question + '<br />' + 'Answer: ' + '<em>' + data.Responses[f].Response + '</em>');  
  			cardBlock.append(paraQuest);  
  			card.append(cardBlock);  
  			paraFoot.append('Answered on ' + data.Responses[f].ResponseDate, '<br/>' ,sub);  
  			footer.append(paraFoot);  
  			card.append(footer);  
  			cards.append(card, '<br/>');  
  		}  
    
  		$(container).html(cards);  
  	}  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/pollResponses.js

  
  // Start of /var/pollsite/dev/inc/thinkster.js/transform/suggestedFriends.js
  
  thinkster.transform.suggestedFriends = function(container, data) {  
    var ul = $('<ul class="list-unstyled" />');  
    
    for(var f = 0; f < data.Friends.length; f++) {  
      var li = $('<li class="media my-4" />').data('poll-id', data.Friends[f].ProfileId);  
      var addButton = $('<button type="button" class="btn btn-success btn-sm d-flex mr-3"><i class="fa fa-user-plus"></i></button>');  
      var body = $('<div class="media-body" />');  
      var profileId = data.Friends[f].ProfileId;  
    
      addButton.append($('<span class="sr-only" />').text('Add ' + data.Friends[f].DisplayName + ' as a friend')).data('poll-id', profileId);  
    
      addButton.click(function() {  
        var b = this;  
    
        $(this).prop('disabled', true);  
    
        master.AddFriend($(this).data('poll-id'), function(json) {  
          var result = jQuery.parseJSON(json);  
    
          if(result.Success) {  
            var itemToRemove = $(b).parents('li').first();  
    
            itemToRemove.html($('<div class="alert alert-success">friend request sent!</div>'));  
    
            setTimeout(function() {  
              itemToRemove.hide('slow', function() {  
                $(this).remove();  
    
                if(ul.find('li').length == 0) {  
                  ul.parent().trigger('digifi.refresh');  
                }  
              });  
            }, 3000);  
    
    
          } else {  
            alert('Unable to add friend at this time. Sorry about that.');  
            $(this).prop('disabled', false);  
          }  
    
        }, alert);  
      });  
    
      li.append($('<img width="64" height="64" class="d-flex mr-3" />').attr('src', data.Friends[f].AvatarUrl).attr('alt', 'Avatar').attr('title', 'Avatar for ' + data.Friends[f].DisplayName));  
      body.append($('<a />').text(data.Friends[f].DisplayName).attr('href', '/profile/' + data.Friends[f].ProfileName));  
      body.append($('<br />'), $('<span class="small text-muted" />').text(data.Friends[f].Location).append($('<br />'), data.Friends[f].FriendsInCommon > 0 ? data.Friends[f].FriendsInCommon + ' friend' + (data.Friends[f].FriendsInCommon == 1 ? '' : 's') + ' in common' : ''));  
    
      li.append(body, addButton);  
      ul.append(li);  
    }  
    
     $(container).html(ul);  
  };
  
  // End of /var/pollsite/dev/inc/thinkster.js/transform/suggestedFriends.js
  	                    

  thinkster.connect = function() {
    var socket = io('https://devpush.thinkster.ca');

    socket.on('connect', function(token) {
      master.GetPushToken(function(token) {
        socket.emit('authenticate', token);   
      }, alert);
    });


    socket.on('chat message', function(msg){
      //$('#messages').append($('<li>').text(msg));
      console.log(msg);
    });
  };


  $(document).ready(function(){

    // Start of /var/pollsite/dev/inc/thinkster.js/initialize/doLogin.js
    var doLogin = function(e) {      var em = $('#li_email').val().trim();      var pw = $('#li_password').val().trim();          e.stopPropagation();          master.Login(em, pw, function(json) {        var obj = jQuery.parseJSON(json);            if(obj.Success) {         window.location = window.location;        } else {          alert(obj.Message);        }          }, alert);    };        $('#ts_login').digifiEnterKey(doLogin);    $('#li_login').click(doLogin);            $('#header_fb_login').click(function() {      FB.login(function(response) {        if(response.status == 'connected') {              master.FacebookLogin(function(json) {            var result = jQuery.parseJSON(json);            if(result.Success) {              window.location = window.location;            } else {              alert(result.Message);            }          }, alert);            } else {          alert('Sign in to Facebook failed or was cancelled.');        }        // handle the response      }, {scope: 'public_profile,email'});    });
    // End of /var/pollsite/dev/inc/thinkster.js/initialize/doLogin.js

    

  });

})(jQuery, thinkster);